package com.xiphiastec.newatsi.util;

import android.util.Log;

import java.io.IOException;

/**
 * Created by Admin on 23-03-2017.
 */

public class RetrofitError {
    public static String showGenericError(Throwable t) {
        if (t instanceof IOException) {
            return "Network connection error";
        } else if (t instanceof IllegalStateException) {
            return "Conversion error";
        }
        return null;

    }

    public static String getResponseCodeError(int code) {
        switch (code) {
            case 400:
                Log.e("Error 400", "Bad Request");
                return "Bad Request";
            case 404:
                Log.e("Error 404", "Not Found");
                return "Request not found";
            case 405:
                Log.e("Error 405", "Method not found");
                return "Method not found";
            default:
                Log.e("Error", "Generic Error");
                return "Something went wrong please try again!";
        }
    }
}
