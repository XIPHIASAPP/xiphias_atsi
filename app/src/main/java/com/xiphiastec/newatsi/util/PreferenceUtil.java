package com.xiphiastec.newatsi.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.login.presenter.LoginPresenter;

public class PreferenceUtil {


    private static final String PREFS_TOKEN = "prefs_token";
    private static final String PREFES_TOKEN_PARENT="prefs_parent_token";
    private static final String PREFS_STUDENT = "prefs_student";
    private static final String PREFS_TEACHER = "prefs_teacher";
    private static final String PREFS_PARENT = "prefs_parent";
    private static final String PREFS_ADMIN = "prefs_admin";
    private static final String PREFS_SITE_ID = "prefs_site_id";
    private static final String PREFS_ORGANISATION_ID = "prefs_organisation_id";
    private static final String PREFS_EMAIL = "prefs_email";
    private static final String PREFS_STUDENT_ID = "prefs_student_id";
    private static final String PREFS_TEACHER_ID = "prefs_teacher_id";
    private static final String PREFS_PARENT_ID = "prefs_parent_id";
    private static final String PREFS_USER_NAME = "prefs_user_name";
    private static PreferenceUtil sInstance;
    private final SharedPreferences mPreferences;

    private PreferenceUtil(@NonNull Context context) {
        this.mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static PreferenceUtil getInstance(@NonNull Context context) {
        if (sInstance == null) {
            sInstance = new PreferenceUtil(context.getApplicationContext());
        }
        return sInstance;
    }



    public void setParentToken(String token) {
        mPreferences.edit().putString(PREFES_TOKEN_PARENT,token).apply();
    }

    public String getParentToken(){

     return   mPreferences.getString(PREFES_TOKEN_PARENT, "");
    }

    public void setToken(String token) {
        mPreferences.edit().putString(PREFS_TOKEN,token).apply();
    }


    public void setLoginDetails(LoginPresenter.LoginResponse response) {
        mPreferences.edit()
                .putString(PREFS_TOKEN, response.token)
                .putBoolean(PREFS_STUDENT, response.isStudent)
                .putBoolean(PREFS_TEACHER, response.isTeacher)
                .putBoolean(PREFS_PARENT, response.isParent)
                .putBoolean(PREFS_ADMIN, response.isAdmin)
                .putInt(PREFS_SITE_ID, response.siteId)
                .putInt(PREFS_ORGANISATION_ID, response.organisationId)
                .putString(PREFS_EMAIL, response.email)
                .putString(PREFS_STUDENT_ID, response.email)
                .putString(PREFS_TEACHER_ID, response.email)
                .putString(PREFS_PARENT_ID, response.email)
                .putString(PREFS_USER_NAME, response.email)
                .apply();

        if(mPreferences.getBoolean(PREFS_PARENT, false)){
            setParentToken(getToken());
        }
    }

    public void clear() {
        mPreferences.edit().clear().apply();
    }

    public boolean getParentLogin() {
        return mPreferences.getBoolean(PREFS_PARENT, false);
    }

    public boolean getStudentLogin() {
        return mPreferences.getBoolean(PREFS_STUDENT, false);
    }

    public boolean getTeacherLogin() {
        return mPreferences.getBoolean(PREFS_TEACHER, false);
    }

    public boolean getAdminLogin() {
        return mPreferences.getBoolean(PREFS_ADMIN, false);
    }

    public String getToken() {
        return mPreferences.getString(PREFS_TOKEN, "");
    }

    public String getStudentId() {
        return String.valueOf(mPreferences.getInt(PREFS_SITE_ID, -1));
    }

    public String getUserName() {
        return mPreferences.getString(PREFS_USER_NAME, "N/A");
    }
}
