package com.xiphiastec.newatsi.util;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.R;

/**
 * Created by pavan-xiphias on 3/3/18.
 */

public class AtsiProgressDialog extends Dialog {
    public AtsiProgressDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.atsi_dialog);
//        getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.MATCH_PARENT);
      //  setCancelable(false);
    }


    public void showDialog(){
        this.show();
    }

    public void dismissDialog(){
        this.dismiss();
    }
}
