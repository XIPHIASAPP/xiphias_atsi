package com.xiphiastec.newatsi.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;

import com.xiphiastec.newatsi.app.ATSIApplication;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by Admin on 10-02-2017.
 */

public class Utils {

    public static final String BASEURL = "https://www.atsi.in/atsitestservice/api/";

    public static Bitmap getBitmapFromView(View view) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return returnedBitmap;
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    public static Spanned convertStringToHtml(String string) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(string, Html.FROM_HTML_MODE_LEGACY);
        }
        return Html.fromHtml(string);
    }

    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivity = (ConnectivityManager) ATSIApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public static String showError(Throwable throwable) {
        return new NetworkError(throwable).getAppErrorMessage();
    }

    private boolean isOnline(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) ATSIApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (NetworkInfo anInfo : info)
                    if (anInfo.getState() == NetworkInfo.State.CONNECTED) return true;
        }
        return false;
    }


    public static String getDay(String input) {
        String day=null;
        try {
            SimpleDateFormat inFormat = new SimpleDateFormat("MM/dd/yyyy");
            Date date = inFormat.parse(input);
            day = (String) DateFormat.format("dd", date); // 20
        }catch (Exception e){
            Log.d("ATSI",e.getMessage());
        }
        return  day;
    }

    public  static String getMonth(String input){
        String mon=null;
        try {
            SimpleDateFormat inFormat = new SimpleDateFormat("MM/dd/yyyy");
            Date date = inFormat.parse(input);
            mon = (String) DateFormat.format("MMM", date); // 20
        }catch (Exception e){
            Log.d("ATSI",e.getMessage());

        }
        return  mon;
    }


    public static String checkEmpty(String s) {
        return TextUtils.isEmpty(s)  ? "NA" : s;
    }

}
