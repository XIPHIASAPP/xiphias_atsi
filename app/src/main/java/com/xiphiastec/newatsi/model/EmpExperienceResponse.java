package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class EmpExperienceResponse extends StatusResponse {
    @SerializedName("employee_experience_info")
    @Expose
    public List<EmployeeExperienceInfo> mList = null;

}