package com.xiphiastec.newatsi.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StudentProfileResponse extends StatusResponse {

    @SerializedName("student_academic")
    @Expose
    public StudentAcademic studentAcademic;
    @SerializedName("student_applied_details")
    @Expose
    public StudentAppliedDetails mAppliedDetails;
    @SerializedName("student_profile_information")
    @Expose
    public StudentProfileInfo mProfileInfo;
    @SerializedName("student_document")
    @Expose
    public List<StudentDocument> mDocument = null;

    public static class StudentProfileInfo implements Parcelable {
        public static final Creator<StudentProfileInfo> CREATOR = new Creator<StudentProfileInfo>() {
            @Override
            public StudentProfileInfo createFromParcel(Parcel in) {
                return new StudentProfileInfo(in);
            }

            @Override
            public StudentProfileInfo[] newArray(int size) {
                return new StudentProfileInfo[size];
            }
        };
        @SerializedName("admission_number")
        @Expose
        public String admissionNumber;
        @SerializedName("student_name")
        @Expose
        public String studentName;
        @SerializedName("register_number")
        @Expose
        public String registerNumber;
        @SerializedName("date_of_birth")
        @Expose
        public String dateOfBirth;
        @SerializedName("gender_name")
        @Expose
        public String genderName;
        @SerializedName("birth_place")
        @Expose
        public String birthPlace;
        @SerializedName("nationality_name")
        @Expose
        public String nationalityName;
        @SerializedName("blood_group")
        @Expose
        public String bloodGroup;
        @SerializedName("mother_tongue")
        @Expose
        public String motherTongue;
        @SerializedName("category")
        @Expose
        public String category;
        @SerializedName("religion")
        @Expose
        public String religion;
        @SerializedName("caste")
        @Expose
        public String caste;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("alternative_email")
        @Expose
        public String alternativeEmail;
        @SerializedName("phone")
        @Expose
        public String phone;
        @SerializedName("alternative_phone")
        @Expose
        public String alternativePhone;

        protected StudentProfileInfo(Parcel in) {
            admissionNumber = in.readString();
            studentName = in.readString();
            registerNumber = in.readString();
            dateOfBirth = in.readString();
            genderName = in.readString();
            birthPlace = in.readString();
            nationalityName = in.readString();
            bloodGroup = in.readString();
            motherTongue = in.readString();
            category = in.readString();
            religion = in.readString();
            caste = in.readString();
            email = in.readString();
            alternativeEmail = in.readString();
            phone = in.readString();
            alternativePhone = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(admissionNumber);
            dest.writeString(studentName);
            dest.writeString(registerNumber);
            dest.writeString(dateOfBirth);
            dest.writeString(genderName);
            dest.writeString(birthPlace);
            dest.writeString(nationalityName);
            dest.writeString(bloodGroup);
            dest.writeString(motherTongue);
            dest.writeString(category);
            dest.writeString(religion);
            dest.writeString(caste);
            dest.writeString(email);
            dest.writeString(alternativeEmail);
            dest.writeString(phone);
            dest.writeString(alternativePhone);
        }

        @Override
        public int describeContents() {
            return 0;
        }

    }

    public static class StudentAppliedDetails implements Parcelable {
        public static final Creator<StudentAppliedDetails> CREATOR = new Creator<StudentAppliedDetails>() {
            @Override
            public StudentAppliedDetails createFromParcel(Parcel in) {
                return new StudentAppliedDetails(in);
            }

            @Override
            public StudentAppliedDetails[] newArray(int size) {
                return new StudentAppliedDetails[size];
            }
        };
        @SerializedName("program_name")
        @Expose
        public String programName;
        @SerializedName("created_date")
        @Expose
        public String createdDate;
        @SerializedName("caste")
        @Expose
        public String caste;
        @SerializedName("sub_caste")
        @Expose
        public String subCaste;
        @SerializedName("picture")
        @Expose
        public String picture;

        protected StudentAppliedDetails(Parcel in) {
            programName = in.readString();
            createdDate = in.readString();
            caste = in.readString();
            subCaste = in.readString();
            picture = in.readString();
        }

        @Override
        public String toString() {
            return "StudentAppliedDetails{" +
                    "programName='" + programName + '\'' +
                    ", createdDate='" + createdDate + '\'' +
                    ", caste='" + caste + '\'' +
                    ", subCaste='" + subCaste + '\'' +
                    ", picture='" + picture + '\'' +
                    '}';
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(programName);
            dest.writeString(createdDate);
            dest.writeString(caste);
            dest.writeString(subCaste);
            dest.writeString(picture);
        }

        @Override
        public int describeContents() {
            return 0;
        }


    }

    public static class StudentDocument implements Parcelable {

        public static final Creator<StudentDocument> CREATOR = new Creator<StudentDocument>() {
            @Override
            public StudentDocument createFromParcel(Parcel in) {
                return new StudentDocument(in);
            }

            @Override
            public StudentDocument[] newArray(int size) {
                return new StudentDocument[size];
            }
        };
        @SerializedName("mandatory")
        @Expose
        public boolean mandatory;
        @SerializedName("document_name")
        @Expose
        public String documentName;
        @SerializedName("uploaded")
        @Expose
        public boolean uploaded;
        @SerializedName("is_verified")
        @Expose
        public boolean isVerified;

        protected StudentDocument(Parcel in) {
            mandatory = in.readByte() != 0;
            documentName = in.readString();
            uploaded = in.readByte()!=0;
            isVerified = in.readByte() != 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeByte((byte) (mandatory ? 1 : 0));
            dest.writeString(documentName);
            dest.writeByte((byte) (uploaded ? 1 : 0));
            dest.writeByte((byte) (isVerified ? 1 : 0));
        }

        @Override
        public int describeContents() {
            return 0;
        }

    }

    public static class StudentAcademic implements Parcelable {

        public static final Creator<StudentAcademic> CREATOR = new Creator<StudentAcademic>() {
            @Override
            public StudentAcademic createFromParcel(Parcel in) {
                return new StudentAcademic(in);
            }

            @Override
            public StudentAcademic[] newArray(int size) {
                return new StudentAcademic[size];
            }
        };
        @SerializedName("program")
        @Expose
        public String program;
        @SerializedName("academic_batch")
        @Expose
        public String academicBatch;
        @SerializedName("class_name")
        @Expose
        public String className;
        @SerializedName("batch")
        @Expose
        public String batch;
        @SerializedName("roll_number_prefix")
        @Expose
        public String rollNumberPrefix;
        @SerializedName("is_current")
        @Expose
        public boolean isCurrent;

        protected StudentAcademic(Parcel in) {
            program = in.readString();
            academicBatch = in.readString();
            className = in.readString();
            batch = in.readString();
            rollNumberPrefix = in.readString();
            isCurrent = in.readByte() != 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(program);
            dest.writeString(academicBatch);
            dest.writeString(className);
            dest.writeString(batch);
            dest.writeString(rollNumberPrefix);
            dest.writeByte((byte) (isCurrent ? 1 : 0));
        }

        @Override
        public int describeContents() {
            return 0;
        }


    }
}