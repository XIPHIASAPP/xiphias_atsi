package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EmpLeaveResponse extends StatusResponse {
    @SerializedName("employee_leave_information")
    @Expose
    public List<EmployeeLeaveInformation> mLeaveInformations = null;

//    @SerializedName("employee_leave_cancel_information")
//    @Expose
//    public List<EmployeeLeaveInformation> mLeaveCancelInformations = null;

}