package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.SerializedName;



public class StudentDetailsBody {

    @SerializedName("examtype_id")
    private String exampeId;
    @SerializedName("date")
    private String date;
    @SerializedName("student_id")
    private String student_id;
    @SerializedName("from_date")
    private String from_date;
    @SerializedName("to_date")
    private String to_date;
    @SerializedName("is_month")
    private boolean is_month;
    @SerializedName("month")
    private String month;
    @SerializedName("syllabus_id")
    private int syllabusId;

    public void setSyllabusId(int syllabusId) {
        this.syllabusId = syllabusId;
    }

    public void setExampeId(String exampeId) {
        this.exampeId = exampeId;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setStudentId(String student_id) {
        this.student_id = student_id;
    }

    public void setFromDate(String from_date) {
        this.from_date = from_date;
    }

    public void setToDate(String to_date) {
        this.to_date = to_date;
    }

    public void setMonthBoolean(boolean is_month) {
        this.is_month = is_month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
}
