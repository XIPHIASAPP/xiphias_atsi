package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 6/3/18.
 */

public class EmployeeLeaveInformation {
        @SerializedName("employee_full_name")
        @Expose
        public String employeeFullName;
        @SerializedName("leave_application_id")
        @Expose
        public String leaveApplicationId;
        @SerializedName("leave_type")
        @Expose
        public String leaveType;
        @SerializedName("leave_from")
        @Expose
        public String leaveFrom;
        @SerializedName("leave_to")
        @Expose
        public String leaveTo;
        @SerializedName("approver_name")
        @Expose
        public String approverName;
        @SerializedName("approver_status")
        @Expose
        public String approverStatus;
}
