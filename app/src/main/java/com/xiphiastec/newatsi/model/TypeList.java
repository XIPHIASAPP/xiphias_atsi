package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class TypeList extends StatusResponse {
    @SerializedName("attendence_type")
    @Expose
    public List<AttendanceType> attendenceType = null;
    @SerializedName("shift_list")
    @Expose
    public List<ShiftList> shiftList = null;


    public static class ShiftList {
        @SerializedName("shift_id")
        @Expose
        public String shiftId;
        @SerializedName("shift_name")
        @Expose
        public String shiftName;

    }

}