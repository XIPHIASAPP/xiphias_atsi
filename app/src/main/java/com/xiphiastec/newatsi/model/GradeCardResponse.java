package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class GradeCardResponse extends StatusResponse {
    @SerializedName("student_grade")
    @Expose
    private List<GradeCard> studentGrade = null;
    @SerializedName("institution_name")
    @Expose
    private String institutionName;
    @SerializedName("student_name")
    @Expose
    private String studentName;
    @SerializedName("student_registration")
    @Expose
    private String studentRegistration;
    @SerializedName("student_roll_number")
    @Expose
    private String studentRollNumber;
    @SerializedName("sgpa")
    @Expose
    private String sgpa;
    @SerializedName("cgpa")
    @Expose
    private String cgpa;
    @SerializedName("total_credit")
    @Expose
    private String totalCredit;
    @SerializedName("total_creditpoints")
    @Expose
    private String totalCreditpoints;
    @SerializedName("percentage")
    @Expose
    private String percentage;
    @SerializedName("logo_url")
    @Expose
    private String logoUrl;
    @SerializedName("program_name")
    @Expose
    private String programName;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("class_name")
    @Expose
    private String className;

    public List<GradeCard> getStudentGrade() {
        return studentGrade;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public String getStudentName() {
        return studentName;
    }

    public String getStudentRegistration() {
        return studentRegistration;
    }

    public String getStudentRollNumber() {
        return studentRollNumber;
    }

    public String getSgpa() {
        return sgpa;
    }

    public String getCgpa() {
        return cgpa;
    }

    public String getTotalCredit() {
        return totalCredit;
    }

    public String getTotalCreditpoints() {
        return totalCreditpoints;
    }

    public String getPercentage() {
        return percentage;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public String getProgramName() {
        return programName;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public String getResult() {
        return result;
    }

    public String getClassName() {
        return className;
    }

}
