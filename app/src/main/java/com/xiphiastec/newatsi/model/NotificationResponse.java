package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationResponse extends StatusResponse {
    @SerializedName("todays_notification")
    @Expose
    public List<Notification> mTodayList = null;

}