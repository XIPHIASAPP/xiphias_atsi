package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public class StudentStatistic {
    @SerializedName("subject_name")
    @Expose
    public String subjectName;
    @SerializedName("obtained_marks")
    @Expose
    public String obtainedMarks;
}
