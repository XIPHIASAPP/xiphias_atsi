package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public class EmpSalaryResponse extends StatusResponse {
    @SerializedName("employee_name")
    @Expose
    public String employeeName;
    @SerializedName("payslip_id")
    @Expose
    public String payslipId;
    @SerializedName("working_days")
    @Expose
    public String workingDays;
    @SerializedName("basic_salary")
    @Expose
    public String basicSalary;
    @SerializedName("absent_days")
    @Expose
    public String absentDays;
    @SerializedName("absent_amount")
    @Expose
    public String absentAmount;
    @SerializedName("income_tax")
    @Expose
    public String incomeTax;
    @SerializedName("HRA")
    @Expose
    public String HRA;
    @SerializedName("conveyance")
    @Expose
    public String conveyance;
    @SerializedName("loan_emi")
    @Expose
    public String loanEmi;
    @SerializedName("total_earning")
    @Expose
    public String totalEarning;
    @SerializedName("total_deduction")
    @Expose
    public String totalDeduction;
    @SerializedName("gross_amount")
    @Expose
    public String grossAmount;
    @SerializedName("net_salary")
    @Expose
    public String netSalary;
    @SerializedName("month_name")
    @Expose
    public String monthName;
    @SerializedName("date")
    @Expose
    public String date;
}
