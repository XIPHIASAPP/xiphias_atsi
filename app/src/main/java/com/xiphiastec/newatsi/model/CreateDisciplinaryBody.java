package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class CreateDisciplinaryBody {
    @SerializedName("employee_id")
    @Expose
    public String employeeId;
    @SerializedName("date_of_indiscipline")
    @Expose
    public String dateOfIndiscipline;
    @SerializedName("description_by_employee")
    @Expose
    public String descriptionByEmployee;
    @SerializedName("indiscipline_action")
    @Expose
    public String indisciplineAction;
    @SerializedName("action_by_management")
    @Expose
    public String actionByManagement;

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public void setDateOfIndiscipline(String dateOfIndiscipline) {
        this.dateOfIndiscipline = dateOfIndiscipline;
    }

    public void setActionByManagement(String actionByManagement) {
        this.actionByManagement = actionByManagement;
    }

    public void setDescriptionByEmployee(String descriptionByEmployee) {
        this.descriptionByEmployee = descriptionByEmployee;
    }

    public void setIndisciplineAction(String indisciplineAction) {
        this.indisciplineAction = indisciplineAction;
    }
}
