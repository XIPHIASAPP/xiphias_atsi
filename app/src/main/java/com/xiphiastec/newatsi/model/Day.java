package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class Day {

    @SerializedName("periods")
    @Expose
    public List<Object> periods = null;
    @SerializedName("day")
    @Expose
    public String day;

}
