package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public class EmployeeAttendanceInformation {

    @SerializedName("employee_id")
    @Expose
    public String employeeId;
    @SerializedName("employee_full_name")
    @Expose
    public String employeeFullName;
    @SerializedName("shift_name")
    @Expose
    public String shiftName;
    @SerializedName("attendance_type")
    @Expose
    public String attendanceType;
    @SerializedName("attendance_date")
    @Expose
    public String attendanceDate;
    @SerializedName("in_time")
    @Expose
    public String inTime;
    @SerializedName("out_time")
    @Expose
    public String outTime;


}