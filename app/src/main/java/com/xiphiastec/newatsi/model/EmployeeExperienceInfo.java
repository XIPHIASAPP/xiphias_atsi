package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public class EmployeeExperienceInfo {
    @SerializedName("employee_name")
    @Expose
    public String employeeName;
    @SerializedName("branch")
    @Expose
    public String branch;
    @SerializedName("location")
    @Expose
    public String location;
    @SerializedName("designation")
    @Expose
    public String designation;
    @SerializedName("start_date")
    @Expose
    public String startDate;
    @SerializedName("end_date")
    @Expose
    public String endDate;
    @SerializedName("salary")
    @Expose
    public String salary;

}