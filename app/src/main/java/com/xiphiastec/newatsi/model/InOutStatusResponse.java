package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class InOutStatusResponse extends StatusResponse {
    @SerializedName("student_in_out_status")
    @Expose
    public List<StudentInOutStatus> mList = null;

}
