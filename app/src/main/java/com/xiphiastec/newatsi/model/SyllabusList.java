package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SyllabusList {

    @SerializedName("subject_name")
    @Expose
    private String subjectName;
    @SerializedName("subject_group")
    @Expose
    private String subjectGroup;
    @SerializedName("month_name")
    @Expose
    private String monthName;
    @SerializedName("syllabus_id")
    @Expose
    private int syllabusId;

    public String getSubjectName() {
        return subjectName;
    }

    public String getSubjectGroup() {
        return subjectGroup;
    }

    public String getMonthName() {
        return monthName;
    }

    public int getSyllabusId() {
        return syllabusId;
    }
}