package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 3/3/18.
 */

public  class SubjectAttendanceResponse extends StatusResponse {
    @SerializedName("attendance_student")
    @Expose
    public List<AttendanceStudent> mList = null;
    @SerializedName("date")
    @Expose
    public String date;

}
