package com.xiphiastec.newatsi.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class EmployeeAsset implements Parcelable {

    public static final Creator<EmployeeAsset> CREATOR = new Creator<EmployeeAsset>() {
        @Override
        public EmployeeAsset createFromParcel(Parcel in) {
            return new EmployeeAsset(in);
        }

        @Override
        public EmployeeAsset[] newArray(int size) {
            return new EmployeeAsset[size];
        }
    };
    @SerializedName("asset_name")
    @Expose
    public String assetName;
    @SerializedName("model_number")
    @Expose
    public String modelNumber;
    @SerializedName("issue_date")
    @Expose
    public String issueDate;
    @SerializedName("return_date")
    @Expose
    public String returnDate;
    @SerializedName("remark")
    @Expose
    public String remark;

    protected EmployeeAsset(Parcel in) {
        assetName = in.readString();
        modelNumber = in.readString();
        issueDate = in.readString();
        returnDate = in.readString();
        remark = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(assetName);
        dest.writeString(modelNumber);
        dest.writeString(issueDate);
        dest.writeString(returnDate);
        dest.writeString(remark);
    }

    @Override
    public int describeContents() {
        return 0;
    }


}
