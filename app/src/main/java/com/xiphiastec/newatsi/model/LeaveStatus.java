package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 9/3/18.
 */

public class LeaveStatus {
    @SerializedName("status_id")
    @Expose
    public int mStatusId;

    @SerializedName("status_name")
    @Expose
    public String mStatusName;

    public int getmStatusId() {
        return mStatusId;
    }

    public void setmStatusId(int mStatusId) {
        this.mStatusId = mStatusId;
    }

    public String getmStatusName() {
        return mStatusName;
    }

    public void setmStatusName(String mStatusName) {
        this.mStatusName = mStatusName;
    }
}
