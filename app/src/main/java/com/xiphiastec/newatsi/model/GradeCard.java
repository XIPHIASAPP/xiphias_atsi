package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public class GradeCard {
    @SerializedName("subject_name")
    @Expose
    private String subjectName;
    @SerializedName("subject_code")
    @Expose
    private String subjectCode;
    @SerializedName("grade")
    @Expose
    private String grade;
    @SerializedName("points")
    @Expose
    private int points;

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public void setCreditPoints(int creditPoints) {
        this.creditPoints = creditPoints;
    }

    @SerializedName("credit")

    @Expose
    private int credit;
    @SerializedName("credit_points")
    @Expose
    private int creditPoints;

    @Override
    public String toString() {
        return "GradeCard{" +
                "subjectName='" + subjectName + '\'' +
                ", subjectCode='" + subjectCode + '\'' +
                ", grade='" + grade + '\'' +
                ", points=" + points +
                ", credit=" + credit +
                ", creditPoints=" + creditPoints +
                '}';
    }

    public String getSubjectName() {
        return subjectName;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public String getGrade() {
        return grade;
    }

    public int getPoints() {
        return points;
    }

    public int getCredit() {
        return credit;
    }

    public int getCreditPoints() {
        return creditPoints;
    }
}
