package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public class DropDownActionsResponse extends StatusResponse {
    @SerializedName("emp_create_dropdown")
    @Expose
    public List<EmpCreateDropdown> mList = null;


}