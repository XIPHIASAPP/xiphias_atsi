package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class DashboardResponse extends StatusResponse {
    @SerializedName("time_table_student")
    @Expose
    public List<TimeTableStudent> timeTableStudent = null;
    @SerializedName("syllabus_list")
    @Expose
    public List<SyllabusList> syllabusList = null;
    @SerializedName("student_statistics")
    @Expose
    public List<StudentStatistic> studentStatistics = null;
    @SerializedName("total_marks")
    @Expose
    public String totalMarks;
    @SerializedName("notification_count")
    @Expose
    public int notificationCount;

}