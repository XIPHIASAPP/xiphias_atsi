package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class StudentInOutStatus {
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("out_time")
    @Expose
    public String outTime;
    @SerializedName("in_time")
    @Expose
    public String inTime;
    @SerializedName("reason")
    @Expose
    public String reason;
}
