package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 10/3/18.
 */

public class CancelRequestBody {

    @SerializedName("application_id")
    @Expose
    public String mApplicationId;

    @SerializedName("reason")
    @Expose
    public String reason;

    public String getmApplicationId() {
        return mApplicationId;
    }

    public void setmApplicationId(String mApplicationId) {
        this.mApplicationId = mApplicationId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

}
