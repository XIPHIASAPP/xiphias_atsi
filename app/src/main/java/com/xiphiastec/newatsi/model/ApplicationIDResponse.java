package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApplicationIDResponse extends StatusResponse {
    @SerializedName("leave_applicationids")
    @Expose
    public List<LeaveApplicationId> mLeaveApplicationIds = null;
    @SerializedName("cancel_leave_applicationid")
    @Expose
    public List<CancelLeaveApplicationId> mCancelLeaveApplicationIds = null;

    public static class CancelLeaveApplicationId {
        @SerializedName("cancel_application_id")
        @Expose
        public String cancelApplicationId;

    }

    public static class LeaveApplicationId {

        @SerializedName("leave_application_id")
        @Expose
        public String leaveApplicationId;

    }
}

