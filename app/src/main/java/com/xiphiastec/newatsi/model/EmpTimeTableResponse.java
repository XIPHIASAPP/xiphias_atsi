package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public class EmpTimeTableResponse extends StatusResponse {
    @SerializedName("time_table_employee_monday")
    @Expose
    public List<TimeTableEmployee> monday = null;
    @SerializedName("time_table_employee_tuesday")
    @Expose
    public List<TimeTableEmployee> tuesday = null;
    @SerializedName("time_table_employee_wednesday")
    @Expose
    public List<TimeTableEmployee> wednesday = null;
    @SerializedName("time_table_employee_thursday")
    @Expose
    public List<TimeTableEmployee> thursday = null;
    @SerializedName("time_table_employee_friday")
    @Expose
    public List<TimeTableEmployee> friday = null;
    @SerializedName("time_table_employee_saturday")
    @Expose
    public List<TimeTableEmployee> saturday = null;
    @SerializedName("time_table_employee_sunday")
    @Expose
    public List<TimeTableEmployee> sunday = null;

}
