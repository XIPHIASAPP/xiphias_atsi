package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class EmpProfileResponse extends StatusResponse {
    @SerializedName("employee_profile_information")
    @Expose
    public EmployeeProfileInformation mInformation;
    @SerializedName("employee_address")
    @Expose
    public EmployeeAddress employeeAddress;
    @SerializedName("employee_family")
    @Expose
    public ArrayList<EmployeeFamily> employeeFamily = null;
    @SerializedName("employee_designation")
    @Expose
    public List<EmployeeDesignation> employeeDesignation = null;
    @SerializedName("employee_asset")
    @Expose
    public ArrayList<EmployeeAsset> employeeAsset = null;




}
