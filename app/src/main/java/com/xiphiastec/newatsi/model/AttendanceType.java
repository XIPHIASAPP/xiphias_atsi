package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class AttendanceType {
    @SerializedName("attendence_type_id")
    @Expose
    public String attendenceTypeId;
    @SerializedName("attendence_type_name")
    @Expose
    public String attendenceTypeName;
}
