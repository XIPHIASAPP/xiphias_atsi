package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public class EmployeeDesciplinary {

    @SerializedName("employee_full_name")
    @Expose
    public String employeeFullName;
    @SerializedName("indiscipline_act")
    @Expose
    public String indisciplineAct;
    @SerializedName("action_by_managmt")
    @Expose
    public String actionByManagmt;
    @SerializedName("description_by_emp")
    @Expose
    public String descriptionByEmp;
    @SerializedName("created_on")
    @Expose
    public String createdOn;
    @SerializedName("created_by_name")
    @Expose
    public String createdByName;
    @SerializedName("updated_on")
    @Expose
    public String updatedOn;
}
