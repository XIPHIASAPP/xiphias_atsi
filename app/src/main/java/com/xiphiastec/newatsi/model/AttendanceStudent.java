package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 3/3/18.
 */

public  class AttendanceStudent {
    @SerializedName("is_present")
    @Expose
    public boolean isPresent;
    @SerializedName("reason")
    @Expose
    public String reason;
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("subject_name")
    @Expose
    public String subjectName;

}
