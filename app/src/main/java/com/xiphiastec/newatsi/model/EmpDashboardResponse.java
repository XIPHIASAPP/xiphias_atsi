package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public class EmpDashboardResponse extends StatusResponse {
    @SerializedName("time_table_employee")
    @Expose
    public List<TimeTableEmployee> mList = null;
    @SerializedName("notification_count")
    public int notificationCount;



}
