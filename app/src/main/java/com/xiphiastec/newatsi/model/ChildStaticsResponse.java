package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class ChildStaticsResponse extends StatusResponse {
    @SerializedName("parent_statistics")
    @Expose
    public List<ParentStatistic> parentStatistics = null;
    @SerializedName("student_id")
    @Expose
    public int studentId;

}
