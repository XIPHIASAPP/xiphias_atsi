package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by xiphias0101 on 11/12/2017.
 */

public class EmployeeDetailsBody {
    @SerializedName("attendance_type_id")
    String attendanceTypeId;
    @SerializedName("shift_id")
    String shiftId;
    @SerializedName("month_year")
    String monthYear;
    @SerializedName("date")
    String date;
    @SerializedName("leave_id")
    String leaveId;

    public void setLeaveId(String leaveId) {
        this.leaveId = leaveId;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setAttendanceTypeId(String attendanceTypeId) {
        this.attendanceTypeId = attendanceTypeId;
    }

    public void setShiftId(String shiftId) {
        this.shiftId = shiftId;
    }

    public void setMonthYear(String monthYear) {
        this.monthYear = monthYear;
    }
}
