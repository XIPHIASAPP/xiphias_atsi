package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public class EmployeeQualificationInfo {

    @SerializedName("employee_fullname")
    @Expose
    public String employeeFullname;
    @SerializedName("qualification")
    @Expose
    public String qualification;
    @SerializedName("board_university")
    @Expose
    public String boardUniversity;
    @SerializedName("speciality")
    @Expose
    public String speciality;
    @SerializedName("passing_year")
    @Expose
    public String passingYear;
    @SerializedName("score")
    @Expose
    public String score;
    @SerializedName("certificate_name")
    @Expose
    public String certificateName;
    @SerializedName("certificate_path")
    @Expose
    public String certificatePath;

}
