package com.xiphiastec.newatsi.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class EmployeeFamily implements Parcelable {

    public static final Creator<EmployeeFamily> CREATOR = new Creator<EmployeeFamily>() {
        @Override
        public EmployeeFamily createFromParcel(Parcel in) {
            return new EmployeeFamily(in);
        }

        @Override
        public EmployeeFamily[] newArray(int size) {
            return new EmployeeFamily[size];
        }
    };
    @SerializedName("relation_type")
    @Expose
    public String relationType;
    @SerializedName("parent_name")
    @Expose
    public String parentName;
    @SerializedName("parent_email_id")
    @Expose
    public String parentEmailId;
    @SerializedName("parent_mobile_number")
    @Expose
    public String parentMobileNumber;
    @SerializedName("parent_phone_number")
    @Expose
    public String parentPhoneNumber;
    @SerializedName("parent_fax_number")
    @Expose
    public String parentFaxNumber;
    @SerializedName("profession")
    @Expose
    public String profession;
    @SerializedName("company")
    @Expose
    public String company;
    @SerializedName("company_address")
    @Expose
    public String companyAddress;
    @SerializedName("designation")
    @Expose
    public String designation;
    @SerializedName("company_email")
    @Expose
    public String companyEmail;
    @SerializedName("company_phone_number")
    @Expose
    public String companyPhoneNumber;
    @SerializedName("dependant_employee")
    @Expose
    public boolean dependantEmployee;

    protected EmployeeFamily(Parcel in) {
        relationType = in.readString();
        parentName = in.readString();
        parentEmailId = in.readString();
        parentMobileNumber = in.readString();
        parentPhoneNumber = in.readString();
        parentFaxNumber = in.readString();
        profession = in.readString();
        company = in.readString();
        companyAddress = in.readString();
        designation = in.readString();
        companyEmail = in.readString();
        companyPhoneNumber = in.readString();
        dependantEmployee = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(relationType);
        dest.writeString(parentName);
        dest.writeString(parentEmailId);
        dest.writeString(parentMobileNumber);
        dest.writeString(parentPhoneNumber);
        dest.writeString(parentFaxNumber);
        dest.writeString(profession);
        dest.writeString(company);
        dest.writeString(companyAddress);
        dest.writeString(designation);
        dest.writeString(companyEmail);
        dest.writeString(companyPhoneNumber);
        dest.writeByte((byte) (dependantEmployee ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

}
