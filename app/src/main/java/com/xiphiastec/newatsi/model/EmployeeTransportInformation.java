package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public class EmployeeTransportInformation {

    @SerializedName("vehicle_number")
    @Expose
    public String vehicleNumber;
    @SerializedName("driver_name")
    @Expose
    public String driverName;
    @SerializedName("conductor_name")
    @Expose
    public String conductorName;
    @SerializedName("vehicle_type")
    @Expose
    public String vehicleType;
    @SerializedName("first_name")
    @Expose
    public Object firstName;
    @SerializedName("route_name")
    @Expose
    public String routeName;
    @SerializedName("stoppage_name")
    @Expose
    public String stoppageName;
    @SerializedName("employee_name")
    @Expose
    public String employeeName;
}
