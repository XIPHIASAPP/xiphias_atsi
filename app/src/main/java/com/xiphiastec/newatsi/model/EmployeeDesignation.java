package com.xiphiastec.newatsi.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class EmployeeDesignation implements Parcelable {
    public static final Creator<EmployeeDesignation> CREATOR = new Creator<EmployeeDesignation>() {
        @Override
        public EmployeeDesignation createFromParcel(Parcel in) {
            return new EmployeeDesignation(in);
        }

        @Override
        public EmployeeDesignation[] newArray(int size) {
            return new EmployeeDesignation[size];
        }
    };
    @SerializedName("designation_type")
    @Expose
    public String designationType;
    @SerializedName("department")
    @Expose
    public String department;
    @SerializedName("teacher_type")
    @Expose
    public String teacherType;
    @SerializedName("staff_type")
    @Expose
    public String staffType;
    @SerializedName("grade_id")
    @Expose
    public int gradeId;
    @SerializedName("grade_name")
    @Expose
    public String gradeName;
    @SerializedName("currently_working")
    @Expose
    public boolean currentlyWorking;

    protected EmployeeDesignation(Parcel in) {
        designationType = in.readString();
        department = in.readString();
        teacherType = in.readString();
        staffType = in.readString();
        gradeId = in.readInt();
        gradeName = in.readString();
        currentlyWorking = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(designationType);
        dest.writeString(department);
        dest.writeString(teacherType);
        dest.writeString(staffType);
        dest.writeInt(gradeId);
        dest.writeString(gradeName);
        dest.writeByte((byte) (currentlyWorking ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

}
