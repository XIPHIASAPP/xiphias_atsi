package com.xiphiastec.newatsi.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class Student implements Parcelable {
    public static final Creator<Student> CREATOR = new Creator<Student>() {
        @Override
        public Student createFromParcel(Parcel in) {
            return new Student(in);
        }

        @Override
        public Student[] newArray(int size) {
            return new Student[size];
        }
    };
    @SerializedName("full_name")
    @Expose
    public String fullName;
    @SerializedName("gender")
    @Expose
    public String gender;
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("token")
    @Expose
    public String token;

    protected Student(Parcel in) {
        fullName = in.readString();
        gender = in.readString();
        id = in.readInt();
        token=in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fullName);
        dest.writeString(gender);
        dest.writeInt(id);
        dest.writeString(token);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
