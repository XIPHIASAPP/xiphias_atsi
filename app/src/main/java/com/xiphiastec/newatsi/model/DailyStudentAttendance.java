package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 3/3/18.
 */

public class DailyStudentAttendance {
    @SerializedName("is_present")
    @Expose
    public boolean isPresent;
    @SerializedName("reason")
    @Expose
    public String reason;
    @SerializedName("date")
    @Expose
    public String date;

    public boolean isPresent() {
        return isPresent;
    }

    public String getReason() {
        return reason;
    }

    public String getDate() {
        return date;
    }
}
