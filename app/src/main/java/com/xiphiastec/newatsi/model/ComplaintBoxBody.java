package com.xiphiastec.newatsi.model;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public class ComplaintBoxBody {
    private String complaint;
    private String incident_date;
    private String place_of_occurence;
    private String category;
    private String name_of_person;
    private String attacment;

    public ComplaintBoxBody(String complaint, String incident_date, String place_of_occurence, String category, String name_of_person, String attacment) {
        this.complaint = complaint;
        this.incident_date = incident_date;
        this.place_of_occurence = place_of_occurence;
        this.category = category;
        this.name_of_person = name_of_person;
        this.attacment = attacment;
    }
}
