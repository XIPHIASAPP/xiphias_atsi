package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 8/3/18.
 */

public class LeaveRequestBody {

    @SerializedName("status_id")
    @Expose
    public int mStatusId;

    public int getLeaveStatus() {
        return mStatusId;
    }

    public void setLeaveStatus(int leaveStatus) {
        this.mStatusId = leaveStatus;
    }
}
