package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Leave {
    @SerializedName("leave_from")
    @Expose
    private String leaveFrom;
    @SerializedName("leave_to")
    @Expose
    private String leaveTo;
    @SerializedName("minimun_limit")
    @Expose
    private String minimunLimit;
    @SerializedName("maximum_limit")
    @Expose
    private String maximumLimit;
    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("half_day_from")
    @Expose
    private String halfDayFrom;
    @SerializedName("half_day_to")
    @Expose
    private String halfDayTo;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("leave_type")
    @Expose
    private String leaveType;
    @SerializedName("application_id")
    @Expose
    private String applicationId;

    public void setLeaveFrom(String leaveFrom) {
        this.leaveFrom = leaveFrom;
    }

    public void setLeaveTo(String leaveTo) {
        this.leaveTo = leaveTo;
    }

    public void setMinimunLimit(String minimunLimit) {
        this.minimunLimit = minimunLimit;
    }

    public void setMaximumLimit(String maximumLimit) {
        this.maximumLimit = maximumLimit;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public void setHalfDayFrom(String halfDayFrom) {
        this.halfDayFrom = halfDayFrom;
    }

    public void setHalfDayTo(String halfDayTo) {
        this.halfDayTo = halfDayTo;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setLeaveType(String leaveType) {
        this.leaveType = leaveType;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }
}