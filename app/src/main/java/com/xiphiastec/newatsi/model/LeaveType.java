package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public   class LeaveType extends StatusResponse{
        @SerializedName("list_leave_type")
        @Expose
        public List<ListLeaveType> listLeaveType = null;

        public class ListLeaveType {

            @SerializedName("leave_id")
            @Expose
            public String leaveId;
            @SerializedName("leave_type")
            @Expose
            public String leaveType;
            @SerializedName("leave_code")
            @Expose
            public String leaveCode;
        }
    }