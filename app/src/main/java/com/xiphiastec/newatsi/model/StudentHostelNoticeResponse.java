package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class StudentHostelNoticeResponse extends StatusResponse {
    @SerializedName("hostel_notice")
    @Expose
    public List<HostelNotice> hostelNotice = null;

    @Override
    public String toString() {
        return "StudentHostelNoticeResponse{" +
                "hostelNotice=" + hostelNotice +
                ", status=" + status +
                ", message='" + message + '\'' +
                '}';
    }

    public List<HostelNotice> getHostelNotice() {
        return hostelNotice;
    }

    public boolean isStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

}
