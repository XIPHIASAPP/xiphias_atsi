package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public class DisciplineViolation {
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("issue")
    @Expose
    public String issue;
    @SerializedName("fine")
    @Expose
    public String fine;

}
