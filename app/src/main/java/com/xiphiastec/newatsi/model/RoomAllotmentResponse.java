package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class RoomAllotmentResponse extends StatusResponse {
    @SerializedName("building_name")
    @Expose
    private String buildingName;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("floor")
    @Expose
    private String floor;
    @SerializedName("room")
    @Expose
    private String room;
    @SerializedName("bed_number")
    @Expose
    private String bedNumber;
    @SerializedName("allotment_date")
    @Expose
    private String allotmentDate;
    @SerializedName("incharge_name")
    @Expose
    private String inchargeName;
    @SerializedName("incharge_phone_number")
    @Expose
    private String inchargePhoneNumber;

    public String getBuildingName() {
        return buildingName;
    }

    public String getAddress() {
        return address;
    }

    public String getFloor() {
        return floor;
    }

    public String getRoom() {
        return room;
    }

    public String getBedNumber() {
        return bedNumber;
    }

    public String getAllotmentDate() {
        return allotmentDate;
    }

    public String getInchargeName() {
        return inchargeName;
    }

    public String getInchargePhoneNumber() {
        return inchargePhoneNumber;
    }
}