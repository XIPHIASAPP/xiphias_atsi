package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 9/3/18.
 */

public class LeaveStatusResponse extends StatusResponse {

    @SerializedName("list_leave_status")
    @Expose
    public List<LeaveStatus> mListStatus;

    public List<LeaveStatus> getmListStatus() {
        return mListStatus;
    }

    public void setmListStatus(List<LeaveStatus> mListStatus) {
        this.mListStatus = mListStatus;
    }
}
