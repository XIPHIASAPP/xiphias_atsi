package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeaveDetails extends StatusResponse {
    @SerializedName("leave_balance")
    @Expose
    public List<LeaveBalance> leaveBalance = null;

    public List<LeaveBalance> getLeaveBalance() {
        return leaveBalance;
    }

    public void setLeaveBalance(List<LeaveBalance> leaveBalance) {
        this.leaveBalance = leaveBalance;
    }

    public class LeaveBalance {
        @SerializedName("minimun_limit")
        @Expose
        public String minimunLimit;
        @SerializedName("maximum_limit")
        @Expose
        public String maximumLimit;
        @SerializedName("balance")
        @Expose
        public String balance;
        @SerializedName("application_id")
        @Expose
        public String applicationId;

    }
}
