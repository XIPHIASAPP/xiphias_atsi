package com.xiphiastec.newatsi.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class EmployeeProfileInformation implements Parcelable {

    public static final Creator<EmployeeProfileInformation> CREATOR = new Creator<EmployeeProfileInformation>() {
        @Override
        public EmployeeProfileInformation createFromParcel(Parcel in) {
            return new EmployeeProfileInformation(in);
        }

        @Override
        public EmployeeProfileInformation[] newArray(int size) {
            return new EmployeeProfileInformation[size];
        }
    };
    @SerializedName("unique_id")
    @Expose
    public String uniqueId;
    @SerializedName("employee_name")
    @Expose
    public String employeeName;
    @SerializedName("picture")
    @Expose
    public int picture;
    @SerializedName("marital_status")
    @Expose
    public String maritalStatus;
    @SerializedName("gender")
    @Expose
    public String gender;
    @SerializedName("category_name")
    @Expose
    public String categoryName;
    @SerializedName("cast")
    @Expose
    public String cast;
    @SerializedName("religion_name")
    @Expose
    public String religionName;
    @SerializedName("nationality_name")
    @Expose
    public String nationalityName;
    @SerializedName("date_of_birth")
    @Expose
    public String dateOfBirth;
    @SerializedName("email_id")
    @Expose
    public String emailId;
    @SerializedName("mobile_number")
    @Expose
    public String mobileNumber;
    @SerializedName("fax_number")
    @Expose
    public String faxNumber;
    @SerializedName("mother_tongue")
    @Expose
    public String motherTongue;
    @SerializedName("joining_date")
    @Expose
    public String joiningDate;
    @SerializedName("blood_group")
    @Expose
    public String bloodGroup;
    @SerializedName("pan_number")
    @Expose
    public String panNumber;
    @SerializedName("voter_id")
    @Expose
    public String voterId;
    @SerializedName("driving_licence_number")
    @Expose
    public String drivingLicenceNumber;
    @SerializedName("passport_number")
    @Expose
    public String passportNumber;
    @SerializedName("aadhar_card_number")
    @Expose
    public String aadharCardNumber;
    @SerializedName("other_unique_id")
    @Expose
    public String otherUniqueId;
    @SerializedName("visa_detail")
    @Expose
    public String visaDetail;
    @SerializedName("biometric")
    @Expose
    public int biometric;
    @SerializedName("shift")
    @Expose
    public String shift;

    protected EmployeeProfileInformation(Parcel in) {
        uniqueId = in.readString();
        employeeName = in.readString();
        picture = in.readInt();
        maritalStatus = in.readString();
        gender = in.readString();
        categoryName = in.readString();
        cast = in.readString();
        religionName = in.readString();
        nationalityName = in.readString();
        dateOfBirth = in.readString();
        emailId = in.readString();
        mobileNumber = in.readString();
        faxNumber = in.readString();
        motherTongue = in.readString();
        joiningDate = in.readString();
        bloodGroup = in.readString();
        panNumber = in.readString();
        voterId = in.readString();
        drivingLicenceNumber = in.readString();
        passportNumber = in.readString();
        aadharCardNumber = in.readString();
        otherUniqueId = in.readString();
        visaDetail = in.readString();
        biometric = in.readInt();
        shift = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uniqueId);
        dest.writeString(employeeName);
        dest.writeInt(picture);
        dest.writeString(maritalStatus);
        dest.writeString(gender);
        dest.writeString(categoryName);
        dest.writeString(cast);
        dest.writeString(religionName);
        dest.writeString(nationalityName);
        dest.writeString(dateOfBirth);
        dest.writeString(emailId);
        dest.writeString(mobileNumber);
        dest.writeString(faxNumber);
        dest.writeString(motherTongue);
        dest.writeString(joiningDate);
        dest.writeString(bloodGroup);
        dest.writeString(panNumber);
        dest.writeString(voterId);
        dest.writeString(drivingLicenceNumber);
        dest.writeString(passportNumber);
        dest.writeString(aadharCardNumber);
        dest.writeString(otherUniqueId);
        dest.writeString(visaDetail);
        dest.writeInt(biometric);
        dest.writeString(shift);
    }

    @Override
    public int describeContents() {
        return 0;
    }

}