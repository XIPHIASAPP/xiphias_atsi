package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public class EmployeeScheduleInterview {
    @SerializedName("interviewer_name")
    @Expose
    public String interviewerName;
    @SerializedName("applicant_name")
    @Expose
    public String applicantName;
    @SerializedName("job_title")
    @Expose
    public String jobTitle;
    @SerializedName("interview_round")
    @Expose
    public String interviewRound;
    @SerializedName("on_date")
    @Expose
    public String onDate;
    @SerializedName("time_from")
    @Expose
    public String timeFrom;
    @SerializedName("time_to")
    @Expose
    public String timeTo;

}
