package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public class StudentDisciplineResponse extends StatusResponse {
    @SerializedName("student_discipline_violation")
    @Expose
    public List<DisciplineViolation> mList = null;

    public List<DisciplineViolation> getList() {
        return mList;
    }

    public boolean isStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

}
