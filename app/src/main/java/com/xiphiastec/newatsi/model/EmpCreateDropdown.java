package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public class EmpCreateDropdown {
    @SerializedName("text")
    @Expose
    public String text;
    @SerializedName("value")
    @Expose
    public String value;

}