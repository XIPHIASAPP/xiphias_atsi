package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class Semester extends StatusResponse {
    @SerializedName("exam_type")
    @Expose
    public List<ExamType> examType = null;


    public  class ExamType {
        @SerializedName("examtype")
        @Expose
        public String examtype;
        @SerializedName("examtypeid")
        @Expose
        public String examtypeid;
    }
}
