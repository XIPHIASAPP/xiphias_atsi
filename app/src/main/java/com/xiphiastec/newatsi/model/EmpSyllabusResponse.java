package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class EmpSyllabusResponse extends StatusResponse {
    @SerializedName("syllabus_covered")
    @Expose
    public List<SyllabusCovered> mList = null;

}
