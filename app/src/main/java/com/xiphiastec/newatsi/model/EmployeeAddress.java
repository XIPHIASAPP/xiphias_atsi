package com.xiphiastec.newatsi.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class EmployeeAddress implements Parcelable {

    public static final Creator<EmployeeAddress> CREATOR = new Creator<EmployeeAddress>() {
        @Override
        public EmployeeAddress createFromParcel(Parcel in) {
            return new EmployeeAddress(in);
        }

        @Override
        public EmployeeAddress[] newArray(int size) {
            return new EmployeeAddress[size];
        }
    };
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("phone_number")
    @Expose
    public String phoneNumber;
    @SerializedName("address1")
    @Expose
    public String address1;
    @SerializedName("address2")
    @Expose
    public String address2;
    @SerializedName("zip_postal_code")
    @Expose
    public String zipPostalCode;
    @SerializedName("present_address")
    @Expose
    public boolean presentAddress;
    @SerializedName("permanent_address")
    @Expose
    public boolean permanentAddress;
    @SerializedName("country_name")
    @Expose
    public String countryName;
    @SerializedName("state_name")
    @Expose
    public String stateName;

    protected EmployeeAddress(Parcel in) {
        email = in.readString();
        phoneNumber = in.readString();
        address1 = in.readString();
        address2 = in.readString();
        zipPostalCode = in.readString();
        presentAddress = in.readByte() != 0;
        permanentAddress = in.readByte() != 0;
        countryName = in.readString();
        stateName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
        dest.writeString(phoneNumber);
        dest.writeString(address1);
        dest.writeString(address2);
        dest.writeString(zipPostalCode);
        dest.writeByte((byte) (presentAddress ? 1 : 0));
        dest.writeByte((byte) (permanentAddress ? 1 : 0));
        dest.writeString(countryName);
        dest.writeString(stateName);
    }

    @Override
    public int describeContents() {
        return 0;
    }


}
