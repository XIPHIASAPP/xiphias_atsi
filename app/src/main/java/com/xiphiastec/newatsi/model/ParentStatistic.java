package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public class ParentStatistic {

    @SerializedName("subject_name")
    @Expose
    public String subjectName;
    @SerializedName("obtain_marks")
    @Expose
    public String obtainMarks;
    @SerializedName("total_marks")
    @Expose
    public String totalMarks;


}
