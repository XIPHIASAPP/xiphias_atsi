package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FeeDetailsResponse extends StatusResponse {
        @SerializedName("fee_details")
        @Expose
        public List<FeeDetailsResponse.FeeDetail> feeDetails = null;
        @SerializedName("fine_details")
        @Expose
        public List<FeeDetailsResponse.FineDetail> fineDetails = null;
        @SerializedName("total_paid")
        @Expose
        public String totalPaid;
        @SerializedName("pending_amount")
        @Expose
        public String pendingAmount;
        @SerializedName("student_id")
        @Expose
        public Integer studentId;
        @SerializedName("student_name")
        @Expose
        public String studentname;
        @SerializedName("student_register_no")
        @Expose
        public String studentRegisterNo;

        public static class FeeDetail {
            @SerializedName("fee_name")
            @Expose
            public String feeName;
            @SerializedName("payment_type")
            @Expose
            public String paymentType;
            @SerializedName("payment_mode")
            @Expose
            public String paymentMode;
            @SerializedName("installment")
            @Expose
            public String installment;
            @SerializedName("amount")
            @Expose
            public String amount;
            @SerializedName("paid_date")
            @Expose
            public String paidDate;

        }

        public static class FineDetail {
            @SerializedName("fine_type")
            @Expose
            public String fineType;
            @SerializedName("fine_description")
            @Expose
            public String fineDescription;
            @SerializedName("fine_start_date")
            @Expose
            public String fineStartDate;
            @SerializedName("fine_taken_date")
            @Expose
            public String fineTakenDate;
            @SerializedName("amount")
            @Expose
            public String amount;


        }
    }