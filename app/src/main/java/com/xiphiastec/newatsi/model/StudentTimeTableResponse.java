package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class StudentTimeTableResponse extends StatusResponse {
    @SerializedName("time_table_student_monday")
    @Expose
    public List<TimeTableStudent> timeTableStudentMonday = null;
    @SerializedName("time_table_student_tuesday")
    @Expose
    public List<TimeTableStudent> timeTableStudentTuesday = null;
    @SerializedName("time_table_student_wednesday")
    @Expose
    public List<TimeTableStudent> timeTableStudentWednesday = null;
    @SerializedName("time_table_student_thursday")
    @Expose
    public List<TimeTableStudent> timeTableStudentThursday = null;
    @SerializedName("time_table_student_friday")
    @Expose
    public List<TimeTableStudent> timeTableStudentFriday = null;
    @SerializedName("time_table_student_saturday")
    @Expose
    public List<TimeTableStudent> timeTableStudentSaturday = null;
    @SerializedName("time_table_student_sunday")
    @Expose
    public List<TimeTableStudent> timeTableStudentSunday = null;
    @SerializedName("days")
    @Expose
    public List<Day> days = null;

}
