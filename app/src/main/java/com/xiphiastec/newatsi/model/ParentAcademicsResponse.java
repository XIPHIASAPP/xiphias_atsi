package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class ParentAcademicsResponse extends StatusResponse {
    @SerializedName("student_id")
    @Expose
    public int studentId;
    @SerializedName("student")
    @Expose
    public String student;
    @SerializedName("award_id")
    @Expose
    public int awardId;
    @SerializedName("program_id")
    @Expose
    public int programId;
    @SerializedName("program")
    @Expose
    public String program;
    @SerializedName("academic_batch_id")
    @Expose
    public int academicBatchId;
    @SerializedName("academic_batch")
    @Expose
    public String academicBatch;
    @SerializedName("class_id")
    @Expose
    public int classId;
    @SerializedName("classs")
    @Expose
    public String classs;
    @SerializedName("batch_id")
    @Expose
    public int batchId;
    @SerializedName("batch")
    @Expose
    public String batch;
    @SerializedName("roll_number_prefix_id")
    @Expose
    public int rollNumberPrefixId;
    @SerializedName("roll_number_prefix")
    @Expose
    public String rollNumberPrefix;
    @SerializedName("roll_number")
    @Expose
    public int rollNumber;
    @SerializedName("cycle_type_id")
    @Expose
    public int cycleTypeId;
    @SerializedName("cycle_name")
    @Expose
    public String cycleName;
    @SerializedName("roll_number_name")
    @Expose
    public String rollNumberName;
    @SerializedName("is_graduated")
    @Expose
    public boolean isGraduated;
    @SerializedName("pass_out_year")
    @Expose
    public int passOutYear;
    @SerializedName("is_current")
    @Expose
    public boolean isCurrent;

}
