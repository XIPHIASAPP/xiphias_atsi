package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public class TimeTableStudent {
    @SerializedName("employee_name")
    @Expose
    public String employeeName;
    @SerializedName("subject_name")
    @Expose
    public String subjectName;
    @SerializedName("day_name")
    @Expose
    public String dayName;
    @SerializedName("start_time")
    @Expose
    public String startTime;
    @SerializedName("end_time")
    @Expose
    public String endTime;
}
