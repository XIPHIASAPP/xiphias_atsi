package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class HallTicketResponse extends StatusResponse {
    @SerializedName("exam_details")
    @Expose
    public List<ExamDetail> examDetails = null;
    @SerializedName("registration_number")
    @Expose
    public String registrationNumber;
    @SerializedName("logo_url")
    @Expose
    public String logoUrl;
    @SerializedName("name_of_the_student")
    @Expose
    public String nameOfTheStudent;
    @SerializedName("paper_registred")
    @Expose
    public String paperRegistred;
    @SerializedName("acadamic_batch")
    @Expose
    public String acadamicBatch;
    @SerializedName("date_of_birth")
    @Expose
    public String dateOfBirth;
    @SerializedName("institute_name")
    @Expose
    public String instituteName;
    @SerializedName("class_type")
    @Expose
    public String classType;

}
