package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class ParentDashboardResponse extends StatusResponse {
    @SerializedName("students")
    @Expose
    public List<Student> students = null;
    @SerializedName("notification_count")
    @Expose
    public int notificationCount;
}
