package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pavan-xiphias on 3/3/18.
 */

public class DailyAttendanceResponse extends StatusResponse {
    @SerializedName("daily_Student_attendance")
    @Expose
    public List<DailyStudentAttendance> mList = null;
    @SerializedName("date")
    @Expose
    public String date;
}