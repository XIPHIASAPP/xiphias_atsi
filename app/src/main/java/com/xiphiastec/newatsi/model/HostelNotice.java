package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class HostelNotice {
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("notice")
    @Expose
    public String notice;

    @Override
    public String toString() {
        return "HostelNotice{" +
                "date='" + date + '\'' +
                ", notice='" + notice + '\'' +
                '}';
    }

    public String getDate() {
        return date;
    }

    public String getNotice() {
        return notice;
    }
}
