package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public  class StudentExamDetail {
    @SerializedName("exam_date")
    @Expose
    public String examDate;
    @SerializedName("exam_time")
    @Expose
    public String examTime;
    @SerializedName("exam_type")
    @Expose
    public String examType;
    @SerializedName("subject")
    @Expose
    public String subject;
    @SerializedName("subject_code")
    @Expose
    public String subjectCode;
}
