package com.xiphiastec.newatsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pavan-xiphias on 13/3/18.
 */

public class SyllabusCovered {

    @SerializedName("subject_name")
    @Expose
    public String subjectName;
    @SerializedName("topic_name")
    @Expose
    public String topicName;
    @SerializedName("week")
    @Expose
    public String week;
    @SerializedName("subject_group")
    @Expose
    public String subjectGroup;
    @SerializedName("month")
    @Expose
    public String month;
    @SerializedName("date")
    @Expose
    public String date;
}
