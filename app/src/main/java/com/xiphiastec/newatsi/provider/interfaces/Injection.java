package com.xiphiastec.newatsi.provider.interfaces;

import android.content.Context;
import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.base.interfaces.CommonApiService;
import com.xiphiastec.newatsi.base.interfaces.EmployeeApiService;
import com.xiphiastec.newatsi.base.interfaces.ParentApiServices;
import com.xiphiastec.newatsi.base.interfaces.StudentApiServices;
import com.xiphiastec.newatsi.provider.schedulers.ImmediateScheduler;
import com.xiphiastec.newatsi.provider.schedulers.SchedulerProvider;

/**
 * Created by xiphias0101 on 09/12/2017.
 */

public interface Injection {
    SchedulerProvider getSchedulerProvider();

    ImmediateScheduler getImmediateScheduler();

    CommonApiService getCommonApiService(@NonNull Context context);

    ParentApiServices getParentApiService(@NonNull Context instance);

    StudentApiServices getStudentApiService(@NonNull Context instance);

    EmployeeApiService getEmployeeServices(@NonNull Context context);
}
