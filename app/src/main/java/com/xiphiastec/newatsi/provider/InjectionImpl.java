package com.xiphiastec.newatsi.provider;

import android.content.Context;
import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.base.NetworkingClient;
import com.xiphiastec.newatsi.base.interfaces.CommonApiService;
import com.xiphiastec.newatsi.base.interfaces.EmployeeApiService;
import com.xiphiastec.newatsi.base.interfaces.ParentApiServices;
import com.xiphiastec.newatsi.base.interfaces.StudentApiServices;
import com.xiphiastec.newatsi.provider.interfaces.Injection;
import com.xiphiastec.newatsi.provider.schedulers.ImmediateScheduler;
import com.xiphiastec.newatsi.provider.schedulers.SchedulerProvider;
import com.xiphiastec.newatsi.util.Utils;


public class InjectionImpl implements Injection {
    @Override
    public SchedulerProvider getSchedulerProvider() {
        return new SchedulerProvider();
    }

    @Override
    public ImmediateScheduler getImmediateScheduler() {
        return new ImmediateScheduler();
    }

    @NonNull
    @Override
    public CommonApiService getCommonApiService(@NonNull Context context) {
        return NetworkingClient.getInstance(context, Utils.BASEURL).commonServices();
    }

    @Override
    public ParentApiServices getParentApiService(@NonNull Context context) {
        return NetworkingClient.getInstance(context, Utils.BASEURL).parentServices();
    }

    @Override
    public EmployeeApiService getEmployeeServices(@NonNull Context context) {
        return NetworkingClient.getInstance(context, Utils.BASEURL).employeeService();
    }

    @Override
    public StudentApiServices getStudentApiService(@NonNull Context context) {
        return NetworkingClient.getInstance(context, Utils.BASEURL).studentServices();
    }
}
