package com.xiphiastec.newatsi;

/**
 * Created by Admin on 10-02-2017.
 */

public class Constants {
    public static final String EXTRA_FRAGMENT_TITLE = "extra_fragment_title";
    public static final String ARGS_FRAGMENT_TITLE = "extra_fragment_title";
    public static final String EXTRA_USER = "extra_user";
    public static final String BASE_URL = "https://www.atsi.in/atsiservices/api/";
    public static final String ARGS_STUDENT_ID = "args_studetnt_id";
    public static final String EXTRA_RESPONSE = "extra_response";
    public static final String ARGS_SYLLABUS_ID = "syllabus_id";
    public static final String ARGS_SYLLABUS_BUNDLE = "syllabus_bundle";
    public static final String ARGS_PROFILE_BUNDLE = "profile_bundle";
    public static final String ARGS_ACADEMICS_BUNDLE = "academics_bundle";
    public static final String ARGS_DOCUMENTS_BUNDLE = "documents_bundle";
    public static final String ARGS_APPLIED_BUNDLE = "applied_bundle";
}
