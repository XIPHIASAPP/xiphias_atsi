package com.xiphiastec.newatsi.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 30-03-2017.
 */

public class CommonFragmentViewPager extends FragmentStatePagerAdapter {
    private List<Fragment> mList = new ArrayList<>();
    private List<String> mStringList = new ArrayList<>();

    public CommonFragmentViewPager(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(Fragment fragment, String s) {
        mList.add(fragment);
        mStringList.add(s);
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        return mList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mStringList.get(position);
    }

    @Override
    public int getCount() {
        return mStringList.size();
    }

    public void clear() {
        mList.clear();
        mStringList.clear();
        notifyDataSetChanged();
    }
}
