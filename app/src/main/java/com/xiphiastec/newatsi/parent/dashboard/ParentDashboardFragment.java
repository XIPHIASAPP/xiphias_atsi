package com.xiphiastec.newatsi.parent.dashboard;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.transition.TransitionManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.xiphiastec.newatsi.Constants;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsParentBaseFragment;
import com.xiphiastec.newatsi.model.ParentDashboardResponse;
import com.xiphiastec.newatsi.model.Student;
import com.xiphiastec.newatsi.model.SyllabusList;
import com.xiphiastec.newatsi.student.activities.StudentActivity;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;
import com.xiphiastec.newatsi.util.PreferenceUtil;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


import static butterknife.ButterKnife.bind;

public class ParentDashboardFragment extends AbsParentBaseFragment
        implements ParentDashboardContract.View {

    @BindView(R.id.recycler_view)
    EmptyViewRecyclerView mRecyclerView;
    @BindView(R.id.tv_empty)
    TextView mTvEmpty;
    @BindView(R.id.root_layout)
    FrameLayout mRootLayout;
    Unbinder unbinder;
    @BindView(R.id.text)
    TextView mText;
    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.image)
    ImageView mImage;

    AtsiProgressDialog atsiProgressDialog;
    private ParentDashboardPresenter mPresenter;
    private ParentDashboardResponse mDashboardResponse;
    private ParentDashboardAdapter mAdapter;

    public ParentDashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new ParentDashboardPresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && getActionBar() != null) {
            getActionBar().setTitle(R.string.dashboard);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new ParentDashboardAdapter();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActionBar() != null) {

            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/RobotoMono-Regular.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.dashboard));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getActionBar().setTitle(SS);
        }
        mPresenter.onSubscriber();

        String parentToken=PreferenceUtil.getInstance(getActivity()).getParentToken();
        PreferenceUtil.getInstance(getActivity()).setToken(parentToken);
        Log.d("Parent",PreferenceUtil.getInstance(getActivity()).getParentToken());
        Log.d("Parent",PreferenceUtil.getInstance(getActivity()).getToken());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onUnSubscriber();
    }

    private List<SyllabusList> populateItemsShow() {
        List<SyllabusList> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            //list.add(new SyllabusModel());
        }
        return list;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_parent_dashboard, container, false);
        unbinder = ButterKnife.bind(this, view);
        mText.setText(getCurrentDayText());
        mTitle.setText(getHelloText());
        return view;
    }

    private void setupRecyclerView(ParentDashboardResponse response) {
        mDashboardResponse = response;
        ArrayList arrayList = new ArrayList();
        arrayList.add("Children's");
        mAdapter.swapData(arrayList);
    }

    private String getHelloText() {
        return PreferenceUtil.getInstance(getContext()).getUserName();
    }

    private String getCurrentDayText() {
        //"Hi <name>, hope your day starts well. Here your " +
        // Good afternoon, time table Wednesday
        //return getTimeOfDay() + ", time table for " + getWeekDay();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM, EEE", Locale.getDefault());
        return dateFormat.format(calendar.getTime());
    }

    private List<Student> getNames() {
        return mDashboardResponse.students;
    }

    @Override
    public void onLoading() {
        TransitionManager.beginDelayedTransition(mRootLayout);
       atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        TransitionManager.beginDelayedTransition(mRootLayout);
        atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onResponse(ParentDashboardResponse response) {
        setupRecyclerView(response);
    }

    @Override
    public void onComplete() {
        TransitionManager.beginDelayedTransition(mRootLayout);
       atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    class ParentDashboardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private static final int TYPE_CHILDREN = 0;
        private static final int TYPE_STATISTICS = 1;
        private static final int TYPE_SYLLABUS = 2;

        private ArrayList mArrayList = new ArrayList();

        ParentDashboardAdapter() {
        }

        @Override
        public int getItemViewType(int i) {
            return i == 0 ? TYPE_CHILDREN : super.getItemViewType(i);
            //return i == 0 ? TYPE_CHILDREN : i == 1 ? TYPE_STATISTICS : i == 2 ? TYPE_SYLLABUS : TYPE_CHILDREN;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType) {
                case TYPE_CHILDREN:
                    return new ChildrenViewHolder(LayoutInflater.from(getContext())
                            .inflate(R.layout.fragment_parent_dashboard_children, parent, false));
                case TYPE_STATISTICS:
                    return new StatisticsViewHolder(LayoutInflater.from(getContext())
                            .inflate(R.layout.fragment_parent_dashboard_statistics, parent, false));
                case TYPE_SYLLABUS:
                    return new SyllabusViewHolder(LayoutInflater.from(getContext())
                            .inflate(R.layout.fragment_parent_dashboard_syllabus, parent, false));
            }
            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int i) {
            switch (getItemViewType(i)) {
                case TYPE_CHILDREN:
                    ChildrenViewHolder childrenViewHolder = (ChildrenViewHolder) holder;
                    bindChildren(childrenViewHolder);
                    break;
                case TYPE_STATISTICS:
                    StatisticsViewHolder statisticsViewHolder = (StatisticsViewHolder) holder;
                    bindStatistics(statisticsViewHolder);
                    break;
                case TYPE_SYLLABUS:
                    SyllabusViewHolder syllabusViewHolder = (SyllabusViewHolder) holder;
                    bindSyllabus(syllabusViewHolder);
                    break;
            }
        }

        private void bindSyllabus(SyllabusViewHolder holder) {
            holder.mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            holder.mRecyclerView.setAdapter(new SyllabusCoveredAdapter(populateItemsShow()));
        }

        private void bindStatistics(StatisticsViewHolder holder) {
            List<SyllabusList> list = new ArrayList<>();
            holder.mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
            holder.mRecyclerView.setAdapter(new StatisticsAdapter(list));
        }

        private void bindChildren(ChildrenViewHolder holder) {
            holder.mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false));
            holder.mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            holder.mRecyclerView.setAdapter(new ChildrenListAdapter(getNames()));
        }

        @Override
        public int getItemCount() {
            return mArrayList.size();
        }

        void swapData(ArrayList arrayList) {
            mArrayList = arrayList;
            notifyDataSetChanged();
        }


        class SyllabusViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.recycler_view)
            RecyclerView mRecyclerView;

            SyllabusViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }

        class StatisticsViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.recycler_view)
            RecyclerView mRecyclerView;

            StatisticsViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }

        class ChildrenViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.recycler_view)
            RecyclerView mRecyclerView;

            ChildrenViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }

    class SyllabusCoveredAdapter extends RecyclerView.Adapter<SyllabusCoveredAdapter.SyllabusCoveredHolder> {

        private List<SyllabusList> mList = new ArrayList<>();

        SyllabusCoveredAdapter(List<SyllabusList> list) {
            mList = list;
        }

        @Override
        public SyllabusCoveredHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new SyllabusCoveredHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_syllabus, parent, false));
        }

        @Override
        public void onBindViewHolder(SyllabusCoveredHolder holder, int position) {

        }

        @Override
        public int getItemCount() {
            return mList.size();
        }

        class SyllabusCoveredHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.month)
            TextView mMonth;
            @BindView(R.id.subject_name)
            TextView mSubjectName;
            @BindView(R.id.subject_group)
            TextView mSubjectGroup;

            SyllabusCoveredHolder(View itemView) {
                super(itemView);
                bind(this, itemView);
            }
        }
    }

    class ChildrenListAdapter extends RecyclerView.Adapter<ChildrenListAdapter.ChildrenViewHolder> {
        List<Student> mList = new ArrayList<>();

        ChildrenListAdapter(List<Student> list) {
            mList = list;
        }

        @Override
        public ChildrenViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ChildrenViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.simple_item, parent, false));
        }

        @Override
        public void onBindViewHolder(ChildrenViewHolder holder, int position) {

            final Student student = mList.get(holder.getAdapterPosition());

            if(student.fullName.trim().isEmpty() || student.fullName==null)
                holder.txtName.setText("NA");
            else
                holder.txtName.setText(student.fullName);

            if(student.gender!=null)
            if(student.gender.equalsIgnoreCase("1"))
                holder.ivProfilePic.setImageResource(R.drawable.ic_man);
            else
                holder.ivProfilePic.setImageResource(R.drawable.ic_woman);

            holder.itemView.setOnClickListener(view ->
                    startActivity(new Intent(getActivity(), StudentActivity.class)
                            .putExtra(Constants.EXTRA_USER, student)));
        }

        @Override
        public int getItemCount() {
            return mList.size();
        }

        class ChildrenViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.txtName)
            TextView txtName;

            @BindView(R.id.ivProfilePic)
            ImageView ivProfilePic;

            ChildrenViewHolder(View itemView) {
                super(itemView);
                bind(this, itemView);
            }
        }
    }

    class StatisticsAdapter extends RecyclerView.Adapter<StatisticsAdapter.ViewHolder> {
        List<SyllabusList> mStatisticsModelList = new ArrayList<>();

        StatisticsAdapter(List<SyllabusList> list) {
            this.mStatisticsModelList = list;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(getContext())
                    .inflate(R.layout.item_fragment_statistic, parent, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

        }

        @Override
        public int getItemCount() {
            return mStatisticsModelList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public ViewHolder(View itemView) {
                super(itemView);
            }
        }
    }
}
