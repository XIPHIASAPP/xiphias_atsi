package com.xiphiastec.newatsi.parent.dashboard;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.ParentDashboardResponse;

/**
 * Created by xiphias0101 on 11/12/2017.
 */

public interface ParentDashboardContract {
    interface View extends BaseView<ParentDashboardResponse> {

    }

    interface Presenter extends BasePresenter<View> {
        void dashboard();
    }
}
