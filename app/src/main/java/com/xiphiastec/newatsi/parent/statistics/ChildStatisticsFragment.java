package com.xiphiastec.newatsi.parent.statistics;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.transition.TransitionManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.Constants;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsParentBaseFragment;

import com.xiphiastec.newatsi.model.ChildStaticsResponse;
import com.xiphiastec.newatsi.model.ParentStatistic;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by hemanths on 04/08/17.
 */

public class ChildStatisticsFragment extends AbsParentBaseFragment implements ChildStatisticsContract.View {

    @BindView(R.id.recycler_view)
    EmptyViewRecyclerView recyclerView;
    @BindView(R.id.tv_empty)
    TextView tvEmpty;
    Unbinder unbinder;
    @BindView(R.id.root_layout)
    ViewGroup mRootLayout;
    private ChildStatisticsPresenter mPresenter;
    AtsiProgressDialog atsiProgressDialog;

    public ChildStatisticsFragment() {
    }

    public static ChildStatisticsFragment newInstance(int id) {
        Bundle args = new Bundle();
        args.putInt(Constants.ARGS_STUDENT_ID, id);
        ChildStatisticsFragment fragment = new ChildStatisticsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new ChildStatisticsPresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
        if (getArguments() != null) {
            int STUDENT_ID = getArguments().getInt(Constants.ARGS_STUDENT_ID);
            mPresenter.setStudentId(STUDENT_ID);
        }
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && getActionBar() != null) {
            getActionBar().setTitle(R.string.statistics);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_parent_statistics, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActionBar() != null)
            getActionBar().setTitle(R.string.statistics);
        mPresenter.onSubscriber();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onUnSubscriber();
    }

    @Override
    public void onLoading() {
        TransitionManager.beginDelayedTransition(mRootLayout);
    }

    @Override
    public void onShowError(String error) {
        tvEmpty.setVisibility(View.VISIBLE);
        tvEmpty.setText(error);
        recyclerView.setEmptyView(tvEmpty);
    }

    @Override
    public void onResponse(ChildStaticsResponse response) {
        StatisticsAdapter adapter = new StatisticsAdapter(response.parentStatistics);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
        recyclerView.setPadding(8, 8, 8, 8);

    }

    @Override
    public void onComplete() {
        TransitionManager.beginDelayedTransition(mRootLayout);
    }

    class StatisticsAdapter extends RecyclerView.Adapter<StatisticsAdapter.ViewHolder> {

        private final List<ParentStatistic> parentStastics;

        StatisticsAdapter(List<ParentStatistic> parentStastics) {
            this.parentStastics = parentStastics;
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(getContext())
                    .inflate(R.layout.item_fragment_statistic, parent, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            ParentStatistic statistic = parentStastics.get(position);
            holder.text.setText(statistic.obtainMarks);
            holder.title.setText(statistic.subjectName);
        }

        @Override
        public int getItemCount() {
            return parentStastics.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.text)
            TextView text;
            @BindView(R.id.title)
            TextView title;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }
}
