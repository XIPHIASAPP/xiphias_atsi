package com.xiphiastec.newatsi.parent.statistics;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.ChildStaticsResponse;
import com.xiphiastec.newatsi.model.StudentDetailsBody;

/**
 * Created by xiphias0101 on 11/12/2017.
 */

public interface ChildStatisticsContract {
    interface View extends BaseView<ChildStaticsResponse> {

    }

    interface Presenter extends BasePresenter<View> {
        void loadStatics(StudentDetailsBody body);
    }
}
