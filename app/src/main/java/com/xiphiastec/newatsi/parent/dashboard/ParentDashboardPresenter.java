package com.xiphiastec.newatsi.parent.dashboard;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.base.AbsParentPresenter;

public class ParentDashboardPresenter extends AbsParentPresenter implements
        ParentDashboardContract.Presenter {

    @NonNull
    private ParentDashboardContract.View mView;

    public ParentDashboardPresenter(@NonNull ParentDashboardContract.View view) {
        mView = view;
    }

    @Override
    public void dashboard() {
        mDisposable.add(mServices.dashboard()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status && response.students.size() > 0) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    @Override
    public void onSubscriber() {
        dashboard();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }


}
