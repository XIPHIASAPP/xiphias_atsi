package com.xiphiastec.newatsi.parent.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsBaseActivity;
import com.xiphiastec.newatsi.login.ChangePasswordActivity;
import com.xiphiastec.newatsi.login.LoginActivity;
import com.xiphiastec.newatsi.parent.dashboard.ParentDashboardFragment;
import com.xiphiastec.newatsi.student.notification.NotificationActivity;
import com.xiphiastec.newatsi.util.PreferenceUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ParentActivity extends AbsBaseActivity {

    private static final String TAG = "ParentActivity";
    private static final String SELECTED_ITEM = "arg_selected_item";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    private TextView txtViewCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);

        changeFragment(new ParentDashboardFragment(), false);
        setUpNavigation();

    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    public void setNotifCount(int count) {
        txtViewCount.setText(count + "");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_patent, menu);

        final View notificaitons = menu.findItem(R.id.action_notifications).getActionView();
        txtViewCount = notificaitons.findViewById(R.id.txtCount);
        notificaitons.setOnClickListener(v -> startActivity(new Intent(ParentActivity.this, NotificationActivity.class)));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                PreferenceUtil.getInstance(this).clear();
                startActivity(new Intent(this, LoginActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
                break;
            case R.id.action_change_password:
                startActivity(new Intent(this, ChangePasswordActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateToolbarText(CharSequence title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    public void changeFragment(Fragment fragment, boolean hasAddBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up);
        transaction.replace(R.id.contentContainer, fragment, fragment.getTag());
        if (hasAddBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    @SuppressWarnings("ConstantConditions")
    private void setUpNavigation() {
        mToolbar.setTitle(R.string.dashboard);
        setSupportActionBar(mToolbar);

        mToolbar.setNavigationOnClickListener(view -> {
            onBackPressed();
        });

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }catch(Exception e){
            Log.d(TAG,"Action bar error");
        }
//        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
//            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
//                //noinspection ConstantConditions
//                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            } else {
//                //noinspection ConstantConditions
//                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//            }
//        });
    }

}
