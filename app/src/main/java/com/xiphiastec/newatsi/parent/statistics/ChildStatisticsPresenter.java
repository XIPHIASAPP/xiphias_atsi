package com.xiphiastec.newatsi.parent.statistics;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.base.AbsParentPresenter;
import com.xiphiastec.newatsi.model.StudentDetailsBody;

/**
 * Created by hemanths on 04/08/17.
 */

public class ChildStatisticsPresenter extends AbsParentPresenter implements
        ChildStatisticsContract.Presenter {
    @NonNull
    private ChildStatisticsContract.View mView;
    private int mStudentId;

    public ChildStatisticsPresenter(@NonNull ChildStatisticsContract.View view) {
        mView = view;
    }

    @Override
    public void onSubscriber() {
        StudentDetailsBody body = new StudentDetailsBody();
        body.setStudentId(String.valueOf(mStudentId));
        loadStatics(body);
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }

    @Override
    public void loadStatics(StudentDetailsBody body) {
        mDisposable.add(mServices.statistics(body)
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.success && response.parentStatistics.size() > 0) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    public void setStudentId(int studentId) {
        mStudentId = studentId;
    }



}
