package com.xiphiastec.newatsi.student.gradecard;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.transition.TransitionManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import android.widget.ImageView;
import android.widget.LinearLayout;

import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsStudentBaseFragment;

import com.xiphiastec.newatsi.model.ExamTypeBody;
import com.xiphiastec.newatsi.model.GradeCardResponse;
import com.xiphiastec.newatsi.model.Semester;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;
import com.xiphiastec.newatsi.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;




public class StudentGradeCardFragment extends AbsStudentBaseFragment
        implements StudentGradeCardContract.View, AdapterView.OnItemSelectedListener {

    @BindView(R.id.school_logo)
    ImageView mSchoolLogo;
    @BindView(R.id.school_title)
    TextView mSchoolTitle;
    @BindView(R.id.tv_class_name)
    TextView mTvClassName;
    @BindView(R.id.tv_register_number)
    TextView mTvRegisterNumber;
    @BindView(R.id.tv_student_name)
    TextView mTvStudentName;
    @BindView(R.id.tv_roll_number)
    TextView mTvRollNumber;
    @BindView(R.id.tv_percentage)
    TextView mTvPercentage;
    @BindView(R.id.tv_result)
    TextView mTvResult;
    @BindView(R.id.grade_card_content)
    LinearLayout mGradeCardContent;
    Unbinder unbinder;
    @BindView(R.id.types)
    Spinner mTypes;
    @BindView(R.id.root_layout)
    LinearLayout mRootLayout;
    @BindView(R.id.rvGrade)
    RecyclerView rvGrade;

    private StudentGradeCardPresenter mPresenter;
    private ArrayAdapter<String> mTypesAdapter;

    GradeAdapter gradeAdapter;
    private Semester mSemester;

    AtsiProgressDialog atsiProgressDialog;

    public StudentGradeCardFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sub_card, container, false);
        unbinder = ButterKnife.bind(this, view);
        setupSpinner();
        return view;
    }

    @Override
    public void onLoading() {
        atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
        Snackbar.make(mRootLayout, error, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new StudentGradeCardPresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
    }

    @Override
    public void onResponse(GradeCardResponse response) {
        if (response.isStatus() && response.getStudentGrade().size()>0) {
            mGradeCardContent.setVisibility(View.VISIBLE);
            parseData(response);
        } else {
            Snackbar.make(mRootLayout, response.getMessage(), Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.grade_card));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getActionBar().setTitle(SS);
        }
        mTypesAdapter.clear();
        mPresenter.onSubscriber();
    }

    private void setupSpinner() {
        mTypesAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item);
        mTypesAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mTypes.setAdapter(mTypesAdapter);
        mTypes.setOnItemSelectedListener(this);
    }

    public Semester getSemester() {
        return mSemester;
    }

    public void setSemester(Semester semester) {
        mSemester = semester;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position != 0) {
            String examId = getSemester().examType.get(position - 1).examtypeid;
            mPresenter.gradeCardDetails(new ExamTypeBody(examId));
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }



    private void parseData(GradeCardResponse response) {

        mGradeCardContent.setVisibility(View.VISIBLE);

        mTvClassName.setText(Utils.checkEmpty(response.getClassName()));
        mTvRegisterNumber.setText(Utils.checkEmpty(response.getStudentRegistration()));
        mTvRollNumber.setText(Utils.checkEmpty(response.getStudentRollNumber()));
        mTvStudentName.setText(Utils.checkEmpty(response.getStudentName()));
        Glide.with(getContext())
                .load(response.getLogoUrl())
                .error(R.drawable.ic_grade)
                .placeholder(R.drawable.ic_grade)
                .into(mSchoolLogo);
        mSchoolTitle.setText(Utils.checkEmpty(response.getInstitutionName()));
        setUpRecyclerView(response);
        mTvPercentage.setText(Utils.checkEmpty(response.getPercentage()));
        mTvResult.setText(Utils.checkEmpty(response.getResult()));
    }


    private void setUpRecyclerView(GradeCardResponse response){
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvGrade.setLayoutManager(mLayoutManager);
        rvGrade.setItemAnimator(new DefaultItemAnimator());

        gradeAdapter=new GradeAdapter(getActivity(),response.getStudentGrade());
        rvGrade.setAdapter(gradeAdapter);
        gradeAdapter.notifyDataSetChanged();
        //gradeAdapter.swapData(response.getStudentGrade());
    }



    @Override
    public void onComplete() {
       atsiProgressDialog.dismissDialog();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        mPresenter.onUnSubscriber();
    }

    @Override
    public void onExamTypeResponse(Semester semester) {
        setSemester(semester);
        TransitionManager.beginDelayedTransition(mRootLayout);
        mTypesAdapter.add("--Select Exam--");
        for (Semester.ExamType examType : semester.examType) {
            mTypesAdapter.add(examType.examtype);
        }
    }
}
