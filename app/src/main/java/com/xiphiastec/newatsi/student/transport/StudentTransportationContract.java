package com.xiphiastec.newatsi.student.transport;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;

/**
 * Created by Admin on 12-12-2017.
 */

public interface StudentTransportationContract {

    interface View extends BaseView<StudentTransportPresenter.StudentTransportResponse> {

    }

    interface Presenter extends BasePresenter<View> {
        void transportDetails();
    }
}
