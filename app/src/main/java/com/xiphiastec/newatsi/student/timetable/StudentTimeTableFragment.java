package com.xiphiastec.newatsi.student.timetable;


import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsStudentBaseFragment;
import com.xiphiastec.newatsi.model.StudentTimeTableResponse;
import com.xiphiastec.newatsi.model.TimeTableStudent;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class StudentTimeTableFragment extends AbsStudentBaseFragment implements StudentTimeTableContract.View {
    private static final String TAG = "StudentTimeTable";

    DisplayMetrics metrics;
    int width;

    Context mContext;
    Unbinder unbinder;


    @BindView(R.id.lvExp)
    ExpandableListView timeTable;

    @BindView(R.id.txtError)
    TextView txtError;


    @BindView(R.id.root_layout)
    ConstraintLayout mRootLayout;


    ExpandableListAdapter listAdapter;

    List<String> dayHeaderGroup;

    HashMap<String, List<TimeTableStudent>> timeTableChild;

    private StudentTimeTablePresenter mPresenter;

    AtsiProgressDialog atsiProgressDialog;


    public StudentTimeTableFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.sub_time_table_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        width = metrics.widthPixels;


        dayHeaderGroup=new ArrayList<>();
        timeTableChild=new HashMap<String, List<TimeTableStudent>>();

        timeTable.setIndicatorBounds(width - GetDipsFromPixel(50), width - GetDipsFromPixel(30));

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new StudentTimeTablePresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.time_table));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(SS);
        }
        listAdapter=null;
        dayHeaderGroup.clear();
        timeTableChild.clear();
        mPresenter.onSubscriber();
    }

    @Override
    public void onLoading() {
        atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
        txtError.setVisibility(View.VISIBLE);
        txtError.setText(error);
       Snackbar.make(mRootLayout, error, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onResponse(StudentTimeTableResponse response) {
        if (response.success) {
            timeTable.setVisibility(View.VISIBLE);
            // Adding child data
            dayHeaderGroup.add("Monday");
            dayHeaderGroup.add("Tuesday");
            dayHeaderGroup.add("Wednesday");
            dayHeaderGroup.add("Thursday");
            dayHeaderGroup.add("Friday");
            dayHeaderGroup.add("Saturday");
            dayHeaderGroup.add("Sunday");

            List<TimeTableStudent> monday = response.timeTableStudentMonday;
            timeTableChild.put(dayHeaderGroup.get(0), monday);
            List<TimeTableStudent> tuesday = response.timeTableStudentTuesday;
            timeTableChild.put(dayHeaderGroup.get(1), tuesday);
            List<TimeTableStudent> wednesday = response.timeTableStudentWednesday;
            timeTableChild.put(dayHeaderGroup.get(2), wednesday);
            List<TimeTableStudent> thursday = response.timeTableStudentThursday;
            timeTableChild.put(dayHeaderGroup.get(3), thursday);
            List<TimeTableStudent> friday = response.timeTableStudentFriday;
            timeTableChild.put(dayHeaderGroup.get(4), friday);
            List<TimeTableStudent> saturday = response.timeTableStudentSaturday;
            timeTableChild.put(dayHeaderGroup.get(5), saturday);
            List<TimeTableStudent> sunday = response.timeTableStudentSunday;
            timeTableChild.put(dayHeaderGroup.get(6), sunday);

            listAdapter = new ExpandableListAdapter(getActivity(), dayHeaderGroup, timeTableChild);

            // setting list adapter
            timeTable.setAdapter(listAdapter);
        }else{
            txtError.setVisibility(View.VISIBLE);
            txtError.setText("No Timetable found");
        }
    }




    @Override
    public void onComplete() {
       atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onUnSubscriber();
    }


    public int GetDipsFromPixel(float pixels)
    {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }
}
