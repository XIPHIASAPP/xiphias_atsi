package com.xiphiastec.newatsi.student.hostel.inoutstatus;

import com.xiphiastec.newatsi.base.AbsStudentPresenter;
import com.xiphiastec.newatsi.model.StudentDetailsBody;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class InOutStatusPresenter extends AbsStudentPresenter implements InOutStatusContract.Presenter {

    private InOutStatusContract.View mView;

    InOutStatusPresenter(InOutStatusContract.View view) {
        mView = view;
    }

    @Override
    public void onSubscriber() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        StudentDetailsBody body = new StudentDetailsBody();
        body.setDate(format.format(new Date()));
        inOutStatus(body);
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }

    @Override
    public void inOutStatus(StudentDetailsBody date) {
        mDisposable.add(mServices.inOutStatus(date)
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> {
                        },
                        () -> mView.onComplete()));
    }
}
