package com.xiphiastec.newatsi.student.attendance.dailyattendance;

import com.xiphiastec.newatsi.base.AbsStudentPresenter;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by Admin on 31-03-2017.
 */

public class DailyAttendancePresenter extends AbsStudentPresenter implements DailyAttendanceContract.Presenter {

    private DailyAttendanceContract.View mView;

    DailyAttendancePresenter(DailyAttendanceContract.View view) {
        mView = view;
    }

    @Override
    public void onSubscriber() {

    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }

    @Override
    public void getSubjectAttendence(String date) {
        Map<String, Object> map = new HashMap<>();
        map.put("date", date);
        mDisposable.add(mServices.getSubjectAttendanceWith(map)
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.success && response.mList.size() > 0) {
                                mView.renderSubjectResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> {
                        },
                        () -> mView.onComplete()));
    }

    @Override
    public void getDailyAttendence(String date) {
        Map<String, Object> map = new HashMap<>();
        map.put("date", date);
        mDisposable.add(mServices.getDailyAttendanceWithDate(map)
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.success && response.mList.size() > 0) {
                                mView.renderDailyResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> {
                        },
                        () -> mView.onComplete()));
    }


}
