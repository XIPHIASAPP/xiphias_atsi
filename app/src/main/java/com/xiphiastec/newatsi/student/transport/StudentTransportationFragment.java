package com.xiphiastec.newatsi.student.transport;


import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsStudentBaseFragment;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class StudentTransportationFragment extends AbsStudentBaseFragment implements
        StudentTransportationContract.View {

    Context mContext;
    @BindView(R.id.recycler_view)
    EmptyViewRecyclerView mRecyclerView;
    Unbinder unbinder;
    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    @BindView(R.id.rootLayout)
    LinearLayout rootLayout;

    private StudentTransportPresenter mPresenter;

    AtsiProgressDialog atsiProgressDialog;

    public StudentTransportationFragment() {
        // Required empty public constructor
    }


    @Override
    public void onResume() {
        super.onResume();
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.transportation));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(SS);
        }
        mPresenter.onSubscriber();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_student_transportation, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new StudentTransportPresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
    }

    @Override
    public void onLoading() {
       atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        Snackbar.make(rootLayout, error, Snackbar.LENGTH_SHORT).show();
        tvEmpty.setVisibility(View.VISIBLE);
        tvEmpty.setText(error);
        atsiProgressDialog.dismissDialog();

    }

    @Override
    public void onResponse(StudentTransportPresenter.StudentTransportResponse response) {
        StudentTransportAdapter adapter = new StudentTransportAdapter(getActivity());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setAdapter(adapter);
        //mTvRegistrationNumber.setText(response.studentRegisterNumber);
        //mTvName.setText(response.studentName);

        if (response.status) {
            if (response.transport.size() > 0) {
                adapter.swapData(response.transport);
            } else {
                showEmpty(response);
            }
        } else {
            showEmpty(response);
        }
    }

    private void showEmpty(StudentTransportPresenter.StudentTransportResponse response) {
        tvEmpty.setVisibility(View.VISIBLE);
        tvEmpty.setText(response.message);
    }

    @Override
    public void onComplete() {
        atsiProgressDialog.dismissDialog();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onUnSubscriber();
    }

}
