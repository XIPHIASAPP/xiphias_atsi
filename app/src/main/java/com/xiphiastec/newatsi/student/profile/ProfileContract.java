package com.xiphiastec.newatsi.student.profile;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.StudentProfileResponse;

/**
 * Created by Admin on 12-12-2017.
 */

public interface ProfileContract {

    interface View extends BaseView<StudentProfileResponse> {

    }

    interface Presenter extends BasePresenter<View> {
        void profile();
    }
}
