package com.xiphiastec.newatsi.student.attendance.dailyattendance;


import android.app.DatePickerDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsStudentBaseFragment;
import com.xiphiastec.newatsi.model.DailyAttendanceResponse;
import com.xiphiastec.newatsi.model.StatusResponse;

import com.xiphiastec.newatsi.model.SubjectAttendanceResponse;

import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class DailyAttendanceFragment extends AbsStudentBaseFragment
        implements DailyAttendanceContract.View, DatePickerDialog.OnDateSetListener {

    private static final String TAG = "DailyAttendance";
    @BindView(R.id.et_search_date)
    AppCompatTextView searchDate;

    @BindView(R.id.dailyRecycler)
    RecyclerView dailyRecycler;

    @BindView(R.id.subjectRecycler)
    RecyclerView subjectRecycler;

    @BindView(R.id.txtError)
    TextView txtError;

//    @BindView(R.id.tv_empty)
//    AppCompatTextView tvEmpty;
    @BindView(R.id.root_layout)
    LinearLayout mRootLayout;
    private DatePickerDialog mDatePickerDialog;
    private DailyAttendancePresenter presenter;
    private Unbinder unbinder;
    private Calendar mCalendar;
    private SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    private DailyAttendanceAdapter dailyAdapter;
    private SubjectAttendanceAdapter subjectAdapter;

    boolean isDaily=true,isSubject;


    AtsiProgressDialog atsiProgressDialog;

    @BindView(R.id.vDaily)
    View vDaily;
    @BindView(R.id.vSubject)
    View vSubject;

    @BindView(R.id.cvAttendanceDetails)
    View cvAttendanceDetails;


    public DailyAttendanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_daily_attendance, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.attendance));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getActionBar().setTitle(SS);
        }

        String formattedDate = mSimpleDateFormat.format(mCalendar.getTime());
        searchDate.setText(formattedDate);

        presenter.getDailyAttendence(formattedDate);
    }

    private void setupViews() {
        mCalendar = Calendar.getInstance();

        mDatePickerDialog= new DatePickerDialog(getActivity(), this,
                mCalendar.get(Calendar.YEAR),
                mCalendar.get(Calendar.MONTH),
                mCalendar.get(Calendar.DAY_OF_MONTH));

        mDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime());

        dailyAdapter = new DailyAttendanceAdapter(getActivity());
        subjectAdapter= new SubjectAttendanceAdapter(getActivity());
    }

    @OnClick({R.id.ib_pick_date, R.id.ib_search_date})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ib_pick_date:
                mDatePickerDialog.show();
                break;
            case R.id.ib_search_date:
                dailyAdapter.clear();

                if(isDaily)
                presenter.getDailyAttendence(searchDate.getText().toString());
                else
                presenter.getSubjectAttendence(searchDate.getText().toString());

                break;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new DailyAttendancePresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
    }

    @Override
    public void onDestroyView() {
        presenter.onUnSubscriber();
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onLoading() {
        dailyAdapter.clear();
        subjectAdapter.clear();
        atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
        txtError.setVisibility(View.VISIBLE);
        txtError.setText(error);
        Snackbar.make(mRootLayout, error, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onComplete() {
        atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onResponse(StatusResponse response) {

    }


    @OnClick({R.id.llDaily})
    public void viewDailyClicked(){

        isDaily=true;
        String formattedDate = mSimpleDateFormat.format(mCalendar.getTime());
        searchDate.setText(formattedDate);

        dailyRecycler.setVisibility(View.VISIBLE);
        subjectRecycler.setVisibility(View.GONE);
        presenter.getDailyAttendence(mSimpleDateFormat.format(mCalendar.getTime()));
        dailyRecycler.setAdapter(dailyAdapter);

        vDaily.setVisibility(View.VISIBLE);
        vSubject.setVisibility(View.GONE);

    }


    @OnClick(R.id.llSubject)
    public void viewSubjectClicked(){

        isDaily=false;
        String formattedDate = mSimpleDateFormat.format(mCalendar.getTime());
        searchDate.setText(formattedDate);

        dailyRecycler.setVisibility(View.GONE);
        subjectRecycler.setVisibility(View.VISIBLE);

        presenter.getSubjectAttendence(mSimpleDateFormat.format(mCalendar.getTime()));

        subjectRecycler.setAdapter(subjectAdapter);

        vDaily.setVisibility(View.GONE);
        vSubject.setVisibility(View.VISIBLE);

    }


    @Override
    public void renderDailyResponse(DailyAttendanceResponse response) {

        if(response.mList.size()>0) {
            cvAttendanceDetails.setVisibility(View.VISIBLE);
            txtError.setVisibility(View.GONE);
            dailyAdapter.swapData(response.mList);
            dailyRecycler.setItemAnimator(new DefaultItemAnimator());
            dailyRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
            dailyRecycler.setAdapter(dailyAdapter);
        }else{
            dailyAdapter.clear();
            if(isDaily){
                dailyRecycler.setVisibility(View.VISIBLE);
            }else{
                subjectRecycler.setVisibility(View.VISIBLE);
            }
            txtError.setVisibility(View.VISIBLE);
            txtError.setText("No Record Found");
        }
    }

    @Override
    public void renderSubjectResponse(SubjectAttendanceResponse response) {
        if(response.mList.size()>0) {
            cvAttendanceDetails.setVisibility(View.VISIBLE);
            txtError.setVisibility(View.GONE);
            subjectAdapter.swapData(response.mList);
            subjectRecycler.setItemAnimator(new DefaultItemAnimator());
            subjectRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
            subjectRecycler.setAdapter(subjectAdapter);
        }else{
            subjectAdapter.clear();
            txtError.setVisibility(View.VISIBLE);
            txtError.setText("No Record Found");
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Log.d(TAG, "onDateSet: " + year + " " + month + " " + dayOfMonth);
        String strMonth = String.valueOf(++month);
        String day = String.valueOf(dayOfMonth);
        if (month < 10) {
            strMonth = "0" + month;
        }
        if (dayOfMonth < 10) {

            day = "0" + dayOfMonth;
        }
        searchDate.setText(year + "-" + strMonth + "-" + day);
    }
}
