package com.xiphiastec.newatsi.student.hostel.discipline;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.StudentDisciplineResponse;

/**
 * Created by Admin on 12-12-2017.
 */

public interface DisciplineContract {

    interface View extends BaseView<StudentDisciplineResponse> {

    }

    interface Presenter extends BasePresenter<View> {
        void discipline();
    }
}
