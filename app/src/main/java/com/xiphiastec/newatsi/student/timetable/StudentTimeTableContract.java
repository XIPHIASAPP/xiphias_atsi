package com.xiphiastec.newatsi.student.timetable;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.StudentTimeTableResponse;

/**
 * Created by Admin on 12-12-2017.
 */

public interface StudentTimeTableContract {

    interface View extends BaseView<StudentTimeTableResponse> {

    }

    interface Presenter extends BasePresenter<View> {
        void timeTable();
    }
}
