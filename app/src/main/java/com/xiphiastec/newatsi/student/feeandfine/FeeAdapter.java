package com.xiphiastec.newatsi.student.feeandfine;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.FeeDetailsResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by xiphias0101 on 20/02/2018.
 */

public class FeeAdapter extends RecyclerView.Adapter<FeeAdapter.FeeViewHolder>{

    private final List<FeeDetailsResponse.FeeDetail> feeDetails;

    private Context context;

    public FeeAdapter(Context context,List<FeeDetailsResponse.FeeDetail> feeDetails){
        this.context=context;
        this.feeDetails=feeDetails;
    }

    @Override
    public FeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View  view=LayoutInflater.from(context).inflate(R.layout.item_fee_details, parent, false);
        return new FeeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FeeViewHolder holder, int position) {
        FeeDetailsResponse.FeeDetail feeDetail = feeDetails.get(position);
        holder.date.setText(feeDetail.paidDate);
        holder.feeName.setText(feeDetail.feeName);
        holder.feeAmount.setText(feeDetail.amount);
        holder.paymentType.setText(feeDetail.paymentType);
    }



    @Override
    public int getItemCount() {
        return feeDetails.size();
    }

    public class FeeViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.fee_name)
        TextView feeName;
        @BindView(R.id.payment_type)
        TextView paymentType;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.fee_amount)
        TextView feeAmount;

        public FeeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
