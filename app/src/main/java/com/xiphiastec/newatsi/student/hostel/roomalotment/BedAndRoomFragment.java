package com.xiphiastec.newatsi.student.hostel.roomalotment;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsStudentBaseFragment;
import com.xiphiastec.newatsi.model.RoomAllotmentResponse;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;
import com.xiphiastec.newatsi.util.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BedAndRoomFragment extends AbsStudentBaseFragment
        implements BedAndRoomContract.View {

    Context mContext;

    Unbinder unbinder;

    @BindViews({R.id.name,
            R.id.address,
            R.id.building_floor,
            R.id.room,
            R.id.date,
            R.id.in_charge_name,
            R.id.in_charge_number})
    List<TextView> mViewList;

    @BindView(R.id.root_layout)
    LinearLayout rootLayout;

    @BindView(R.id.cvRoomDetails)
    CardView cvRoomDetails;

    @BindView(R.id.txtError)
    TextView txtError;



    private RoomAllotmentPresenter mPresenter;
    AtsiProgressDialog atsiProgressDialog;

    public BedAndRoomFragment() {
        // Required empty public constructorl
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new RoomAllotmentPresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bedand_room, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (getActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.room_details));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getActionBar().setTitle(SS);
        }
        mPresenter.onSubscriber();
    }

    @Override
    public void onLoading() {
        txtError.setVisibility(View.GONE);
        atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
        txtError.setVisibility(View.VISIBLE);
        txtError.setText(error);
        Snackbar.make(rootLayout, error, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(RoomAllotmentResponse response) {
        cvRoomDetails.setVisibility(View.VISIBLE);
        mViewList.get(0).setText(Utils.checkEmpty(response.getBuildingName()));
        mViewList.get(1).setText(Utils.checkEmpty(response.getAddress()));
        mViewList.get(2).setText(Utils.checkEmpty(response.getFloor()));
        mViewList.get(3).setText(Utils.checkEmpty(response.getRoom()));
        mViewList.get(4).setText(Utils.checkEmpty(response.getAllotmentDate()));
        mViewList.get(5).setText(Utils.checkEmpty(response.getInchargeName()));
        mViewList.get(6).setText(Utils.checkEmpty(response.getInchargePhoneNumber()));

    }

    @Override
    public void onComplete() {
        atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onUnSubscriber();
    }
}
