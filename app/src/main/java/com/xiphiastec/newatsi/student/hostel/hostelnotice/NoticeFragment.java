package com.xiphiastec.newatsi.student.hostel.hostelnotice;


import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsStudentBaseFragment;
import com.xiphiastec.newatsi.model.StudentDetailsBody;
import com.xiphiastec.newatsi.model.StudentHostelNoticeResponse;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class NoticeFragment extends AbsStudentBaseFragment implements
        NoticeContract.View, DatePickerDialog.OnDateSetListener {

    Context mContext;
    @BindView(R.id.et_search_date)
    AppCompatTextView mEtSearchDate;
    @BindView(R.id.recycler_view)
    EmptyViewRecyclerView mRecyclerView;
    @BindView(R.id.tv_empty)
    TextView mTvEmpty;
    Unbinder unbinder;

    @BindView(R.id.details)
    View mDetails;
    private Calendar mCalendar;
    private NoticePresenter mPresenter;
    private StudentNoticeAdapter mAdapter;
    private DatePickerDialog mDatePickerDialog;
    private SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    AtsiProgressDialog atsiProgressDialog;

    public NoticeFragment() {
        // Required empty public constructor
    }

    private void setupViews() {
        mCalendar = Calendar.getInstance();

        mDatePickerDialog= new DatePickerDialog(getActivity(), this,
                mCalendar.get(Calendar.YEAR),
                mCalendar.get(Calendar.MONTH),
                mCalendar.get(Calendar.DAY_OF_MONTH));

        mDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime());

        mAdapter = new StudentNoticeAdapter(getActivity());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new NoticePresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notice, container, false);
        unbinder = ButterKnife.bind(this, view);
        setupViews();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.notice));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getActionBar().setTitle(SS);
        }
        String formattedDate = mSimpleDateFormat.format(mCalendar.getTime());
        mEtSearchDate.setText(formattedDate);
        mAdapter.clear();
        mPresenter.onSubscriber();
    }

    @Override
    public void onLoading() {
       atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
        Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
        mTvEmpty.setText(error);
        mTvEmpty.setVisibility(View.VISIBLE);
        mRecyclerView.setEmptyView(mTvEmpty);
    }

    @Override
    public void onComplete() {
       atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onResponse(StudentHostelNoticeResponse response) {
        mAdapter.swapData(response.hostelNotice);
        mDetails.setVisibility(View.VISIBLE);
    }


    @OnClick({R.id.ib_date_pick, R.id.ib_search_date})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ib_date_pick:
                mDatePickerDialog.show();
                break;
            case R.id.ib_search_date:
                mAdapter.clear();
                String date = mEtSearchDate.getText().toString();
                if (!date.isEmpty()) {
                    StudentDetailsBody body = new StudentDetailsBody();
                    body.setDate(date);
                    mPresenter.noticeWithDate(body);
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onUnSubscriber();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String strMonth = String.valueOf(++month);
        String day = String.valueOf(dayOfMonth);
        if (month < 10) {
            strMonth = "0" + month;
        }
        if (dayOfMonth < 10) {

            day = "0" + dayOfMonth;
        }
        mEtSearchDate.setText(year + "-" + strMonth + "-" + day);
    }
}
