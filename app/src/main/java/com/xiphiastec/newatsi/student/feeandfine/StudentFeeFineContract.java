package com.xiphiastec.newatsi.student.feeandfine;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.FeeDetailsResponse;

/**
 * Created by Admin on 12-12-2017.
 */

public interface StudentFeeFineContract {

    interface View extends BaseView<FeeDetailsResponse> {

    }

    interface Presenter extends BasePresenter<View> {
        void feeFine();
    }
}
