package com.xiphiastec.newatsi.student.hallticket;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsStudentBaseFragment;

import com.xiphiastec.newatsi.model.HallTicketResponse;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class HallTicketFragment extends AbsStudentBaseFragment implements
        HallTicketContract.View {

    @BindView(R.id.ivSchoolLogo)
    ImageView schoolLogo;
    @BindView(R.id.tvSchoolTitle)
    TextView schoolTitle;
//    @BindView(R.id.school_class_title)
//    TextView schoolClassTitle;
//    @BindView(R.id.progressBar)
//    View mProgressBar;
//    @BindView(R.id.hall_ticket)
//    View mHallTicket;
    Unbinder mUnbinder;
    @BindView(R.id.rootLayout)
    FrameLayout mRootLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.txtError)
    TextView txtError;

    @BindView(R.id.cvHStudent)
    CardView cvHStudent;
    @BindView(R.id.cvHTimetable)
    CardView cvHTimetable;

    @BindViews({R.id.tv_register_number,//0
            R.id.tv_student_name,//1
            R.id.tv_registered_paper,//2
            R.id.tv_academic_batch,//3
            R.id.tv_date_of_birth,//4
            R.id.tv_class_type})//5
            List<TextView> mViewList;

    private HallTicketPresenter mPresenter;
    AtsiProgressDialog atsiProgressDialog;

    public HallTicketFragment() {
        // Required empty public constructor
    }

    public static HallTicketFragment newInstance() {
        Bundle args = new Bundle();
        HallTicketFragment fragment = new HallTicketFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hall_ticket, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new HallTicketPresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.hall_ticket));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getActionBar().setTitle(SS);
        }
        mPresenter.onSubscriber();
    }

    @Override
    public void onLoading() {
        atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        txtError.setVisibility(View.VISIBLE);
        txtError.setText(error);
        atsiProgressDialog.dismissDialog();
        Snackbar.make(mRootLayout, error, BaseTransientBottomBar.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(HallTicketResponse response) {

        if (response.examDetails.size() > 0) {

            Glide.with(getContext())
                .load(response.logoUrl)
                .asBitmap()
                .into(schoolLogo);

        schoolTitle.setText(response.instituteName.isEmpty()?"Hall Ticket":response.instituteName);

        mViewList.get(0).setText(response.registrationNumber);
        mViewList.get(2).setText(response.paperRegistred);
        mViewList.get(4).setText(response.dateOfBirth);
        mViewList.get(3).setText(response.acadamicBatch);
        mViewList.get(5).setText(response.classType);
        mViewList.get(1).setText(response.nameOfTheStudent);

        cvHTimetable.setVisibility(View.VISIBLE);
        cvHStudent.setVisibility(View.VISIBLE);

        HallTicketTimeTableAdapter adapter = new HallTicketTimeTableAdapter(getActivity(),response.examDetails);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        } else {
            onShowError("No record found");
        }

    }

    @Override
    public void onComplete() {
        atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onUnSubscriber();
    }

}
