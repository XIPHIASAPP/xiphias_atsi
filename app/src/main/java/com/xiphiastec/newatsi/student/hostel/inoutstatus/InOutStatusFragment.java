package com.xiphiastec.newatsi.student.hostel.inoutstatus;

import android.app.DatePickerDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsStudentBaseFragment;
import com.xiphiastec.newatsi.model.InOutStatusResponse;
import com.xiphiastec.newatsi.model.StudentDetailsBody;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class InOutStatusFragment extends AbsStudentBaseFragment implements
        InOutStatusContract.View, DatePickerDialog.OnDateSetListener {

    @BindView(R.id.recycler_view)
    EmptyViewRecyclerView mRecyclerView;
    @BindView(R.id.rootLayout)
    LinearLayout rootLayout;
    Unbinder mUnbinder;
    @BindView(R.id.et_search_date)
    AppCompatTextView mEtSearchDate;
    @BindView(R.id.tv_empty)
    TextView tvEmpty;
    @BindView(R.id.details)
    LinearLayout mDetails;
    @BindView(R.id.ib_pick_date)
    LinearLayout ib_date_pick;

    private InOutStatusPresenter mPresenter;
    private SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    private Calendar mCalendar;
    private DatePickerDialog mDatePickerDialog;
    private InOutStatusAdapter adapter;

    AtsiProgressDialog atsiProgressDialog;

    public InOutStatusFragment() {
        // Required empty public constructor
    }


    private void setupViews() {
        mCalendar = Calendar.getInstance();

        mDatePickerDialog= new DatePickerDialog(getActivity(), this,
                                    mCalendar.get(Calendar.YEAR),
                                    mCalendar.get(Calendar.MONTH),
                                    mCalendar.get(Calendar.DAY_OF_MONTH));
        mDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime());


        adapter = new InOutStatusAdapter(getActivity());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_hostel_in_out_status, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        setupViews();
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new InOutStatusPresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.in_out_status));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getActionBar().setTitle(SS);
        }
        String formattedDate = mSimpleDateFormat.format(mCalendar.getTime());
        mEtSearchDate.setText(formattedDate);
        adapter.clear();
        mPresenter.onSubscriber();
    }

    @Override
    public void onLoading() {
        tvEmpty.setVisibility(View.GONE);
        atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
        Snackbar.make(rootLayout, error, Snackbar.LENGTH_SHORT).show();
        tvEmpty.setVisibility(View.VISIBLE);
        tvEmpty.setText(error);
    }


    @Override
    public void onResponse(InOutStatusResponse response) {

        if(response.mList.size()>0) {
            mDetails.setVisibility(View.VISIBLE);
            adapter.swapData(response.mList);
        }else{
            tvEmpty.setVisibility(View.VISIBLE);
            tvEmpty.setText("No Record Found");
        }
    }

    @Override
    public void onComplete() {
       atsiProgressDialog.dismissDialog();
    }


    @OnClick({R.id.ib_pick_date,R.id.et_search_date,R.id.ib_search_date})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ib_date_pick:
            case R.id.et_search_date:
                mDatePickerDialog.show();
                break;
            case R.id.ib_search_date:
                adapter.clear();
                String date = mEtSearchDate.getText().toString();
                if (!date.isEmpty()) {
                    StudentDetailsBody body = new StudentDetailsBody();
                    body.setDate(date);
                    mPresenter.inOutStatus(body);
                }
                break;
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onUnSubscriber();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String strmonth = String.valueOf(++month);
        String day = String.valueOf(dayOfMonth);
        if (month < 10) {
            strmonth = "0" + strmonth;
        }
        if (dayOfMonth < 10) {

            day = "0" + dayOfMonth;
        }
        mEtSearchDate.setText(year + "-" + strmonth + "-" + day);
    }
}
