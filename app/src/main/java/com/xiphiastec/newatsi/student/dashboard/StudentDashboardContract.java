package com.xiphiastec.newatsi.student.dashboard;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.DashboardResponse;
import com.xiphiastec.newatsi.model.StudentProfileResponse;

/**
 * Created by Admin on 12-12-2017.
 */

public interface StudentDashboardContract {

    interface View extends BaseView<DashboardResponse> {
        void onProfileResponse(StudentProfileResponse response);
    }

    interface Presenter extends BasePresenter<View> {
        void getDashboardInfo();

        void profile();
    }
}
