package com.xiphiastec.newatsi.student.gradecard;

import com.xiphiastec.newatsi.base.AbsStudentPresenter;
import com.xiphiastec.newatsi.model.ExamTypeBody;
import com.xiphiastec.newatsi.util.Utils;

public class StudentGradeCardPresenter extends AbsStudentPresenter implements StudentGradeCardContract.Presenter {


    private StudentGradeCardContract.View mView;

    StudentGradeCardPresenter(StudentGradeCardContract.View view) {
        mView = view;
    }

    @Override
    public void onSubscriber() {
        examTypes();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }

    @Override
    public void gradeCardDetails(ExamTypeBody examTypeBody) {
        mDisposable.add(mServices.getGradeCardDetails(examTypeBody)
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> {
                        },
                        () -> mView.onComplete()));
    }

    @Override
    public void examTypes() {
        mDisposable.add(mServices.examTypes()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status) {
                                mView.onExamTypeResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(Utils.showError(throwable)),
                        () -> mView.onComplete()));
    }
}
