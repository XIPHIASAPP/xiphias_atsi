package com.xiphiastec.newatsi.student.hallticket;

import com.xiphiastec.newatsi.base.AbsStudentPresenter;
import com.xiphiastec.newatsi.util.Utils;

public class HallTicketPresenter extends AbsStudentPresenter implements HallTicketContract.Presenter {
    private HallTicketContract.View mView;

    HallTicketPresenter(HallTicketContract.View view) {
        mView = view;
    }

    @Override
    public void onSubscriber() {
        hallTicket();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }

    public void hallTicket() {
        mDisposable.add(mServices.hallTicket()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response ->  mView.onResponse(response),
                                        throwable -> mView.onShowError(Utils.showError(throwable)),
                                        () -> mView.onComplete()));
    }
}
