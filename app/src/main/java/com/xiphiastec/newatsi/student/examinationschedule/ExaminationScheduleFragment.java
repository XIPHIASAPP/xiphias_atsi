package com.xiphiastec.newatsi.student.examinationschedule;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.transition.TransitionManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;


import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsStudentBaseFragment;
import com.xiphiastec.newatsi.model.ExaminationScheduleResponse;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class ExaminationScheduleFragment extends AbsStudentBaseFragment
        implements ExaminationScheduleContract.View {

    private static final int REQ_PERMISSION_CODE = 9020;
    private static final int ANNUAL_VIEW_TYPE=1;
    private static final int SEMISTER_VIEW_TYPE=2;

    Context mContext;
    @BindView(R.id.recycler_view)
    EmptyViewRecyclerView mRecyclerView;

    @BindView(R.id.tv_empty)
    TextView tv_empty;

    @BindView(R.id.root_layout)
    FrameLayout mRootLayout;
    AtsiProgressDialog atsiProgressDialog;

    private ExaminationSchedulePresenter mPresenter;
    private Unbinder unbinder;
    private ExamScheduleAdapter mAdapter;

    public ExaminationScheduleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_examination_schedule, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    private boolean showExplainDialog() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED) {
                new AlertDialog.Builder(getActivity())
                        .setTitle("Permission")
                        .setMessage("Inorder to save image to gallery app need permission")
                        .setPositiveButton("Grant", (dialogInterface, i) -> showPermissionDialog())
                        .create().show();
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    private void showPermissionDialog() {
        ActivityCompat.requestPermissions((getActivity()), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQ_PERMISSION_CODE);
    }

    @Override
    public void onLoading() {
        TransitionManager.beginDelayedTransition(mRootLayout);
        atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        tv_empty.setVisibility(View.VISIBLE);
        tv_empty.setText(error);
        TransitionManager.beginDelayedTransition(mRootLayout);
        atsiProgressDialog.dismissDialog();
        Snackbar.make(getView(), error, Snackbar.LENGTH_SHORT).show();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new ExaminationSchedulePresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new ExamScheduleAdapter(getActivity());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        super.onResume();
        if (getActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.exam_schedule));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getActionBar().setTitle(SS);
        }
        mPresenter.onSubscriber();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onUnSubscriber();
    }

    private void loadFromSaved(ExaminationScheduleResponse response) {
        TransitionManager.beginDelayedTransition(mRootLayout);
        if (response.studentExamDetails.size() > 0)
            mAdapter.swapData(response.studentExamDetails);
    }

    @Override
    public void onResponse(ExaminationScheduleResponse response) {
        loadFromSaved(response);
       // mDetails.setVisibility(View.VISIBLE);
    }

    @Override
    public void onComplete() {
        TransitionManager.beginDelayedTransition(mRootLayout);
        atsiProgressDialog.dismissDialog();
    }

}
