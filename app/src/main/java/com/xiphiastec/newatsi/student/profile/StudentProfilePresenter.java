package com.xiphiastec.newatsi.student.profile;

import com.xiphiastec.newatsi.base.AbsStudentPresenter;

public class StudentProfilePresenter extends AbsStudentPresenter implements
        ProfileContract.Presenter {

    private ProfileContract.View mView;

    StudentProfilePresenter(ProfileContract.View view) {
        mView = view;
    }

    public void profile() {
        mDisposable.add(mServices.profile()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> {
                        },
                        () -> mView.onComplete()));
    }

    @Override
    public void onSubscriber() {
        profile();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }


}
