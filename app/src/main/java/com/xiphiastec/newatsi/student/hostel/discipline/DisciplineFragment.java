package com.xiphiastec.newatsi.student.hostel.discipline;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsStudentBaseFragment;
import com.xiphiastec.newatsi.model.StudentDisciplineResponse;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DisciplineFragment extends AbsStudentBaseFragment implements
        DisciplineContract.View {

    @BindView(R.id.recycler_view)
    EmptyViewRecyclerView mRecyclerView;
    @BindView(R.id.tv_empty)
    TextView mTvEmpty;
    Unbinder unbinder;
    @BindView(R.id.details)
    View mDetails;
    private DisciplinePresenter mPresenter;
    private DisciplineAdapter mAdapter;

    AtsiProgressDialog atsiProgressDialog;

    public DisciplineFragment() {
        // Required empty public constructor
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupRecyclerView();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new DisciplinePresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_discipline_violation, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    private void setupRecyclerView() {
        mAdapter = new DisciplineAdapter(getActivity());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onLoading() {
        mTvEmpty.setVisibility(View.GONE);
        atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
        Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
        mTvEmpty.setText(error);
        mTvEmpty.setVisibility(View.VISIBLE);
        mRecyclerView.setEmptyView(mTvEmpty);
    }

    @Override
    public void onComplete() {
       atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onResponse(StudentDisciplineResponse response) {
        mAdapter.swapData(response.mList);
        mDetails.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.discipline));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getActionBar().setTitle(SS);
        }
        mPresenter.discipline();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onUnSubscriber();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
