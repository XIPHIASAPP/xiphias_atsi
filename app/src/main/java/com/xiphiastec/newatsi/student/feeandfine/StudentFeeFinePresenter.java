package com.xiphiastec.newatsi.student.feeandfine;


import com.xiphiastec.newatsi.base.AbsStudentPresenter;
import com.xiphiastec.newatsi.util.Utils;

public class StudentFeeFinePresenter extends AbsStudentPresenter implements StudentFeeFineContract.Presenter {

    private StudentFeeFineContract.View mView;

    StudentFeeFinePresenter(StudentFeeFineContract.View view) {
        mView = view;
    }

    @Override
    public void onSubscriber() {
        feeFine();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }

    @Override
    public void feeFine() {
        mDisposable.add(mServices.feeFine()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(Utils.showError(throwable)),
                        () -> mView.onComplete()));
    }


}
