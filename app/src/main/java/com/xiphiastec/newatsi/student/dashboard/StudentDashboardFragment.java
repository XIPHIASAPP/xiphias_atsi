package com.xiphiastec.newatsi.student.dashboard;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsStudentBaseFragment;
import com.xiphiastec.newatsi.model.DashboardResponse;
import com.xiphiastec.newatsi.model.StudentStatistic;
import com.xiphiastec.newatsi.student.activities.StudentActivity;
import com.xiphiastec.newatsi.student.attendance.dailyattendance.DailyAttendanceFragment;
import com.xiphiastec.newatsi.student.examinationschedule.ExaminationScheduleFragment;
import com.xiphiastec.newatsi.student.feeandfine.FeeFineDetailsFragment;
import com.xiphiastec.newatsi.student.gradecard.StudentGradeCardFragment;
import com.xiphiastec.newatsi.student.hallticket.HallTicketFragment;
import com.xiphiastec.newatsi.student.hostel.complainbox.ComplaintBoxFragment;
import com.xiphiastec.newatsi.student.hostel.discipline.DisciplineFragment;
import com.xiphiastec.newatsi.student.hostel.hostelnotice.NoticeFragment;
import com.xiphiastec.newatsi.student.hostel.inoutstatus.InOutStatusFragment;
import com.xiphiastec.newatsi.student.hostel.roomalotment.BedAndRoomFragment;
import com.xiphiastec.newatsi.model.StudentProfileResponse;
import com.xiphiastec.newatsi.model.StudentProfileResponse.StudentDocument;
import com.xiphiastec.newatsi.student.profile.AcademicsFragment;
import com.xiphiastec.newatsi.student.profile.DocumentsUploadedFragment;
import com.xiphiastec.newatsi.student.profile.ProfileDetailsFragment;
import com.xiphiastec.newatsi.student.timetable.StudentTimeTableFragment;
import com.xiphiastec.newatsi.student.transport.StudentTransportationFragment;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;
import com.xiphiastec.newatsi.util.PreferenceUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;

import static butterknife.ButterKnife.bind;

public class StudentDashboardFragment extends AbsStudentBaseFragment implements StudentDashboardContract.View {
    @BindView(R.id.rootLayout)
    ConstraintLayout rootLayout;
    private StudentDashBoardPresenter mPresenter;
    private Unbinder unbinder;
    private StudentProfileResponse
            mResponse;
    AtsiProgressDialog atsiProgressDialog;

    public StudentDashboardFragment() {
        // Required empty public constructor
    }

    public static StudentDashboardFragment newInstance() {
        Bundle args = new Bundle();
        StudentDashboardFragment fragment = new StudentDashboardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick({R.id.llExam,//1
            R.id.llAttendance,//2
            R.id.llTransport,//3
            R.id.llFine,//4
            R.id.llGrade,//5
            R.id.llTimetable,//6
            R.id.llHallTicket,//7
            R.id.llInout,//8
            R.id.llDiscipline,//9
            R.id.llNotice,//10
            R.id.llRoom,//11
            R.id.llComplaint,//12
            R.id.llAcademics,//13
            R.id.llDetails,//14
            R.id.llDocuments//15
    })
    public void subFragments(View view) {
        StudentActivity activity = (StudentActivity) getActivity();
        if (activity != null) {
            switch (view.getId()) {
                case R.id.llAttendance:
                    activity.changeFragment(new DailyAttendanceFragment(), true);
                    break;
                case R.id.llTransport:
                    activity.changeFragment(new StudentTransportationFragment(), true);
                    break;
                case R.id.llFine:
                    activity.changeFragment(new FeeFineDetailsFragment(), true);
                    break;
                case R.id.llGrade:
                    activity.changeFragment(new StudentGradeCardFragment(), true);
                    break;
                case R.id.llTimetable:
                    activity.changeFragment(new StudentTimeTableFragment(), true);
                    break;
                case R.id.llHallTicket:
                    activity.changeFragment(new HallTicketFragment(), true);
                    break;
                case R.id.llInout:
                    activity.changeFragment(new InOutStatusFragment(), true);
                    break;
                case R.id.llDiscipline:
                    activity.changeFragment(new DisciplineFragment(), true);
                    break;
                case R.id.llComplaint:
                    if(!PreferenceUtil.getInstance(getActivity()).getParentLogin())
                        activity.changeFragment(new ComplaintBoxFragment(), true);
                    else
                      Snackbar.make(rootLayout,"Permission denied for Login",Snackbar.LENGTH_SHORT).show();
                    break;
                case R.id.llNotice:
                    activity.changeFragment(new NoticeFragment(), true);
                    break;
                case R.id.llRoom:
                    activity.changeFragment(new BedAndRoomFragment(), true);
                    break;
                case R.id.llExam:
                    activity.changeFragment(new ExaminationScheduleFragment(), true);
                    break;
                case R.id.llAcademics:
                    if (mResponse == null) {
                        Toast.makeText(getContext(), "Please try again!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    activity.changeFragment(
                            AcademicsFragment.newInstance(mResponse.studentAcademic), true);
                    break;
                case R.id.llDocuments:
                    if (mResponse == null) {
                        Toast.makeText(getContext(), "Please try again!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    activity.changeFragment(
                            DocumentsUploadedFragment.newInstance(
                                    (ArrayList<StudentDocument>) mResponse.mDocument),
                            true);
                    break;
                case R.id.llDetails:
                    if (mResponse == null) {
                        Toast.makeText(getContext(), "Please try again!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    activity.changeFragment(
                            ProfileDetailsFragment.newInstance(
                                    mResponse.mProfileInfo,
                                    mResponse.mAppliedDetails),
                            true);
                    break;
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.student_dashboard, container, false);
        unbinder = bind(this, view);
        Log.d("Token","token="+PreferenceUtil.getInstance(getActivity()).getToken());

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new StudentDashBoardPresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
    }

    private String getCurrentDayText() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM, EEE", Locale.getDefault());
        return dateFormat.format(calendar.getTime());
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onSubscriber();
        if (getActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.dashboard));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getActionBar().setTitle(SS);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onUnSubscriber();
    }

    @Override
    public void onLoading() {
        atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
        Snackbar.make(rootLayout, error, Snackbar.LENGTH_SHORT)
                .show();
    }

    @Override
    public void onResponse(DashboardResponse response) {

        if(response.status) {
//            List<Integer> list = new ArrayList<>();
//            if (response.timeTableStudent.size() > 0)
//                list.add(FullStudentDashboardAdapter.TYPE_TIME_TABLE);
//            if (response.studentStatistics.size() > 0)
//                list.add(FullStudentDashboardAdapter.TYPE_STATISTICS);
//            if (response.syllabusList.size() > 0)
//                list.add(FullStudentDashboardAdapter.TYPE_SYLLABUS);

//            FullStudentDashboardAdapter adapter = new FullStudentDashboardAdapter(list, response);
//            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//            recyclerView.setItemAnimator(new DefaultItemAnimator());
//            recyclerView.setAdapter(adapter);

            if (getActivity() != null) {
                ((StudentActivity) getActivity()).setNotificationCount(response.notificationCount);
            }
        }else{
            Snackbar.make(rootLayout, response.message, Snackbar.LENGTH_SHORT)
                    .show();
        }

    }

    @Override
    public void onComplete() {
        atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onProfileResponse(StudentProfileResponse response) {
        if(response.status) {
            mResponse = response;
        }else{
            atsiProgressDialog.dismissDialog();
            Snackbar.make(rootLayout, response.message, Snackbar.LENGTH_SHORT)
                    .show();
        }
    }


//    class FullStudentDashboardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
//        static final int TYPE_TIME_TABLE = 0;
//        static final int TYPE_STATISTICS = 1;
//        static final int TYPE_SYLLABUS = 2;
//        private List<Integer> mViewList = new ArrayList<>();
//        private DashboardResponse dashboardResponse;
//
//        FullStudentDashboardAdapter(List<Integer> list, DashboardResponse response) {
//            this.mViewList = list;
//            this.dashboardResponse = response;
//        }
//
//        private List<DashboardResponse.SyllabusList> populateItemsShow() {
//            if (dashboardResponse.syllabusList.size() > 0) {
//                return dashboardResponse.syllabusList;
//            }
//            return null;
//        }
//
//        @Override
//        public int getItemViewType(int position) {
//            if (mViewList.get(position) == TYPE_STATISTICS) return TYPE_STATISTICS;
//            if (mViewList.get(position) == TYPE_TIME_TABLE) return TYPE_TIME_TABLE;
//            if (mViewList.get(position) == TYPE_SYLLABUS) return TYPE_SYLLABUS;
//            return TYPE_SYLLABUS;
//            //return position == 0 ? TYPE_TIME_TABLE : position == 1 ? TYPE_STATISTICS : position == 2 ? TYPE_SYLLABUS : TYPE_TIME_TABLE;
//        }
//
//        @Override
//        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
//            switch (i) {
//                case TYPE_TIME_TABLE:
//                    return new TimeTableViewHolder(LayoutInflater.from(getContext())
//                            .inflate(R.layout.fragment_student_dashboard_time_table, parent, false));
//                case TYPE_STATISTICS:
//                    return new StatisticsViewHolder(LayoutInflater.from(getContext())
//                            .inflate(R.layout.fragment_student_dashboard_statistics, parent, false));
//                case TYPE_SYLLABUS:
//                    return new SyllabusViewHolder(LayoutInflater.from(getContext())
//                            .inflate(R.layout.fragment_student_dashboard_syllabus, parent, false));
//            }
//            return null;
//        }
//
//        @Override
//        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//            switch (getItemViewType(position)) {
//                case TYPE_SYLLABUS:
//                    SyllabusViewHolder viewHolder = (SyllabusViewHolder) holder;
//                    bindSyllabusHolder(viewHolder);
//                    break;
//                case TYPE_TIME_TABLE:
//                    TimeTableViewHolder tableViewHolder = (TimeTableViewHolder) holder;
//                    bindTimeTable(tableViewHolder);
//                    break;
//                case TYPE_STATISTICS:
//                    StatisticsViewHolder statisticsViewHolder = (StatisticsViewHolder) holder;
//                    bindStatistic(statisticsViewHolder);
//                    break;
//            }
//        }
//
//        private void bindStatistic(StatisticsViewHolder holder) {
//            holder.mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
//            holder.mRecyclerView.setAdapter(new StatisticsAdapter(getStudentSatistics()));
//        }
//
//        private List<DashboardResponse.StudentStatistic> getStudentSatistics() {
//            if (dashboardResponse == null) {
//                return null;
//            }
//            return dashboardResponse.studentStatistics;
//        }
//
//        private void bindTimeTable(TimeTableViewHolder holder) {
//            holder.mTvWelcomeMessage.setText(getHelloText());
//            holder.text.setText(getCurrentDayText());
//            holder.recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1, LinearLayoutManager.VERTICAL, false));
//            holder.recyclerView.setItemAnimator(new DefaultItemAnimator());
//            holder.recyclerView.setAdapter(new TimeTableAdapter(loadTimeTable()));
//
//        }
//
//        private String getHelloText() {
//            return "Hello, " + PreferenceUtil.getInstance(getContext()).getUserName();
//        }
//
//        private List<DashboardResponse.TimeTableStudent> loadTimeTable() {
//            if (dashboardResponse.timeTableStudent.size() > 0) {
//                return dashboardResponse.timeTableStudent;
//            }
//            return null;
//        }
//
//        private void bindSyllabusHolder(SyllabusViewHolder holder) {
//            holder.syllabusRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//            holder.syllabusRecyclerView.setItemAnimator(new DefaultItemAnimator());
//            holder.syllabusRecyclerView.setAdapter(new SyllabusCoveredAdapter(populateItemsShow()));
//        }
//
//        @Override
//        public int getItemCount() {
//            return mViewList.size();
//        }
//
//        class TimeTableViewHolder extends RecyclerView.ViewHolder {
//            @BindView(R.id.title)
//            TextView mTvWelcomeMessage;
//            @BindView(R.id.text)
//            TextView text;
//
//            @BindView(R.id.recycler_view)
//            RecyclerView recyclerView;
//
//            TimeTableViewHolder(View itemView) {
//                super(itemView);
//                bind(this, itemView);
//            }
//        }
//
//        class SyllabusViewHolder extends RecyclerView.ViewHolder {
//            @BindView(R.id.recycler_view)
//            RecyclerView syllabusRecyclerView;
//
//            SyllabusViewHolder(View inflate) {
//                super(inflate);
//                bind(this, inflate);
//            }
//        }
//
//        class StatisticsViewHolder extends RecyclerView.ViewHolder {
//            @BindView(R.id.recycler_view)
//            RecyclerView mRecyclerView;
//
//            StatisticsViewHolder(View view) {
//                super(view);
//                bind(this, view);
//            }
//        }
//    }

//    class TimeTableAdapter extends RecyclerView.Adapter<TimeTableAdapter.ViewHolder> {
//        private final List<DashboardResponse.TimeTableStudent> timeTableStudents;
//
//
//        TimeTableAdapter(List<DashboardResponse.TimeTableStudent> timeTableStudents) {
//            this.timeTableStudents = timeTableStudents;
//        }
//
//        @Override
//        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            return new ViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_time_table, parent, false));
//        }
//
//        @Override
//        public void onBindViewHolder(ViewHolder holder, int position) {
//            DashboardResponse.TimeTableStudent timeTableStudent = timeTableStudents.get(position);
//            holder.time.setText(timeTableStudent.startTime + " - " + timeTableStudent.endTime);
//            holder.text.setText(timeTableStudent.subjectName + " by - " + timeTableStudent.employeeName);
//        }
//
//        @Override
//        public int getItemCount() {
//            return timeTableStudents.size();
//        }
//
//        class ViewHolder extends RecyclerView.ViewHolder {
//            @BindView(R.id.time)
//            TextView time;
//            @BindView(R.id.text)
//            TextView text;
//
//            public ViewHolder(View itemView) {
//                super(itemView);
//                bind(this, itemView);
//            }
//        }
//    }

//    class SyllabusCoveredAdapter extends RecyclerView.Adapter<SyllabusCoveredAdapter.SyllabusCoveredHolder> {
//
//        private List<DashboardResponse.SyllabusList> mList = new ArrayList<>();
//
//        SyllabusCoveredAdapter(List<DashboardResponse.SyllabusList> list) {
//            mList = list;
//        }
//
//        @Override
//        public SyllabusCoveredHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            return new SyllabusCoveredHolder(LayoutInflater.from(getContext())
//                    .inflate(R.layout.item_syllabus, parent, false));
//        }
//
//        @Override
//        public void onBindViewHolder(SyllabusCoveredHolder holder, int position) {
//            final DashboardResponse.SyllabusList syllabus = mList.get(position);
//            holder.mMonth.setText(Html.fromHtml("<b>Month: </b>" + syllabus.monthName));
//            holder.mSubjectGroup.setText(Html.fromHtml("<b>Group: </b>" + syllabus.subjectGroup));
//            holder.mSubjectName.setText(Html.fromHtml("<b>Subject: </b>" + syllabus.subjectName));
//            holder.itemView.setOnClickListener(v -> {
//                if (getActivity() != null) {
//                    ((StudentActivity) getActivity()).changeFragment(
//                            SyllabusDetailsFragment.newInstance(
//                                    syllabus.syllabusId,
//                                    Integer.parseInt(PreferenceUtil.getInstance(getContext()).getStudentId())),
//                            true);
//                }
//            });
//        }
//
//        @Override
//        public int getItemCount() {
//            return mList.size();
//        }
//
//        class SyllabusCoveredHolder extends RecyclerView.ViewHolder {
//            @BindView(R.id.month)
//            TextView mMonth;
//            @BindView(R.id.subject_name)
//            TextView mSubjectName;
//            @BindView(R.id.subject_group)
//            TextView mSubjectGroup;
//
//            SyllabusCoveredHolder(View itemView) {
//                super(itemView);
//                bind(this, itemView);
//            }
//        }
//    }

    class StatisticsAdapter extends RecyclerView.Adapter<StatisticsAdapter.ViewHolder> {
        List<StudentStatistic> mStatisticsModelList = new ArrayList<>();

        StatisticsAdapter(List<StudentStatistic> list) {
            this.mStatisticsModelList = list;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(getContext())
                    .inflate(R.layout.item_fragment_statistic, parent, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            StudentStatistic statistic = mStatisticsModelList.get(position);
            holder.text.setText(statistic.obtainedMarks);
            holder.title.setText(statistic.subjectName);
        }

        @Override
        public int getItemCount() {
            return mStatisticsModelList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.text)
            TextView text;
            @BindView(R.id.title)
            TextView title;

            public ViewHolder(View itemView) {
                super(itemView);
                bind(this, itemView);
            }
        }
    }
}
