package com.xiphiastec.newatsi.student.transport;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.util.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 5/3/18.
 */

class StudentTransportAdapter extends RecyclerView.Adapter<StudentTransportAdapter.StudentViewHolder> {
    private List<StudentTransportPresenter.StudentTransportResponse.Transport> mList = new ArrayList<>();
    Context context;

    public StudentTransportAdapter(Context context){
        this.context=context;
    }

    @Override
    public StudentTransportAdapter.StudentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new StudentTransportAdapter.StudentViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_emp_transport, parent, false));
    }

    @Override
    public void onBindViewHolder(StudentTransportAdapter.StudentViewHolder holder, int position) {
        StudentTransportPresenter.StudentTransportResponse.Transport transport = mList.get(position);
//            String vehicleName = "<b>Type: </b>" + transport.vehicleType + "\n<b>Number: </b>" + transport.vehicleNumber;
//            holder.mTvVehicleTypeNumber.setText(Utils.convertStringToHtml(vehicleName));
//            holder.mTvRouteName.setText(Utils.convertStringToHtml("<b>Route:</b> " + transport.routeName));
//            holder.mTvTripName.setText("Trip name: " + transport.tripName);
//            holder.mTvStoppageName.setText(Utils.convertStringToHtml("<b>Stop:</b> " + transport.stoppageName));

        holder.mTvRouteName.setText(Utils.checkEmpty(transport.routeName));
        holder.mTvStoppageName.setText(Utils.checkEmpty(transport.stoppageName));
        holder.mTvVehicleTypeNumber.setText(Utils.checkEmpty(transport.vehicleType + " - " + transport.vehicleNumber));
//            holder.tv_driver.setText(transport.dri);
//            holder.tv_conductor.setText(transport.conductorName);


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    void swapData(List<StudentTransportPresenter.StudentTransportResponse.Transport> transport) {
        mList.addAll(transport);
        notifyDataSetChanged();
    }

    class StudentViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_vehicle_type_number)
        TextView mTvVehicleTypeNumber;
        @BindView(R.id.tv_route_name)
        TextView mTvRouteName;
        @BindView(R.id.tv_stoppage_name)
        TextView mTvStoppageName;
//            @BindView(R.id.tv_trip_name)
//            TextView mTvTripName;
            @BindView(R.id.tv_driver)
            TextView tv_driver;
            @BindView(R.id.tv_conductor)
            TextView tv_conductor;

        public StudentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
