package com.xiphiastec.newatsi.student.hostel.roomalotment;

import com.xiphiastec.newatsi.base.AbsStudentPresenter;


/**
 * Created by Admin on 01-04-2017.
 */

public class RoomAllotmentPresenter extends AbsStudentPresenter implements BedAndRoomContract.Presenter {

    private BedAndRoomContract.View mView;

    RoomAllotmentPresenter(BedAndRoomContract.View view) {
        mView = view;
    }

    @Override
    public void onSubscriber() {
        roomDetails();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }

    public void roomDetails() {
        mDisposable.add(mServices.getRoomAllotment()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> {
                            /*TODO: Show error*/
                        },
                        () -> mView.onComplete()));
    }




}
