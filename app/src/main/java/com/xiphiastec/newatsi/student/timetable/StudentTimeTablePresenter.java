package com.xiphiastec.newatsi.student.timetable;

import com.xiphiastec.newatsi.base.AbsStudentPresenter;

/**
 * Created by Admin on 03-04-2017.
 */

public class StudentTimeTablePresenter
        extends AbsStudentPresenter implements StudentTimeTableContract.Presenter {

    private StudentTimeTableContract.View mView;

      StudentTimeTablePresenter(StudentTimeTableContract.View view) {
        mView = view;
    }

    @Override
    public void onSubscriber() {
        timeTable();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }


    public void timeTable() {
        mDisposable.add(mServices.getTimeTable()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.success) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> {
                        },
                        () -> mView.onComplete()));
    }

}
