package com.xiphiastec.newatsi.student.notification;

import com.xiphiastec.newatsi.base.AbsStudentPresenter;


/**
 * Created by Admin on 03-04-2017.
 */

public class NotificationPresenter extends AbsStudentPresenter implements NotificationContract.Presenter{

 private NotificationContract.View mView;

    public NotificationPresenter(NotificationContract.View view) {
        mView = view;
    }

    @Override
    public void onSubscriber() {

    }

    @Override
    public void onUnSubscriber() {
     mDisposable.clear();
    }

    @Override
    public void notification() {
        mDisposable.add(mServices.notification()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status && response.mTodayList.size()>0) {
                                mView.renderNotification(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> {
                        },
                        () -> mView.onComplete()));
    }

    @Override
    public void oldNotification() {
        mDisposable.add(mServices.moreNotifications()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status && response.mTodayList.size()>0) {
                                mView.renderMoreNotification(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> {
                        },
                        () -> mView.onComplete()));
    }

}
