package com.xiphiastec.newatsi.student.transport;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.xiphiastec.newatsi.base.AbsStudentPresenter;
import com.xiphiastec.newatsi.model.StatusResponse;

import java.util.List;

/**
 * Created by Admin on 03-04-2017.
 */

public class StudentTransportPresenter extends AbsStudentPresenter implements StudentTransportationContract.Presenter {

    private StudentTransportationContract.View mView;

    StudentTransportPresenter(StudentTransportationContract.View view) {
        mView = view;
    }

    public void transportDetails() {
        mDisposable.add(mServices.transportDetails()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> {
                            /*TODO: Handle Error*/
                        },
                        () -> mView.onComplete()));
    }

    @Override
    public void onSubscriber() {
        transportDetails();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }


    public static class StudentTransportResponse extends StatusResponse {
        @SerializedName("transport")
        @Expose
        public List<Transport> transport = null;
        @SerializedName("student_register_number")
        @Expose
        public String studentRegisterNumber;
        @SerializedName("student_name")
        @Expose
        public String studentName;


        @Override
        public String toString() {
            return "StudentTransportResponse{" +
                    "transport=" + transport +
                    ", studentRegisterNumber='" + studentRegisterNumber + '\'' +
                    ", studentName='" + studentName + '\'' +
                    ", status=" + status +
                    ", message='" + message + '\'' +
                    '}';
        }

        public static class Transport {
            @SerializedName("education_level")
            @Expose
            public String educationLevel;
            @SerializedName("award_name")
            @Expose
            public String awardName;
            @SerializedName("program_name")
            @Expose
            public String programName;
            @SerializedName("class_name")
            @Expose
            public String className;
            @SerializedName("academic_batch_name")
            @Expose
            public String academicBatchName;
            @SerializedName("batch_name")
            @Expose
            public String batchName;
            @SerializedName("vehicle_type")
            @Expose
            public String vehicleType;
            @SerializedName("vehicle_number")
            @Expose
            public String vehicleNumber;
            @SerializedName("route_name")
            @Expose
            public String routeName;
            @SerializedName("trip_name")
            @Expose
            public String tripName;
            @SerializedName("stoppage_name")
            @Expose
            public String stoppageName;

            @Override
            public String toString() {
                return "Transport{" +
                        "educationLevel='" + educationLevel + '\'' +
                        ", awardName='" + awardName + '\'' +
                        ", programName='" + programName + '\'' +
                        ", className='" + className + '\'' +
                        ", academicBatchName='" + academicBatchName + '\'' +
                        ", batchName='" + batchName + '\'' +
                        ", vehicleType='" + vehicleType + '\'' +
                        ", vehicleNumber='" + vehicleNumber + '\'' +
                        ", routeName='" + routeName + '\'' +
                        ", tripName='" + tripName + '\'' +
                        ", stoppageName='" + stoppageName + '\'' +
                        '}';
            }


        }
    }
}
