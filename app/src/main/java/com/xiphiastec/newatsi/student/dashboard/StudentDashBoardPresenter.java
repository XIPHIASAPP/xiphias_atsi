package com.xiphiastec.newatsi.student.dashboard;

import com.xiphiastec.newatsi.base.AbsStudentPresenter;
import com.xiphiastec.newatsi.util.NetworkError;

public class StudentDashBoardPresenter extends AbsStudentPresenter implements
        StudentDashboardContract.Presenter {

    private StudentDashboardContract.View mView;

    StudentDashBoardPresenter(StudentDashboardContract.View view) {
        mView = view;
    }

    @Override
    public void onSubscriber() {
        //getDashboardInfo();
        profile();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }

    @Override
    public void profile() {
        mDisposable.add(mServices.profile()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                                mView.onProfileResponse(response);
                        }, throwable -> {
                            mView.onShowError(new NetworkError(throwable).getAppErrorMessage());
                        },
                        () -> mView.onComplete()));
    }

    @Override
    public void getDashboardInfo() {
        mDisposable.add(mServices.getDashboardInfo()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                                mView.onResponse(response);
                        }, throwable -> {
                            mView.onShowError(new NetworkError(throwable).getAppErrorMessage());
                        },
                        () -> mView.onComplete()));
    }


}
