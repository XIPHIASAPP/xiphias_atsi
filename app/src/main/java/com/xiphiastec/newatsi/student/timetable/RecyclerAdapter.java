package com.xiphiastec.newatsi.student.timetable;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.TimeTableStudent;

import java.util.List;

/**
 * Created by xiphias0101 on 24/01/2018.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>{

    Context context;
    List<TimeTableStudent> tables;

    public RecyclerAdapter(Context context, List<TimeTableStudent> table){
        this.context=context;
        this.tables=table;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.table_item, parent, false);
        //itemView.setBackgroundColor(Color.parseColor("#E0F7FA"));

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

//        int arr[]={context.getResources().getColor(R.color.orange),
//                context.getResources().getColor(R.color.yellow),
//                context.getResources().getColor(R.color.green),
//                context.getResources().getColor(R.color.blue),
//                context.getResources().getColor(R.color.teal),
//                context.getResources().getColor(R.color.brwn),
//                context.getResources().getColor(R.color.lme)};


       // holder.itemView.setBackgroundColor(arr[position]);
        TimeTableStudent timeTable = tables.get(position);
        holder.txtSubject.setText(timeTable.subjectName);
        holder.txtTime.setText(timeTable.startTime+"-"+timeTable.endTime);
        holder.txtTeacher.setText(timeTable.employeeName);
    }

    @Override
    public int getItemCount() {
        return tables.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtSubject, txtTime, txtTeacher;

        public MyViewHolder(View view) {
            super(view);
            txtSubject = (TextView) view.findViewById(R.id.txtSubject);
            txtTime =    (TextView) view.findViewById(R.id.txtTime);
            txtTeacher = (TextView) view.findViewById(R.id.txtTeacher);
        }
    }
}
