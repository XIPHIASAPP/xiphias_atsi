package com.xiphiastec.newatsi.student.examinationschedule;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;

import com.xiphiastec.newatsi.model.StudentExamDetail;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 5/3/18.
 */

class ExamScheduleAdapter extends RecyclerView.Adapter<ExamScheduleAdapter.ExamViewHolder> {

    private static final int REQ_PERMISSION_CODE = 9020;
    private static final int ANNUAL_VIEW_TYPE=1;
    private static final int SEMISTER_VIEW_TYPE=2;

    private ArrayList<StudentExamDetail> mList = new ArrayList<>();
    Context context;

    public ExamScheduleAdapter(Context context){
        this.context=context;
    }

    @Override
    public ExamScheduleAdapter.ExamViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ExamScheduleAdapter.ExamViewHolder viewholder=null;
        if(viewType==ANNUAL_VIEW_TYPE){
            viewholder=new ExamScheduleAdapter.ExamViewHolder(LayoutInflater.from(context)
                    .inflate(R.layout.exam_item_annual, parent, false));
        }else if(viewType==SEMISTER_VIEW_TYPE){
            viewholder=new ExamScheduleAdapter.ExamViewHolder(LayoutInflater.from(context)
                    .inflate(R.layout.exam_item_semister, parent, false));
        }

        return  viewholder;
    }

    @Override
    public void onBindViewHolder(ExamScheduleAdapter.ExamViewHolder holder, int position) {
        StudentExamDetail detail = mList.get(position);
        holder.mTvExamType.setText(detail.examType);
        holder.mTvTimeDate.setText(detail.examTime + " " + detail.examDate);
        holder.mTvSubject.setText(detail.subject);
        holder.mTvSubjectCode.setText(detail.subjectCode);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mList.get(position).examType.equalsIgnoreCase("Annual")?ANNUAL_VIEW_TYPE:SEMISTER_VIEW_TYPE;
    }

    void swapData(List<StudentExamDetail> studentExamDetails) {
        mList.clear();
        mList.addAll(studentExamDetails);
        notifyDataSetChanged();
    }

    public class ExamViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTime)
        TextView mTvTimeDate;
        @BindView(R.id.tvSubject)
        TextView mTvSubject;
        @BindView(R.id.tvExamType)
        TextView mTvExamType;
        @BindView(R.id.tvSubjectCode)
        TextView mTvSubjectCode;

        public ExamViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
