package com.xiphiastec.newatsi.student.profile;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.transition.TransitionManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.xiphiastec.newatsi.Constants;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.StudentProfileResponse.StudentDocument;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DocumentsUploadedFragment extends Fragment {

    Unbinder unbinder;
    @BindView(R.id.recycler_view)
    EmptyViewRecyclerView mRecyclerView;
    @BindView(R.id.root_layout)
    LinearLayout mRootLayout;

    private List<StudentDocument> mList = new ArrayList<>();

    public DocumentsUploadedFragment() {
        // Required empty public constructor
    }

    public static DocumentsUploadedFragment newInstance(ArrayList<StudentDocument> documents) {
        DocumentsUploadedFragment fragment = new DocumentsUploadedFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(Constants.ARGS_DOCUMENTS_BUNDLE, documents);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mList = getArguments().getParcelableArrayList(Constants.ARGS_DOCUMENTS_BUNDLE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupRecyclerView();
    }

    private void setupRecyclerView() {
        TransitionManager.beginDelayedTransition(mRootLayout);
        if (mList != null) {
            DocumentUploadedAdapter adapter = new DocumentUploadedAdapter(getActivity(),mList);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            mRecyclerView.setAdapter(adapter);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_documents_uploaded, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.document));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(SS);
        }
    }

//    class DocumentUploadedAdapter extends RecyclerView.Adapter<DocumentUploadedAdapter.DocumentViewHolder> {
//        private List<StudentDocument> mList = new ArrayList<>();
//
//        DocumentUploadedAdapter(List<StudentDocument> list) {
//            mList = list;
//        }
//
//        @Override
//        public DocumentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            return new DocumentViewHolder(LayoutInflater.from(getContext())
//                    .inflate(R.layout.item_document, parent, false));
//        }
//
//        @Override
//        public void onBindViewHolder(DocumentViewHolder holder, int position) {
//            StudentDocument document = mList.get(position);
//            boolean mandatory = document.mandatory;
//
//            boolean isVerified = document.isVerified;
//            holder.mVerified.setImageResource(isVerified ?
//                    R.drawable.ic_check_black_24dp :
//                    R.drawable.ic_close_black_24dp);
//            holder.mVerified.setColorFilter(isVerified ?
//                    ContextCompat.getColor(getActivity(), R.color.material_green_500) :
//                    ContextCompat.getColor(getActivity(), R.color.material_red_500), PorterDuff.Mode.SRC_IN);
//
//            boolean upload = TextUtils.isEmpty(document.uploaded);
//            holder.mUploaded.setImageResource(upload ?
//                    R.drawable.ic_check :
//                    R.drawable.ic_uncheck);
//
//            if(document.documentName!=null) {
//                if (mandatory)
//                    holder.mDocumentName.setText(Html.fromHtml(document.documentName + "<sup><small><font color=#cc0029> *</font></small></sup>"));
//                else {
//                    holder.mDocumentName.setText(Html.fromHtml(document.documentName));
//                }
//            }
//        }
//
//        @Override
//        public int getItemCount() {
//            return mList.size();
//        }
//
//        public class DocumentViewHolder extends RecyclerView.ViewHolder {
//            @BindView(R.id.uploaded)
//            ImageView mUploaded;
////            @BindView(R.id.mandatory)
////            ImageView mMandatory;
//            @BindView(R.id.verified)
//            ImageView mVerified;
//            @BindView(R.id.tv_document)
//            TextView mDocumentName;
//
//            public DocumentViewHolder(View itemView) {
//                super(itemView);
//                ButterKnife.bind(this, itemView);
//            }
//        }
//    }
}
