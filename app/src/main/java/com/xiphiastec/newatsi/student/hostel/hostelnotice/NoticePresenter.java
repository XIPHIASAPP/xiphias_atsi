package com.xiphiastec.newatsi.student.hostel.hostelnotice;

import com.xiphiastec.newatsi.base.AbsStudentPresenter;
import com.xiphiastec.newatsi.model.StudentDetailsBody;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Admin on 21-04-2017.
 */

public class NoticePresenter extends AbsStudentPresenter implements NoticeContract.Presenter {


    private NoticeContract.View mView;

    NoticePresenter(NoticeContract.View view) {
        mView = view;
    }

    @Override
    public void onSubscriber() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        StudentDetailsBody body = new StudentDetailsBody();
        body.setDate(format.format(new Date()));
        noticeWithDate(body);
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }

    @Override
    public void noticeWithDate(StudentDetailsBody body) {
        mDisposable.add(mServices.noticeWithDate(body)
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status && response.hostelNotice.size() > 0) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> {
                        },
                        () -> mView.onComplete()));
    }



}
