package com.xiphiastec.newatsi.student.gradecard;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.ExamTypeBody;
import com.xiphiastec.newatsi.model.GradeCardResponse;
import com.xiphiastec.newatsi.model.Semester;

/**
 * Created by Admin on 13-12-2017.
 */

public interface StudentGradeCardContract {

    interface View extends BaseView<GradeCardResponse> {
        void onExamTypeResponse(Semester semester);
    }

    interface Presenter extends BasePresenter<View> {
        void gradeCardDetails(ExamTypeBody examTypeBody);
        void examTypes();
    }
}
