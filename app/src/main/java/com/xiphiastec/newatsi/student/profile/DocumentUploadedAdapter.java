package com.xiphiastec.newatsi.student.profile;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.StudentProfileResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 5/3/18.
 */

class DocumentUploadedAdapter extends RecyclerView.Adapter<DocumentUploadedAdapter.DocumentViewHolder> {
    private List<StudentProfileResponse.StudentDocument> mList = new ArrayList<>();
    Context context;

    DocumentUploadedAdapter(Context context,List<StudentProfileResponse.StudentDocument> list) {
        mList = list;
        this.context=context;
    }

    @Override
    public DocumentUploadedAdapter.DocumentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DocumentUploadedAdapter.DocumentViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_document, parent, false));
    }

    @Override
    public void onBindViewHolder(DocumentUploadedAdapter.DocumentViewHolder holder, int position) {
        StudentProfileResponse.StudentDocument document = mList.get(position);
        boolean mandatory = document.mandatory;

        boolean isVerified = document.isVerified;
        holder.mVerified.setImageResource(isVerified ?
                R.drawable.ic_check_black_24dp :
                R.drawable.ic_close_black_24dp);
        holder.mVerified.setColorFilter(isVerified ?
                ContextCompat.getColor(context, R.color.material_green_500) :
                ContextCompat.getColor(context, R.color.material_red_500), PorterDuff.Mode.SRC_IN);

        boolean upload = document.uploaded;
        holder.mUploaded.setImageResource(upload ?
                R.drawable.ic_check :
                R.drawable.ic_uncheck);

        if(document.documentName!=null) {
            if (mandatory)
                holder.mDocumentName.setText(Html.fromHtml(document.documentName + "<sup><small><font color=#cc0029> *</font></small></sup>"));
            else {
                holder.mDocumentName.setText(Html.fromHtml(document.documentName));
            }
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class DocumentViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.uploaded)
        ImageView mUploaded;

        @BindView(R.id.verified)
        ImageView mVerified;
        @BindView(R.id.tv_document)
        TextView mDocumentName;

        public DocumentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
