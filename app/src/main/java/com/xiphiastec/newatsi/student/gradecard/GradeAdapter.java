package com.xiphiastec.newatsi.student.gradecard;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.GradeCard;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 7/3/18.
 */

class GradeAdapter extends RecyclerView.Adapter<GradeAdapter.GradeViewHolder> {
    private List<GradeCard> mList = new ArrayList<>();
    Context context;

    public GradeAdapter(Context context,List<GradeCard> list){

        this.context=context;

        mList=list;
    }

    @Override
    public GradeAdapter.GradeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GradeAdapter.GradeViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_grade_student, parent, false));
    }

    @Override
    public void onBindViewHolder(GradeAdapter.GradeViewHolder holder, int position) {
        GradeCard gradeCard = mList.get(position);

        holder.txtSubjectName.setText(gradeCard.getSubjectName());
        holder.txtGrade.setText(gradeCard.getGrade());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    void swapData(List<GradeCard> grades) {

        mList.addAll(grades);

        notifyDataSetChanged();
    }

    class GradeViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtSubjectName)
        TextView txtSubjectName;
        @BindView(R.id.txtGrade)
        TextView txtGrade;

        public GradeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

