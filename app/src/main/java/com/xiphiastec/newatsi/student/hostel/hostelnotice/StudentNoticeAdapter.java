package com.xiphiastec.newatsi.student.hostel.hostelnotice;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.HostelNotice;
import com.xiphiastec.newatsi.util.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 5/3/18.
 */

class StudentNoticeAdapter extends RecyclerView.Adapter<StudentNoticeAdapter.NoticeViewHolder> {
    private List<HostelNotice> mList = new ArrayList<>();
    Context context;

    StudentNoticeAdapter(Context context) {

        this.context=context;
    }

    @Override
    public StudentNoticeAdapter.NoticeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new StudentNoticeAdapter.NoticeViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_student_notice, parent, false));
    }

    @Override
    public void onBindViewHolder(StudentNoticeAdapter.NoticeViewHolder holder, int position) {
        HostelNotice hostelNotice = mList.get(position);
        holder.mTvDay.setText(Utils.checkEmpty(Utils.getDay(hostelNotice.date)));
        holder.mTvMon.setText(Utils.getMonth(Utils.checkEmpty(hostelNotice.date)));

        holder.mTvNotice.setText(Utils.checkEmpty(hostelNotice.notice));
        if(position%2==0){
            holder.mTvNotice.setBackgroundResource(R.color.noticeEven);
        }else{
            holder.mTvNotice.setBackgroundResource(R.color.noticeOdd);
        }

    }

    @Override
    public int getItemCount() {
        return mList==null?0:mList.size();
    }

    void swapData(List<HostelNotice> hostelNotice) {
        mList=hostelNotice;
        notifyDataSetChanged();
    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }


    class NoticeViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_day)
        TextView mTvDay;
        @BindView(R.id.tv_mon)
        TextView mTvMon;
        @BindView(R.id.tv_notice)
        TextView mTvNotice;

       public  NoticeViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
