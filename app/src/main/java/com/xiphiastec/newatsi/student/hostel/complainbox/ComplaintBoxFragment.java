package com.xiphiastec.newatsi.student.hostel.complainbox;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nononsenseapps.filepicker.FilePickerActivity;
import com.nononsenseapps.filepicker.Utils;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsStudentBaseFragment;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ComplaintBoxFragment extends AbsStudentBaseFragment implements
        DatePickerDialog.OnDateSetListener, ComplaintBoxContract.View {

    private static final int FILE_CODE = 9002;
    @BindView(R.id.etCdate)
    TextView mDate;
    @BindView(R.id.etComplaint)
    EditText complaint;
    @BindView(R.id.etOccurence)
    EditText placeOccurrence;
    @BindView(R.id.spnCategory)
    Spinner spinner;
    @BindView(R.id.etPersonName)
    EditText personName;
    @BindView(R.id.root)
    LinearLayout mRoot;
    Unbinder mUnbinder;
    @BindView(R.id.ivImage)
    ImageView mImagePreview;
//    @BindView(R.id.images)
//    LinearLayout mImages;
    private DatePickerDialog mDatePickerDialog;
    private ComplaintBoxPresenter mPresenter;
    private String mImageFile = "", mSelectedDate = "";

    AtsiProgressDialog atsiProgressDialog;

    public ComplaintBoxFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new ComplaintBoxPresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);

    }

    private void setupViews() {
        Calendar now = Calendar.getInstance();

        mDatePickerDialog= new DatePickerDialog(getActivity(), this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));

        mDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime());


    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.hostel_complaint_box_fragment, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        setupViews();
        return view;
    }


    @OnClick({R.id.btnAttach, R.id.llDatePick, R.id.submit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAttach:
                // This always works
                Intent i = new Intent(getContext(), FilePickerActivity.class);
                // This works if you defined the intent filter
                // Intent i = new Intent(Intent.ACTION_GET_CONTENT);

                // Set these depending on your use case. These are the defaults.
                i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
                i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
                i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);

                // Configure initial directory by specifying a String.
                // You could specify a String like "/storage/emulated/0/", but that can
                // dangerous. Always use Android's API calls to get paths to the SD-card or
                // internal memory.
                i.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());

                startActivityForResult(i, FILE_CODE);
                break;

            case R.id.etCdate:
            case R.id.llDatePick:
                mDatePickerDialog.show();
                break;

            case R.id.submit:
                mPresenter.submitComplaint(
                        mDate.getText().toString(),
                        complaint.getText().toString(),
                        placeOccurrence.getText().toString(),
                        spinner.getSelectedItem().toString(),
                        personName.getText().toString(),
                        mImageFile);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_CODE && resultCode == Activity.RESULT_OK) {
            // Use the provided utility method to parse the result
            List<Uri> files = Utils.getSelectedFilesFromResult(data);
            for (Uri uri : files) {
                mImagePreview.setImageURI(uri);
                //Glide.with(getActivity()).load(uri).asBitmap().into(mImagePreview);

                File file = Utils.getFileForUri(uri);
                // Do something with the result...
                mImageFile = getStringFile(file);
                Toast.makeText(getContext(), file.getName(), Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (getActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.complaint_box));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getActionBar().setTitle(SS);
        }
    }

    @Override
    public void onLoading() {
       atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
        Snackbar.make(mRoot, error, BaseTransientBottomBar.LENGTH_SHORT).show();
    }

    @Override
    public void onComplete() {
        atsiProgressDialog.dismissDialog();
    }


    @Override
    public void onResponse(ComplaintBoxPresenter.ComplaintBoxResponse response) {
        Snackbar.make(mRoot, response.message, Snackbar.LENGTH_SHORT).show();
        clearForm();
    }


    public void clearForm(){
        mDate.setText("Select Date");
        complaint.setText("");
        placeOccurrence.setText("");
        personName.setText("");
        spinner.setSelection(0);

    }

    public String getStringFile(File f) {
        InputStream inputStream = null;
        String encodedFile = "", lastVal;
        try {
            inputStream = new FileInputStream(f.getAbsolutePath());

            byte[] buffer = new byte[10240];//specify the size to allow
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            Base64OutputStream output64 = new Base64OutputStream(output, Base64.DEFAULT);

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output64.write(buffer, 0, bytesRead);
            }
            output64.close();
            encodedFile = output.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        lastVal = encodedFile;
        return lastVal;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onUnSubscriber();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String strMonth = String.valueOf(++month);
        String day = String.valueOf(dayOfMonth);
        if (month < 10) {
            strMonth = "0" + month;
        }
        if (dayOfMonth < 10) {

            day = "0" + dayOfMonth;
        }
        mSelectedDate = year + "-" + strMonth + "-" + day;
        mDate.setText(mSelectedDate);
    }
}
