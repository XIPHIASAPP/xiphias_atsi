package com.xiphiastec.newatsi.student.hallticket;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.HallTicketResponse;

/**
 * Created by Admin on 13-12-2017.
 */

public interface HallTicketContract {

    interface View extends BaseView<HallTicketResponse> {

    }

    interface Presenter extends BasePresenter<View> {
        void hallTicket();
    }
}
