package com.xiphiastec.newatsi.student.hostel.complainbox;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;

/**
 * Created by Admin on 12-12-2017.
 */

public interface ComplaintBoxContract {

    interface View extends BaseView<ComplaintBoxPresenter.ComplaintBoxResponse> {

    }

    interface Presenter extends BasePresenter<View> {
        void submitComplaint(String date, String complaint, String place,
                             String category, String personName, String imageString);
    }
}
