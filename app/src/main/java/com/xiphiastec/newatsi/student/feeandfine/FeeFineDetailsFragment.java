package com.xiphiastec.newatsi.student.feeandfine;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsStudentBaseFragment;
import com.xiphiastec.newatsi.model.FeeDetailsResponse;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FeeFineDetailsFragment extends AbsStudentBaseFragment implements
        StudentFeeFineContract.View {

    @BindViews({R.id.tvName,
            R.id.tvRegistrationNumber,
            R.id.tvTotalPaid,
            R.id.tvPending})
    List<TextView> mViewList;

    @BindView(R.id.rvFee)
    RecyclerView feeRecyclerView;

    @BindView(R.id.rvFine)
    RecyclerView fineRecyclerView;

    Unbinder unbinder;
    private StudentFeeFinePresenter mPresenter;

    FeeAdapter feeAdapter;

    FineAdapter fineAdapter;

    AtsiProgressDialog atsiProgressDialog;


    @BindView(R.id.vStudent)
    View vStudent;
    @BindView(R.id.vfee)
    View vFee;
    @BindView(R.id.vfine)
    View vFine;


    @BindView(R.id.name)
    TextView name;


    @BindView(R.id.cvStudentDetails)
    CardView cvStudentDetails;
    @BindView(R.id.cvFeeDetails)
    CardView cvFeeDetails;
    @BindView(R.id.cvFineDetails)
    CardView cvFineDetails;


    public FeeFineDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fee_details_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);

        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.fee_fine_details));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getActionBar().setTitle(SS);
        }
        mPresenter.onSubscriber();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new StudentFeeFinePresenter(this);
    }

    @Override
    public void onLoading() {
        atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onComplete() {
        atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onResponse(FeeDetailsResponse response) {
        mViewList.get(0).setText(String.format("%s", response.studentname));
        mViewList.get(1).setText(String.format("%s", response.studentRegisterNo));
        mViewList.get(2).setText(String.format("%s", response.totalPaid));
        mViewList.get(3).setText(String.format("%s", response.pendingAmount));

        List<Integer> list = new ArrayList<>();
        if (response.feeDetails.size() > 0) {
            feeAdapter=new FeeAdapter(getActivity(),response.feeDetails);
            feeRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            feeRecyclerView.setItemAnimator(new DefaultItemAnimator());
            feeRecyclerView.setAdapter(feeAdapter);
        }
        if (response.fineDetails.size() > 0) {
            fineAdapter=new FineAdapter(getActivity(),response.fineDetails);
            fineRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            fineRecyclerView.setItemAnimator(new DefaultItemAnimator());
            fineRecyclerView.setAdapter(fineAdapter);
        }

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onUnSubscriber();
    }

    @OnClick({R.id.llStudent})
    public void viewStudentClicked(){

        cvStudentDetails.setVisibility(View.VISIBLE);
        cvFeeDetails.setVisibility(View.GONE);
        cvFineDetails.setVisibility(View.GONE);

        vStudent.setVisibility(View.VISIBLE);
        vFee.setVisibility(View.GONE);
        vFine.setVisibility(View.GONE);

    }

    @OnClick(R.id.llFee)
    public void viewFeeClicked(){

        cvFeeDetails.setVisibility(View.VISIBLE);
        cvStudentDetails.setVisibility(View.GONE);
        cvFineDetails.setVisibility(View.GONE);

        vStudent.setVisibility(View.GONE);
        vFee.setVisibility(View.VISIBLE);
        vFine.setVisibility(View.GONE);

        name.setText("Fee Details");

    }



    @OnClick({R.id.llFine})
    public void viewFineClicked(){

        cvFineDetails.setVisibility(View.VISIBLE);
        cvFeeDetails.setVisibility(View.GONE);
        cvStudentDetails.setVisibility(View.GONE);

        vStudent.setVisibility(View.GONE);
        vFee.setVisibility(View.GONE);
        vFine.setVisibility(View.VISIBLE);

        name.setText("Fine Details");

    }
}
