package com.xiphiastec.newatsi.student.notification;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.Notification;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 10/3/18.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {

    private List<Notification> mList = new ArrayList<>();
    Context context;


    public NotificationAdapter(Context context){
        this.context=context;
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NotificationViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_notification_atsi, parent, false));
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, int position) {
        Notification notification = mList.get(position);
        holder.txtNotification.setText(notification.notification);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void swapData(List<Notification> list) {
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }


    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtNotification)
        TextView txtNotification;

        public NotificationViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
