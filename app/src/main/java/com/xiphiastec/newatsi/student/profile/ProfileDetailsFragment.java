package com.xiphiastec.newatsi.student.profile;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.transition.TransitionManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xiphiastec.newatsi.Constants;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.StudentProfileResponse.StudentAppliedDetails;
import com.xiphiastec.newatsi.model.StudentProfileResponse.StudentProfileInfo;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.util.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileDetailsFragment extends Fragment {

    @BindView(R.id.iv_profile_icon)
    ImageView mIvProfileIcon;

    Unbinder unbinder;
    @BindView(R.id.root_layout)
    NestedScrollView mRootLayout;
    @BindViews({R.id.tv_program_name,//0
//            R.id.tv_created_on,//1
//            R.id.tv_applied_caste,//2
//            R.id.tv_applied_sub_caste,//3
            R.id.tv_register_number,//1
            R.id.tv_admission_number,//2
            R.id.tv_student_name,//3
            R.id.tv_email,//4
            R.id.tv_alternative_email_id,//5
            R.id.tv_phone_number,//6
            R.id.tv_mother_tongue,//7
            R.id.tv_date_of_birth,//8
            R.id.tv_gender,//9
            R.id.tv_birth_place,//10
            R.id.tv_caste,//11
            R.id.tv_category,//12
            R.id.tv_blood_group,//13
            R.id.name})//14
            List<TextView> mViewList;

    private StudentProfileInfo mProfileInfo;
    private StudentAppliedDetails mAppliedDetails;

    private void setupAllied() {
        TransitionManager.beginDelayedTransition(mRootLayout);
        if (mAppliedDetails != null) {
            Glide.with(getContext())
                    .load(mAppliedDetails.picture)
                    .error(R.mipmap.empprofile)
                    .placeholder(R.mipmap.empprofile)
                    .into(mIvProfileIcon);

            mViewList.get(0).setText(mAppliedDetails.programName);
//            mViewList.get(1).setText(mAppliedDetails.createdDate);
//            mViewList.get(2).setText(mAppliedDetails.caste);
//            mViewList.get(3).setText(mAppliedDetails.subCaste);
        }
    }
    public ProfileDetailsFragment() {
        // Required empty public constructor
    }

    public static ProfileDetailsFragment newInstance(StudentProfileInfo info, StudentAppliedDetails details) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.ARGS_PROFILE_BUNDLE, info);
        bundle.putParcelable(Constants.ARGS_APPLIED_BUNDLE, details);
        ProfileDetailsFragment fragment = new ProfileDetailsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private void setupProfile() {
        TransitionManager.beginDelayedTransition(mRootLayout);
        if (mProfileInfo != null) {
            mViewList.get(1).setText(Utils.checkEmpty(mProfileInfo.registerNumber));
            mViewList.get(2).setText(Utils.checkEmpty(mProfileInfo.admissionNumber));
            mViewList.get(3).setText(Utils.checkEmpty(mProfileInfo.studentName));
            mViewList.get(4).setText(Utils.checkEmpty(mProfileInfo.email));
            mViewList.get(5).setText(Utils.checkEmpty(mProfileInfo.alternativeEmail));
            mViewList.get(6).setText(Utils.checkEmpty(mProfileInfo.phone));
            mViewList.get(7).setText(Utils.checkEmpty(mProfileInfo.motherTongue));
            mViewList.get(8).setText(Utils.checkEmpty(mProfileInfo.dateOfBirth));
            mViewList.get(9).setText(Utils.checkEmpty(mProfileInfo.genderName));
            mViewList.get(10).setText(Utils.checkEmpty(mProfileInfo.birthPlace));
            mViewList.get(11).setText(Utils.checkEmpty(mProfileInfo.caste));
            mViewList.get(12).setText(Utils.checkEmpty(mProfileInfo.category));
            mViewList.get(13).setText(Utils.checkEmpty(mProfileInfo.bloodGroup));
            mViewList.get(14).setText(Utils.checkEmpty(mProfileInfo.studentName));
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mAppliedDetails = getArguments().getParcelable(Constants.ARGS_APPLIED_BUNDLE);
            mProfileInfo = getArguments().getParcelable(Constants.ARGS_PROFILE_BUNDLE);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_details, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupAllied();
        setupProfile();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.profile));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(SS);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
