package com.xiphiastec.newatsi.student.examinationschedule;

import com.xiphiastec.newatsi.base.AbsStudentPresenter;

/**
 * Created by Admin on 20-04-2017.
 */

public class ExaminationSchedulePresenter extends AbsStudentPresenter implements ExaminationScheduleContract.Presenter {

    private ExaminationScheduleContract.View mView;

    ExaminationSchedulePresenter(ExaminationScheduleContract.View view) {
        mView = view;
    }

    @Override
    public void onSubscriber() {
        examSchedule();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }


    public void examSchedule() {
        mDisposable.add(mServices.examSchedule()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> {
                        },
                        () -> mView.onComplete()));
    }

}
