package com.xiphiastec.newatsi.student.attendance.dailyattendance;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.DailyStudentAttendance;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 5/3/18.
 */

class DailyAttendanceAdapter extends RecyclerView.Adapter<DailyAttendanceAdapter.DailyViewHolder> {

    private List<DailyStudentAttendance> dailyStudentAttendance = new ArrayList<>();
    Context context;

    public DailyAttendanceAdapter(Context context) {
        this.context=context;
    }

    void swapData(List<DailyStudentAttendance> dailyStudentAttendance) {
        this.dailyStudentAttendance = dailyStudentAttendance;
        notifyDataSetChanged();
    }

    @Override
    public DailyAttendanceAdapter.DailyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DailyAttendanceAdapter.DailyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_simple_attendance, parent, false));
    }

    @Override
    public void onBindViewHolder(DailyAttendanceAdapter.DailyViewHolder holder, int position) {
        DailyStudentAttendance attendance = dailyStudentAttendance.get(position);
        //holder.date.setText(attendance.date);
        holder.reason.setText(Html.fromHtml("<font color=#0D47A1><B>Reason :  <BR><B></font>"+attendance.reason));
        holder.present.setText(attendance.isPresent ? "P" : "A");
        if (attendance.isPresent) {
            holder.present.setBackgroundResource(R.drawable.gradient_green_button_background);
            holder.reason.setVisibility(View.INVISIBLE);
        }else {
            holder.present.setBackgroundResource(R.drawable.gradient_red_button_background);
            holder.reason.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return dailyStudentAttendance.size();
    }

    public void clear() {
        dailyStudentAttendance.clear();
        notifyDataSetChanged();
    }

    public class DailyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.present)
        TextView present;
        @BindView(R.id.reason)
        TextView reason;

        public DailyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
