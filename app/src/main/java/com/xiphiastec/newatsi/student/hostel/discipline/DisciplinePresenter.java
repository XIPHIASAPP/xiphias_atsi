package com.xiphiastec.newatsi.student.hostel.discipline;

import com.xiphiastec.newatsi.base.AbsStudentPresenter;

/**
 * Created by Admin on 25-04-2017.
 */

public class DisciplinePresenter extends AbsStudentPresenter implements DisciplineContract.Presenter {

    private DisciplineContract.View mView;

    public DisciplinePresenter(DisciplineContract.View view) {
        mView = view;
    }

    @Override
    public void onSubscriber() {

    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }

    @Override
    public void discipline() {
        mDisposable.add(mServices.discipline()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status && response.mList.size() > 0) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> {
                        },
                        () -> mView.onComplete()));
    }

}
