package com.xiphiastec.newatsi.student.profile;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.Constants;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.StudentProfileResponse.StudentAcademic;
import com.xiphiastec.newatsi.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AcademicsFragment extends Fragment {

    private static final String TAG = "Academics";
    @BindView(R.id.tv_program_name)
    TextView mTvProgramName;
    @BindView(R.id.tv_academic_batch)
    TextView mTvAcademicBatch;
    @BindView(R.id.tv_class)
    TextView mTvClass;
    @BindView(R.id.tv_batch)
    TextView mTvBatch;
    @BindView(R.id.tv_roll_number)
    TextView mTvRollNumber;

//    @BindView(R.id.layout)
//    ScrollView mLayout;
    Unbinder unbinder;
    private StudentAcademic mAcademic;

    public AcademicsFragment() {
        // Required empty public constructor
    }

    public static AcademicsFragment newInstance(StudentAcademic academic) {
        AcademicsFragment fragment = new AcademicsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.ARGS_ACADEMICS_BUNDLE, academic);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mAcademic = getArguments().getParcelable(Constants.ARGS_ACADEMICS_BUNDLE);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_acadamics, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mAcademic != null) {
            mTvProgramName.setText(Utils.checkEmpty(mAcademic.program));
            mTvAcademicBatch.setText(Utils.checkEmpty(mAcademic.academicBatch));
            mTvClass.setText(Utils.checkEmpty(mAcademic.className));
            mTvBatch.setText(Utils.checkEmpty(mAcademic.batch));
            mTvRollNumber.setText(Utils.checkEmpty(mAcademic.rollNumberPrefix));
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.academics);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
