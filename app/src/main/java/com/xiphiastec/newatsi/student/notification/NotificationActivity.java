package com.xiphiastec.newatsi.student.notification;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsBaseActivity;
import com.xiphiastec.newatsi.model.NotificationResponse;
import com.xiphiastec.newatsi.model.StatusResponse;
import com.xiphiastec.newatsi.model.MoreNotificationResponse;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class NotificationActivity extends AbsBaseActivity
        implements NotificationContract.View{

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    private static final String TAG = "Notification";

    Context mContext;
    @BindView(R.id.recycler_view)
    EmptyViewRecyclerView mRecyclerView;
    @BindView(R.id.tv_empty)
    TextView mTvEmpty;

    boolean isNotification = true;
    @BindView(R.id.previous_notification)
    Button mPreviousNotification;
    @BindView(R.id.root_layout)
    CoordinatorLayout mRootLayout;
    Unbinder unbinder;
    AtsiProgressDialog atsiProgressDialog;

    private NotificationPresenter mPresenter;
    private NotificationAdapter mAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_notification);
        unbinder = ButterKnife.bind(this);
        mPresenter = new NotificationPresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(NotificationActivity.this,R.style.full_screen_dialog);


        mAdapter=new NotificationAdapter(NotificationActivity.this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(NotificationActivity.this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);


        mToolbar.setTitle("Notifications");
        setSupportActionBar(mToolbar);
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(view -> onBackPressed());

    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.notification();
    }

    @Override
    public void onLoading() {
        mTvEmpty.setVisibility(View.GONE);
        atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
        mTvEmpty.setVisibility(View.VISIBLE);
        mTvEmpty.setText("No Notifications");
        mRecyclerView.setVisibility(View.GONE);
        Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onComplete() {
        atsiProgressDialog.dismissDialog();
        mPreviousNotification.setEnabled(true);
    }

    @Override
    public void onResponse(StatusResponse response) {

    }


    @Override
    public void renderNotification(NotificationResponse response) {
        mRecyclerView.setVisibility(View.VISIBLE);
        mAdapter.swapData(response.mTodayList);
        Toast.makeText(NotificationActivity.this,""+response.mTodayList.size(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void renderMoreNotification(MoreNotificationResponse response) {
        mRecyclerView.setVisibility(View.VISIBLE);
        mAdapter.swapData(response.mTodayList);
    }


    @OnClick(R.id.previous_notification)
    public void onViewClicked() {

        mPresenter.oldNotification();

    }

    @Override
    protected void onStop() {
        super.onStop();
        unbinder.unbind();
        mPresenter.onUnSubscriber();
    }
}
