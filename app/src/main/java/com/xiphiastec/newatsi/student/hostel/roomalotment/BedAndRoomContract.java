package com.xiphiastec.newatsi.student.hostel.roomalotment;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.RoomAllotmentResponse;
import com.xiphiastec.newatsi.student.hostel.complainbox.ComplaintBoxContract;

/**
 * Created by Admin on 12-12-2017.
 */

public interface BedAndRoomContract {

    interface View extends BaseView<RoomAllotmentResponse> {

    }

    interface Presenter extends BasePresenter<ComplaintBoxContract.View> {
        void roomDetails();
    }
}
