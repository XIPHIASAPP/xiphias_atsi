package com.xiphiastec.newatsi.student.hostel.inoutstatus;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.StudentInOutStatus;
import com.xiphiastec.newatsi.util.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 5/3/18.
 */

class InOutStatusAdapter extends RecyclerView.Adapter<InOutStatusAdapter.InOutViewHolder> {

    private List<StudentInOutStatus> list = new ArrayList<>();
    Context context;

    InOutStatusAdapter(Context context) {
        this.context=context;
    }

    @Override
    public InOutStatusAdapter.InOutViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new InOutStatusAdapter.InOutViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_inout, parent, false));
    }

    @Override
    public void onBindViewHolder(InOutStatusAdapter.InOutViewHolder holder, int position) {
        StudentInOutStatus status = list.get(position);
        holder.inTime.setText(Utils.checkEmpty(status.inTime));
        holder.outTime.setText(Utils.checkEmpty(status.outTime));
        holder.reason.setText(Utils.checkEmpty(status.reason));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void swapData(List<StudentInOutStatus> studentInOutStatus) {
        list = studentInOutStatus;
        notifyDataSetChanged();
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }

    public class InOutViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvOut_time)
        TextView outTime;
        @BindView(R.id.tvIn_time)
        TextView inTime;
        @BindView(R.id.tvReason)
        TextView reason;

        public InOutViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
