package com.xiphiastec.newatsi.student.profile;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsStudentBaseFragment;
import com.xiphiastec.newatsi.student.activities.StudentActivity;
import com.xiphiastec.newatsi.model.StudentProfileResponse;
import com.xiphiastec.newatsi.model.StudentProfileResponse.StudentDocument;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ProfileFragment extends AbsStudentBaseFragment implements
        ProfileContract.View {

    Unbinder unbinder;
    @BindView(R.id.root_layout)
    FrameLayout mRootLayout;
    private StudentProfilePresenter mPresenter;
    private StudentProfileResponse mResponse;
    AtsiProgressDialog atsiProgressDialog;


    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new StudentProfilePresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_student_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onSubscriber();
    }

    @Override
    public void onLoading() {
       atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
        Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
        Snackbar.make(mRootLayout, error, Snackbar.LENGTH_SHORT)
                .setAction("Retry", v -> mPresenter.profile()).show();
    }

    @Override
    public void onResponse(StudentProfileResponse response) {
        mResponse = response;
    }

    @Override
    public void onComplete() {
       atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onUnSubscriber();
    }

    @OnClick({R.id.academics, R.id.profile_details, R.id.document})
    public void onViewClicked(View view) {
        if (mResponse == null) {
            Toast.makeText(getContext(), "Please try again!", Toast.LENGTH_SHORT).show();
            return;
        }
        Fragment fragment;
        switch (view.getId()) {
            case R.id.academics:
                fragment = AcademicsFragment.newInstance(mResponse.studentAcademic);
                break;
            case R.id.profile_details:
                fragment = ProfileDetailsFragment
                        .newInstance(mResponse.mProfileInfo, mResponse.mAppliedDetails);
                break;
            default:
            case R.id.document:
                fragment = DocumentsUploadedFragment
                        .newInstance((ArrayList<StudentDocument>) mResponse.mDocument);
                break;
        }
        if (getActivity() != null) {
            ((StudentActivity) getActivity()).changeFragment(fragment, true);
        }
    }
}
