package com.xiphiastec.newatsi.student.hostel.complainbox;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.xiphiastec.newatsi.base.AbsStudentPresenter;
import com.xiphiastec.newatsi.model.ComplaintBoxBody;
import com.xiphiastec.newatsi.model.StatusResponse;


/**
 * Created by Admin on 01-04-2017.
 */

public class ComplaintBoxPresenter extends AbsStudentPresenter implements ComplaintBoxContract.Presenter {

    private ComplaintBoxContract.View mView;

    ComplaintBoxPresenter(ComplaintBoxContract.View view) {
        mView = view;
    }

    @Override
    public void onSubscriber() {

    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }

    @Override
    public void submitComplaint(@NonNull String date,
                                @NonNull String complaint,
                                @NonNull String place,
                                @NonNull String category,
                                @NonNull String personName,
                                @Nullable String imageString) {

        if (date.equals("Select date")) {
            mView.onShowError("Please select date.");
            return;
        }
        if (TextUtils.isEmpty(personName)) {
            mView.onShowError("Please enter person name.");
            return;
        }
        if (TextUtils.isEmpty(place)) {
            mView.onShowError("Please enter place of occurrence.");
            return;
        }
        if (category.equals("--Select category--")) {
            mView.onShowError("Please select category.");
            return;
        }
        if (TextUtils.isEmpty(complaint)) {
            mView.onShowError("Please enter your complaint.");
            return;
        }

        ComplaintBoxBody complaintBoxBody = new
                ComplaintBoxBody(complaint, date, place, category, personName, imageString);

        mDisposable.add(mServices.submitComplaint(complaintBoxBody)
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> {
                        },
                        () -> mView.onComplete()));
    }


    public static class ComplaintBoxResponse extends StatusResponse {

    }



}
