package com.xiphiastec.newatsi.student.attendance.dailyattendance;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.AttendanceStudent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 5/3/18.
 */

class SubjectAttendanceAdapter extends RecyclerView.Adapter<SubjectAttendanceAdapter.SubjectViewHolder> {

    private List<AttendanceStudent> list = new ArrayList<>();
    Context context;

    public SubjectAttendanceAdapter(Context context) {
        this.context =context;
    }

    @Override
    public SubjectAttendanceAdapter.SubjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SubjectAttendanceAdapter.SubjectViewHolder(LayoutInflater.from(context).inflate(R.layout.item_attendance, parent, false));
    }

    @Override
    public void onBindViewHolder(SubjectAttendanceAdapter.SubjectViewHolder holder, int position) {
        AttendanceStudent attendance = list.get(position);

        holder.tvReason.setText(Html.fromHtml("<font color=#0D47A1><B>Reason :  <B></font>"+attendance.reason));
        holder.tvStatus.setText(attendance.isPresent ? "P" : "A");
        holder.tvSubjectName.setText(attendance.subjectName);

        if (attendance.isPresent) {
            holder.tvStatus.setBackgroundResource(R.drawable.gradient_green_button_background);
            holder.tvReason.setVisibility(View.GONE);
        }else {
            holder.tvStatus.setBackgroundResource(R.drawable.gradient_red_button_background);
            holder.tvReason.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }

    void swapData(List<AttendanceStudent> attendanceStudent) {
        list = attendanceStudent;
        notifyDataSetChanged();
    }

    public class SubjectViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_status)
        TextView tvStatus;
        @BindView(R.id.tv_subject_name)
        TextView tvSubjectName;
        @BindView(R.id.tv_reason)
        TextView tvReason;

        public SubjectViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}