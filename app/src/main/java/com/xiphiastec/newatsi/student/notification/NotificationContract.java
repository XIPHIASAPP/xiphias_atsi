package com.xiphiastec.newatsi.student.notification;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.NotificationResponse;
import com.xiphiastec.newatsi.model.MoreNotificationResponse;


/**
 * Created by Admin on 13-12-2017.
 */

public interface NotificationContract {

    interface View extends BaseView {
        void renderNotification(NotificationResponse response);
        void renderMoreNotification(MoreNotificationResponse response);
    }

    interface Presenter extends BasePresenter<View> {
        void notification();
        void oldNotification();
    }
}
