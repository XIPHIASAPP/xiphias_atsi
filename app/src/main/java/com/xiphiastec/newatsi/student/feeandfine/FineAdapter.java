package com.xiphiastec.newatsi.student.feeandfine;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.FeeDetailsResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by xiphias0101 on 20/02/2018.
 */

public class FineAdapter extends RecyclerView.Adapter<FineAdapter.FineViewHolder>{
    List<FeeDetailsResponse.FineDetail> fineDetails;
    private Context context;

        public FineAdapter(Context context, List<FeeDetailsResponse.FineDetail> fineDetails){
            this.context=context;
            this.fineDetails=fineDetails;

        }

    @Override
    public FineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =LayoutInflater.from(context).inflate(R.layout.item_fine_detail, parent, false);
        return new FineViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FineViewHolder holder, int position) {
        FeeDetailsResponse.FineDetail fineDetail = fineDetails.get(position);
        holder.feeAmount.setText(fineDetail.amount);
        holder.feeName.setText(fineDetail.fineDescription);
        holder.paidDate.setText(fineDetail.fineStartDate);
        holder.paymentType.setText(fineDetail.fineType);
    }


    @Override
    public int getItemCount() {
        return fineDetails.size();
    }

    public class FineViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.fee_name)
        TextView feeName;
        @BindView(R.id.payment_type)
        TextView paymentType;
        @BindView(R.id.paid_date)
        TextView paidDate;
        @BindView(R.id.fee_amount)
        TextView feeAmount;

        public FineViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
