package com.xiphiastec.newatsi.student.hallticket;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.ExamDetail;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 5/3/18.
 */

class HallTicketTimeTableAdapter extends
        RecyclerView.Adapter<HallTicketTimeTableAdapter.HallTicketViewHolder> {

            private final List<ExamDetail> examDetails;
            Context context;

        public HallTicketTimeTableAdapter(Context context,List<ExamDetail> examDetails) {
            this.examDetails = examDetails;
            this.context=context;
        }

    @Override
    public HallTicketTimeTableAdapter.HallTicketViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HallTicketTimeTableAdapter.HallTicketViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.hallticket_item, parent, false));
    }

    @Override
    public void onBindViewHolder(HallTicketTimeTableAdapter.HallTicketViewHolder holder, int position) {
        ExamDetail examDetail = examDetails.get(position);
        holder.mDate.setText(examDetail.examinationDate);
        holder.mSubject.setText(examDetail.subjectName);
    }

    @Override
    public int getItemCount() {
        return examDetails.size();
    }

    public class HallTicketViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvSubject)
        TextView mSubject;
        @BindView(R.id.tvDate)
        TextView mDate;

        public HallTicketViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
