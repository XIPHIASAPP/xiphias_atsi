package com.xiphiastec.newatsi.student.attendance.dailyattendance;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.DailyAttendanceResponse;
import com.xiphiastec.newatsi.model.SubjectAttendanceResponse;

public interface DailyAttendanceContract {
    interface View extends BaseView {
        void renderDailyResponse(DailyAttendanceResponse response);
        void renderSubjectResponse(SubjectAttendanceResponse response);
    }

    interface Presenter extends BasePresenter{
        void getSubjectAttendence(String date);
        void getDailyAttendence(String date);
    }
}
