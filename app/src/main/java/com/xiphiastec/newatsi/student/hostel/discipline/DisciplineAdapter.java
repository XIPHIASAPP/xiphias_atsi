package com.xiphiastec.newatsi.student.hostel.discipline;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.DisciplineViolation;
import com.xiphiastec.newatsi.util.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 5/3/18.
 */

class DisciplineAdapter extends RecyclerView.Adapter<DisciplineAdapter.DisciplineHolder> {

    private List<DisciplineViolation> mList = new ArrayList<>();
    Context context;

    public DisciplineAdapter(Context context){
        this.context=context;
    }

    @Override
    public DisciplineAdapter.DisciplineHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DisciplineAdapter.DisciplineHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_discipline, parent, false));
    }

    @Override
    public void onBindViewHolder(DisciplineAdapter.DisciplineHolder holder, int position) {
        DisciplineViolation violation = mList.get(position);
        holder.mTvDay.setText(Utils.getDay(violation.date));
        holder.mTvMon.setText(Utils.getMonth(violation.date));
        holder.mFine.setText(violation.fine);
        holder.mDiscipline.setText(violation.issue);

        if(position%2==0){
            holder.llDisciplinelayout.setBackgroundResource(R.color.noticeEven);
        }else{
            holder.llDisciplinelayout.setBackgroundResource(R.color.noticeOdd);
        }


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void swapData(List<DisciplineViolation> list) {
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }

    class DisciplineHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_discipline)
        TextView mDiscipline;
        @BindView(R.id.tv_fine)
        TextView mFine;
        @BindView(R.id.tv_day)
        TextView mTvDay;
        @BindView(R.id.tv_mon)
        TextView mTvMon;
        @BindView(R.id.llDisciplinelayout)
        LinearLayout llDisciplinelayout;
        DisciplineHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
