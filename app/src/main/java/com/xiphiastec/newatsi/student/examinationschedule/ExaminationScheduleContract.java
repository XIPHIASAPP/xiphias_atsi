package com.xiphiastec.newatsi.student.examinationschedule;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.ExaminationScheduleResponse;

public interface ExaminationScheduleContract {

    interface View extends BaseView<ExaminationScheduleResponse> {

    }

    interface Presenter extends BasePresenter<View> {
        void examSchedule();
    }
}
