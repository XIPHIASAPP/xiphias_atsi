package com.xiphiastec.newatsi.student.hostel.inoutstatus;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.InOutStatusResponse;
import com.xiphiastec.newatsi.model.StudentDetailsBody;
import com.xiphiastec.newatsi.student.hostel.complainbox.ComplaintBoxContract;

/**
 * Created by Admin on 12-12-2017.
 */

public interface InOutStatusContract {
    interface View extends BaseView<InOutStatusResponse> {

    }

    interface Presenter extends BasePresenter<ComplaintBoxContract.View> {
        void inOutStatus(StudentDetailsBody date);
    }
}
