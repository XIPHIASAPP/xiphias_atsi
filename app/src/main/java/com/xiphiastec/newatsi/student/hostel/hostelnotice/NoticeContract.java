package com.xiphiastec.newatsi.student.hostel.hostelnotice;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.StudentDetailsBody;
import com.xiphiastec.newatsi.model.StudentHostelNoticeResponse;

/**
 * Created by Admin on 13-12-2017.
 */

public interface NoticeContract {

    interface View extends BaseView<StudentHostelNoticeResponse> {

    }

    interface Presenter extends BasePresenter<View> {
        void noticeWithDate(StudentDetailsBody date);
    }

}
