package com.xiphiastec.newatsi.base.interfaces;

import com.xiphiastec.newatsi.model.StatusResponse;
import com.xiphiastec.newatsi.login.presenter.ChangePasswordPresenter;
import com.xiphiastec.newatsi.login.presenter.ForgotPresenter;
import com.xiphiastec.newatsi.login.presenter.LoginPresenter;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Admin on 20-03-2017.
 */

public interface CommonApiService {
    @POST("login")
    Observable<LoginPresenter.LoginResponse> login(@Body LoginPresenter.LoginBody body);

    @POST("forgotpassword")
    Observable<ForgotPresenter.ForgotResponse> forgotPassword(@Body ForgotPresenter.ForgotPasswordBody body);

    @POST("forgotpassword")
    Observable<ForgotPresenter.ForgotResponse> confirmForgotPassword(@Body ForgotPresenter.ForgotPasswordBody body);

    @POST("ChangePassword")
    Observable<StatusResponse> changePassword(@Body ChangePasswordPresenter.ChangePasswordBody changePasswordBody);
}
