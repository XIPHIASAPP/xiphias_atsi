package com.xiphiastec.newatsi.base;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.app.ATSIApplication;
import com.xiphiastec.newatsi.base.interfaces.CommonApiService;
import com.xiphiastec.newatsi.provider.InjectionImpl;

/**
 * Created by xiphias0101 on 09/12/2017.
 */

public abstract class AbsCommonPresenter extends AbsBasePresenter {
    @NonNull
    protected CommonApiService mService;

    public AbsCommonPresenter() {
        mService = new InjectionImpl().getCommonApiService(ATSIApplication.getInstance());
    }
}
