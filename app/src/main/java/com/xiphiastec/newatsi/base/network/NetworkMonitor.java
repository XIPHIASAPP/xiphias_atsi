package com.xiphiastec.newatsi.base.network;

public interface NetworkMonitor {
    boolean isConnected();
}