package com.xiphiastec.newatsi.base.interfaces;

import com.xiphiastec.newatsi.model.ChildStaticsResponse;
import com.xiphiastec.newatsi.model.ParentAcademicsResponse;
import com.xiphiastec.newatsi.model.ParentDashboardResponse;
import com.xiphiastec.newatsi.model.StudentDetailsBody;


import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ParentApiServices {
    @GET("Parentdetails")
    Observable<ParentDashboardResponse> dashboard();

    @POST("parentacademics")
    Observable<ParentAcademicsResponse> academics(@Body StudentDetailsBody body);

    @POST("parentstatistics")
    Observable<ChildStaticsResponse> statistics(@Body StudentDetailsBody body);

}
