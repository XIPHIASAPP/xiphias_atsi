package com.xiphiastec.newatsi.base.interfaces;

/**
 * Created by xiphias0101 on 09/12/2017.
 */

public interface BasePresenter<V extends BaseView> {

    void onSubscriber();

    void onUnSubscriber();
}
