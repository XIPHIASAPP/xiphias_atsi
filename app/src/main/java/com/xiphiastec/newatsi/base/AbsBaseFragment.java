package com.xiphiastec.newatsi.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.xiphiastec.newatsi.util.KeyboardUtil;

/**
 * Created by xiphias0101 on 09/12/2017.
 */

public abstract class AbsBaseFragment extends Fragment {
    protected void snackBarMessage(@NonNull View view, @StringRes int message) {
        snackBarMessage(view, getString(message));
    }

    @Nullable
    public ActionBar getActionBar() {
        return getActivity() != null ? ((AppCompatActivity) getActivity()).getSupportActionBar() : null;
    }

    protected void hideKeyboard(View view) {
        new KeyboardUtil().hideKeyboard(view, getContext());
    }

    protected void snackBarMessage(@NonNull View view, @NonNull String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT)
                .show();
    }
}
