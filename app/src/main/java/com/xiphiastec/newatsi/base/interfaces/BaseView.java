package com.xiphiastec.newatsi.base.interfaces;

import com.xiphiastec.newatsi.model.StatusResponse;

/**
 * Created by xiphias0101 on 09/12/2017.
 */

public interface BaseView<T extends StatusResponse> {
    void onLoading();

    void onShowError(String error);

    void onComplete();

    void onResponse(T response);
}
