package com.xiphiastec.newatsi.base.interfaces;

/**
 * Created by xiphias0101 on 09/12/2017.
 */

public interface Modules {
    CommonApiService commonServices();

    ParentApiServices parentServices();

    StudentApiServices studentServices();

    EmployeeApiService employeeService();
}
