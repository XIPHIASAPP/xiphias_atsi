package com.xiphiastec.newatsi.base;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.base.network.NoNetworkException;
import com.xiphiastec.newatsi.provider.InjectionImpl;
import com.xiphiastec.newatsi.provider.schedulers.BaseSchedulerProvider;

import java.net.SocketTimeoutException;

import io.reactivex.disposables.CompositeDisposable;

public abstract class AbsBasePresenter {
    @NonNull
    protected CompositeDisposable mDisposable;
    @NonNull
    protected BaseSchedulerProvider mProvider;

    public AbsBasePresenter() {
        mProvider = new InjectionImpl().getSchedulerProvider();
        mDisposable = new CompositeDisposable();
    }

    protected String exceptionInReadableString(Throwable throwable) {
        if (throwable instanceof NoNetworkException) {
            return "No internet connection";
        } else if (throwable instanceof SocketTimeoutException) {
            return "Something wrong with server communication";
        }
        return "Something went wrong please try again!";
    }
}
