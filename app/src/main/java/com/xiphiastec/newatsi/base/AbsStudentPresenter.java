package com.xiphiastec.newatsi.base;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.app.ATSIApplication;
import com.xiphiastec.newatsi.base.interfaces.StudentApiServices;

import com.xiphiastec.newatsi.provider.InjectionImpl;

/**
 * Created by Admin on 21-04-2017.
 */

public abstract class AbsStudentPresenter extends AbsBasePresenter {  @NonNull
protected StudentApiServices mServices;

    public AbsStudentPresenter() {
        mServices = new InjectionImpl().getStudentApiService(ATSIApplication.getInstance());
    }
}
