package com.xiphiastec.newatsi.base.interfaces;

import com.xiphiastec.newatsi.model.CancelRequestBody;
import com.xiphiastec.newatsi.model.CreateDisciplinaryBody;
import com.xiphiastec.newatsi.model.DropDownActionsResponse;
import com.xiphiastec.newatsi.model.EmpDashboardResponse;
import com.xiphiastec.newatsi.model.EmpDisciplinaryResponse;
import com.xiphiastec.newatsi.model.EmpExperienceResponse;
import com.xiphiastec.newatsi.model.EmpInterViewScheduleResponse;
import com.xiphiastec.newatsi.model.EmpProfileResponse;
import com.xiphiastec.newatsi.model.EmpQualificationResponse;
import com.xiphiastec.newatsi.model.EmpSalaryResponse;
import com.xiphiastec.newatsi.model.EmpSyllabusResponse;
import com.xiphiastec.newatsi.model.EmpTimeTableResponse;
import com.xiphiastec.newatsi.model.EmpTransportationResponse;
import com.xiphiastec.newatsi.model.EmployeeAttendanceResponse;
import com.xiphiastec.newatsi.model.LeaveRequestBody;
import com.xiphiastec.newatsi.model.LeaveStatusResponse;
import com.xiphiastec.newatsi.model.NotificationResponse;
import com.xiphiastec.newatsi.model.StatusResponse;
import com.xiphiastec.newatsi.model.TypeList;
import com.xiphiastec.newatsi.model.ApplicationIDResponse;
import com.xiphiastec.newatsi.model.EmpLeaveResponse;
import com.xiphiastec.newatsi.model.Leave;
import com.xiphiastec.newatsi.model.LeaveDetails;
import com.xiphiastec.newatsi.model.LeaveType;
import com.xiphiastec.newatsi.model.EmployeeDetailsBody;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Admin on 25-04-2017.
 */

public interface EmployeeApiService {
    @GET("employeeattendance")
    Observable<EmployeeAttendanceResponse> attendance();

    @POST("employeeattendancesearchpost")
    Observable<EmployeeAttendanceResponse> attendance(@Body EmployeeDetailsBody body);

    @GET("employeeexperience")
    Observable<EmpExperienceResponse> experience();

    @GET("employeedashboardtimetable")
    Observable<EmpDashboardResponse> dashboard();

    @GET("employeeattendencesearchget")
    Observable<TypeList> attendanceTypeList();


    @GET("employeeprofile")
    Observable<EmpProfileResponse> profile();

    @GET("empsyllabuscovered")
    Observable<EmpSyllabusResponse> syllabus();

    @GET("employeetransport")
    Observable<EmpTransportationResponse> transportation();

    @GET("employeeindeciplinarycreateddropdown")
    Observable<DropDownActionsResponse> actions();

    @GET("employeedesciplinaryaction")
    Observable<EmpDisciplinaryResponse> disciplinary();

    @POST("employeesalaryslip")
    Observable<EmpSalaryResponse> salarySlip(@Body EmployeeDetailsBody salaryDate);

    @GET("scheduleinterview")
    Observable<EmpInterViewScheduleResponse> interview();

    @POST("employeeindisciplinarycreated")
    Observable<StatusResponse> createDisciplinary(@Body CreateDisciplinaryBody disciplinary);

    @GET("todaynotification")
    Observable<NotificationResponse> notification();

    @GET("viewmorenotification")
    Observable<NotificationResponse> moreNotifications();

    @GET("employeequalificationdetails")
    Observable<EmpQualificationResponse> qualification();



    @POST("employeecreateleave")
    Observable<StatusResponse> create(@Body Leave leave);

    @GET("listleavetype")
    Observable<LeaveType> leaveTypes();

    @POST("employeeleavebalance")
    Observable<LeaveDetails> leaveDetails(@Body EmployeeDetailsBody leaveId);

    @GET("employeetimetable")
    Observable<EmpTimeTableResponse> timeTable();

    @GET("employeecancelleavelist")
    Observable<EmpLeaveResponse> cancelLeave();

    @GET("generateapplicationid")
    Observable<ApplicationIDResponse> applicationIds();

    //Employee Leave Services
    @GET("leavestatus")
    Observable<LeaveStatusResponse> getLeaveStatus();

    //Employee leave details
    @POST("employeeleavedetails")
    Observable<EmpLeaveResponse> leaveDetails(@Body LeaveRequestBody body);

    //Employee cancel leave
    @POST("employeecreatecancelleave")
    Observable<EmpLeaveResponse> cancelLeave(@Body CancelRequestBody body);

}
