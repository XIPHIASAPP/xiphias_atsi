package com.xiphiastec.newatsi.base;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.xiphiastec.newatsi.BuildConfig;
import com.xiphiastec.newatsi.base.interfaces.CommonApiService;
import com.xiphiastec.newatsi.base.interfaces.EmployeeApiService;
import com.xiphiastec.newatsi.base.interfaces.Modules;
import com.xiphiastec.newatsi.base.interfaces.ParentApiServices;
import com.xiphiastec.newatsi.base.interfaces.StudentApiServices;
import com.xiphiastec.newatsi.base.network.LiveNetworkMonitor;
import com.xiphiastec.newatsi.base.network.NoNetworkException;
import com.xiphiastec.newatsi.util.PreferenceUtil;

import java.io.File;
import java.util.concurrent.TimeUnit;

import io.reactivex.schedulers.Schedulers;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkingClient implements Modules {
    private static NetworkingClient network;
    private Retrofit mRetrofit;
    private Context mContext;
    private LiveNetworkMonitor mMonitor;

    private NetworkingClient(@NonNull Context context,
                             @NonNull String baseurl) {
        mContext = context;
        mMonitor = new LiveNetworkMonitor(context);

        mRetrofit = new Retrofit.Builder()
                .baseUrl(baseurl)
                .client(buildOkHttp3Client())
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();

    }

    public static NetworkingClient getInstance(@NonNull Context context,
                                               @NonNull String baseurl) {
        if (network == null) {
            network = new NetworkingClient(context, baseurl);
        }
        return network;
    }

    private OkHttpClient buildOkHttp3Client() {
        OkHttpClient.Builder httpBuilder = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .readTimeout(90, TimeUnit.SECONDS);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.HEADERS : HttpLoggingInterceptor.Level.NONE);
        interceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);

        return httpBuilder
                .addInterceptor(chain -> {
                    if (mMonitor.isConnected()) {
                        return chain.proceed(chain.request());
                    } else {
                        throw new NoNetworkException();
                    }
                })
                .addInterceptor(interceptor)
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Cache-Control", "public, max-age=" + 60)
                            .header("token", PreferenceUtil.getInstance(mContext).getToken());
                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                })
                .cache(createOkHttpCache())
                .build();
    }

    private Cache createOkHttpCache() {
        File httpCacheDirectory = new File(mContext.getCacheDir(), "http");
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        return new Cache(httpCacheDirectory, cacheSize);
    }

    @Override
    public CommonApiService commonServices() {
        return mRetrofit.create(CommonApiService.class);
    }

    @Override
    public ParentApiServices parentServices() {
        return mRetrofit.create(ParentApiServices.class);
    }

    @Override
    public EmployeeApiService employeeService() {
        return mRetrofit.create(EmployeeApiService.class);
    }

    @Override
    public StudentApiServices studentServices() {
        return mRetrofit.create(StudentApiServices.class);
    }
}
