package com.xiphiastec.newatsi.base;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.app.ATSIApplication;
import com.xiphiastec.newatsi.base.interfaces.ParentApiServices;
import com.xiphiastec.newatsi.provider.InjectionImpl;


public abstract class AbsParentPresenter extends AbsBasePresenter {
    @NonNull
    protected ParentApiServices mServices;

    public AbsParentPresenter() {
        mServices = new InjectionImpl().getParentApiService(ATSIApplication.getInstance());
    }
}
