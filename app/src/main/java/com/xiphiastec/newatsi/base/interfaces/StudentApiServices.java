package com.xiphiastec.newatsi.base.interfaces;


import com.xiphiastec.newatsi.model.ComplaintBoxBody;
import com.xiphiastec.newatsi.model.DailyAttendanceResponse;
import com.xiphiastec.newatsi.model.DashboardResponse;
import com.xiphiastec.newatsi.model.ExamTypeBody;
import com.xiphiastec.newatsi.model.ExaminationScheduleResponse;
import com.xiphiastec.newatsi.model.FeeDetailsResponse;
import com.xiphiastec.newatsi.model.GradeCardResponse;
import com.xiphiastec.newatsi.model.HallTicketResponse;
import com.xiphiastec.newatsi.model.InOutStatusResponse;
import com.xiphiastec.newatsi.model.NotificationResponse;
import com.xiphiastec.newatsi.model.RoomAllotmentResponse;
import com.xiphiastec.newatsi.model.Semester;
import com.xiphiastec.newatsi.model.StudentDisciplineResponse;
import com.xiphiastec.newatsi.model.StudentHostelNoticeResponse;
import com.xiphiastec.newatsi.model.StudentTimeTableResponse;
import com.xiphiastec.newatsi.model.SubjectAttendanceResponse;
import com.xiphiastec.newatsi.model.StudentDetailsBody;
import com.xiphiastec.newatsi.model.MoreNotificationResponse;
import com.xiphiastec.newatsi.student.hostel.complainbox.ComplaintBoxPresenter;
import com.xiphiastec.newatsi.model.StudentProfileResponse;
import com.xiphiastec.newatsi.student.transport.StudentTransportPresenter;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface StudentApiServices {
    @POST("studenthostelinoutstatus")
    Observable<InOutStatusResponse> inOutStatus(@Body StudentDetailsBody body);

    @GET("studentstatistics")
    Observable<DashboardResponse> getDashboardInfo();

    @GET("studentdailyattendance")
    Observable<DailyAttendanceResponse> getDailyAttendance();

    @POST("studentdailyattendance")
    Observable<DailyAttendanceResponse> getDailyAttendanceWithDate(@Body Map<String, Object> map);

    @POST("studentattendancedetails")
    Observable<SubjectAttendanceResponse> getSubjectAttendance();

    @POST("studentattendancedetails")
    Observable<SubjectAttendanceResponse> getSubjectAttendanceWith(@Body Map<String, Object> map);

    @GET("studentroomdetails")
    Observable<RoomAllotmentResponse> getRoomAllotment();

    @POST("studentcomplaintbox")
    Observable<ComplaintBoxPresenter.ComplaintBoxResponse> submitComplaint(@Body ComplaintBoxBody body);

    @GET("studentprofile")
    Observable<StudentProfileResponse> profile();

    @GET("todaynotification")
    Observable<NotificationResponse> getNotifications();

    @GET("studenttimetable")
    Observable<StudentTimeTableResponse> getTimeTable();

    @GET("studenttransportdetails")
    Observable<StudentTransportPresenter.StudentTransportResponse> transportDetails();

    @POST("studentgrade")
    Observable<GradeCardResponse> getGradeCardDetails(@Body ExamTypeBody examTypeBody);

    @GET("studentexamschedule")
    Observable<ExaminationScheduleResponse> examSchedule();

    @POST("studenthostelnotice")
    Observable<StudentHostelNoticeResponse> noticeWithDate(@Body StudentDetailsBody body);

    @GET("studentdisciplineviolation")
    Observable<StudentDisciplineResponse> discipline();

    @GET("viewmorenotification")
    Observable<NotificationResponse> oldNotification();

    @GET("studenthallticket")
    Observable<HallTicketResponse> hallTicket();

    @GET("studentfeedetails")
    Observable<FeeDetailsResponse> feeFine();

    @GET("studentexamtype")
    Observable<Semester> examTypes();


    //notifications
    @GET("todaynotification")
    Observable<NotificationResponse> notification();

    @GET("viewmorenotification")
    Observable<MoreNotificationResponse> moreNotifications();


}
