package com.xiphiastec.newatsi.login.contract;

import com.xiphiastec.newatsi.model.StatusResponse;
import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;

/**
 * Created by xiphias0101 on 09/12/2017.
 */

public interface ChangePasswordContract {
    interface View extends BaseView<StatusResponse> {

    }

    interface Presenter extends BasePresenter<LoginContract.View> {
        void changePassword(String password, String password2, String password3);
    }
}
