package com.xiphiastec.newatsi.login;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageButton;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.login.contract.ForgotContract;
import com.xiphiastec.newatsi.login.presenter.ForgotPresenter;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;

public class ForgotPasswordActivity extends AppCompatActivity implements ForgotContract.View{
    @BindView(R.id.et_email)
    TextInputEditText mEtEmail;
    @BindView(R.id.ib_back)
    AppCompatImageButton mIbBack;
    @BindView(R.id.container)
    FrameLayout mContainer;
    @BindView(R.id.forgot_password)
    Button mForgotPassword;
    @BindView(R.id.et_password)
    TextInputEditText mEtPassword;
    @BindView(R.id.et_confirm_password)
    TextInputEditText mEtConfirmPassword;
    @BindView(R.id.fields)
    LinearLayout mFields;
    Unbinder unbinder;
    private CompositeDisposable mCompositeDisposable;
    private ForgotPresenter mPresenter;
    private boolean isPasswordVerified;
    AtsiProgressDialog atsiProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        mPresenter = new ForgotPresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(ForgotPasswordActivity.this,R.style.full_screen_dialog);
        mCompositeDisposable = new CompositeDisposable();

    }

    @Override
    protected void onStart() {
        super.onStart();
        unbinder = ButterKnife.bind(this);
    }

    @Override
    public void onLoading() {
        atsiProgressDialog.showDialog();
        mForgotPassword.setEnabled(false);
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
        Snackbar.make(mContainer,error,Snackbar.LENGTH_SHORT).show();
        if(mEtEmail.getVisibility()==View.VISIBLE)
                mEtEmail.setError(error);
    }

    @Override
    public void onComplete() {
        atsiProgressDialog.dismissDialog();
        mForgotPassword.setEnabled(true);
    }

    @Override
    public void onResponse(ForgotPresenter.ForgotResponse response) {
        if (!isPasswordVerified && response.success) {
            mEtEmail.setText("");
            mEtEmail.setVisibility(View.VISIBLE);
            mEtPassword.setVisibility(View.GONE);
            mEtConfirmPassword.setVisibility(View.GONE);
        } else {
            mEtEmail.setVisibility(View.GONE);
            mEtPassword.setVisibility(View.VISIBLE);
            mEtConfirmPassword.setVisibility(View.VISIBLE);
        }
    }

    @OnClick({R.id.ib_back, R.id.forgot_password})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ib_back:
                onBackPressed();
                break;

            case R.id.forgot_password:

                if (mEtEmail.getVisibility() == View.VISIBLE) {
                    if(!mEtEmail.getText().toString().isEmpty())
                        mPresenter.forgot(mEtEmail.getText().toString());
                    else
                        mEtEmail.setError("Please enter valid email Id");
                } else {
                    String password = mEtPassword.getText().toString();
                    String confirmPassword = mEtConfirmPassword.getText().toString();
                    if (TextUtils.isEmpty(password) || TextUtils.isEmpty(confirmPassword)) {
                        Toast.makeText(ForgotPasswordActivity.this, "Enter all fields!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    isPasswordVerified = true;
                    mPresenter.changePassword(password, confirmPassword);
                }

                View view1 = ForgotPasswordActivity.this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.dispose();
        }
        unbinder.unbind();
    }
}
