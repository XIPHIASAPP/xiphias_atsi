package com.xiphiastec.newatsi.login;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsBaseFragment;
import com.xiphiastec.newatsi.login.contract.LoginContract;
import com.xiphiastec.newatsi.login.presenter.LoginPresenter;
import com.xiphiastec.newatsi.parent.activities.ParentActivity;
import com.xiphiastec.newatsi.student.activities.StudentActivity;
import com.xiphiastec.newatsi.teacher.activities.TeachersActivity;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;
import com.xiphiastec.newatsi.util.PreferenceUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

public class LoginFragment extends AbsBaseFragment implements LoginContract.View {

    @BindView(R.id.et_email)
    TextInputEditText mEtEmail;
    @BindView(R.id.et_password)
    TextInputEditText mEtPassword;

    @BindView(R.id.activity_login)
    CoordinatorLayout root;
    @BindView(R.id.sign_in)
    Button mSignIn;
    private LoginPresenter mLoginPresenter;
    private Unbinder mUnbinder;
    private CompositeDisposable mCompositeDisposable;
    AtsiProgressDialog atsiProgressDialog;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLoginPresenter = new LoginPresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //initialize();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mCompositeDisposable == null) {
            return;
        }
        mCompositeDisposable.clear();
    }

    private void initialize() {
        Observable<Boolean> emailValid = RxTextView.afterTextChangeEvents(mEtEmail)
                .map(textViewAfterTextChangeEvent -> textViewAfterTextChangeEvent.view().getText())
                .map(charSequence -> Patterns.EMAIL_ADDRESS.matcher(charSequence).matches());

        Observable<Boolean> passwordValid = RxTextView.afterTextChangeEvents(mEtPassword)
                .map(textViewAfterTextChangeEvent -> textViewAfterTextChangeEvent.view().getText().length() > 4);

        Observable<Boolean> loginEnabled = Observable.combineLatest(emailValid, passwordValid, (a, b) -> a && b);

        mCompositeDisposable = new CompositeDisposable();
        mCompositeDisposable.add(loginEnabled.subscribe(aBoolean -> mSignIn.setEnabled(aBoolean)));

        mEtPassword.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH
                    || actionId == EditorInfo.IME_ACTION_DONE
                    || event.getAction() == KeyEvent.ACTION_DOWN
                    && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                checkLogin(v);
                return true;
            }
            return false;
        });
    }

    @OnClick({R.id.tv_forgot_password, R.id.sign_in})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_in:
                checkLogin(view);
                break;
            case R.id.tv_forgot_password:
                if ((getActivity()) != null) {
                    ((LoginActivity) getActivity()).openForgotPassword();
                }
                break;
        }
    }

    private void checkLogin(View view) {
        String email = mEtEmail.getText().toString();
        String password = mEtPassword.getText().toString();
        hideKeyboard(view);
        mLoginPresenter.login(email, password);
    }

    @Override
    public void onLoading() {
        atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(root.getWindowToken(), 0);
        snackBarMessage(getView(), error);
//        mSignIn.setText(R.string.login);
        mEtPassword.setError(error);
    }

    @Override
    public void onResponse(LoginPresenter.LoginResponse response) {
        Activity activity = getActivity();
        if (activity == null) {
            return;
        }
        PreferenceUtil.getInstance(activity).setLoginDetails(response);
        if (response.isParent) {
            startActivity(new Intent(activity, ParentActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
        } else if (response.isStudent) {
            startActivity(new Intent(activity, StudentActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
        } else if (response.isTeacher) {
            startActivity(new Intent(activity, TeachersActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
        } else if (response.isAdmin) {
               /*TODO: Start Admin Panel*/
            Toast.makeText(getContext(), "Admin panel not implemented", Toast.LENGTH_SHORT).show();
        }
        activity.finish();

    }

    @Override
    public void onComplete() {
        atsiProgressDialog.dismissDialog();
//        mSignIn.setEnabled(true);
    }
}
