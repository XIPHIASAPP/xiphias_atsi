package com.xiphiastec.newatsi.login.contract;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.login.presenter.ForgotPresenter;

/**
 * Created by xiphias0101 on 09/12/2017.
 */

public interface ForgotContract {
    interface View extends BaseView<ForgotPresenter.ForgotResponse> {

    }

    interface Presenter extends BasePresenter<LoginContract.View> {
        void forgot(String email);

        void changePassword(String password, String password2);
    }
}
