package com.xiphiastec.newatsi.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsBaseActivity;
import com.xiphiastec.newatsi.parent.activities.ParentActivity;
import com.xiphiastec.newatsi.student.activities.StudentActivity;
import com.xiphiastec.newatsi.teacher.activities.TeachersActivity;
import com.xiphiastec.newatsi.util.PreferenceUtil;

/**
 * Created by xiphias0101 on 09/12/2017.
 */

public class LoginActivity extends AbsBaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (PreferenceUtil.getInstance(this).getParentLogin()) {
            startWithBaseOnRole(ParentActivity.class);
            return;
        } else if (PreferenceUtil.getInstance(this).getStudentLogin()) {
            startWithBaseOnRole(StudentActivity.class);
            return;
        } else if (PreferenceUtil.getInstance(this).getTeacherLogin()) {
            startWithBaseOnRole(TeachersActivity.class);
            return;
        }

        setContentView(R.layout.activity_login_content);
        changeFragment(new LoginFragment(), false);
    }

    private void startWithBaseOnRole(Class c) {
        startActivity(new Intent(this, c).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        overridePendingTransition(0, 0);
        finish();
    }

    private void changeFragment(Fragment fragment, boolean hasAddBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up);
        transaction.replace(R.id.fragment_container, fragment);

        if (hasAddBackStack) {
            transaction.addToBackStack(null).commit();
            return;
        }

        transaction.commit();
    }

    public void openForgotPassword() {
        startActivity(new Intent(this, ForgotPasswordActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

}
