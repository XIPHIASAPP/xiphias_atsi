package com.xiphiastec.newatsi.login;

import android.content.Intent;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.login.contract.ChangePasswordContract;
import com.xiphiastec.newatsi.login.presenter.ChangePasswordPresenter;
import com.xiphiastec.newatsi.model.StatusResponse;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;
import com.xiphiastec.newatsi.util.PreferenceUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ChangePasswordActivity extends AppCompatActivity implements ChangePasswordContract.View {

    @BindView(R.id.et_old_password)
    TextInputEditText mEtOldPassword;
    @BindView(R.id.et_new_password)
    TextInputEditText mEtNewPassword;
    @BindView(R.id.et_confirm_password)
    TextInputEditText mEtConfirmPassword;
    @BindView(R.id.btnSubmit)
    Button mBtnSubmit;
    @BindView(R.id.root_layout)
    CoordinatorLayout mRootLayout;
    Unbinder unbinder;
    private ChangePasswordPresenter mChangePasswordPresenter;
    AtsiProgressDialog atsiProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        mChangePasswordPresenter = new ChangePasswordPresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(ChangePasswordActivity.this,R.style.full_screen_dialog);


    }

    @Override
    protected void onStart() {
        super.onStart();
        unbinder = ButterKnife.bind(this);
    }

    @Override
    public void onLoading() {
        atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();


        Snackbar.make(mRootLayout, error, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onComplete() {
        atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onResponse(StatusResponse response) {
        mEtOldPassword.setText("");
        mEtNewPassword.setText("");
        mEtConfirmPassword.setText("");

        PreferenceUtil.getInstance(ChangePasswordActivity.this).clear();
        startActivity(new Intent(ChangePasswordActivity.this, LoginActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
        Snackbar.make(mRootLayout, response.message, BaseTransientBottomBar.LENGTH_LONG).show();
    }

    @OnClick(R.id.btnSubmit)
    public void onClick() {

        if(!mEtOldPassword.getText().toString().isEmpty() &&
           !mEtNewPassword.getText().toString().isEmpty() &&
           !mEtConfirmPassword.getText().toString().isEmpty()) {

            mChangePasswordPresenter.changePassword(
                    mEtOldPassword.getText().toString(),
                    mEtNewPassword.getText().toString(),
                    mEtConfirmPassword.getText().toString());
        }else{

            if(mEtOldPassword.getText().toString().isEmpty())
                mEtOldPassword.setError("Please enter old password");
            else if(mEtNewPassword.getText().toString().isEmpty())
                mEtNewPassword.setError("Please enter new password");

            else if(mEtConfirmPassword.getText().toString().isEmpty())
                mEtConfirmPassword.setError("Please enter confirm password");
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbinder.unbind();
    }
}
