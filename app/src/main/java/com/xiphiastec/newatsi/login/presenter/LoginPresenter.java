package com.xiphiastec.newatsi.login.presenter;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Patterns;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.xiphiastec.newatsi.base.AbsCommonPresenter;
import com.xiphiastec.newatsi.model.StatusResponse;
import com.xiphiastec.newatsi.login.contract.LoginContract;

/**
 * Created by xiphias0101 on 09/12/2017.
 */

public class LoginPresenter extends AbsCommonPresenter implements LoginContract.Presenter {
    @NonNull
    private LoginContract.View mView;

    public LoginPresenter(@NonNull LoginContract.View view) {
        mView = view;
    }

    @Override
    public void onSubscriber() {

    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }

    @Override
    public void login(String email, String password) {
        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            mView.onShowError("Please enter all fields!");
            return;
        }
//        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
//            mView.onShowError("Please enter valid E-Mail ID");
//            return;
//        }
        LoginBody body = new LoginBody();
        body.setEmail(email.trim());
        body.setPassword(password.trim());

        mDisposable.add(mService.login(body)
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.success) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> {
                        },
                        () -> mView.onComplete()));
    }

    public class LoginBody {
        private String email;
        private String password;

        public void setEmail(String email) {
            this.email = email;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    public class LoginResponse extends StatusResponse {
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("password")
        @Expose
        public String password;
        @SerializedName("user_name")
        @Expose
        public String userName;
        @SerializedName("is_admin")
        @Expose
        public boolean isAdmin;
        @SerializedName("is_student")
        @Expose
        public boolean isStudent;
        @SerializedName("is_parent")
        @Expose
        public boolean isParent;
        @SerializedName("is_teacher")
        @Expose
        public boolean isTeacher;
        @SerializedName("site_id")
        @Expose
        public int siteId;
        @SerializedName("organisation_id")
        @Expose
        public int organisationId;
        @SerializedName("student_id")
        @Expose
        public int studentId;
        @SerializedName("parent_id")
        @Expose
        public int parentId;
        @SerializedName("teacher_id")
        @Expose
        public int teacherId;
        @SerializedName("token")
        @Expose
        public String token;
    }
}
