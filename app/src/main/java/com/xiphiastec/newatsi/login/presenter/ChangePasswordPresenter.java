package com.xiphiastec.newatsi.login.presenter;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.base.AbsCommonPresenter;
import com.xiphiastec.newatsi.login.contract.ChangePasswordContract;

/**
 * Created by xiphias0101 on 09/12/2017.
 */

public class ChangePasswordPresenter extends AbsCommonPresenter
        implements ChangePasswordContract.Presenter {
    @NonNull
    private ChangePasswordContract.View mView;

    public ChangePasswordPresenter(@NonNull ChangePasswordContract.View view) {
        mView = view;
    }

    @Override
    public void onSubscriber() {

    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }

    @Override
    public void changePassword(String password, String password2, String password3) {
        ChangePasswordBody body = new ChangePasswordBody();
        body.setOldPassword(password);
        body.setNewPassword(password2);
        body.setComparePassword(password3);
        mDisposable.add(mService.changePassword(body)
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.success) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> {
                        },
                        () -> mView.onComplete()));
    }

    public class ChangePasswordBody {
        String old_password;
        String new_password;
        String compare_password;

        void setOldPassword(String old_password) {
            this.old_password = old_password;
        }

        void setNewPassword(String new_password) {
            this.new_password = new_password;
        }

        void setComparePassword(String compare_password) {
            this.compare_password = compare_password;
        }
    }
}
