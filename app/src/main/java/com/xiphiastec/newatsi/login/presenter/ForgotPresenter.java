package com.xiphiastec.newatsi.login.presenter;

import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.xiphiastec.newatsi.base.AbsCommonPresenter;
import com.xiphiastec.newatsi.model.StatusResponse;
import com.xiphiastec.newatsi.login.contract.ForgotContract;

/**
 * Created by xiphias0101 on 09/12/2017.
 */

public class ForgotPresenter extends AbsCommonPresenter implements ForgotContract.Presenter {
    @NonNull
    private ForgotContract.View mView;

    public ForgotPresenter(@NonNull ForgotContract.View view) {
        mView = view;
    }

    @Override
    public void onSubscriber() {

    }

    @Override
    public void onUnSubscriber() {

    }

    @Override
    public void forgot(String email) {
        ForgotPasswordBody body = new ForgotPasswordBody();
        body.setEmail(email);
        mDisposable.add(mService.forgotPassword(body)
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> {
                        },
                        () -> mView.onComplete()));
    }

    @Override
    public void changePassword(String password, String password2) {
        ForgotPasswordBody body = new ForgotPasswordBody();
        body.setPassword(password);
        body.setConfirmPassword(password2);
        mDisposable.add(mService.confirmForgotPassword(body)
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> {
                        },
                        () -> mView.onComplete()));
    }

    public class ForgotResponse extends StatusResponse {
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("password")
        @Expose
        public String password;
        @SerializedName("confirm_password")
        @Expose
        public String confirmPassword;
    }


    public class ForgotPasswordBody {
        String email;
        String password;
        String confirm_password;

        public void setEmail(String email) {
            this.email = email;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public void setConfirmPassword(String confirm_password) {
            this.confirm_password = confirm_password;
        }
    }
}
