package com.xiphiastec.newatsi.login.contract;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.login.presenter.LoginPresenter;

/**
 * Created by xiphias0101 on 09/12/2017.
 */

public interface LoginContract {
    interface View extends BaseView<LoginPresenter.LoginResponse> {

    }

    interface Presenter extends BasePresenter<View> {
        void login(String email, String password);
    }
}
