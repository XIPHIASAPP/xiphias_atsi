package com.xiphiastec.newatsi.teacher.salarydetails;

import android.app.DatePickerDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsEmployeeBaseFragment;
import com.xiphiastec.newatsi.model.EmpSalaryResponse;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.model.EmployeeDetailsBody;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class EmpSalaryFragment extends AbsEmployeeBaseFragment
        implements DatePickerDialog.OnDateSetListener, EmpSalaryContract.View {
    private static final String TAG = "EmpSalaryDetails";

    @BindView(R.id.root_layout)
    LinearLayout rootLayout;
    Unbinder unbinder;
    @BindView(R.id.from_date_text)
    AppCompatTextView fromDateText;

    @BindView(R.id.txtError)
    AppCompatTextView txtError;


    @BindViews({R.id.name,//0
            R.id.amount,//1
            R.id.deduct,//2
            R.id.gross,//3
            R.id.net_salary,//4
            R.id.month,//5
            R.id.days,//6
            R.id.absent//7

    })
    List<TextView> salaryies;

    @BindView(R.id.cvSalary)
    CardView cvSalary;

    private DatePickerDialog datePickerDialog;
    private EmpSalaryPresenter mPresenter;
    private SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    private Calendar mCalendar;

    AtsiProgressDialog atsiProgressDialog;

    public EmpSalaryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_employee_salary_details, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mPresenter = new EmpSalaryPresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);


        mCalendar = Calendar.getInstance();

//        datePickerDialog = DatePickerDialog.newInstance(
//                this,
//                mCalendar.get(Calendar.YEAR),
//                mCalendar.get(Calendar.MONTH),
//                mCalendar.get(Calendar.DAY_OF_MONTH)
//        );


        datePickerDialog= new DatePickerDialog(getActivity(), this,
                mCalendar.get(Calendar.YEAR),
                mCalendar.get(Calendar.MONTH),
                mCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());

    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        atsiProgressDialog.dismissDialog();

        String formattedDate = mSimpleDateFormat.format(mCalendar.getTime());
        fromDateText.setText(formattedDate);
        mPresenter.setToday(formattedDate);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onUnSubscriber();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.salary));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getActionBar().setTitle(SS);
        }
        mPresenter.onSubscriber();
    }

    @Override
    public void onLoading() {
        cvSalary.setVisibility(View.GONE);
        atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
        txtError.setVisibility(View.VISIBLE);
        txtError.setText(error);
        Snackbar.make(rootLayout, error, Snackbar.LENGTH_SHORT)
                .show();
    }

    @Override
    public void onResponse(EmpSalaryResponse response) {
        cvSalary.setVisibility(View.VISIBLE);
        salaryies.get(0).setText(response.employeeName);
        salaryies.get(1).setText(response.totalEarning);
        salaryies.get(2).setText(response.totalDeduction);
        salaryies.get(3).setText(response.grossAmount);
        salaryies.get(4).setText(response.netSalary);
        salaryies.get(5).setText(response.monthName);
        salaryies.get(6).setText(response.workingDays);
        salaryies.get(7).setText(response.absentDays);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.removeItem(R.id.action_notifications);
    }

    @Override
    public void onComplete() {
        atsiProgressDialog.dismissDialog();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.from_date_wise, R.id.ib_search_date})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.from_date_wise:
                //noinspection ConstantConditions
                datePickerDialog.show();
                break;
            case R.id.ib_search_date:
                cvSalary.setVisibility(View.GONE);
                String date = fromDateText.getText().toString();
                EmployeeDetailsBody body = new EmployeeDetailsBody();
                body.setDate(date);
                mPresenter.salary(body);
                break;
        }
    }

//    @Override
//    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
//        String month = String.valueOf(++monthOfYear);
//        String day = String.valueOf(dayOfMonth);
//        if (monthOfYear < 10) {
//            month = "0" + monthOfYear;
//        }
//        if (dayOfMonth < 10) {
//
//            day = "0" + dayOfMonth;
//        }
//        fromDateText.setText(year + "-" + month + "-" + day);
//    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String strMonth = String.valueOf(++month);
        String day = String.valueOf(dayOfMonth);
        if (month < 10) {
            strMonth = "0" + month;
        }
        if (dayOfMonth < 10) {

            day = "0" + dayOfMonth;
        }
        fromDateText.setText(year + "-" + strMonth + "-" + day);

    }
}
