package com.xiphiastec.newatsi.teacher.leave.presenter;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.model.CancelRequestBody;
import com.xiphiastec.newatsi.model.LeaveRequestBody;
import com.xiphiastec.newatsi.teacher.AbsEmployeePresenter;
import com.xiphiastec.newatsi.teacher.leave.contract.EmpLeaveListContract;

/**
 * Created by hemanths on 11/08/17.
 */

public class EmpLeaveListPresenter extends AbsEmployeePresenter
        implements EmpLeaveListContract.Presenter {
    @NonNull
    private EmpLeaveListContract.View mView;

    public EmpLeaveListPresenter(@NonNull EmpLeaveListContract.View view) {
        mView = view;
    }

    @Override
    public void onSubscriber() {
        //getLeaveDetails();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }

    @Override
    public void getLeaveDetails(LeaveRequestBody body) {
        mDisposable.add(mService.leaveDetails(body)
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status && response.mLeaveInformations.size() > 0) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    @Override
    public void getLeaveStatus() {
        mDisposable.add(mService.getLeaveStatus()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                    if (response.status && response.getmListStatus().size() > 0) {
                            mView.renderLeaveStatus(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    @Override
    public void cancelLeave(CancelRequestBody body) {
        mDisposable.add(mService.cancelLeave(body)
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status) {
                                mView.renderCancalStatus(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }


}
