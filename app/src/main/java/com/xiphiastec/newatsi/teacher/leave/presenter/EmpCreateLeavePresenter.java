package com.xiphiastec.newatsi.teacher.leave.presenter;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.teacher.AbsEmployeePresenter;
import com.xiphiastec.newatsi.teacher.leave.contract.EmpCreateLeaveContract;
import com.xiphiastec.newatsi.model.Leave;
import com.xiphiastec.newatsi.model.EmployeeDetailsBody;

/**
 * Created by hemanths on 12/08/17.
 */

public class EmpCreateLeavePresenter extends AbsEmployeePresenter
        implements EmpCreateLeaveContract.Presenter {
    @NonNull
    private EmpCreateLeaveContract.View mView;

    public EmpCreateLeavePresenter(@NonNull EmpCreateLeaveContract.View view) {
        mView = view;
    }

    @Override
    public void leaveTypes() {
        mDisposable.add(mService.leaveTypes()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {

                                mView.onLeaveTypesResponse(response);

                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    @Override
    public void createLeave(Leave leave) {
        mDisposable.add(mService.create(leave)
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    @Override
    public void leaveDetails(EmployeeDetailsBody body) {
        mDisposable.add(mService.leaveDetails(body)
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status && response.leaveBalance.size() > 0) {
                                mView.onLeaveDetailsResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    @Override
    public void onSubscriber() {
        leaveTypes();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }
}
