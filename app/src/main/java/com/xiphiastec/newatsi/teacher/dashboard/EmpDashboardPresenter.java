package com.xiphiastec.newatsi.teacher.dashboard;

import android.support.annotation.NonNull;


import com.xiphiastec.newatsi.teacher.AbsEmployeePresenter;


/**
 * Created by hemanths on 10/08/17.
 */

public class EmpDashboardPresenter extends AbsEmployeePresenter
        implements EmpDashboardContract.Presenter {
    @NonNull
    private EmpDashboardContract.View mView;

    EmpDashboardPresenter(@NonNull EmpDashboardContract.View view) {
        mView = view;
    }

    public void dashboard() {
        mDisposable.add(mService.dashboard()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {

                                mView.renderTimetable(response);

                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    @Override
    public void onSubscriber() {
        dashboard();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }


}
