package com.xiphiastec.newatsi.teacher.transport;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.EmployeeTransportInformation;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 5/3/18.
 */

class EmpTransportationAdapter extends RecyclerView.Adapter<EmpTransportationAdapter.TransViewHolder> {

    private List<EmployeeTransportInformation> list = new ArrayList<>();
    Context context;

    EmpTransportationAdapter(Context context,List<EmployeeTransportInformation> employeeTransportInformation) {
        list = employeeTransportInformation;
        this.context=context;
    }

    @Override
    public EmpTransportationAdapter.TransViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EmpTransportationAdapter.TransViewHolder(LayoutInflater.from(context).inflate(R.layout.item_emp_transport, parent, false));
    }

    @Override
    public void onBindViewHolder(EmpTransportationAdapter.TransViewHolder holder, int position) {
        EmployeeTransportInformation information = list.get(position);
        holder.mTvRouteName.setText(information.routeName);
        holder.mTvStoppageName.setText(information.stoppageName);
        holder.mTvVehicleTypeNumber.setText(information.vehicleType + " - " + information.vehicleNumber);
        holder.tv_driver.setText(information.conductorName);
        holder.tv_conductor.setText(information.conductorName);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class TransViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_vehicle_type_number)
        TextView mTvVehicleTypeNumber;
        @BindView(R.id.tv_route_name)
        TextView mTvRouteName;
        @BindView(R.id.tv_stoppage_name)
        TextView mTvStoppageName;
        @BindView(R.id.tv_driver)
        TextView tv_driver;
        @BindView(R.id.tv_conductor)
        TextView tv_conductor;

        public TransViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}