package com.xiphiastec.newatsi.teacher.syllabus;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.teacher.AbsEmployeePresenter;

/**
 * Created by hemanths on 11/08/17.
 */

public class EmpSyllabusPresenter extends AbsEmployeePresenter
        implements EmpSyllabusContract.Presenter {
    @NonNull
    private EmpSyllabusContract.View mView;

    EmpSyllabusPresenter(@NonNull EmpSyllabusContract.View view) {
        mView = view;
    }

    @Override
    public void syllabus() {
        mDisposable.add(mService.syllabus()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status && response.mList.size() > 0) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    @Override
    public void onSubscriber() {
        syllabus();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }


}
