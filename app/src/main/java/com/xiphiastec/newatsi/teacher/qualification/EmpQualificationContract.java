package com.xiphiastec.newatsi.teacher.qualification;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.EmpQualificationResponse;

/**
 * Created by xiphias0101 on 12/12/2017.
 */

public interface EmpQualificationContract {
    interface View extends BaseView<EmpQualificationResponse> {

    }

    interface Presenter extends BasePresenter<View> {
        void qualification();
    }
}
