package com.xiphiastec.newatsi.teacher.disciplinary;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsParentBaseFragment;
import com.xiphiastec.newatsi.model.EmpDisciplinaryResponse;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.teacher.activities.TeachersActivity;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class EmpDisciplinaryFragment extends AbsParentBaseFragment implements
        EmpDisciplinaryContract.View {

    @BindView(R.id.recycler_view)
    EmptyViewRecyclerView recyclerView;
    @BindView(R.id.tv_empty)
    TextView tvEmpty;
    Unbinder unbinder;
    @BindView(R.id.root)
    CoordinatorLayout mRoot;
    private EmpDisciplinaryPresenter mPresenter;
    AtsiProgressDialog atsiProgressDialog;

    @BindView(R.id.fabCreate)
    FloatingActionButton create;

    public EmpDisciplinaryFragment() {
        // Required empty public constructor
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && getActionBar() != null) {
            getActionBar().setTitle(R.string.disciplinary);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mPresenter = new EmpDisciplinaryPresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_desciplinary_action, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.discipline));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getActionBar().setTitle(SS);
        }
        mPresenter.onSubscriber();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //crete.setTitle("Create disciplinary action");
        super.onCreateOptionsMenu(menu, inflater);
        menu.removeItem(R.id.action_notifications);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onLoading() {
       atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
        tvEmpty.setText(error);
        tvEmpty.setVisibility(View.VISIBLE);
        Snackbar.make(mRoot, error, Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void onResponse(EmpDisciplinaryResponse response) {
        DisciplinaryAdapter adapter = new DisciplinaryAdapter(getActivity(),response.mList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onComplete() {
      atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onUnSubscriber();
    }


    @OnClick(R.id.fabCreate)
    public void createDiscipline(){
        if ((getActivity()) != null) {
            ((TeachersActivity) getActivity())
                    .changeFragment(new EmpCreateDisciplinaryActionFragment(), true);
        }
    }

}
