package com.xiphiastec.newatsi.teacher.profile;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.EmployeeDesignation;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class EmpDesignationFragment extends Fragment {

    private static final String ARGS_EMP_DESIGNATION = "args_emp_designation";
    @BindView(R.id.recycler_view)
    EmptyViewRecyclerView recyclerView;
    @BindView(R.id.tv_empty)
    TextView tvEmpty;
    Unbinder unbinder;
    private ArrayList<EmployeeDesignation> employeeDesignation;

    public EmpDesignationFragment() {
        // Required empty public constructor
    }

    public static EmpDesignationFragment newInstance(ArrayList<EmployeeDesignation> employeeDesignation) {

        Bundle args = new Bundle();
        args.putParcelableArrayList(ARGS_EMP_DESIGNATION, employeeDesignation);
        EmpDesignationFragment fragment = new EmpDesignationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            employeeDesignation = getArguments().getParcelableArrayList(ARGS_EMP_DESIGNATION);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_employee_designation, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (employeeDesignation == null) {
            return;
        }
        DesignationAdapter adapter = new DesignationAdapter(getActivity(),employeeDesignation);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Toast.makeText(getContext(), "Adding", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

//    class DesignationAdapter extends RecyclerView.Adapter<DesignationAdapter.ViewHolder> {
//        private final ArrayList<EmployeeDesignation> employeeDesignation;
//
//
//        DesignationAdapter(ArrayList<EmployeeDesignation> employeeDesignation) {
//            this.employeeDesignation = employeeDesignation;
//        }
//
//        @Override
//        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            return new ViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_emp_designation, parent, false));
//        }
//
//        @Override
//        public void onBindViewHolder(ViewHolder holder, int position) {
//            EmployeeDesignation designation = employeeDesignation.get(position);
//            holder.department.setText(Utils.convertStringToHtml(designation.department));
//            holder.designation.setText(Utils.convertStringToHtml("Type: " + designation.designationType));
//            holder.type.setText(Utils.convertStringToHtml("<b>Grade type:</b> " + designation.staffType +
//                    " <b>Grade name:</b> " + designation.gradeName));
//
//        }
//
//        @Override
//        public int getItemCount() {
//            return employeeDesignation.size();
//        }
//
//        public class ViewHolder extends RecyclerView.ViewHolder {
//            @BindView(R.id.designation)
//            TextView designation;
//            @BindView(R.id.department)
//            TextView department;
//            @BindView(R.id.type)
//            TextView type;
//
//            public ViewHolder(View itemView) {
//                super(itemView);
//                ButterKnife.bind(this, itemView);
//            }
//        }
//    }
}
