package com.xiphiastec.newatsi.teacher.experience;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.EmpExperienceResponse;
import com.xiphiastec.newatsi.teacher.attendance.EmpAttendanceContract;

/**
 * Created by xiphias0101 on 11/12/2017.
 */

public interface EmpExperienceContract {
    interface View extends BaseView<EmpExperienceResponse> {

    }

    interface Presenter extends BasePresenter<EmpAttendanceContract.View> {
        void experience();
    }
}
