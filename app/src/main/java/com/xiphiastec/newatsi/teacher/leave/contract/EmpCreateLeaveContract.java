package com.xiphiastec.newatsi.teacher.leave.contract;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.StatusResponse;
import com.xiphiastec.newatsi.model.Leave;
import com.xiphiastec.newatsi.model.LeaveDetails;
import com.xiphiastec.newatsi.model.LeaveType;
import com.xiphiastec.newatsi.model.EmployeeDetailsBody;

/**
 * Created by xiphias0101 on 12/12/2017.
 */

public interface EmpCreateLeaveContract {
    interface View extends BaseView<StatusResponse> {
        void onLeaveTypesResponse(LeaveType type);

        void onLeaveDetailsResponse(LeaveDetails type);
    }

    interface Presenter extends BasePresenter<View> {
        void leaveTypes();

        void createLeave(Leave leave);

        void leaveDetails(EmployeeDetailsBody body);
    }
}
