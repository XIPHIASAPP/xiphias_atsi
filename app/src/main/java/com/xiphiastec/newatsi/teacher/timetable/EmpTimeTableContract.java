package com.xiphiastec.newatsi.teacher.timetable;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.EmpTimeTableResponse;

/**
 * Created by xiphias0101 on 12/12/2017.
 */

public interface EmpTimeTableContract {
    interface View extends BaseView<EmpTimeTableResponse> {

    }

    interface Presenter extends BasePresenter<View> {
        void timeTable();
    }
}
