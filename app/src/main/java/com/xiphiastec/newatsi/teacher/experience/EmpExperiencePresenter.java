package com.xiphiastec.newatsi.teacher.experience;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.teacher.AbsEmployeePresenter;

/**
 * Created by Admin on 25-04-2017.
 */

public class EmpExperiencePresenter extends AbsEmployeePresenter
        implements EmpExperienceContract.Presenter {
    @NonNull
    private EmpExperienceContract.View mView;

    EmpExperiencePresenter(@NonNull EmpExperienceContract.View view) {
        mView = view;
    }

    @Override
    public void experience() {
        mDisposable.add(mService.experience()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status && response.mList.size() > 0) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    @Override
    public void onSubscriber() {
        experience();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }



}
