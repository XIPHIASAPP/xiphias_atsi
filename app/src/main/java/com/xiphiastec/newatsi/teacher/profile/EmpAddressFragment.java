package com.xiphiastec.newatsi.teacher.profile;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.EmployeeAddress;

import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmpAddressFragment extends Fragment {
    private static final String ARGS_EMP_ADDRESS = "args_emp_address";

    Unbinder unbinder;
    @BindViews({R.id.address_1,//1
            R.id.address_2,//1
            R.id.number,//2
            R.id.state,//3
            R.id.zipcode,//4
            R.id.present,//5
            R.id.permanent,//6
    })
    List<TextView> addresses;
    private EmployeeAddress address;

    public EmpAddressFragment() {
        // Required empty public constructor
    }

    public static EmpAddressFragment newInstance(EmployeeAddress employeeAddress) {

        Bundle args = new Bundle();
        args.putParcelable(ARGS_EMP_ADDRESS, employeeAddress);
        EmpAddressFragment fragment = new EmpAddressFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            address = getArguments().getParcelable(ARGS_EMP_ADDRESS);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_employee_address, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addresses.get(0).setText(address.address1);
        addresses.get(1).setText(address.address2);
        addresses.get(2).setText(address.phoneNumber);
        addresses.get(3).setText(address.stateName);
        addresses.get(4).setText(address.zipPostalCode);
        addresses.get(5).setText(address.presentAddress ? "True" : "False");
        addresses.get(6).setText(address.permanentAddress ? "True" : "False");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Toast.makeText(getContext(), "Adding", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }
}
