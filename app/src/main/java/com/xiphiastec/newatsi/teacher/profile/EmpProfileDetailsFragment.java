package com.xiphiastec.newatsi.teacher.profile;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.EmployeeProfileInformation;

import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmpProfileDetailsFragment extends Fragment {
    private static final String ARGS_EMP_PROFILE = "args_emp_profile";
    Unbinder unbinder;

    @BindViews({R.id.name, //0
            R.id.email, //1
            R.id.dob,//2
            R.id.mobile_number,//3
            R.id.join_date,//4
            R.id.gender,//5
            R.id.blood_group,//6
            R.id.pan_number,//7
            R.id.biometrics,//8
            R.id.marital_status,//9
    })
    List<TextView> profile;
    private EmployeeProfileInformation information;

    public EmpProfileDetailsFragment() {
        // Required empty public constructor
    }

    public static EmpProfileDetailsFragment newInstance(EmployeeProfileInformation information) {

        Bundle args = new Bundle();
        args.putParcelable(ARGS_EMP_PROFILE, information);
        EmpProfileDetailsFragment fragment = new EmpProfileDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            information = getArguments().getParcelable(ARGS_EMP_PROFILE);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_employee_profile_details, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        profile.get(0).setText(information.employeeName);
        profile.get(1).setText(information.emailId);
        profile.get(2).setText(information.dateOfBirth);
        profile.get(3).setText(information.mobileNumber);
        profile.get(4).setText(information.joiningDate);
        profile.get(5).setText(information.gender);
        profile.get(6).setText(information.bloodGroup);
        profile.get(7).setText(information.panNumber);
        profile.get(8).setText("" + information.biometric);
        profile.get(9).setText(information.maritalStatus);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
