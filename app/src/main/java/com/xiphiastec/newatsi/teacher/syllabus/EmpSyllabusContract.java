package com.xiphiastec.newatsi.teacher.syllabus;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.EmpSyllabusResponse;

/**
 * Created by xiphias0101 on 12/12/2017.
 */

public interface EmpSyllabusContract {
    interface View extends BaseView<EmpSyllabusResponse> {

    }

    interface Presenter extends BasePresenter<View> {
        void syllabus();
    }
}
