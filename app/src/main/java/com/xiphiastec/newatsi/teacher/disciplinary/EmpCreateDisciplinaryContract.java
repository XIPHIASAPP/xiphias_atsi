package com.xiphiastec.newatsi.teacher.disciplinary;

import com.xiphiastec.newatsi.model.CreateDisciplinaryBody;
import com.xiphiastec.newatsi.model.DropDownActionsResponse;
import com.xiphiastec.newatsi.model.StatusResponse;
import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;

/**
 * Created by xiphias0101 on 12/12/2017.
 */

public interface EmpCreateDisciplinaryContract {
    interface View extends BaseView<StatusResponse> {
        void onDropActionResponse(DropDownActionsResponse response);
    }

    interface Presenter extends BasePresenter<View> {
        void createDisciplinaryAction(CreateDisciplinaryBody body);

        void actions();
    }
}
