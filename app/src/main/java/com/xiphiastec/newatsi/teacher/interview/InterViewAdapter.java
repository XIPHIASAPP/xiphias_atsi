package com.xiphiastec.newatsi.teacher.interview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.EmployeeScheduleInterview;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 5/3/18.
 */

class InterViewAdapter extends RecyclerView.Adapter<InterViewAdapter.ViewHolder> {

    private List<EmployeeScheduleInterview> list;
    private Context context;

    InterViewAdapter(Context context, List<EmployeeScheduleInterview> employeeScheduleInterview) {
        this.context=context;
        this.list = employeeScheduleInterview;
    }

    @Override
    public InterViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new InterViewAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_emp_interview, parent, false));
    }

    @Override
    public void onBindViewHolder(InterViewAdapter.ViewHolder holder, int position) {
        EmployeeScheduleInterview interview = list.get(position);
        holder.applicantName.setText(interview.applicantName);
        holder.name.setText(Html.fromHtml(interview.interviewerName));
        holder.tvJob.setText(Html.fromHtml("<b>Job: </b>" + interview.jobTitle));
        holder.tvRound.setText(Html.fromHtml(" <b>Round: </b>" + interview.interviewRound));
        holder.tvDate.setText(Html.fromHtml( "<b>Date: </b>" + interview.onDate));
        holder.tvTime.setText(Html.fromHtml("<b>From: </b>" + interview.timeFrom));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.applicant_name)
        TextView applicantName;
        @BindView(R.id.tvJob)
        TextView tvJob;
        @BindView(R.id.tvRound)
        TextView tvRound;
        @BindView(R.id.tvTime)
        TextView tvTime;
        @BindView(R.id.tvDate)
        TextView tvDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}