package com.xiphiastec.newatsi.teacher.dashboard;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.TimeTableEmployee;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 9/3/18.
 */

class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.DashViewHolder> {

    private List<TimeTableEmployee> list;
    Context context;

    public DashboardAdapter(Context context, List<TimeTableEmployee> timeTableEmployee) {
        this.list = timeTableEmployee;
        this.context=context;
    }

    @Override
    public DashboardAdapter.DashViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DashboardAdapter.DashViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_emp_time_table, parent, false));
    }

    @Override
    public void onBindViewHolder(DashboardAdapter.DashViewHolder holder, int position) {
        TimeTableEmployee employee = list.get(position);
        holder.name.setText(employee.subjectName);
        //holder.day.setText(employee.dayName);
        holder.time.setText(employee.startTime + " - " + employee.endTime);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DashViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.time)
        TextView time;

        public DashViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}