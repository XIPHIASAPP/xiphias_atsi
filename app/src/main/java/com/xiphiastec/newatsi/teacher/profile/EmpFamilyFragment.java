package com.xiphiastec.newatsi.teacher.profile;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.EmployeeFamily;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmpFamilyFragment extends Fragment {

    private static final String ARGS_EMP_FAMILY = "args_emp_family";
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.txtError)
    TextView txtError;
    Unbinder unbinder;
    private ArrayList<EmployeeFamily> employeeFamily;

    public EmpFamilyFragment() {
        // Required empty public constructor
    }

    public static EmpFamilyFragment newInstance(ArrayList<EmployeeFamily> employeeFamily) {

        Bundle args = new Bundle();
        args.putParcelableArrayList(ARGS_EMP_FAMILY, employeeFamily);
        EmpFamilyFragment fragment = new EmpFamilyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            employeeFamily = getArguments().getParcelableArrayList(ARGS_EMP_FAMILY);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_family_employee, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (employeeFamily != null) {
            EmpFamilyAdapter adapter = new EmpFamilyAdapter(getActivity(),employeeFamily);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Toast.makeText(getContext(), "Adding", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_add_option, menu);
    }



//    class EmpFamilyAdapter extends RecyclerView.Adapter<EmpFamilyAdapter.ViewHolder> {
//        private final ArrayList<EmployeeFamily> employeeFamily;
//
//        EmpFamilyAdapter(ArrayList<EmployeeFamily> employeeFamily) {
//            this.employeeFamily = employeeFamily;
//        }
//
//        @Override
//        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            return new ViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_emp_family, parent, false));
//        }
//
//        @Override
//        public void onBindViewHolder(ViewHolder holder, int position) {
//            EmployeeFamily family = employeeFamily.get(position);
//            holder.tvName.setText(family.parentName);
//            holder.tvNumber.setText(family.parentMobileNumber);
//            holder.tvRelation.setText(family.relationType);
//            holder.tvCompany.setText( family.company+ " - " + family.designation);
//        }
//
//        @Override
//        public int getItemCount() {
//            return employeeFamily.size();
//        }
//
//        public class ViewHolder extends RecyclerView.ViewHolder {
//            @BindView(R.id.tvName)
//            TextView tvName;
//            @BindView(R.id.tvNumber)
//            TextView tvNumber;
//            @BindView(R.id.tvRelation)
//            TextView tvRelation;
//            @BindView(R.id.tvCompany)
//            TextView tvCompany;
//
//            public ViewHolder(View itemView) {
//                super(itemView);
//                ButterKnife.bind(this, itemView);
//            }
//        }
//    }
}
