package com.xiphiastec.newatsi.teacher.profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.EmployeeDesignation;
import com.xiphiastec.newatsi.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 8/3/18.
 */

class DesignationAdapter extends RecyclerView.Adapter<DesignationAdapter.DesiViewHolder> {
    private final ArrayList<EmployeeDesignation> employeeDesignation;
    Context context;

    DesignationAdapter(Context context, ArrayList<EmployeeDesignation> employeeDesignation) {
        this.employeeDesignation = employeeDesignation;
        this.context=context;
    }

    @Override
    public DesignationAdapter.DesiViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DesignationAdapter.DesiViewHolder(LayoutInflater.from(context).inflate(R.layout.item_emp_designation, parent, false));
    }

    @Override
    public void onBindViewHolder(DesignationAdapter.DesiViewHolder holder, int position) {
        EmployeeDesignation designation = employeeDesignation.get(position);
        holder.department.setText(Utils.convertStringToHtml(designation.department));
        holder.designation.setText(Utils.convertStringToHtml("Type: " + designation.designationType));
        holder.type.setText(Utils.convertStringToHtml("<b>Grade type:</b> " + designation.staffType +
                " <b>Grade name:</b> " + designation.gradeName));

    }

    @Override
    public int getItemCount() {
        return employeeDesignation.size();
    }

    public class DesiViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.designation)
        TextView designation;
        @BindView(R.id.department)
        TextView department;
        @BindView(R.id.type)
        TextView type;

        public DesiViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
