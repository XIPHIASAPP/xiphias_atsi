package com.xiphiastec.newatsi.teacher.disciplinary;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsParentBaseFragment;
import com.xiphiastec.newatsi.model.CreateDisciplinaryBody;
import com.xiphiastec.newatsi.model.DropDownActionsResponse;
import com.xiphiastec.newatsi.model.EmpCreateDropdown;
import com.xiphiastec.newatsi.model.StatusResponse;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmpCreateDisciplinaryActionFragment extends AbsParentBaseFragment implements EmpCreateDisciplinaryContract.View {

    private static final String TAG = "EmpCreateDisciplinary";
    @BindView(R.id.rootLayout)
    LinearLayout rootLayout;
    Unbinder unbinder;
    @BindView(R.id.action)
    EditText action;
    @BindView(R.id.indiscipline)
    EditText indiscipline;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.actions)
    Spinner actions;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    private ArrayAdapter<String> arrayAdapter;
    private EmpCreateDisciplinaryPresenter mPresenter;
    private List<EmpCreateDropdown> employees = new ArrayList<>();

    AtsiProgressDialog atsiProgressDialog;

    public EmpCreateDisciplinaryActionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.removeItem(R.id.action_notifications);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    private String getDate() {
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
        return format.format(new Date());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mPresenter = new EmpCreateDisciplinaryPresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && getActionBar() != null) {
            getActionBar().setTitle(R.string.disciplinary);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        arrayAdapter = new ArrayAdapter<>(getActivity(),R.layout.spinner_item);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        arrayAdapter.add("Please Select Action");
        actions.setAdapter(arrayAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActionBar() != null) {
            getActionBar().setTitle(R.string.disciplinary);
        }
        mPresenter.onSubscriber();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_employee_create_dispilnary_action, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onLoading() {
      atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
       atsiProgressDialog.dismissDialog();
        Snackbar.make(getView(), error, Snackbar.LENGTH_SHORT)
                .show();
    }


    @Override
    public void onComplete() {
        atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onResponse(StatusResponse response) {
        getActivity().onBackPressed();
        Toast.makeText(getActivity(), response.message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDropActionResponse(DropDownActionsResponse response) {
        employees = response.mList;
        arrayAdapter.clear();

        arrayAdapter.add("Select option");
        for (EmpCreateDropdown dropdown : response.mList) {
            arrayAdapter.add(dropdown.text);
            arrayAdapter.notifyDataSetChanged();
        }
    }

    @OnClick(R.id.btnSubmit)
    public void onSubmitClick(){

        String actionManagement, indisciplinary, description;

        actionManagement = this.action.getText().toString();
        indisciplinary = this.indiscipline.getText().toString();
        description = this.description.getText().toString();


        if(employees.size()>0) {

            if(actions.getSelectedItemPosition()!=0) {

                CreateDisciplinaryBody body = new CreateDisciplinaryBody();
                body.setEmployeeId(employees.get(actions.getSelectedItemPosition()-1).value);
                body.setDateOfIndiscipline(getDate());

                body.setActionByManagement(actionManagement);
                body.setDescriptionByEmployee(description);
                body.setIndisciplineAction(indisciplinary);
                mPresenter.createDisciplinaryAction(body);

            }
        }else{

            Snackbar.make(rootLayout,"Something went wrong",Snackbar.LENGTH_SHORT).show();

        }
    }
}
