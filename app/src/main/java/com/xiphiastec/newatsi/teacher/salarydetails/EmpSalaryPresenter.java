package com.xiphiastec.newatsi.teacher.salarydetails;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.teacher.AbsEmployeePresenter;
import com.xiphiastec.newatsi.model.EmployeeDetailsBody;

/**
 * Created by hemanths on 11/08/17.
 */

public class EmpSalaryPresenter extends AbsEmployeePresenter
        implements EmpSalaryContract.Presenter {

    @NonNull
    private EmpSalaryContract.View mView;
    private String mToday;

    EmpSalaryPresenter(@NonNull EmpSalaryContract.View view) {
        mView = view;
    }

    @Override
    public void onSubscriber() {
        EmployeeDetailsBody body = new EmployeeDetailsBody();
        body.setDate(mToday);
        salary(body);
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }

    @Override
    public void salary(EmployeeDetailsBody body) {
        mDisposable.add(mService.salarySlip(body)
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    void setToday(String today) {
        mToday = today;
    }



}
