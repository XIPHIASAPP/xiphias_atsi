package com.xiphiastec.newatsi.teacher.profile;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.EmpProfileResponse;
import com.xiphiastec.newatsi.teacher.interview.EmpInterViewScheduleContract;

/**
 * Created by xiphias0101 on 11/12/2017.
 */

public interface EmpProfileContract {
    interface View extends BaseView<EmpProfileResponse> {

    }

    interface Presenter extends BasePresenter<EmpInterViewScheduleContract.View> {
        void profile();
    }
}
