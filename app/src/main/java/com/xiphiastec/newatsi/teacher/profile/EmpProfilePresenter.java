package com.xiphiastec.newatsi.teacher.profile;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.teacher.AbsEmployeePresenter;

/**
 * Created by hemanths on 10/08/17.
 */

public class EmpProfilePresenter extends AbsEmployeePresenter
        implements EmpProfileContract.Presenter {
    @NonNull
    private EmpProfileContract.View mView;

    EmpProfilePresenter(@NonNull EmpProfileContract.View view) {
        mView = view;
    }

    @Override
    public void profile() {
        mDisposable.add(mService.profile()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    @Override
    public void onSubscriber() {
        profile();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }

}
