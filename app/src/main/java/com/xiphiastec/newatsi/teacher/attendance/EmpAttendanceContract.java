package com.xiphiastec.newatsi.teacher.attendance;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.EmployeeAttendanceResponse;
import com.xiphiastec.newatsi.model.EmployeeDetailsBody;
import com.xiphiastec.newatsi.model.TypeList;

/**
 * Created by xiphias0101 on 11/12/2017.
 */

public interface EmpAttendanceContract {
    interface View extends BaseView<EmployeeAttendanceResponse> {
        void onAttendanceResponse(TypeList response);
    }

    interface Presenter extends BasePresenter<View> {
        void attendanceTypes();

        void attendance(EmployeeDetailsBody body);
    }
}
