package com.xiphiastec.newatsi.teacher.qualification;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.EmployeeQualificationInfo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 5/3/18.
 */

class QualificationAdapter extends RecyclerView.Adapter<QualificationAdapter.QViewHolder> {

    private final List<EmployeeQualificationInfo> list;
    Context context;

    QualificationAdapter(Context context,List<EmployeeQualificationInfo> employeeQualificationInfo) {
        this.list = employeeQualificationInfo;
        this.context=context;
    }

    @Override
    public QualificationAdapter.QViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new QualificationAdapter.QViewHolder(LayoutInflater.from(context).inflate(R.layout.item_emp_qualification, parent, false));
    }

    @Override
    public void onBindViewHolder(QualificationAdapter.QViewHolder holder, int position) {
        final EmployeeQualificationInfo info = list.get(position);
        holder.tvCertificate.setText(info.certificateName);
        holder.tvQualification.setText(info.qualification + " - " + info.boardUniversity);
        holder.tvMarks.setText(info.score);
//        holder.download.setOnClickListener(v -> {
//            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(info.certificatePath));
//            context.startActivity(intent);
//        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class QViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvQualification)
        TextView tvQualification;
        @BindView(R.id.tvCertificate)
        TextView tvCertificate;
        @BindView(R.id.tvMarks)
        TextView tvMarks;

        public QViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
