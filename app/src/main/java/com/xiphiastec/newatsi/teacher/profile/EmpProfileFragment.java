package com.xiphiastec.newatsi.teacher.profile;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsEmployeeBaseFragment;
import com.xiphiastec.newatsi.model.EmpProfileResponse;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.teacher.activities.TeachersActivity;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class EmpProfileFragment extends AbsEmployeeBaseFragment implements EmpProfileContract.View {

    Unbinder unbinder;
    @BindView(R.id.root)
    FrameLayout mRoot;
    private EmpProfilePresenter mPresenter;
    private EmpProfileResponse mResponse = null;
    AtsiProgressDialog atsiProgressDialog;

    public EmpProfileFragment() {
        // Required empty public constructor
    }

    public static EmpProfileFragment newInstance() {

        Bundle args = new Bundle();

        EmpProfileFragment fragment = new EmpProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && getActionBar() != null) {
            getActionBar().setTitle(R.string.profile);
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.removeItem(R.id.action_notifications);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mPresenter = new EmpProfilePresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.profile));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getActionBar().setTitle(SS);
        }
        mPresenter.onSubscriber();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onUnSubscriber();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_employee_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onLoading() {
       atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
       atsiProgressDialog.dismissDialog();
        Snackbar.make(mRoot, error, Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void onResponse(EmpProfileResponse response) {
        mResponse = response;
    }

    @Override
    public void onComplete() {
      atsiProgressDialog.dismissDialog();
    }


    @OnClick({R.id.details, R.id.address, R.id.assets, R.id.family})
    public void onViewClicked(View view) {
        if (mResponse == null) {
            Toast.makeText(getContext(), "Please try again", Toast.LENGTH_SHORT).show();
            return;
        }
        if (getActivity() != null) {
            switch (view.getId()) {
                case R.id.details:
                    ((TeachersActivity) getActivity())
                            .changeFragment(EmpProfileDetailsFragment.newInstance(mResponse.mInformation), true);
                    break;
                case R.id.address:
                    ((TeachersActivity) getActivity())
                            .changeFragment(EmpAddressFragment.newInstance(mResponse.employeeAddress), true);
                    break;
                case R.id.assets:
                    ((TeachersActivity) getActivity())
                            .changeFragment(EmpAssetsFragment.newInstance(mResponse.employeeAsset), true);
                    break;
                case R.id.family:
                    ((TeachersActivity) getActivity())
                            .changeFragment(EmpFamilyFragment.newInstance(mResponse.employeeFamily), true);
                    break;
            }
        }
    }
}
