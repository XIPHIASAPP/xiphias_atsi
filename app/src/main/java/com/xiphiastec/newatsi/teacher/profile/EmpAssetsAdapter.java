package com.xiphiastec.newatsi.teacher.profile;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.EmployeeAsset;

import java.util.ArrayList;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 5/3/18.
 */

class EmpAssetsAdapter extends RecyclerView.Adapter<EmpAssetsAdapter.AssetViewHolder> {

    private ArrayList<EmployeeAsset> employeeAsset;
    Context context;
    RecyclerView recyclerView;



    EmpAssetsAdapter(Context context,RecyclerView recyclerView,ArrayList<EmployeeAsset> employeeAsset) {
        this.employeeAsset = employeeAsset;
        this.context=context;
        this.recyclerView=recyclerView;
    }

    @Override
    public EmpAssetsAdapter.AssetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EmpAssetsAdapter.AssetViewHolder(LayoutInflater.from(context).inflate(R.layout.item_emp_assets, parent, false));
    }

    @Override
    public void onBindViewHolder(EmpAssetsAdapter.AssetViewHolder holder, int position) {

        Random random = new Random();
        int color = Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256));
        recyclerView.setBackgroundColor(color);
        EmployeeAsset asset = employeeAsset.get(position);
        holder.assetName.setText(Html.fromHtml("<b>Name: </b>" + asset.assetName));
        holder.assetModel.setText(Html.fromHtml("<b>Model: </b>" + asset.modelNumber));
        holder.date.setText(Html.fromHtml("<b>Date: </b>" + asset.issueDate + " - " + asset.returnDate));
        holder.name.setText(Html.fromHtml("<b>Remark: </b>" + asset.remark));
    }

    @Override
    public int getItemCount() {
        return employeeAsset.size();
    }

    public class AssetViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.view)
        View view;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.asset_name)
        TextView assetName;
        @BindView(R.id.asset_model)
        TextView assetModel;
        @BindView(R.id.date)
        TextView date;

        public AssetViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}