package com.xiphiastec.newatsi.teacher.leave.contract;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.CancelRequestBody;
import com.xiphiastec.newatsi.model.LeaveRequestBody;
import com.xiphiastec.newatsi.model.LeaveStatusResponse;
import com.xiphiastec.newatsi.model.StatusResponse;
import com.xiphiastec.newatsi.model.EmpLeaveResponse;

/**
 * Created by xiphias0101 on 12/12/2017.
 */

public interface EmpLeaveListContract {
    interface View extends BaseView<EmpLeaveResponse> {
        void renderLeaveStatus(LeaveStatusResponse response);
        void renderCancalStatus(StatusResponse response);
    }

    interface Presenter extends BasePresenter<View> {
        void getLeaveDetails(LeaveRequestBody body);
        void getLeaveStatus();
        void cancelLeave(CancelRequestBody body);
    }
}
