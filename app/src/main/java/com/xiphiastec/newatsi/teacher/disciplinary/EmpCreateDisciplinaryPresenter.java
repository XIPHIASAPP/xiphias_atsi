package com.xiphiastec.newatsi.teacher.disciplinary;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.model.CreateDisciplinaryBody;
import com.xiphiastec.newatsi.teacher.AbsEmployeePresenter;

/**
 * Created by hemanths on 11/08/17.
 */

public class EmpCreateDisciplinaryPresenter extends AbsEmployeePresenter
        implements EmpCreateDisciplinaryContract.Presenter {
    @NonNull
    private EmpCreateDisciplinaryContract.View mView;

    EmpCreateDisciplinaryPresenter(@NonNull EmpCreateDisciplinaryContract.View view) {
        mView = view;
    }

    public void createDisciplinaryAction(CreateDisciplinaryBody disciplinary) {
        mDisposable.add(mService.createDisciplinary(disciplinary)
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    @Override
    public void onSubscriber() {
        actions();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }

    @Override
    public void actions() {
        mDisposable.add(mService.actions()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status) {
                                mView.onDropActionResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> {
                        },
                        () -> mView.onComplete()));
    }

}
