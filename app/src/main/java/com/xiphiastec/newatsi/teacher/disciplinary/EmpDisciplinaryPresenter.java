package com.xiphiastec.newatsi.teacher.disciplinary;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.teacher.AbsEmployeePresenter;

/**
 * Created by hemanths on 11/08/17.
 */

public class EmpDisciplinaryPresenter extends AbsEmployeePresenter
        implements EmpDisciplinaryContract.Presenter {
    @NonNull
    private EmpDisciplinaryContract.View mView;

    EmpDisciplinaryPresenter(@NonNull EmpDisciplinaryContract.View view) {
        mView = view;
    }

    @Override
    public void disciplinary() {
        mDisposable.add(mService.disciplinary()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status && response.mList.size() > 0) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    @Override
    public void onSubscriber() {
        disciplinary();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }
}
