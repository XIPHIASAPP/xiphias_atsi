package com.xiphiastec.newatsi.teacher.dashboard;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.EmpDashboardResponse;

/**
 * Created by xiphias0101 on 11/12/2017.
 */

public interface EmpDashboardContract {

    interface View extends BaseView<EmpDashboardResponse> {
    public void  renderTimetable(EmpDashboardResponse response);
    }

    interface Presenter extends BasePresenter<View> {
        void dashboard();
    }
}
