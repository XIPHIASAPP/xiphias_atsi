package com.xiphiastec.newatsi.teacher.profile;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.EmployeeAsset;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;


import java.util.ArrayList;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmpAssetsFragment extends Fragment {

    private static final String ARGS_EMPLOYEE_ASSETS = "args_employee_assets";
    @BindView(R.id.recycler_view)
    EmptyViewRecyclerView recyclerView;
    @BindView(R.id.tv_empty)
    TextView mTvEmpty;
    Unbinder unbinder;
    private ArrayList<EmployeeAsset> employeeAsset;

    public EmpAssetsFragment() {
        // Required empty public constructor
    }

    public static EmpAssetsFragment newInstance(ArrayList<EmployeeAsset> employeeAsset) {

        Bundle args = new Bundle();
        args.putParcelableArrayList(ARGS_EMPLOYEE_ASSETS, employeeAsset);
        EmpAssetsFragment fragment = new EmpAssetsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            employeeAsset = getArguments().getParcelableArrayList(ARGS_EMPLOYEE_ASSETS);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_assets_emp, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (employeeAsset == null) {
            return;
        }
        EmpAssetsAdapter adapter = new EmpAssetsAdapter(employeeAsset);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Toast.makeText(getContext(), "Adding", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    class EmpAssetsAdapter extends RecyclerView.Adapter<EmpAssetsAdapter.ViewHolder> {

        private ArrayList<EmployeeAsset> employeeAsset;



        EmpAssetsAdapter(ArrayList<EmployeeAsset> employeeAsset) {
            this.employeeAsset = employeeAsset;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_emp_assets, parent, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            Random random = new Random();
            int color = Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256));
            holder.view.setBackgroundColor(color);
            EmployeeAsset asset = employeeAsset.get(position);
            holder.assetName.setText(Html.fromHtml("<b>Name: </b>" + asset.assetName));
            holder.assetModel.setText(Html.fromHtml("<b>Model: </b>" + asset.modelNumber));
            holder.date.setText(Html.fromHtml("<b>Date: </b>" + asset.issueDate + " - " + asset.returnDate));
            holder.name.setText(Html.fromHtml("<b>Remark: </b>" + asset.remark));
        }

        @Override
        public int getItemCount() {
            return employeeAsset.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.view)
            TextView view;
            @BindView(R.id.name)
            TextView name;
            @BindView(R.id.asset_name)
            TextView assetName;
            @BindView(R.id.asset_model)
            TextView assetModel;
            @BindView(R.id.date)
            TextView date;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }
}
