package com.xiphiastec.newatsi.teacher.timetable;


import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsEmployeeBaseFragment;
import com.xiphiastec.newatsi.model.EmpTimeTableResponse;
import com.xiphiastec.newatsi.model.TimeTableEmployee;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class EmpTimeTableFragment extends AbsEmployeeBaseFragment implements EmpTimeTableContract.View {
    private static final String TAG = "StudentTimeTable";

    DisplayMetrics metrics;
    int width;

    Context mContext;

    Unbinder unbinder;

    @BindView(R.id.lvExp)
    ExpandableListView timeTable;

    @BindView(R.id.root_layout)
    ConstraintLayout mRootLayout;

    @BindView(R.id.txtError)
    TextView txtError;



    ExpandableListEmpAdapter listAdapter;

    List<String> dayHeaderGroup;

    HashMap<String,  List<TimeTableEmployee>> timeTableChild;

    private EmpTimeTablePresenter mPresenter;

    AtsiProgressDialog atsiProgressDialog;

    public EmpTimeTableFragment() {
        // Required empty public constructor
    }

    public static EmpTimeTableFragment newInstance() {

        Bundle args = new Bundle();

        EmpTimeTableFragment fragment = new EmpTimeTableFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mPresenter = new EmpTimeTablePresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.removeItem(R.id.action_notifications);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_employee_time_table, container, false);
        unbinder = ButterKnife.bind(this, view);
        metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        width = metrics.widthPixels;


        dayHeaderGroup=new ArrayList<>();
        timeTableChild=new HashMap<String,  List<TimeTableEmployee>>();

        timeTable.setIndicatorBounds(width - GetDipsFromPixel(50), width - GetDipsFromPixel(30));

        return view;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (getActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.time_table));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getActionBar().setTitle(SS);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.time_table));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(SS);
        }

        dayHeaderGroup.clear();
        timeTableChild.clear();

        mPresenter.onSubscriber();
    }


    @Override
    public void onLoading() {
        atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        txtError.setText(error);
        txtError.setVisibility(View.VISIBLE);
        Snackbar.make(mRootLayout, error, Snackbar.LENGTH_SHORT).show();
        atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onResponse(EmpTimeTableResponse response) {

        if (response.success) {
            // Adding child data
            dayHeaderGroup.add("Monday");
            dayHeaderGroup.add("Tuesday");
            dayHeaderGroup.add("Wednesday");
            dayHeaderGroup.add("Thursday");
            dayHeaderGroup.add("Friday");
            dayHeaderGroup.add("Saturday");
            dayHeaderGroup.add("Sunday");

            List<TimeTableEmployee> monday = response.monday;
            timeTableChild.put(dayHeaderGroup.get(0), monday);
            List<TimeTableEmployee> tuesday = response.tuesday;
            timeTableChild.put(dayHeaderGroup.get(1), tuesday);
            List<TimeTableEmployee> wednesday = response.wednesday;
            timeTableChild.put(dayHeaderGroup.get(2), wednesday);
            List<TimeTableEmployee> thursday = response.thursday;
            timeTableChild.put(dayHeaderGroup.get(3), thursday);
            List<TimeTableEmployee> friday = response.friday;
            timeTableChild.put(dayHeaderGroup.get(4), friday);
            List<TimeTableEmployee> saturday = response.saturday;
            timeTableChild.put(dayHeaderGroup.get(5), saturday);
            List<TimeTableEmployee> sunday = response.sunday;
            timeTableChild.put(dayHeaderGroup.get(6), sunday);



            listAdapter = new ExpandableListEmpAdapter(getActivity(), dayHeaderGroup, timeTableChild);

            // setting list adapter
            timeTable.setAdapter(listAdapter);
        }

    }

    @Override
    public void onComplete() {
      atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public int GetDipsFromPixel(float pixels)
    {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }
}
