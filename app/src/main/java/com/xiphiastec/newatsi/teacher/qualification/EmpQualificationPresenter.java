package com.xiphiastec.newatsi.teacher.qualification;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.teacher.AbsEmployeePresenter;

import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by hemanths on 12/08/17.
 */

public class EmpQualificationPresenter extends AbsEmployeePresenter
        implements EmpQualificationContract.Presenter {

    @NonNull
    private EmpQualificationContract.View mView;

    EmpQualificationPresenter(@NonNull EmpQualificationContract.View view) {
        mView = view;
    }

    @Override
    public void qualification() {
        mDisposable.add(mService.qualification()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status && response.mList.size() > 0) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    @Override
    public void onSubscriber() {
        qualification();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }



}
