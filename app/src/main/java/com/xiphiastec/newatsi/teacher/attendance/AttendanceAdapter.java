package com.xiphiastec.newatsi.teacher.attendance;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.EmployeeAttendanceInformation;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 5/3/18.
 */

class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.ViewHolder> {

    private List<EmployeeAttendanceInformation> list = new ArrayList<>();
    Context context;

    public AttendanceAdapter(Context context) {
        list = new ArrayList<>();
        this.context=context;
    }

    @Override
    public AttendanceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AttendanceAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_attendance_emp, parent, false));
    }

    @Override
    public void onBindViewHolder(AttendanceAdapter.ViewHolder holder, int position) {
      EmployeeAttendanceInformation information = list.get(position);
        holder.tvIn_time.setText(information.inTime );
        holder.tvOut_time.setText(information.outTime);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void swapData(List<EmployeeAttendanceInformation> employeeAttendanceInformation) {
        list = employeeAttendanceInformation;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvIn_time)
        TextView tvIn_time;
        @BindView(R.id.tvOut_time)
        TextView tvOut_time;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
