package com.xiphiastec.newatsi.teacher.dashboard;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsEmployeeBaseFragment;
import com.xiphiastec.newatsi.model.EmpDashboardResponse;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.teacher.attendance.EmpAttendanceFragment;
import com.xiphiastec.newatsi.teacher.disciplinary.EmpDisciplinaryFragment;
import com.xiphiastec.newatsi.teacher.experience.EmpExperienceFragment;
import com.xiphiastec.newatsi.teacher.interview.EmpInterViewScheduleFragment;
import com.xiphiastec.newatsi.teacher.leave.EmpLeaveRecordFragment;
import com.xiphiastec.newatsi.teacher.profile.EmpProfileFragment;
import com.xiphiastec.newatsi.teacher.qualification.EmpQualificationFragment;
import com.xiphiastec.newatsi.teacher.salarydetails.EmpSalaryFragment;
import com.xiphiastec.newatsi.teacher.syllabus.EmpSyllabusFragment;
import com.xiphiastec.newatsi.teacher.timetable.EmpTimeTableFragment;
import com.xiphiastec.newatsi.teacher.transport.EmpTransportationFragment;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;
import com.xiphiastec.newatsi.util.PreferenceUtil;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;
import com.xiphiastec.newatsi.teacher.activities.TeachersActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class EmpDashboardFragment extends AbsEmployeeBaseFragment implements
        EmpDashboardContract.View {

    private static final String TAG = "EmpDashboard";
    Unbinder unbinder;
    @BindView(R.id.recycler_view)
    EmptyViewRecyclerView recyclerView;
    @BindView(R.id.tv_empty)
    TextView tvEmpty;
    @BindView(R.id.text)
    TextView mText;
    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.image)
    ImageView mImage;
    private EmpDashboardPresenter mPresenter;
    AtsiProgressDialog atsiProgressDialog;

    public EmpDashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new EmpDashboardPresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && getActionBar() != null) {
            getActionBar().setTitle(R.string.dashboard);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.dashboard));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getActionBar().setTitle(SS);
        }
        mPresenter.onSubscriber();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_employee_dashboard, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mText.setText(getCurrentDayText());
        mTitle.setText(getHelloText());
    }

    private String getHelloText() {
        return PreferenceUtil.getInstance(getContext()).getUserName();
    }

    private String getCurrentDayText() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM, EEE", Locale.getDefault());
        return dateFormat.format(calendar.getTime());
    }


    @Override
    public void onLoading() {
      atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
        tvEmpty.setVisibility(View.VISIBLE);
        tvEmpty.setText(error);
        recyclerView.setEmptyView(tvEmpty);
    }

    @Override
    public void onResponse(EmpDashboardResponse response) {

    }

    @Override
    public void onComplete() {
       atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onUnSubscriber();
    }


    @OnClick({  R.id.llAttendance,//1
            R.id.llTimetable,//2
            R.id.llTransport,//3
            R.id.llProfile,//4
            R.id.llDiscipline,//5
            R.id.llExperiance,//6
            R.id.llInterview,//7
            R.id.llLeave,//8
            R.id.llSalary,//9
            R.id.llSyllabus,//10
            R.id.llQualification//11
    })
    public void selectFragment(View view) {
        Fragment fragment = null;

        TeachersActivity teacherActivity= (TeachersActivity) getActivity();

        switch (view.getId()) {

            case R.id.llAttendance:
                fragment = EmpAttendanceFragment.newInstance();
                break;

            case R.id.llTimetable:
                fragment = EmpTimeTableFragment.newInstance();
                break;

            case R.id.llTransport:
                fragment = new EmpTransportationFragment();
                break;

            case R.id.llProfile:
                fragment = EmpProfileFragment.newInstance();
                break;

            case R.id.llDiscipline:
                fragment = new EmpDisciplinaryFragment();
                break;

            case R.id.llExperiance:
                fragment = new EmpExperienceFragment();
                break;

            case R.id.llInterview:
                fragment = EmpInterViewScheduleFragment.newInstance();
                break;
            case R.id.llLeave:
                fragment=new EmpLeaveRecordFragment();
                break;

            case R.id.llSalary:
                fragment = new EmpSalaryFragment();
                break;

            case R.id.llSyllabus:
                fragment = EmpSyllabusFragment.newInstance();
                break;

            case R.id.llQualification:
                fragment = EmpQualificationFragment.newInstance();
                break;

        }

        //   updateToolbarText(item.getTitle());

        if (fragment != null) {
            teacherActivity.changeFragment(fragment, true);
        }
    }


    @Override
    public void renderTimetable(EmpDashboardResponse response) {
        if(response.notificationCount>0) {
            if (getActivity() != null) {
                ((TeachersActivity) getActivity()).setNotificationCount(response.notificationCount);
            }
        }

        if(response.success) {
            DashboardAdapter adapter = new DashboardAdapter(getActivity(), response.mList);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
        }else{
            onShowError(response.message);
        }
    }
}
