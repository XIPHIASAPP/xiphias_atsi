package com.xiphiastec.newatsi.teacher.syllabus;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.SyllabusCovered;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 5/3/18.
 */

class EmpSyllabusAdapter extends RecyclerView.Adapter<EmpSyllabusAdapter.SyllabusViewHolder> {

    private final List<SyllabusCovered> syllabusCovered;
    Context context;

    EmpSyllabusAdapter(Context context,List<SyllabusCovered> syllabusCovered) {
        this.syllabusCovered = syllabusCovered;
        this.context=context;
    }

    @Override
    public EmpSyllabusAdapter.SyllabusViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EmpSyllabusAdapter.SyllabusViewHolder(LayoutInflater.from(context).inflate(R.layout.item_emp_syllabus, parent, false));
    }

    @Override
    public void onBindViewHolder(EmpSyllabusAdapter.SyllabusViewHolder holder, int position) {
        SyllabusCovered covered = syllabusCovered.get(position);
        holder.date.setText(covered.date);
        holder.subjectGroup.setText(covered.subjectGroup);
        holder.subjectName.setText(covered.subjectName);
        holder.topicName.setText(covered.topicName);
    }

    @Override
    public int getItemCount() {
        return syllabusCovered.size();
    }

    public class SyllabusViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.subject_name)
        TextView subjectName;
        @BindView(R.id.topic_name)
        TextView topicName;
        @BindView(R.id.subject_group)
        TextView subjectGroup;
        @BindView(R.id.date)
        TextView date;

        public SyllabusViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
