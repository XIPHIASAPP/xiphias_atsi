package com.xiphiastec.newatsi.teacher.profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.EmployeeFamily;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 8/3/18.
 */

class EmpFamilyAdapter extends RecyclerView.Adapter<EmpFamilyAdapter.FamilyViewHolder> {

    private final ArrayList<EmployeeFamily> employeeFamily;
    Context context;
    EmpFamilyAdapter(Context context,ArrayList<EmployeeFamily> employeeFamily) {
        this.employeeFamily = employeeFamily;
        this.context=context;
    }

    @Override
    public EmpFamilyAdapter.FamilyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EmpFamilyAdapter.FamilyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_emp_family, parent, false));
    }

    @Override
    public void onBindViewHolder(EmpFamilyAdapter.FamilyViewHolder holder, int position) {
        EmployeeFamily family = employeeFamily.get(position);
        holder.tvName.setText(family.parentName);
        holder.tvNumber.setText(family.parentMobileNumber);
        holder.tvRelation.setText(family.relationType);
        holder.tvCompany.setText( family.company+ " - " + family.designation);
    }

    @Override
    public int getItemCount() {
        return employeeFamily.size();
    }

    public class FamilyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvNumber)
        TextView tvNumber;
        @BindView(R.id.tvRelation)
        TextView tvRelation;
        @BindView(R.id.tvCompany)
        TextView tvCompany;

        public FamilyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
