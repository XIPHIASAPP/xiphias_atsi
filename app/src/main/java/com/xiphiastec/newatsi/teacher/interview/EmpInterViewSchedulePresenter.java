package com.xiphiastec.newatsi.teacher.interview;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.teacher.AbsEmployeePresenter;

/**
 * Created by hemanths on 11/08/17.
 */

public class EmpInterViewSchedulePresenter extends AbsEmployeePresenter
        implements EmpInterViewScheduleContract.Presenter {
    @NonNull
    private EmpInterViewScheduleContract.View mView;

    EmpInterViewSchedulePresenter(@NonNull EmpInterViewScheduleContract.View view) {
        mView = view;
    }

    @Override
    public void interviewList() {
        mDisposable.add(mService.interview()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status && response.mList.size() > 0) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    @Override
    public void onSubscriber() {
        interviewList();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }




}
