package com.xiphiastec.newatsi.teacher.interview;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.EmpInterViewScheduleResponse;

public interface EmpInterViewScheduleContract {
    interface View extends BaseView<EmpInterViewScheduleResponse> {

    }

    interface Presenter extends BasePresenter<View> {
        void interviewList();
    }
}
