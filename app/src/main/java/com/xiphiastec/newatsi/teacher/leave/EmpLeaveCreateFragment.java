package com.xiphiastec.newatsi.teacher.leave;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsEmployeeBaseFragment;
import com.xiphiastec.newatsi.model.StatusResponse;
import com.xiphiastec.newatsi.teacher.leave.contract.EmpCreateLeaveContract;
import com.xiphiastec.newatsi.model.Leave;
import com.xiphiastec.newatsi.model.LeaveDetails;
import com.xiphiastec.newatsi.model.LeaveType;
import com.xiphiastec.newatsi.model.LeaveType.ListLeaveType;
import com.xiphiastec.newatsi.teacher.leave.presenter.EmpCreateLeavePresenter;
import com.xiphiastec.newatsi.model.EmployeeDetailsBody;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;

import static com.xiphiastec.newatsi.Constants.ARGS_FRAGMENT_TITLE;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmpLeaveCreateFragment extends AbsEmployeeBaseFragment
        implements AdapterView.OnItemSelectedListener, EmpCreateLeaveContract.View,DatePickerDialog.OnDateSetListener {
    private static final String TAG = "EmpLeaveCreate";
    @BindView(R.id.leave_type)
    Spinner leaveType;
    @BindView(R.id.min_days)
    TextView minDays;
    @BindView(R.id.max_days)
    TextView maxDays;
    @BindView(R.id.balance)
    TextView balance;
    @BindView(R.id.applicant_id)
    TextView applicantId;
    @BindView(R.id.from_half_day)
    CheckBox fromHalfDay;
    @BindView(R.id.to_half_day)
    CheckBox toHalfDay;
    @BindView(R.id.from_date_text)
    TextView fromDateText;
    @BindView(R.id.to_date_text)
    TextView toDateText;
    @BindView(R.id.root_layout)
    LinearLayout rootLayout;
    Unbinder unbinder;
    @BindView(R.id.reason)
    EditText reason;

    private DatePickerDialog fromPickerDialog;
    private DatePickerDialog toPickerDialog;
    private EmpCreateLeavePresenter mPresenter;
    private ArrayAdapter<String> mListAdapter;
    private List<ListLeaveType> listTypes;
    private CompositeDisposable mDisposable;

    AtsiProgressDialog atsiProgressDialog;

    public EmpLeaveCreateFragment() {
        // Required empty public constructor
    }

    public static EmpLeaveCreateFragment newInstance(String title) {
        Bundle args = new Bundle();
        args.putString(ARGS_FRAGMENT_TITLE, title);
        EmpLeaveCreateFragment fragment = new EmpLeaveCreateFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && getActionBar() != null && getArguments() != null) {
            getActionBar().setTitle(getArguments().getString(ARGS_FRAGMENT_TITLE));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActionBar() != null && getArguments() != null)
            getActionBar().setTitle(getArguments().getString(ARGS_FRAGMENT_TITLE));

    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_employee_leave_create, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new EmpCreateLeavePresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        listTypes=new ArrayList<>();

        mPresenter.leaveTypes();

        mListAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1);
        leaveType.setAdapter(mListAdapter);

        leaveType.setOnItemSelectedListener(this);
        Calendar calendar = Calendar.getInstance();


        fromPickerDialog = new DatePickerDialog(getActivity(),this,calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));


        fromPickerDialog.getDatePicker().setMinDate(new Date().getTime());

        toPickerDialog = new DatePickerDialog(getActivity(),this,calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));

        toPickerDialog.getDatePicker().setMinDate(new Date().getTime());

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        //mDisposable.clear();
    }

    @OnClick({R.id.save, R.id.from_date, R.id.to_date})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.save:
                String leaveFrom = fromDateText.getText().toString();
                String leaveTo = toDateText.getText().toString();
                String min = minDays.getText().toString();
                String max = maxDays.getText().toString();
                String balance = this.balance.getText().toString();
                boolean isFromHalfDay = fromHalfDay.isChecked();
                boolean isToHalfDay = toHalfDay.isChecked();
                String leaveTypeId = listTypes.get(leaveType.getSelectedItemPosition()).leaveId;
                String applicationId = applicantId.getText().toString();
                String reason = this.reason.getText().toString();

                Leave leave = new Leave();
                leave.setLeaveFrom(leaveFrom);
                leave.setLeaveTo(leaveTo);
                leave.setMinimunLimit(min);
                leave.setMaximumLimit(max);
                leave.setBalance(balance);
                leave.setHalfDayFrom(String.valueOf(isFromHalfDay));
                leave.setHalfDayTo(String.valueOf(isToHalfDay));
                leave.setReason(reason);
                leave.setLeaveType(leaveTypeId);
                leave.setApplicationId(applicationId);
                mPresenter.createLeave(leave);

                break;
            case R.id.from_date:
                if(fromPickerDialog!=null && toPickerDialog.isShowing()) {
                    // no need to call dialog.show(ft, "DatePicker");
                } else {
                    fromPickerDialog.show();
                }

                break;
            case R.id.to_date:
                if(toPickerDialog!=null && toPickerDialog.isShowing()) {
                    // no need to call dialog.show(ft, "DatePicker");
                } else {
                    toPickerDialog.show();
                }
                break;
        }
    }


    @Override
    public void onLoading() {
        atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
        atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onResponse(StatusResponse response) {
        Activity activity = getActivity();
        if (activity == null) {
            return;
        }
        Toast.makeText(activity, response.message, Toast.LENGTH_SHORT).show();

        getActivity().onBackPressed();
    }

    @Override
    public void onComplete() {
        atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(listTypes.size()>0) {
            String leaveId = listTypes.get(position).leaveId;

            if (leaveId == null) {
                return;
            }
            EmployeeDetailsBody body = new EmployeeDetailsBody();
            body.setLeaveId(leaveId);
            mPresenter.leaveDetails(body);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onLeaveTypesResponse(LeaveType response) {
        mListAdapter.clear();
        atsiProgressDialog.dismissDialog();
        for (ListLeaveType type : response.listLeaveType) {
            listTypes.add(type);
            mListAdapter.add(type.leaveType);
            mListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLeaveDetailsResponse(LeaveDetails response) {
        minDays.setText(response.getLeaveBalance().get(0).minimunLimit);
        maxDays.setText(response.getLeaveBalance().get(0).maximumLimit);
        balance.setText(response.getLeaveBalance().get(0).balance);
        applicantId.setText(response.getLeaveBalance().get(0).applicationId);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String strMonth = String.valueOf(month+1);
        String day = String.valueOf(dayOfMonth);
        if (month < 10) {
            strMonth = "0" + month;
        }
        if (dayOfMonth < 10) {
            day = "0" + dayOfMonth;
        }
        if(toPickerDialog.isShowing())
            toDateText.setText(year + "/" + strMonth + "/" + day);
        else if(fromPickerDialog.isShowing())
            fromDateText.setText(year + "/" + strMonth + "/" + day);

    }
}
