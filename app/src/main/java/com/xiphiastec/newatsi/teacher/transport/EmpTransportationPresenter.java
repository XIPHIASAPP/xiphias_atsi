package com.xiphiastec.newatsi.teacher.transport;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.teacher.AbsEmployeePresenter;

/**
 * Created by hemanths on 11/08/17.
 */

public class EmpTransportationPresenter extends AbsEmployeePresenter
        implements EmpTransportationContract.Presenter {
    @NonNull
    private EmpTransportationContract.View mView;

    EmpTransportationPresenter(@NonNull EmpTransportationContract.View view) {
        mView = view;
    }

    @Override
    public void transportation() {
        mDisposable.add(mService.transportation()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status && response.mList.size() > 0) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    @Override
    public void onSubscriber() {
        transportation();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }


}
