package com.xiphiastec.newatsi.teacher.timetable;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.teacher.AbsEmployeePresenter;

/**
 * Created by hemanths on 21/08/17.
 */

public class EmpTimeTablePresenter extends AbsEmployeePresenter
        implements EmpTimeTableContract.Presenter {
    @NonNull
    private EmpTimeTableContract.View mView;

    EmpTimeTablePresenter(@NonNull EmpTimeTableContract.View view) {
        mView = view;
    }

    @Override
    public void timeTable() {
        mDisposable.add(mService.timeTable()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.success) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    @Override
    public void onSubscriber() {
        timeTable();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }


}

