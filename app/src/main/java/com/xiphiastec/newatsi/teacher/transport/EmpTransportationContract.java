package com.xiphiastec.newatsi.teacher.transport;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.EmpTransportationResponse;

/**
 * Created by xiphias0101 on 12/12/2017.
 */

public interface EmpTransportationContract {
    interface View extends BaseView<EmpTransportationResponse> {

    }

    interface Presenter extends BasePresenter<View> {
        void transportation();
    }
}
