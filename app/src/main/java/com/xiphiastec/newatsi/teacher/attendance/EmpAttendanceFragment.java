package com.xiphiastec.newatsi.teacher.attendance;


import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.transition.TransitionManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsParentBaseFragment;
import com.xiphiastec.newatsi.model.AttendanceType;
import com.xiphiastec.newatsi.model.EmployeeAttendanceResponse;
import com.xiphiastec.newatsi.model.TypeList;
import com.xiphiastec.newatsi.teacher.CustomTypefaceSpan;
import com.xiphiastec.newatsi.model.EmployeeDetailsBody;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class EmpAttendanceFragment extends AbsParentBaseFragment implements
        DatePickerDialog.OnDateSetListener,
        EmpAttendanceContract.View {
    private static final String TAG = "EmployeeAttendance";

    @BindView(R.id.recycler_view)
    EmptyViewRecyclerView recyclerView;
    @BindView(R.id.tv_empty)
    TextView tvEmpty;
    Unbinder unbinder;
    @BindView(R.id.root_layout)
    LinearLayout rootLayout;
    @BindView(R.id.type)
    Spinner type;
    @BindView(R.id.shift)
    Spinner shift;
    @BindView(R.id.search_date)
    AppCompatTextView searchDate;
    private EmpAttendancePresenter mPresenter;
    private AttendanceAdapter adapter;
    private DatePickerDialog mDatePickerDialog;
    private ArrayAdapter<String> typeAdapter;
    private ArrayAdapter<String> shiftAdapter;
    private TypeList typeList;
    private Calendar mCalendar;
    private SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    AtsiProgressDialog atsiProgressDialog;

    public EmpAttendanceFragment() {
        // Required empty public constructor
    }

    public static EmpAttendanceFragment newInstance() {

        Bundle args = new Bundle();

        EmpAttendanceFragment fragment = new EmpAttendanceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mPresenter = new EmpAttendancePresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && getActionBar() != null) {
            getActionBar().setTitle(R.string.attendance);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActionBar() != null) {
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/vollkorn.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(getString(R.string.attendance));
            SS.setSpan (new CustomTypefaceSpan("", typeface), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getActionBar().setTitle(SS);
        }
        String formattedDate = mSimpleDateFormat.format(mCalendar.getTime());
        searchDate.setText(formattedDate);
        mPresenter.onSubscriber();
    }

    private void initSetup() {
        Context context = getContext();
        if (context == null) {
            return;
        }
        typeAdapter = new ArrayAdapter<>(context, R.layout.spinner_item);
        typeAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        type.setAdapter(typeAdapter);

        shiftAdapter = new ArrayAdapter<>(context, R.layout.spinner_item);
        shiftAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        shift.setAdapter(shiftAdapter);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_employee_attendance, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initSetup();
        setupRecyclerView();


        type.post(new Runnable() {
            @Override
            public void run() {
                type.setDropDownVerticalOffset(type.getDropDownVerticalOffset() + type.getHeight());
            }
        });

        shift.post(new Runnable() {
            @Override
            public void run() {
                shift.setDropDownVerticalOffset(shift.getDropDownVerticalOffset() + shift.getHeight());
            }
        });

        mCalendar = Calendar.getInstance();

//        mDatePickerDialog = DatePickerDialog.newInstance(
//                this,
//                mCalendar.get(Calendar.YEAR),
//                mCalendar.get(Calendar.MONTH),
//                mCalendar.get(Calendar.DAY_OF_MONTH)
//        );
//        mDatePickerDialog.setMaxDate(mCalendar);

        mDatePickerDialog= new DatePickerDialog(getActivity(), this,
                mCalendar.get(Calendar.YEAR),
                mCalendar.get(Calendar.MONTH),
                mCalendar.get(Calendar.DAY_OF_MONTH));

        mDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime());


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.removeItem(R.id.action_notifications);
    }

    @Override
    public void onLoading() {
        atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
        tvEmpty.setVisibility(View.VISIBLE);
        tvEmpty.setText(error);
        tvEmpty.setTextColor(ContextCompat.getColor(getContext(), R.color.material_light_white));
        recyclerView.setEmptyView(tvEmpty);
    }

    @Override
    public void onResponse(EmployeeAttendanceResponse response) {
        adapter.swapData(response.mList);
    }

    @Override
    public void onAttendanceResponse(TypeList response) {
        typeList = response;
        typeAdapter.clear();
        shiftAdapter.clear();

        typeAdapter.add("--Select Attendance--");
        shiftAdapter.add("--Select Shift--");

        for (AttendanceType attendanceType : typeList.attendenceType) {
            typeAdapter.add(attendanceType.attendenceTypeName);
            typeAdapter.notifyDataSetChanged();
        }
        for (TypeList.ShiftList shiftList : typeList.shiftList) {
            shiftAdapter.add(shiftList.shiftName);
            shiftAdapter.notifyDataSetChanged();
        }
    }

    private void setupRecyclerView() {
        adapter = new AttendanceAdapter(getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onComplete() {
        TransitionManager.beginDelayedTransition(rootLayout);
        atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.pick_date, R.id.search})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.pick_date:
                mDatePickerDialog.show();
                break;
            case R.id.search:
                String typeId = "";
                String shiftId = "";
                int position = type.getSelectedItemPosition();
                if (position == 0) {
                    Snackbar.make(rootLayout, "Please select attendance!", Snackbar.LENGTH_SHORT).show();
                    return;
                } else {
                    if (typeList.attendenceType.size() > 0)
                        typeId = typeList.attendenceType.get(position - 1).attendenceTypeId;
                }
                position = shift.getSelectedItemPosition();
                if (position == 0) {
                    Snackbar.make(rootLayout, "Please select shift!", Snackbar.LENGTH_SHORT).show();
                    return;
                } else {
                    if (typeList.shiftList.size() > 0)
                        shiftId = typeList.shiftList.get(shift.getSelectedItemPosition() - 1).shiftId;
                }

                String dateString = searchDate.getText().toString();

                if (dateString.isEmpty()) {
                    Snackbar.make(rootLayout, "Please select date.", Snackbar.LENGTH_SHORT).show();
                    return;
                }
                EmployeeDetailsBody body = new EmployeeDetailsBody();
                body.setAttendanceTypeId(typeId);
                body.setMonthYear(dateString);
                body.setShiftId(shiftId);
                mPresenter.attendance(body);
                break;
        }
    }

//    @Override
//    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
//        String month = String.valueOf(++monthOfYear);
//        String day = String.valueOf(dayOfMonth);
//        if (monthOfYear < 10) {
//            month = "0" + monthOfYear;
//        }
//        if (dayOfMonth < 10) {
//            day = "0" + dayOfMonth;
//        }
//        searchDate.setText(year + "-" + month + "-" + day);
//    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String strMonth = String.valueOf(++month);
        String day = String.valueOf(dayOfMonth);
        if (month < 10) {
            strMonth = "0" + month;
        }
        if (dayOfMonth < 10) {
            day = "0" + dayOfMonth;
        }
        searchDate.setText(year + "-" + strMonth + "-" + day);
    }
}
