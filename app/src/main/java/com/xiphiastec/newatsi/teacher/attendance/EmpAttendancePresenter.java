package com.xiphiastec.newatsi.teacher.attendance;

import android.support.annotation.NonNull;


import com.xiphiastec.newatsi.teacher.AbsEmployeePresenter;
import com.xiphiastec.newatsi.model.EmployeeDetailsBody;


public class EmpAttendancePresenter extends AbsEmployeePresenter
        implements EmpAttendanceContract.Presenter {
    @NonNull
    private EmpAttendanceContract.View mView;

    EmpAttendancePresenter(@NonNull EmpAttendanceContract.View view) {
        mView = view;
    }

    public void attendance() {
        mDisposable.add(mService.attendance()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status && response.mList.size() > 0) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    @Override
    public void attendanceTypes() {
        mDisposable.add(mService.attendanceTypeList()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status && response.shiftList.size() > 0) {
                                mView.onAttendanceResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    @Override
    public void attendance(EmployeeDetailsBody body) {
        mDisposable.add(mService.attendance(body)
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .doOnSubscribe(disposable -> mView.onLoading())
                .subscribe(response -> {
                            if (response.status && response.mList.size() > 0) {
                                mView.onResponse(response);
                            } else {
                                mView.onShowError(response.message);
                            }
                        }, throwable -> mView.onShowError(exceptionInReadableString(throwable)),
                        () -> mView.onComplete()));
    }

    @Override
    public void onSubscriber() {
        //attendance();
        attendanceTypes();
    }

    @Override
    public void onUnSubscriber() {
        mDisposable.clear();
    }

}
