package com.xiphiastec.newatsi.teacher.experience;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.EmployeeExperienceInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 5/3/18.
 */

class ExperienceAdapter extends RecyclerView.Adapter<ExperienceAdapter.ExperienceViewHolder> {


    private List<EmployeeExperienceInfo> list = new ArrayList<>();
    private Context context;

    ExperienceAdapter(Context context,List<EmployeeExperienceInfo> list) {
        this.context=context;
        this.list = list;
    }

    @Override
    public ExperienceAdapter.ExperienceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ExperienceAdapter.ExperienceViewHolder(LayoutInflater.from(context).inflate(R.layout.item_experience, parent, false));
    }

    @Override
    public void onBindViewHolder(ExperienceAdapter.ExperienceViewHolder holder, int position) {
        EmployeeExperienceInfo info = list.get(position);
        holder.date.setText(info.startDate + "-" + info.endDate);
        holder.designationBranch.setText(info.designation + " - " + info.branch);
        holder.salary.setText("₹" + info.salary);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ExperienceViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.designation_branch)
        TextView designationBranch;
        @BindView(R.id.salary)
        TextView salary;

        public ExperienceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}