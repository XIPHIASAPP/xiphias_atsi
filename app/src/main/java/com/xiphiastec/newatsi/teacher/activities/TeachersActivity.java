package com.xiphiastec.newatsi.teacher.activities;

import android.content.Context;
import android.content.Intent;
import android.support.transition.TransitionManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.login.ChangePasswordActivity;
import com.xiphiastec.newatsi.login.LoginActivity;
import com.xiphiastec.newatsi.student.notification.NotificationActivity;
import com.xiphiastec.newatsi.teacher.dashboard.EmpDashboardFragment;
import com.xiphiastec.newatsi.util.PreferenceUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TeachersActivity extends AppCompatActivity {

    public static final String TAG="TEACHERSACTIVITY";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    private TextView txtViewCount;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher2);

        ButterKnife.bind(this);

        mToolbar.setTitle("Dashboard");
        setSupportActionBar(mToolbar);

        setUpNavigation();

        changeFragment(new EmpDashboardFragment(), false);
    }


    public void changeFragment(Fragment fragment, boolean hasAddBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up);
        transaction.replace(R.id.contentContainer, fragment, TAG);
        if (hasAddBackStack) {
            transaction.addToBackStack(null).commit();
            return;
        }
        transaction.commit();
    }


    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        }else{
            super.onBackPressed();
        }
    }

    public void setNotificationCount(int count) {
        TransitionManager.beginDelayedTransition(mToolbar);
        txtViewCount.setVisibility(View.VISIBLE);
        txtViewCount.setText(String.valueOf(count));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        final View notificaitons = menu.findItem(R.id.action_notifications).getActionView();
        txtViewCount = notificaitons.findViewById(R.id.txtCount);
        notificaitons.setOnClickListener(v -> startActivity(new Intent(TeachersActivity.this, NotificationActivity.class)));
        return super.onCreateOptionsMenu(menu);
    }

    private void selectFragment(MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.action_logout:
                PreferenceUtil.getInstance(this).clear();
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                Toast.makeText(this, "Logout successfully", Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_change_password:
                startActivity(new Intent(this, ChangePasswordActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                break;

        }
        overridePendingTransition(0, 0);

        updateToolbarText(item.getTitle());

        if (fragment != null) {
            changeFragment(fragment, false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        selectFragment(item);
        return super.onOptionsItemSelected(item);
    }

    private void updateToolbarText(CharSequence title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    private void setUpNavigation() {
        mToolbar.setNavigationOnClickListener(view -> {
                onBackPressed();
        });

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }catch(Exception e){
            Log.d(TAG,"Action bar error");
        }
//        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
//            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
//                //noinspection ConstantConditions
//                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            } else {
//                //noinspection ConstantConditions
//                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//            }
//        });
    }

}
