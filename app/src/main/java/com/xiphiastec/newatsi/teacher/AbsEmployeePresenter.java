package com.xiphiastec.newatsi.teacher;

import android.support.annotation.NonNull;

import com.xiphiastec.newatsi.app.ATSIApplication;
import com.xiphiastec.newatsi.base.AbsBasePresenter;
import com.xiphiastec.newatsi.base.interfaces.EmployeeApiService;
import com.xiphiastec.newatsi.provider.InjectionImpl;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Admin on 25-04-2017.
 */

public abstract class AbsEmployeePresenter extends AbsBasePresenter {

    @NonNull
    protected CompositeDisposable mDisposable;
    @NonNull
    protected EmployeeApiService mService;

    public AbsEmployeePresenter() {
        mService = new InjectionImpl().getEmployeeServices(ATSIApplication.getInstance());
        mDisposable = new CompositeDisposable();
    }
}
