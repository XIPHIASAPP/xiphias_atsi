package com.xiphiastec.newatsi.teacher.leave;

import android.app.Dialog;
import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.CancelRequestBody;
import com.xiphiastec.newatsi.model.LeaveStatusResponse;
import com.xiphiastec.newatsi.model.StatusResponse;
import com.xiphiastec.newatsi.teacher.leave.contract.EmpLeaveListContract;
import com.xiphiastec.newatsi.model.EmpLeaveResponse;
import com.xiphiastec.newatsi.model.EmployeeLeaveInformation;
import com.xiphiastec.newatsi.teacher.leave.presenter.EmpLeaveListPresenter;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 8/3/18.
 */

class LeaveAdapter extends RecyclerView.Adapter<LeaveAdapter.LeaveViewHolder> implements EmpLeaveListContract.View {

    private final List<EmployeeLeaveInformation> list;
    Context context;
    String leaveStatus;
    AtsiProgressDialog atsiProgressDialog;
    private EmpLeaveListPresenter mPresenter;

    LeaveAdapter(Context context,List<EmployeeLeaveInformation> list,String leaveStatus) {
        this.list = list;
        this.context=context;
        this.leaveStatus=leaveStatus;
        atsiProgressDialog=new AtsiProgressDialog(context,R.style.full_screen_dialog);
        mPresenter = new EmpLeaveListPresenter(this);
    }

    @Override
    public LeaveViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LeaveViewHolder(LayoutInflater.from(context).inflate(R.layout.item_leave_employee, parent, false));
    }

    @Override
    public void onBindViewHolder(LeaveViewHolder holder, int position) {

        EmployeeLeaveInformation information = list.get(position);

        holder.tvLeaveId.setText(information.leaveApplicationId);
        holder.tvLeaveFrom.setText(information.leaveFrom);
        holder.tvLeaveTo.setText(information.leaveTo);
        holder.tvLeaveType.setText(information.leaveType);
        holder.tvApprover.setText(information.approverName);
        try {
            if (information.approverStatus.equalsIgnoreCase("Pending")) {
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.material_red_800));
            } else if (information.approverStatus.equalsIgnoreCase("Approved")) {
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.material_green_800));
            } else if(information.approverStatus.equalsIgnoreCase("Cancelled")) {
                holder.ivCancel.setVisibility(View.GONE);
            }else{
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.material_indigo_800));
                holder.ivCancel.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){
            Log.e("Leave Record",e.getMessage());
        }
        holder.tvStatus.setText(information.approverStatus);


        holder.ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_leave_reason);

                WindowManager.LayoutParams lWindowParams = new WindowManager.LayoutParams();
                lWindowParams.width = WindowManager.LayoutParams.MATCH_PARENT; // this is where the magic happens
                lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                dialog.getWindow().setAttributes(lWindowParams);

                // set the custom dialog components - text, image and button
                TextInputEditText txtTask = (TextInputEditText) dialog.findViewById(R.id.tvReason);
                Button btnSend = (Button) dialog.findViewById(R.id.btnConfirm);

                btnSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!(txtTask.getText().toString().isEmpty())) {
                            CancelRequestBody body = new CancelRequestBody();
                            body.setmApplicationId(information.leaveApplicationId);
                            body.setReason(txtTask.getText().toString());
                            mPresenter.cancelLeave(body);
                        }else{
                            Toast.makeText(context,"Please Enter reason to cancel leave",Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });


    }

    private String getFormattedString(EmployeeLeaveInformation information) {
        String finalString;
        finalString = "<b>From: </b>" + information.leaveFrom + " - <b>To: </b>" + information.leaveTo;
        return finalString;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onLoading() {
        atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
        Toast.makeText(context,error,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onComplete() {
        atsiProgressDialog.dismissDialog();

    }

    @Override
    public void onResponse(EmpLeaveResponse response) {

    }

    @Override
    public void renderLeaveStatus(LeaveStatusResponse response) {

    }

    @Override
    public void renderCancalStatus(StatusResponse response) {
        if(response.status){
            Toast.makeText(context,response.message,Toast.LENGTH_SHORT).show();
        }
    }

    public class LeaveViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvLeaveId)
        TextView tvLeaveId;
        @BindView(R.id.tvLeaveFrom)
        TextView tvLeaveFrom;
        @BindView(R.id.tvLeaveTo)
        TextView tvLeaveTo;
        @BindView(R.id.tvLeaveType)
        TextView tvLeaveType;
        @BindView(R.id.tvApprover)
        TextView tvApprover;
        @BindView(R.id.tvStatus)
        TextView tvStatus;

        @BindView(R.id.ivCancel)
        ImageView ivCancel;


        public LeaveViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}