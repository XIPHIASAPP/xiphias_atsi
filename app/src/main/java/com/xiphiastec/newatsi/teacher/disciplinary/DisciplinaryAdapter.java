package com.xiphiastec.newatsi.teacher.disciplinary;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.model.EmployeeDesciplinary;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pavan-xiphias on 5/3/18.
 */

class DisciplinaryAdapter extends RecyclerView.Adapter<DisciplinaryAdapter.DisciplineViewHolder> {

    List<EmployeeDesciplinary> list;

    Context context;

    public DisciplinaryAdapter(Context context,List<EmployeeDesciplinary> infos) {
        this.context=context;
        this.list = infos;
    }

    @Override
    public DisciplinaryAdapter.DisciplineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DisciplinaryAdapter.DisciplineViewHolder(LayoutInflater.from(context).inflate(R.layout.item_emp_disciplinary, parent, false));
    }

    @Override
    public void onBindViewHolder(DisciplinaryAdapter.DisciplineViewHolder holder, int position) {
        EmployeeDesciplinary info = list.get(position);
        holder.tvIndiscipline.setText(info.indisciplineAct);
        holder.tvReason.setText(info.descriptionByEmp);
        holder.tvCreated.setText(info.createdByName + " - " + info.createdOn);
        holder.tvAction.setText(info.actionByManagmt);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DisciplineViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvIndiscipline)
        TextView tvIndiscipline;
        @BindView(R.id.tvReason)
        TextView tvReason;
        @BindView(R.id.tvAction)
        TextView tvAction;
        @BindView(R.id.tvCreated)
        TextView tvCreated;

        public DisciplineViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}