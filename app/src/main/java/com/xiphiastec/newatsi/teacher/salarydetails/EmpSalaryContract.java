package com.xiphiastec.newatsi.teacher.salarydetails;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.EmpSalaryResponse;
import com.xiphiastec.newatsi.model.EmployeeDetailsBody;

/**
 * Created by xiphias0101 on 12/12/2017.
 */

public interface EmpSalaryContract {
    interface View extends BaseView<EmpSalaryResponse> {

    }

    interface Presenter extends BasePresenter<View> {
        void salary(EmployeeDetailsBody body);
    }
}
