package com.xiphiastec.newatsi.teacher.disciplinary;

import com.xiphiastec.newatsi.base.interfaces.BasePresenter;
import com.xiphiastec.newatsi.base.interfaces.BaseView;
import com.xiphiastec.newatsi.model.EmpDisciplinaryResponse;

/**
 * Created by xiphias0101 on 12/12/2017.
 */

public interface EmpDisciplinaryContract {
    interface View extends BaseView<EmpDisciplinaryResponse> {

    }

    interface Presenter extends BasePresenter<View> {
        void disciplinary();
    }
}
