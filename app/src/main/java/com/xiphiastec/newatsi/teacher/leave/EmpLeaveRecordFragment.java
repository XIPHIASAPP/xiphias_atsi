package com.xiphiastec.newatsi.teacher.leave;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.base.AbsEmployeeBaseFragment;
import com.xiphiastec.newatsi.model.LeaveRequestBody;
import com.xiphiastec.newatsi.model.LeaveStatus;
import com.xiphiastec.newatsi.model.LeaveStatusResponse;
import com.xiphiastec.newatsi.model.StatusResponse;
import com.xiphiastec.newatsi.teacher.activities.TeachersActivity;
import com.xiphiastec.newatsi.teacher.leave.contract.EmpLeaveListContract;
import com.xiphiastec.newatsi.model.EmpLeaveResponse;
import com.xiphiastec.newatsi.teacher.leave.presenter.EmpLeaveListPresenter;
import com.xiphiastec.newatsi.util.AtsiProgressDialog;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.xiphiastec.newatsi.Constants.ARGS_FRAGMENT_TITLE;

public class EmpLeaveRecordFragment extends AbsEmployeeBaseFragment implements EmpLeaveListContract.View {

    @BindView(R.id.recycler_view)
    EmptyViewRecyclerView recyclerView;
    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    @BindView(R.id.fabCreateLeave)
    FloatingActionButton fabCreateLeave;


    @BindView(R.id.spnLeaveStatus)
    Spinner spnLeaveStatus;


    AtsiProgressDialog atsiProgressDialog;
    Unbinder unbinder;
    private EmpLeaveListPresenter mPresenter;

    private ArrayAdapter<String> mLeaveStatusAdapter;

    List<LeaveStatus> list;

    public static EmpLeaveRecordFragment newInstance(String title) {

        Bundle args = new Bundle();
        args.putString(ARGS_FRAGMENT_TITLE, title);
        EmpLeaveRecordFragment fragment = new EmpLeaveRecordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && getActionBar() != null && getArguments() != null) {
            getActionBar().setTitle(getArguments().getString(ARGS_FRAGMENT_TITLE));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActionBar() != null && getArguments() != null) {
            getActionBar().setTitle(getArguments().getString(ARGS_FRAGMENT_TITLE));
        }

//        LeaveRequestBody leaveRequestBody=new LeaveRequestBody();
//        leaveRequestBody.setLeaveStatus(0);
//        mPresenter.getLeaveDetails(leaveRequestBody);
//
        mPresenter.getLeaveStatus();

        mPresenter.onSubscriber();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onUnSubscriber();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mPresenter = new EmpLeaveListPresenter(this);
        atsiProgressDialog=new AtsiProgressDialog(getActivity(),R.style.full_screen_dialog);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.removeItem(R.id.action_notifications);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.leave_fragment_employee, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    private void setupSpinner(LeaveStatusResponse response) {

        list=response.getmListStatus();

        List<String> status=getStrStatus(list);

        mLeaveStatusAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item,status);
        mLeaveStatusAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spnLeaveStatus.setAdapter(mLeaveStatusAdapter);
        spnLeaveStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position!=0) {
                    LeaveRequestBody leaveRequestBody = new LeaveRequestBody();
                    leaveRequestBody.setLeaveStatus(list.get(position-1).getmStatusId());
                    mPresenter.getLeaveDetails(leaveRequestBody);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    @Override
    public void onLoading() {
       atsiProgressDialog.showDialog();
    }

    @Override
    public void onShowError(String error) {
        atsiProgressDialog.dismissDialog();
        tvEmpty.setText(error);
        tvEmpty.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(EmpLeaveResponse response) {

        recyclerView.setVisibility(View.VISIBLE);
        tvEmpty.setVisibility(View.GONE);
        if (response.mLeaveInformations.size() > 0) {
            LeaveAdapter adapter = new LeaveAdapter(getActivity(),response.mLeaveInformations,String.valueOf(spnLeaveStatus.getSelectedItem()).trim());
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
        } else {
            showEmpty(response);
        }

    }

    private void showEmpty(EmpLeaveResponse response) {
        tvEmpty.setText(response.message);
        tvEmpty.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void onComplete() {
       atsiProgressDialog.dismissDialog();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.fabCreateLeave)
    public void onFabClicked(){
        TeachersActivity activity = (TeachersActivity) getActivity();

        if (activity != null) {
            activity.changeFragment(EmpLeaveCreateFragment.newInstance("Create leave"), true);
        }
    }

    @Override
    public void renderLeaveStatus(LeaveStatusResponse response) {
        setupSpinner(response);
    }

    @Override
    public void renderCancalStatus(StatusResponse response) {

    }

    public List<String> getStrStatus(List<LeaveStatus> list){
        List<String> status=new ArrayList<>();
        status.add("Select Leave Status");
        for(int i=0;i<list.size();i++){
            status.add(list.get(i).getmStatusName());
        }
        return status;
    }
}
