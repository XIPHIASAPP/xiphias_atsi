package com.xiphiastec.newatsi.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xiphiastec.newatsi.R;


public class CardItemView extends FrameLayout {
    private String mTitleText;
    private Drawable mDrawable;
    private int mImageSize;

    public CardItemView(@NonNull Context context) {
        super(context);
        init(context, null);
    }

    public CardItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CardItemView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void setImageSize(int imageSize) {
        mImageSize = imageSize;
    }

    public void setTitleText(String titleText) {
        mTitleText = titleText;
    }

    public void setDrawable(Drawable drawable) {
        mDrawable = drawable;
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CardItemView, 0, 0);
        mTitleText = a.getString(R.styleable.CardItemView_app_text);
        mDrawable = a.getDrawable(R.styleable.CardItemView_app_drawable);
        mImageSize = a.getDimensionPixelSize(R.styleable.CardItemView_app_size, 48);
        a.recycle();

        View view = LayoutInflater.from(context).inflate(R.layout.item_card_info, this, true);

        ImageView imageView = view.findViewById(R.id.image);
        imageView.setImageDrawable(mDrawable);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(mImageSize, mImageSize);
        imageView.setLayoutParams(params);

        //imageView.getLayoutParams().height = mImageSize;
        //imageView.getLayoutParams().width = mImageSize;

        TextView title = view.findViewById(R.id.title);
        title.setTextSize(12);
        title.setText(mTitleText);
    }
}