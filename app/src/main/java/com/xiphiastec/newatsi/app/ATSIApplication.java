package com.xiphiastec.newatsi.app;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.xiphiastec.newatsi.BuildConfig;
import com.xiphiastec.newatsi.R;


import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class ATSIApplication extends MultiDexApplication {
    private static Context sApplication;

    public static Context getInstance() {
        return sApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        sApplication = this;
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                //.setDefaultFontPath("fonts/Haettenschweiler.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        ButterKnife.setDebug(BuildConfig.DEBUG);
    }
}
