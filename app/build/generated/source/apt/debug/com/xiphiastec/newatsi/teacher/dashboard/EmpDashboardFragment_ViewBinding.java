// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.dashboard;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmpDashboardFragment_ViewBinding implements Unbinder {
  private EmpDashboardFragment target;

  private View view2131361980;

  private View view2131362006;

  private View view2131362007;

  private View view2131361999;

  private View view2131361985;

  private View view2131361989;

  private View view2131361995;

  private View view2131361996;

  private View view2131362002;

  private View view2131362005;

  private View view2131362000;

  @UiThread
  public EmpDashboardFragment_ViewBinding(final EmpDashboardFragment target, View source) {
    this.target = target;

    View view;
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recycler_view, "field 'recyclerView'", EmptyViewRecyclerView.class);
    target.tvEmpty = Utils.findRequiredViewAsType(source, R.id.tv_empty, "field 'tvEmpty'", TextView.class);
    target.mText = Utils.findRequiredViewAsType(source, R.id.text, "field 'mText'", TextView.class);
    target.mTitle = Utils.findRequiredViewAsType(source, R.id.title, "field 'mTitle'", TextView.class);
    target.mImage = Utils.findRequiredViewAsType(source, R.id.image, "field 'mImage'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.llAttendance, "method 'selectFragment'");
    view2131361980 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.selectFragment(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llTimetable, "method 'selectFragment'");
    view2131362006 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.selectFragment(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llTransport, "method 'selectFragment'");
    view2131362007 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.selectFragment(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llProfile, "method 'selectFragment'");
    view2131361999 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.selectFragment(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llDiscipline, "method 'selectFragment'");
    view2131361985 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.selectFragment(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llExperiance, "method 'selectFragment'");
    view2131361989 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.selectFragment(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llInterview, "method 'selectFragment'");
    view2131361995 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.selectFragment(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llLeave, "method 'selectFragment'");
    view2131361996 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.selectFragment(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llSalary, "method 'selectFragment'");
    view2131362002 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.selectFragment(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llSyllabus, "method 'selectFragment'");
    view2131362005 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.selectFragment(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llQualification, "method 'selectFragment'");
    view2131362000 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.selectFragment(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    EmpDashboardFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerView = null;
    target.tvEmpty = null;
    target.mText = null;
    target.mTitle = null;
    target.mImage = null;

    view2131361980.setOnClickListener(null);
    view2131361980 = null;
    view2131362006.setOnClickListener(null);
    view2131362006 = null;
    view2131362007.setOnClickListener(null);
    view2131362007 = null;
    view2131361999.setOnClickListener(null);
    view2131361999 = null;
    view2131361985.setOnClickListener(null);
    view2131361985 = null;
    view2131361989.setOnClickListener(null);
    view2131361989 = null;
    view2131361995.setOnClickListener(null);
    view2131361995 = null;
    view2131361996.setOnClickListener(null);
    view2131361996 = null;
    view2131362002.setOnClickListener(null);
    view2131362002 = null;
    view2131362005.setOnClickListener(null);
    view2131362005 = null;
    view2131362000.setOnClickListener(null);
    view2131362000 = null;
  }
}
