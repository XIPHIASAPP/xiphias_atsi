// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.transport;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StudentTransportationFragment_ViewBinding implements Unbinder {
  private StudentTransportationFragment target;

  @UiThread
  public StudentTransportationFragment_ViewBinding(StudentTransportationFragment target,
      View source) {
    this.target = target;

    target.mRecyclerView = Utils.findRequiredViewAsType(source, R.id.recycler_view, "field 'mRecyclerView'", EmptyViewRecyclerView.class);
    target.tvEmpty = Utils.findRequiredViewAsType(source, R.id.tv_empty, "field 'tvEmpty'", TextView.class);
    target.rootLayout = Utils.findRequiredViewAsType(source, R.id.rootLayout, "field 'rootLayout'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    StudentTransportationFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mRecyclerView = null;
    target.tvEmpty = null;
    target.rootLayout = null;
  }
}
