// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.login;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginFragment_ViewBinding implements Unbinder {
  private LoginFragment target;

  private View view2131362126;

  private View view2131362226;

  @UiThread
  public LoginFragment_ViewBinding(final LoginFragment target, View source) {
    this.target = target;

    View view;
    target.mEtEmail = Utils.findRequiredViewAsType(source, R.id.et_email, "field 'mEtEmail'", TextInputEditText.class);
    target.mEtPassword = Utils.findRequiredViewAsType(source, R.id.et_password, "field 'mEtPassword'", TextInputEditText.class);
    target.root = Utils.findRequiredViewAsType(source, R.id.activity_login, "field 'root'", CoordinatorLayout.class);
    view = Utils.findRequiredView(source, R.id.sign_in, "field 'mSignIn' and method 'onClick'");
    target.mSignIn = Utils.castView(view, R.id.sign_in, "field 'mSignIn'", Button.class);
    view2131362126 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.tv_forgot_password, "method 'onClick'");
    view2131362226 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    LoginFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mEtEmail = null;
    target.mEtPassword = null;
    target.root = null;
    target.mSignIn = null;

    view2131362126.setOnClickListener(null);
    view2131362126 = null;
    view2131362226.setOnClickListener(null);
    view2131362226 = null;
  }
}
