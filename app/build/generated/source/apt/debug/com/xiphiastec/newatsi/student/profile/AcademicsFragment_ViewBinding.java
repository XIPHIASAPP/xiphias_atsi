// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.profile;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AcademicsFragment_ViewBinding implements Unbinder {
  private AcademicsFragment target;

  @UiThread
  public AcademicsFragment_ViewBinding(AcademicsFragment target, View source) {
    this.target = target;

    target.mTvProgramName = Utils.findRequiredViewAsType(source, R.id.tv_program_name, "field 'mTvProgramName'", TextView.class);
    target.mTvAcademicBatch = Utils.findRequiredViewAsType(source, R.id.tv_academic_batch, "field 'mTvAcademicBatch'", TextView.class);
    target.mTvClass = Utils.findRequiredViewAsType(source, R.id.tv_class, "field 'mTvClass'", TextView.class);
    target.mTvBatch = Utils.findRequiredViewAsType(source, R.id.tv_batch, "field 'mTvBatch'", TextView.class);
    target.mTvRollNumber = Utils.findRequiredViewAsType(source, R.id.tv_roll_number, "field 'mTvRollNumber'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AcademicsFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTvProgramName = null;
    target.mTvAcademicBatch = null;
    target.mTvClass = null;
    target.mTvBatch = null;
    target.mTvRollNumber = null;
  }
}
