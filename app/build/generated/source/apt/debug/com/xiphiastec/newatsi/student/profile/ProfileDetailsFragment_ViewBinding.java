// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.profile;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.NestedScrollView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProfileDetailsFragment_ViewBinding implements Unbinder {
  private ProfileDetailsFragment target;

  @UiThread
  public ProfileDetailsFragment_ViewBinding(ProfileDetailsFragment target, View source) {
    this.target = target;

    target.mIvProfileIcon = Utils.findRequiredViewAsType(source, R.id.iv_profile_icon, "field 'mIvProfileIcon'", ImageView.class);
    target.mRootLayout = Utils.findRequiredViewAsType(source, R.id.root_layout, "field 'mRootLayout'", NestedScrollView.class);
    target.mViewList = Utils.listOf(
        Utils.findRequiredViewAsType(source, R.id.tv_program_name, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tv_register_number, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tv_admission_number, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tv_student_name, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tv_email, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tv_alternative_email_id, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tv_phone_number, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tv_mother_tongue, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tv_date_of_birth, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tv_gender, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tv_birth_place, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tv_caste, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tv_category, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tv_blood_group, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.name, "field 'mViewList'", TextView.class));
  }

  @Override
  @CallSuper
  public void unbind() {
    ProfileDetailsFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mIvProfileIcon = null;
    target.mRootLayout = null;
    target.mViewList = null;
  }
}
