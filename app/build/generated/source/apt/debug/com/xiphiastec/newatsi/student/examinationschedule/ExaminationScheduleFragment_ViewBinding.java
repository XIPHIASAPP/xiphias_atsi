// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.examinationschedule;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ExaminationScheduleFragment_ViewBinding implements Unbinder {
  private ExaminationScheduleFragment target;

  @UiThread
  public ExaminationScheduleFragment_ViewBinding(ExaminationScheduleFragment target, View source) {
    this.target = target;

    target.mRecyclerView = Utils.findRequiredViewAsType(source, R.id.recycler_view, "field 'mRecyclerView'", EmptyViewRecyclerView.class);
    target.tv_empty = Utils.findRequiredViewAsType(source, R.id.tv_empty, "field 'tv_empty'", TextView.class);
    target.mRootLayout = Utils.findRequiredViewAsType(source, R.id.root_layout, "field 'mRootLayout'", FrameLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ExaminationScheduleFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mRecyclerView = null;
    target.tv_empty = null;
    target.mRootLayout = null;
  }
}
