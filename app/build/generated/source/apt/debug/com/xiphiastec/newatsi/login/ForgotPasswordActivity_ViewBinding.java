// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.login;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ForgotPasswordActivity_ViewBinding implements Unbinder {
  private ForgotPasswordActivity target;

  private View view2131361948;

  private View view2131361932;

  @UiThread
  public ForgotPasswordActivity_ViewBinding(ForgotPasswordActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ForgotPasswordActivity_ViewBinding(final ForgotPasswordActivity target, View source) {
    this.target = target;

    View view;
    target.mEtEmail = Utils.findRequiredViewAsType(source, R.id.et_email, "field 'mEtEmail'", TextInputEditText.class);
    view = Utils.findRequiredView(source, R.id.ib_back, "field 'mIbBack' and method 'onClick'");
    target.mIbBack = Utils.castView(view, R.id.ib_back, "field 'mIbBack'", AppCompatImageButton.class);
    view2131361948 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mContainer = Utils.findRequiredViewAsType(source, R.id.container, "field 'mContainer'", FrameLayout.class);
    view = Utils.findRequiredView(source, R.id.forgot_password, "field 'mForgotPassword' and method 'onClick'");
    target.mForgotPassword = Utils.castView(view, R.id.forgot_password, "field 'mForgotPassword'", Button.class);
    view2131361932 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mEtPassword = Utils.findRequiredViewAsType(source, R.id.et_password, "field 'mEtPassword'", TextInputEditText.class);
    target.mEtConfirmPassword = Utils.findRequiredViewAsType(source, R.id.et_confirm_password, "field 'mEtConfirmPassword'", TextInputEditText.class);
    target.mFields = Utils.findRequiredViewAsType(source, R.id.fields, "field 'mFields'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ForgotPasswordActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mEtEmail = null;
    target.mIbBack = null;
    target.mContainer = null;
    target.mForgotPassword = null;
    target.mEtPassword = null;
    target.mEtConfirmPassword = null;
    target.mFields = null;

    view2131361948.setOnClickListener(null);
    view2131361948 = null;
    view2131361932.setOnClickListener(null);
    view2131361932 = null;
  }
}
