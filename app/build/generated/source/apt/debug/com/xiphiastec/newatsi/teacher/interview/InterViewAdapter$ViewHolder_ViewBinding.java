// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.interview;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class InterViewAdapter$ViewHolder_ViewBinding implements Unbinder {
  private InterViewAdapter.ViewHolder target;

  @UiThread
  public InterViewAdapter$ViewHolder_ViewBinding(InterViewAdapter.ViewHolder target, View source) {
    this.target = target;

    target.name = Utils.findRequiredViewAsType(source, R.id.name, "field 'name'", TextView.class);
    target.applicantName = Utils.findRequiredViewAsType(source, R.id.applicant_name, "field 'applicantName'", TextView.class);
    target.tvJob = Utils.findRequiredViewAsType(source, R.id.tvJob, "field 'tvJob'", TextView.class);
    target.tvRound = Utils.findRequiredViewAsType(source, R.id.tvRound, "field 'tvRound'", TextView.class);
    target.tvTime = Utils.findRequiredViewAsType(source, R.id.tvTime, "field 'tvTime'", TextView.class);
    target.tvDate = Utils.findRequiredViewAsType(source, R.id.tvDate, "field 'tvDate'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    InterViewAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.name = null;
    target.applicantName = null;
    target.tvJob = null;
    target.tvRound = null;
    target.tvTime = null;
    target.tvDate = null;
  }
}
