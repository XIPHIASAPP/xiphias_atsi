// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.qualification;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class QualificationAdapter$QViewHolder_ViewBinding implements Unbinder {
  private QualificationAdapter.QViewHolder target;

  @UiThread
  public QualificationAdapter$QViewHolder_ViewBinding(QualificationAdapter.QViewHolder target,
      View source) {
    this.target = target;

    target.tvQualification = Utils.findRequiredViewAsType(source, R.id.tvQualification, "field 'tvQualification'", TextView.class);
    target.tvCertificate = Utils.findRequiredViewAsType(source, R.id.tvCertificate, "field 'tvCertificate'", TextView.class);
    target.tvMarks = Utils.findRequiredViewAsType(source, R.id.tvMarks, "field 'tvMarks'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    QualificationAdapter.QViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvQualification = null;
    target.tvCertificate = null;
    target.tvMarks = null;
  }
}
