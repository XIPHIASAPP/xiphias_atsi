// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.parent.statistics;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ChildStatisticsFragment$StatisticsAdapter$ViewHolder_ViewBinding implements Unbinder {
  private ChildStatisticsFragment.StatisticsAdapter.ViewHolder target;

  @UiThread
  public ChildStatisticsFragment$StatisticsAdapter$ViewHolder_ViewBinding(ChildStatisticsFragment.StatisticsAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.text = Utils.findRequiredViewAsType(source, R.id.text, "field 'text'", TextView.class);
    target.title = Utils.findRequiredViewAsType(source, R.id.title, "field 'title'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ChildStatisticsFragment.StatisticsAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.text = null;
    target.title = null;
  }
}
