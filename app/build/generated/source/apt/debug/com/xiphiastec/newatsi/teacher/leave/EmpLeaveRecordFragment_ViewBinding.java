// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.leave;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmpLeaveRecordFragment_ViewBinding implements Unbinder {
  private EmpLeaveRecordFragment target;

  private View view2131361922;

  @UiThread
  public EmpLeaveRecordFragment_ViewBinding(final EmpLeaveRecordFragment target, View source) {
    this.target = target;

    View view;
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recycler_view, "field 'recyclerView'", EmptyViewRecyclerView.class);
    target.tvEmpty = Utils.findRequiredViewAsType(source, R.id.tv_empty, "field 'tvEmpty'", TextView.class);
    view = Utils.findRequiredView(source, R.id.fabCreateLeave, "field 'fabCreateLeave' and method 'onFabClicked'");
    target.fabCreateLeave = Utils.castView(view, R.id.fabCreateLeave, "field 'fabCreateLeave'", FloatingActionButton.class);
    view2131361922 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onFabClicked();
      }
    });
    target.spnLeaveStatus = Utils.findRequiredViewAsType(source, R.id.spnLeaveStatus, "field 'spnLeaveStatus'", Spinner.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    EmpLeaveRecordFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerView = null;
    target.tvEmpty = null;
    target.fabCreateLeave = null;
    target.spnLeaveStatus = null;

    view2131361922.setOnClickListener(null);
    view2131361922 = null;
  }
}
