// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.parent.dashboard;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ParentDashboardFragment$ChildrenListAdapter$ChildrenViewHolder_ViewBinding implements Unbinder {
  private ParentDashboardFragment.ChildrenListAdapter.ChildrenViewHolder target;

  @UiThread
  public ParentDashboardFragment$ChildrenListAdapter$ChildrenViewHolder_ViewBinding(ParentDashboardFragment.ChildrenListAdapter.ChildrenViewHolder target,
      View source) {
    this.target = target;

    target.txtName = Utils.findRequiredViewAsType(source, R.id.txtName, "field 'txtName'", TextView.class);
    target.ivProfilePic = Utils.findRequiredViewAsType(source, R.id.ivProfilePic, "field 'ivProfilePic'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ParentDashboardFragment.ChildrenListAdapter.ChildrenViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtName = null;
    target.ivProfilePic = null;
  }
}
