// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.hostel.inoutstatus;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class InOutStatusAdapter$InOutViewHolder_ViewBinding implements Unbinder {
  private InOutStatusAdapter.InOutViewHolder target;

  @UiThread
  public InOutStatusAdapter$InOutViewHolder_ViewBinding(InOutStatusAdapter.InOutViewHolder target,
      View source) {
    this.target = target;

    target.outTime = Utils.findRequiredViewAsType(source, R.id.tvOut_time, "field 'outTime'", TextView.class);
    target.inTime = Utils.findRequiredViewAsType(source, R.id.tvIn_time, "field 'inTime'", TextView.class);
    target.reason = Utils.findRequiredViewAsType(source, R.id.tvReason, "field 'reason'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    InOutStatusAdapter.InOutViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.outTime = null;
    target.inTime = null;
    target.reason = null;
  }
}
