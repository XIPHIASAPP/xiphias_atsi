// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.dashboard;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StudentDashboardFragment$StatisticsAdapter$ViewHolder_ViewBinding implements Unbinder {
  private StudentDashboardFragment.StatisticsAdapter.ViewHolder target;

  @UiThread
  public StudentDashboardFragment$StatisticsAdapter$ViewHolder_ViewBinding(StudentDashboardFragment.StatisticsAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.text = Utils.findRequiredViewAsType(source, R.id.text, "field 'text'", TextView.class);
    target.title = Utils.findRequiredViewAsType(source, R.id.title, "field 'title'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    StudentDashboardFragment.StatisticsAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.text = null;
    target.title = null;
  }
}
