// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.feeandfine;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FineAdapter$FineViewHolder_ViewBinding implements Unbinder {
  private FineAdapter.FineViewHolder target;

  @UiThread
  public FineAdapter$FineViewHolder_ViewBinding(FineAdapter.FineViewHolder target, View source) {
    this.target = target;

    target.feeName = Utils.findRequiredViewAsType(source, R.id.fee_name, "field 'feeName'", TextView.class);
    target.paymentType = Utils.findRequiredViewAsType(source, R.id.payment_type, "field 'paymentType'", TextView.class);
    target.paidDate = Utils.findRequiredViewAsType(source, R.id.paid_date, "field 'paidDate'", TextView.class);
    target.feeAmount = Utils.findRequiredViewAsType(source, R.id.fee_amount, "field 'feeAmount'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FineAdapter.FineViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.feeName = null;
    target.paymentType = null;
    target.paidDate = null;
    target.feeAmount = null;
  }
}
