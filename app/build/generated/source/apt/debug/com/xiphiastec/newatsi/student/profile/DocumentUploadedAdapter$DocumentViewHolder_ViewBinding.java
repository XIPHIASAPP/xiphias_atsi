// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.profile;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DocumentUploadedAdapter$DocumentViewHolder_ViewBinding implements Unbinder {
  private DocumentUploadedAdapter.DocumentViewHolder target;

  @UiThread
  public DocumentUploadedAdapter$DocumentViewHolder_ViewBinding(DocumentUploadedAdapter.DocumentViewHolder target,
      View source) {
    this.target = target;

    target.mUploaded = Utils.findRequiredViewAsType(source, R.id.uploaded, "field 'mUploaded'", ImageView.class);
    target.mVerified = Utils.findRequiredViewAsType(source, R.id.verified, "field 'mVerified'", ImageView.class);
    target.mDocumentName = Utils.findRequiredViewAsType(source, R.id.tv_document, "field 'mDocumentName'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DocumentUploadedAdapter.DocumentViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mUploaded = null;
    target.mVerified = null;
    target.mDocumentName = null;
  }
}
