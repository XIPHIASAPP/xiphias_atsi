// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.gradecard;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StudentGradeCardFragment_ViewBinding implements Unbinder {
  private StudentGradeCardFragment target;

  @UiThread
  public StudentGradeCardFragment_ViewBinding(StudentGradeCardFragment target, View source) {
    this.target = target;

    target.mSchoolLogo = Utils.findRequiredViewAsType(source, R.id.school_logo, "field 'mSchoolLogo'", ImageView.class);
    target.mSchoolTitle = Utils.findRequiredViewAsType(source, R.id.school_title, "field 'mSchoolTitle'", TextView.class);
    target.mTvClassName = Utils.findRequiredViewAsType(source, R.id.tv_class_name, "field 'mTvClassName'", TextView.class);
    target.mTvRegisterNumber = Utils.findRequiredViewAsType(source, R.id.tv_register_number, "field 'mTvRegisterNumber'", TextView.class);
    target.mTvStudentName = Utils.findRequiredViewAsType(source, R.id.tv_student_name, "field 'mTvStudentName'", TextView.class);
    target.mTvRollNumber = Utils.findRequiredViewAsType(source, R.id.tv_roll_number, "field 'mTvRollNumber'", TextView.class);
    target.mTvPercentage = Utils.findRequiredViewAsType(source, R.id.tv_percentage, "field 'mTvPercentage'", TextView.class);
    target.mTvResult = Utils.findRequiredViewAsType(source, R.id.tv_result, "field 'mTvResult'", TextView.class);
    target.mGradeCardContent = Utils.findRequiredViewAsType(source, R.id.grade_card_content, "field 'mGradeCardContent'", LinearLayout.class);
    target.mTypes = Utils.findRequiredViewAsType(source, R.id.types, "field 'mTypes'", Spinner.class);
    target.mRootLayout = Utils.findRequiredViewAsType(source, R.id.root_layout, "field 'mRootLayout'", LinearLayout.class);
    target.rvGrade = Utils.findRequiredViewAsType(source, R.id.rvGrade, "field 'rvGrade'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    StudentGradeCardFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mSchoolLogo = null;
    target.mSchoolTitle = null;
    target.mTvClassName = null;
    target.mTvRegisterNumber = null;
    target.mTvStudentName = null;
    target.mTvRollNumber = null;
    target.mTvPercentage = null;
    target.mTvResult = null;
    target.mGradeCardContent = null;
    target.mTypes = null;
    target.mRootLayout = null;
    target.rvGrade = null;
  }
}
