// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.notification;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class NotificationAdapter$NotificationViewHolder_ViewBinding implements Unbinder {
  private NotificationAdapter.NotificationViewHolder target;

  @UiThread
  public NotificationAdapter$NotificationViewHolder_ViewBinding(NotificationAdapter.NotificationViewHolder target,
      View source) {
    this.target = target;

    target.txtNotification = Utils.findRequiredViewAsType(source, R.id.txtNotification, "field 'txtNotification'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    NotificationAdapter.NotificationViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtNotification = null;
  }
}
