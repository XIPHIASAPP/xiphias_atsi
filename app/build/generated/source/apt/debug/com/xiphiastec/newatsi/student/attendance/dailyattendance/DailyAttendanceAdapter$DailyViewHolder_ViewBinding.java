// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.attendance.dailyattendance;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DailyAttendanceAdapter$DailyViewHolder_ViewBinding implements Unbinder {
  private DailyAttendanceAdapter.DailyViewHolder target;

  @UiThread
  public DailyAttendanceAdapter$DailyViewHolder_ViewBinding(DailyAttendanceAdapter.DailyViewHolder target,
      View source) {
    this.target = target;

    target.present = Utils.findRequiredViewAsType(source, R.id.present, "field 'present'", TextView.class);
    target.reason = Utils.findRequiredViewAsType(source, R.id.reason, "field 'reason'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DailyAttendanceAdapter.DailyViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.present = null;
    target.reason = null;
  }
}
