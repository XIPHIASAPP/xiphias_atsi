// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.disciplinary;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DisciplinaryAdapter$DisciplineViewHolder_ViewBinding implements Unbinder {
  private DisciplinaryAdapter.DisciplineViewHolder target;

  @UiThread
  public DisciplinaryAdapter$DisciplineViewHolder_ViewBinding(DisciplinaryAdapter.DisciplineViewHolder target,
      View source) {
    this.target = target;

    target.tvIndiscipline = Utils.findRequiredViewAsType(source, R.id.tvIndiscipline, "field 'tvIndiscipline'", TextView.class);
    target.tvReason = Utils.findRequiredViewAsType(source, R.id.tvReason, "field 'tvReason'", TextView.class);
    target.tvAction = Utils.findRequiredViewAsType(source, R.id.tvAction, "field 'tvAction'", TextView.class);
    target.tvCreated = Utils.findRequiredViewAsType(source, R.id.tvCreated, "field 'tvCreated'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DisciplinaryAdapter.DisciplineViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvIndiscipline = null;
    target.tvReason = null;
    target.tvAction = null;
    target.tvCreated = null;
  }
}
