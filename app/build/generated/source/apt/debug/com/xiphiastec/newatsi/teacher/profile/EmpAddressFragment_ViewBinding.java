// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.profile;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmpAddressFragment_ViewBinding implements Unbinder {
  private EmpAddressFragment target;

  @UiThread
  public EmpAddressFragment_ViewBinding(EmpAddressFragment target, View source) {
    this.target = target;

    target.addresses = Utils.listOf(
        Utils.findRequiredViewAsType(source, R.id.address_1, "field 'addresses'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.address_2, "field 'addresses'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.number, "field 'addresses'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.state, "field 'addresses'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.zipcode, "field 'addresses'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.present, "field 'addresses'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.permanent, "field 'addresses'", TextView.class));
  }

  @Override
  @CallSuper
  public void unbind() {
    EmpAddressFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.addresses = null;
  }
}
