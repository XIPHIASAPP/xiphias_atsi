// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.profile;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.FrameLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmpProfileFragment_ViewBinding implements Unbinder {
  private EmpProfileFragment target;

  private View view2131361896;

  private View view2131361828;

  private View view2131361839;

  private View view2131361923;

  @UiThread
  public EmpProfileFragment_ViewBinding(final EmpProfileFragment target, View source) {
    this.target = target;

    View view;
    target.mRoot = Utils.findRequiredViewAsType(source, R.id.root, "field 'mRoot'", FrameLayout.class);
    view = Utils.findRequiredView(source, R.id.details, "method 'onViewClicked'");
    view2131361896 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.address, "method 'onViewClicked'");
    view2131361828 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.assets, "method 'onViewClicked'");
    view2131361839 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.family, "method 'onViewClicked'");
    view2131361923 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    EmpProfileFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mRoot = null;

    view2131361896.setOnClickListener(null);
    view2131361896 = null;
    view2131361828.setOnClickListener(null);
    view2131361828 = null;
    view2131361839.setOnClickListener(null);
    view2131361839 = null;
    view2131361923.setOnClickListener(null);
    view2131361923 = null;
  }
}
