// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.attendance;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AttendanceAdapter$ViewHolder_ViewBinding implements Unbinder {
  private AttendanceAdapter.ViewHolder target;

  @UiThread
  public AttendanceAdapter$ViewHolder_ViewBinding(AttendanceAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.tvIn_time = Utils.findRequiredViewAsType(source, R.id.tvIn_time, "field 'tvIn_time'", TextView.class);
    target.tvOut_time = Utils.findRequiredViewAsType(source, R.id.tvOut_time, "field 'tvOut_time'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AttendanceAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvIn_time = null;
    target.tvOut_time = null;
  }
}
