// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.disciplinary;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmpDisciplinaryFragment_ViewBinding implements Unbinder {
  private EmpDisciplinaryFragment target;

  private View view2131361921;

  @UiThread
  public EmpDisciplinaryFragment_ViewBinding(final EmpDisciplinaryFragment target, View source) {
    this.target = target;

    View view;
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recycler_view, "field 'recyclerView'", EmptyViewRecyclerView.class);
    target.tvEmpty = Utils.findRequiredViewAsType(source, R.id.tv_empty, "field 'tvEmpty'", TextView.class);
    target.mRoot = Utils.findRequiredViewAsType(source, R.id.root, "field 'mRoot'", CoordinatorLayout.class);
    view = Utils.findRequiredView(source, R.id.fabCreate, "field 'create' and method 'createDiscipline'");
    target.create = Utils.castView(view, R.id.fabCreate, "field 'create'", FloatingActionButton.class);
    view2131361921 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.createDiscipline();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    EmpDisciplinaryFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerView = null;
    target.tvEmpty = null;
    target.mRoot = null;
    target.create = null;

    view2131361921.setOnClickListener(null);
    view2131361921 = null;
  }
}
