// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.timetable;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmpTimeTableFragment_ViewBinding implements Unbinder {
  private EmpTimeTableFragment target;

  @UiThread
  public EmpTimeTableFragment_ViewBinding(EmpTimeTableFragment target, View source) {
    this.target = target;

    target.timeTable = Utils.findRequiredViewAsType(source, R.id.lvExp, "field 'timeTable'", ExpandableListView.class);
    target.mRootLayout = Utils.findRequiredViewAsType(source, R.id.root_layout, "field 'mRootLayout'", ConstraintLayout.class);
    target.txtError = Utils.findRequiredViewAsType(source, R.id.txtError, "field 'txtError'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    EmpTimeTableFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.timeTable = null;
    target.mRootLayout = null;
    target.txtError = null;
  }
}
