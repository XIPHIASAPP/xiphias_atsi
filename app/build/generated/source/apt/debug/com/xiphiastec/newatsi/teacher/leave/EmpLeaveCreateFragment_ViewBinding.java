// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.leave;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmpLeaveCreateFragment_ViewBinding implements Unbinder {
  private EmpLeaveCreateFragment target;

  private View view2131362096;

  private View view2131361936;

  private View view2131362163;

  @UiThread
  public EmpLeaveCreateFragment_ViewBinding(final EmpLeaveCreateFragment target, View source) {
    this.target = target;

    View view;
    target.leaveType = Utils.findRequiredViewAsType(source, R.id.leave_type, "field 'leaveType'", Spinner.class);
    target.minDays = Utils.findRequiredViewAsType(source, R.id.min_days, "field 'minDays'", TextView.class);
    target.maxDays = Utils.findRequiredViewAsType(source, R.id.max_days, "field 'maxDays'", TextView.class);
    target.balance = Utils.findRequiredViewAsType(source, R.id.balance, "field 'balance'", TextView.class);
    target.applicantId = Utils.findRequiredViewAsType(source, R.id.applicant_id, "field 'applicantId'", TextView.class);
    target.fromHalfDay = Utils.findRequiredViewAsType(source, R.id.from_half_day, "field 'fromHalfDay'", CheckBox.class);
    target.toHalfDay = Utils.findRequiredViewAsType(source, R.id.to_half_day, "field 'toHalfDay'", CheckBox.class);
    target.fromDateText = Utils.findRequiredViewAsType(source, R.id.from_date_text, "field 'fromDateText'", TextView.class);
    target.toDateText = Utils.findRequiredViewAsType(source, R.id.to_date_text, "field 'toDateText'", TextView.class);
    target.rootLayout = Utils.findRequiredViewAsType(source, R.id.root_layout, "field 'rootLayout'", LinearLayout.class);
    target.reason = Utils.findRequiredViewAsType(source, R.id.reason, "field 'reason'", EditText.class);
    view = Utils.findRequiredView(source, R.id.save, "method 'onViewClicked'");
    view2131362096 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.from_date, "method 'onViewClicked'");
    view2131361936 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.to_date, "method 'onViewClicked'");
    view2131362163 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    EmpLeaveCreateFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.leaveType = null;
    target.minDays = null;
    target.maxDays = null;
    target.balance = null;
    target.applicantId = null;
    target.fromHalfDay = null;
    target.toHalfDay = null;
    target.fromDateText = null;
    target.toDateText = null;
    target.rootLayout = null;
    target.reason = null;

    view2131362096.setOnClickListener(null);
    view2131362096 = null;
    view2131361936.setOnClickListener(null);
    view2131361936 = null;
    view2131362163.setOnClickListener(null);
    view2131362163 = null;
  }
}
