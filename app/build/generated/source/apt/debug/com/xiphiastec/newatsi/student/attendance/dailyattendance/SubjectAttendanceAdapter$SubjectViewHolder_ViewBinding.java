// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.attendance.dailyattendance;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SubjectAttendanceAdapter$SubjectViewHolder_ViewBinding implements Unbinder {
  private SubjectAttendanceAdapter.SubjectViewHolder target;

  @UiThread
  public SubjectAttendanceAdapter$SubjectViewHolder_ViewBinding(SubjectAttendanceAdapter.SubjectViewHolder target,
      View source) {
    this.target = target;

    target.tvStatus = Utils.findRequiredViewAsType(source, R.id.tv_status, "field 'tvStatus'", TextView.class);
    target.tvSubjectName = Utils.findRequiredViewAsType(source, R.id.tv_subject_name, "field 'tvSubjectName'", TextView.class);
    target.tvReason = Utils.findRequiredViewAsType(source, R.id.tv_reason, "field 'tvReason'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SubjectAttendanceAdapter.SubjectViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvStatus = null;
    target.tvSubjectName = null;
    target.tvReason = null;
  }
}
