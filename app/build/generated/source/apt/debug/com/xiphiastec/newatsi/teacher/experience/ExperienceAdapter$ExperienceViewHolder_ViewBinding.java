// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.experience;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ExperienceAdapter$ExperienceViewHolder_ViewBinding implements Unbinder {
  private ExperienceAdapter.ExperienceViewHolder target;

  @UiThread
  public ExperienceAdapter$ExperienceViewHolder_ViewBinding(ExperienceAdapter.ExperienceViewHolder target,
      View source) {
    this.target = target;

    target.date = Utils.findRequiredViewAsType(source, R.id.date, "field 'date'", TextView.class);
    target.designationBranch = Utils.findRequiredViewAsType(source, R.id.designation_branch, "field 'designationBranch'", TextView.class);
    target.salary = Utils.findRequiredViewAsType(source, R.id.salary, "field 'salary'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ExperienceAdapter.ExperienceViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.date = null;
    target.designationBranch = null;
    target.salary = null;
  }
}
