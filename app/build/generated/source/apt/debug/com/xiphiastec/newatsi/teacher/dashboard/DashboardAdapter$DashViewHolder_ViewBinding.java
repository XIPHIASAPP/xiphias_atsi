// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.dashboard;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DashboardAdapter$DashViewHolder_ViewBinding implements Unbinder {
  private DashboardAdapter.DashViewHolder target;

  @UiThread
  public DashboardAdapter$DashViewHolder_ViewBinding(DashboardAdapter.DashViewHolder target,
      View source) {
    this.target = target;

    target.name = Utils.findRequiredViewAsType(source, R.id.name, "field 'name'", TextView.class);
    target.time = Utils.findRequiredViewAsType(source, R.id.time, "field 'time'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DashboardAdapter.DashViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.name = null;
    target.time = null;
  }
}
