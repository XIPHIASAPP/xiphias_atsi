// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TeachersActivity_ViewBinding implements Unbinder {
  private TeachersActivity target;

  @UiThread
  public TeachersActivity_ViewBinding(TeachersActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public TeachersActivity_ViewBinding(TeachersActivity target, View source) {
    this.target = target;

    target.mToolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'mToolbar'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    TeachersActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mToolbar = null;
  }
}
