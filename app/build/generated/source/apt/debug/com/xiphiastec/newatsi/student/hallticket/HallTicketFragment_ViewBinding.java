// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.hallticket;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HallTicketFragment_ViewBinding implements Unbinder {
  private HallTicketFragment target;

  @UiThread
  public HallTicketFragment_ViewBinding(HallTicketFragment target, View source) {
    this.target = target;

    target.schoolLogo = Utils.findRequiredViewAsType(source, R.id.ivSchoolLogo, "field 'schoolLogo'", ImageView.class);
    target.schoolTitle = Utils.findRequiredViewAsType(source, R.id.tvSchoolTitle, "field 'schoolTitle'", TextView.class);
    target.mRootLayout = Utils.findRequiredViewAsType(source, R.id.rootLayout, "field 'mRootLayout'", FrameLayout.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recycler_view, "field 'recyclerView'", RecyclerView.class);
    target.txtError = Utils.findRequiredViewAsType(source, R.id.txtError, "field 'txtError'", TextView.class);
    target.cvHStudent = Utils.findRequiredViewAsType(source, R.id.cvHStudent, "field 'cvHStudent'", CardView.class);
    target.cvHTimetable = Utils.findRequiredViewAsType(source, R.id.cvHTimetable, "field 'cvHTimetable'", CardView.class);
    target.mViewList = Utils.listOf(
        Utils.findRequiredViewAsType(source, R.id.tv_register_number, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tv_student_name, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tv_registered_paper, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tv_academic_batch, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tv_date_of_birth, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tv_class_type, "field 'mViewList'", TextView.class));
  }

  @Override
  @CallSuper
  public void unbind() {
    HallTicketFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.schoolLogo = null;
    target.schoolTitle = null;
    target.mRootLayout = null;
    target.recyclerView = null;
    target.txtError = null;
    target.cvHStudent = null;
    target.cvHTimetable = null;
    target.mViewList = null;
  }
}
