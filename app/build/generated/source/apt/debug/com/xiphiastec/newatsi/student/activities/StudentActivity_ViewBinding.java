// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StudentActivity_ViewBinding implements Unbinder {
  private StudentActivity target;

  @UiThread
  public StudentActivity_ViewBinding(StudentActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public StudentActivity_ViewBinding(StudentActivity target, View source) {
    this.target = target;

    target.mToolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'mToolbar'", Toolbar.class);
    target.mainActivity = Utils.findRequiredViewAsType(source, R.id.main_activity, "field 'mainActivity'", CoordinatorLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    StudentActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mToolbar = null;
    target.mainActivity = null;
  }
}
