// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.profile;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DocumentsUploadedFragment_ViewBinding implements Unbinder {
  private DocumentsUploadedFragment target;

  @UiThread
  public DocumentsUploadedFragment_ViewBinding(DocumentsUploadedFragment target, View source) {
    this.target = target;

    target.mRecyclerView = Utils.findRequiredViewAsType(source, R.id.recycler_view, "field 'mRecyclerView'", EmptyViewRecyclerView.class);
    target.mRootLayout = Utils.findRequiredViewAsType(source, R.id.root_layout, "field 'mRootLayout'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DocumentsUploadedFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mRecyclerView = null;
    target.mRootLayout = null;
  }
}
