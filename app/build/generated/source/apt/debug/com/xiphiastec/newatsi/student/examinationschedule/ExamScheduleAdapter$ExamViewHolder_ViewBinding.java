// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.examinationschedule;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ExamScheduleAdapter$ExamViewHolder_ViewBinding implements Unbinder {
  private ExamScheduleAdapter.ExamViewHolder target;

  @UiThread
  public ExamScheduleAdapter$ExamViewHolder_ViewBinding(ExamScheduleAdapter.ExamViewHolder target,
      View source) {
    this.target = target;

    target.mTvTimeDate = Utils.findRequiredViewAsType(source, R.id.tvTime, "field 'mTvTimeDate'", TextView.class);
    target.mTvSubject = Utils.findRequiredViewAsType(source, R.id.tvSubject, "field 'mTvSubject'", TextView.class);
    target.mTvExamType = Utils.findRequiredViewAsType(source, R.id.tvExamType, "field 'mTvExamType'", TextView.class);
    target.mTvSubjectCode = Utils.findRequiredViewAsType(source, R.id.tvSubjectCode, "field 'mTvSubjectCode'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ExamScheduleAdapter.ExamViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTvTimeDate = null;
    target.mTvSubject = null;
    target.mTvExamType = null;
    target.mTvSubjectCode = null;
  }
}
