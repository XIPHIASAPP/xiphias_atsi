// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.profile;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmpProfileDetailsFragment_ViewBinding implements Unbinder {
  private EmpProfileDetailsFragment target;

  @UiThread
  public EmpProfileDetailsFragment_ViewBinding(EmpProfileDetailsFragment target, View source) {
    this.target = target;

    target.profile = Utils.listOf(
        Utils.findRequiredViewAsType(source, R.id.name, "field 'profile'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.email, "field 'profile'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.dob, "field 'profile'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.mobile_number, "field 'profile'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.join_date, "field 'profile'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.gender, "field 'profile'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.blood_group, "field 'profile'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.pan_number, "field 'profile'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.biometrics, "field 'profile'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.marital_status, "field 'profile'", TextView.class));
  }

  @Override
  @CallSuper
  public void unbind() {
    EmpProfileDetailsFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.profile = null;
  }
}
