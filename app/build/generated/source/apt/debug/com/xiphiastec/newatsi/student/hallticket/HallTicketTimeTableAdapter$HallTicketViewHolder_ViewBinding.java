// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.hallticket;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HallTicketTimeTableAdapter$HallTicketViewHolder_ViewBinding implements Unbinder {
  private HallTicketTimeTableAdapter.HallTicketViewHolder target;

  @UiThread
  public HallTicketTimeTableAdapter$HallTicketViewHolder_ViewBinding(HallTicketTimeTableAdapter.HallTicketViewHolder target,
      View source) {
    this.target = target;

    target.mSubject = Utils.findRequiredViewAsType(source, R.id.tvSubject, "field 'mSubject'", TextView.class);
    target.mDate = Utils.findRequiredViewAsType(source, R.id.tvDate, "field 'mDate'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    HallTicketTimeTableAdapter.HallTicketViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mSubject = null;
    target.mDate = null;
  }
}
