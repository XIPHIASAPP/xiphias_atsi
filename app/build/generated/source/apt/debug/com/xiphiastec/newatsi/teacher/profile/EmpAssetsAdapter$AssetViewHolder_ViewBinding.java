// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.profile;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmpAssetsAdapter$AssetViewHolder_ViewBinding implements Unbinder {
  private EmpAssetsAdapter.AssetViewHolder target;

  @UiThread
  public EmpAssetsAdapter$AssetViewHolder_ViewBinding(EmpAssetsAdapter.AssetViewHolder target,
      View source) {
    this.target = target;

    target.view = Utils.findRequiredView(source, R.id.view, "field 'view'");
    target.name = Utils.findRequiredViewAsType(source, R.id.name, "field 'name'", TextView.class);
    target.assetName = Utils.findRequiredViewAsType(source, R.id.asset_name, "field 'assetName'", TextView.class);
    target.assetModel = Utils.findRequiredViewAsType(source, R.id.asset_model, "field 'assetModel'", TextView.class);
    target.date = Utils.findRequiredViewAsType(source, R.id.date, "field 'date'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    EmpAssetsAdapter.AssetViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.view = null;
    target.name = null;
    target.assetName = null;
    target.assetModel = null;
    target.date = null;
  }
}
