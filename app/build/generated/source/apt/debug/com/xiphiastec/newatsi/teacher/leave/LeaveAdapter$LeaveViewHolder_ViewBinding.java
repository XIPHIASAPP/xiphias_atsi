// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.leave;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LeaveAdapter$LeaveViewHolder_ViewBinding implements Unbinder {
  private LeaveAdapter.LeaveViewHolder target;

  @UiThread
  public LeaveAdapter$LeaveViewHolder_ViewBinding(LeaveAdapter.LeaveViewHolder target,
      View source) {
    this.target = target;

    target.tvLeaveId = Utils.findRequiredViewAsType(source, R.id.tvLeaveId, "field 'tvLeaveId'", TextView.class);
    target.tvLeaveFrom = Utils.findRequiredViewAsType(source, R.id.tvLeaveFrom, "field 'tvLeaveFrom'", TextView.class);
    target.tvLeaveTo = Utils.findRequiredViewAsType(source, R.id.tvLeaveTo, "field 'tvLeaveTo'", TextView.class);
    target.tvLeaveType = Utils.findRequiredViewAsType(source, R.id.tvLeaveType, "field 'tvLeaveType'", TextView.class);
    target.tvApprover = Utils.findRequiredViewAsType(source, R.id.tvApprover, "field 'tvApprover'", TextView.class);
    target.tvStatus = Utils.findRequiredViewAsType(source, R.id.tvStatus, "field 'tvStatus'", TextView.class);
    target.ivCancel = Utils.findRequiredViewAsType(source, R.id.ivCancel, "field 'ivCancel'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    LeaveAdapter.LeaveViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvLeaveId = null;
    target.tvLeaveFrom = null;
    target.tvLeaveTo = null;
    target.tvLeaveType = null;
    target.tvApprover = null;
    target.tvStatus = null;
    target.ivCancel = null;
  }
}
