// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.attendance;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmpAttendanceFragment_ViewBinding implements Unbinder {
  private EmpAttendanceFragment target;

  private View view2131362072;

  private View view2131362108;

  @UiThread
  public EmpAttendanceFragment_ViewBinding(final EmpAttendanceFragment target, View source) {
    this.target = target;

    View view;
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recycler_view, "field 'recyclerView'", EmptyViewRecyclerView.class);
    target.tvEmpty = Utils.findRequiredViewAsType(source, R.id.tv_empty, "field 'tvEmpty'", TextView.class);
    target.rootLayout = Utils.findRequiredViewAsType(source, R.id.root_layout, "field 'rootLayout'", LinearLayout.class);
    target.type = Utils.findRequiredViewAsType(source, R.id.type, "field 'type'", Spinner.class);
    target.shift = Utils.findRequiredViewAsType(source, R.id.shift, "field 'shift'", Spinner.class);
    target.searchDate = Utils.findRequiredViewAsType(source, R.id.search_date, "field 'searchDate'", AppCompatTextView.class);
    view = Utils.findRequiredView(source, R.id.pick_date, "method 'onViewClicked'");
    view2131362072 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.search, "method 'onViewClicked'");
    view2131362108 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    EmpAttendanceFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerView = null;
    target.tvEmpty = null;
    target.rootLayout = null;
    target.type = null;
    target.shift = null;
    target.searchDate = null;

    view2131362072.setOnClickListener(null);
    view2131362072 = null;
    view2131362108.setOnClickListener(null);
    view2131362108 = null;
  }
}
