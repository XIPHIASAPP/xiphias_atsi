// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.salarydetails;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmpSalaryFragment_ViewBinding implements Unbinder {
  private EmpSalaryFragment target;

  private View view2131361938;

  private View view2131361952;

  @UiThread
  public EmpSalaryFragment_ViewBinding(final EmpSalaryFragment target, View source) {
    this.target = target;

    View view;
    target.rootLayout = Utils.findRequiredViewAsType(source, R.id.root_layout, "field 'rootLayout'", LinearLayout.class);
    target.fromDateText = Utils.findRequiredViewAsType(source, R.id.from_date_text, "field 'fromDateText'", AppCompatTextView.class);
    target.txtError = Utils.findRequiredViewAsType(source, R.id.txtError, "field 'txtError'", AppCompatTextView.class);
    target.cvSalary = Utils.findRequiredViewAsType(source, R.id.cvSalary, "field 'cvSalary'", CardView.class);
    view = Utils.findRequiredView(source, R.id.from_date_wise, "method 'onViewClicked'");
    view2131361938 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ib_search_date, "method 'onViewClicked'");
    view2131361952 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.salaryies = Utils.listOf(
        Utils.findRequiredViewAsType(source, R.id.name, "field 'salaryies'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.amount, "field 'salaryies'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.deduct, "field 'salaryies'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.gross, "field 'salaryies'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.net_salary, "field 'salaryies'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.month, "field 'salaryies'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.days, "field 'salaryies'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.absent, "field 'salaryies'", TextView.class));
  }

  @Override
  @CallSuper
  public void unbind() {
    EmpSalaryFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rootLayout = null;
    target.fromDateText = null;
    target.txtError = null;
    target.cvSalary = null;
    target.salaryies = null;

    view2131361938.setOnClickListener(null);
    view2131361938 = null;
    view2131361952.setOnClickListener(null);
    view2131361952 = null;
  }
}
