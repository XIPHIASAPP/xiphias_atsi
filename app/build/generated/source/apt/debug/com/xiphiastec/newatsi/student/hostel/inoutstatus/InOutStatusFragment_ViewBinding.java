// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.hostel.inoutstatus;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class InOutStatusFragment_ViewBinding implements Unbinder {
  private InOutStatusFragment target;

  private View view2131361917;

  private View view2131361951;

  private View view2131361952;

  @UiThread
  public InOutStatusFragment_ViewBinding(final InOutStatusFragment target, View source) {
    this.target = target;

    View view;
    target.mRecyclerView = Utils.findRequiredViewAsType(source, R.id.recycler_view, "field 'mRecyclerView'", EmptyViewRecyclerView.class);
    target.rootLayout = Utils.findRequiredViewAsType(source, R.id.rootLayout, "field 'rootLayout'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.et_search_date, "field 'mEtSearchDate' and method 'onViewClicked'");
    target.mEtSearchDate = Utils.castView(view, R.id.et_search_date, "field 'mEtSearchDate'", AppCompatTextView.class);
    view2131361917 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.tvEmpty = Utils.findRequiredViewAsType(source, R.id.tv_empty, "field 'tvEmpty'", TextView.class);
    target.mDetails = Utils.findRequiredViewAsType(source, R.id.details, "field 'mDetails'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.ib_pick_date, "field 'ib_date_pick' and method 'onViewClicked'");
    target.ib_date_pick = Utils.castView(view, R.id.ib_pick_date, "field 'ib_date_pick'", LinearLayout.class);
    view2131361951 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ib_search_date, "method 'onViewClicked'");
    view2131361952 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    InOutStatusFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mRecyclerView = null;
    target.rootLayout = null;
    target.mEtSearchDate = null;
    target.tvEmpty = null;
    target.mDetails = null;
    target.ib_date_pick = null;

    view2131361917.setOnClickListener(null);
    view2131361917 = null;
    view2131361951.setOnClickListener(null);
    view2131361951 = null;
    view2131361952.setOnClickListener(null);
    view2131361952 = null;
  }
}
