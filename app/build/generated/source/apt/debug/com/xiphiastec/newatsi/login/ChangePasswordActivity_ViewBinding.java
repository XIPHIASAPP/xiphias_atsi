// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.login;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ChangePasswordActivity_ViewBinding implements Unbinder {
  private ChangePasswordActivity target;

  private View view2131361851;

  @UiThread
  public ChangePasswordActivity_ViewBinding(ChangePasswordActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ChangePasswordActivity_ViewBinding(final ChangePasswordActivity target, View source) {
    this.target = target;

    View view;
    target.mEtOldPassword = Utils.findRequiredViewAsType(source, R.id.et_old_password, "field 'mEtOldPassword'", TextInputEditText.class);
    target.mEtNewPassword = Utils.findRequiredViewAsType(source, R.id.et_new_password, "field 'mEtNewPassword'", TextInputEditText.class);
    target.mEtConfirmPassword = Utils.findRequiredViewAsType(source, R.id.et_confirm_password, "field 'mEtConfirmPassword'", TextInputEditText.class);
    view = Utils.findRequiredView(source, R.id.btnSubmit, "field 'mBtnSubmit' and method 'onClick'");
    target.mBtnSubmit = Utils.castView(view, R.id.btnSubmit, "field 'mBtnSubmit'", Button.class);
    view2131361851 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick();
      }
    });
    target.mRootLayout = Utils.findRequiredViewAsType(source, R.id.root_layout, "field 'mRootLayout'", CoordinatorLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ChangePasswordActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mEtOldPassword = null;
    target.mEtNewPassword = null;
    target.mEtConfirmPassword = null;
    target.mBtnSubmit = null;
    target.mRootLayout = null;

    view2131361851.setOnClickListener(null);
    view2131361851 = null;
  }
}
