// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.parent.dashboard;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ParentDashboardFragment$ParentDashboardAdapter$SyllabusViewHolder_ViewBinding implements Unbinder {
  private ParentDashboardFragment.ParentDashboardAdapter.SyllabusViewHolder target;

  @UiThread
  public ParentDashboardFragment$ParentDashboardAdapter$SyllabusViewHolder_ViewBinding(ParentDashboardFragment.ParentDashboardAdapter.SyllabusViewHolder target,
      View source) {
    this.target = target;

    target.mRecyclerView = Utils.findRequiredViewAsType(source, R.id.recycler_view, "field 'mRecyclerView'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ParentDashboardFragment.ParentDashboardAdapter.SyllabusViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mRecyclerView = null;
  }
}
