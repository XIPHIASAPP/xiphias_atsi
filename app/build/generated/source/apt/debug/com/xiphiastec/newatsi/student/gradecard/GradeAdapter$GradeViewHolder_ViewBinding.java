// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.gradecard;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class GradeAdapter$GradeViewHolder_ViewBinding implements Unbinder {
  private GradeAdapter.GradeViewHolder target;

  @UiThread
  public GradeAdapter$GradeViewHolder_ViewBinding(GradeAdapter.GradeViewHolder target,
      View source) {
    this.target = target;

    target.txtSubjectName = Utils.findRequiredViewAsType(source, R.id.txtSubjectName, "field 'txtSubjectName'", TextView.class);
    target.txtGrade = Utils.findRequiredViewAsType(source, R.id.txtGrade, "field 'txtGrade'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    GradeAdapter.GradeViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtSubjectName = null;
    target.txtGrade = null;
  }
}
