// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.parent.dashboard;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ParentDashboardFragment$SyllabusCoveredAdapter$SyllabusCoveredHolder_ViewBinding implements Unbinder {
  private ParentDashboardFragment.SyllabusCoveredAdapter.SyllabusCoveredHolder target;

  @UiThread
  public ParentDashboardFragment$SyllabusCoveredAdapter$SyllabusCoveredHolder_ViewBinding(ParentDashboardFragment.SyllabusCoveredAdapter.SyllabusCoveredHolder target,
      View source) {
    this.target = target;

    target.mMonth = Utils.findRequiredViewAsType(source, R.id.month, "field 'mMonth'", TextView.class);
    target.mSubjectName = Utils.findRequiredViewAsType(source, R.id.subject_name, "field 'mSubjectName'", TextView.class);
    target.mSubjectGroup = Utils.findRequiredViewAsType(source, R.id.subject_group, "field 'mSubjectGroup'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ParentDashboardFragment.SyllabusCoveredAdapter.SyllabusCoveredHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mMonth = null;
    target.mSubjectName = null;
    target.mSubjectGroup = null;
  }
}
