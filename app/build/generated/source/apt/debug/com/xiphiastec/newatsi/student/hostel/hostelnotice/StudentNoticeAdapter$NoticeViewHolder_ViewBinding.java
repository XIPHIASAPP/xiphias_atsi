// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.hostel.hostelnotice;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StudentNoticeAdapter$NoticeViewHolder_ViewBinding implements Unbinder {
  private StudentNoticeAdapter.NoticeViewHolder target;

  @UiThread
  public StudentNoticeAdapter$NoticeViewHolder_ViewBinding(StudentNoticeAdapter.NoticeViewHolder target,
      View source) {
    this.target = target;

    target.mTvDay = Utils.findRequiredViewAsType(source, R.id.tv_day, "field 'mTvDay'", TextView.class);
    target.mTvMon = Utils.findRequiredViewAsType(source, R.id.tv_mon, "field 'mTvMon'", TextView.class);
    target.mTvNotice = Utils.findRequiredViewAsType(source, R.id.tv_notice, "field 'mTvNotice'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    StudentNoticeAdapter.NoticeViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTvDay = null;
    target.mTvMon = null;
    target.mTvNotice = null;
  }
}
