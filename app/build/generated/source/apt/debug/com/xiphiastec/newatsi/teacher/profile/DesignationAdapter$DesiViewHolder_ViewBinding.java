// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.profile;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DesignationAdapter$DesiViewHolder_ViewBinding implements Unbinder {
  private DesignationAdapter.DesiViewHolder target;

  @UiThread
  public DesignationAdapter$DesiViewHolder_ViewBinding(DesignationAdapter.DesiViewHolder target,
      View source) {
    this.target = target;

    target.designation = Utils.findRequiredViewAsType(source, R.id.designation, "field 'designation'", TextView.class);
    target.department = Utils.findRequiredViewAsType(source, R.id.department, "field 'department'", TextView.class);
    target.type = Utils.findRequiredViewAsType(source, R.id.type, "field 'type'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DesignationAdapter.DesiViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.designation = null;
    target.department = null;
    target.type = null;
  }
}
