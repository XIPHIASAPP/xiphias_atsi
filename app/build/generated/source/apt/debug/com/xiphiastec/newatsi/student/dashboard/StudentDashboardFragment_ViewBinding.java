// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.dashboard;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StudentDashboardFragment_ViewBinding implements Unbinder {
  private StudentDashboardFragment target;

  private View view2131361988;

  private View view2131361980;

  private View view2131362007;

  private View view2131361991;

  private View view2131361992;

  private View view2131362006;

  private View view2131361993;

  private View view2131361994;

  private View view2131361985;

  private View view2131361998;

  private View view2131362001;

  private View view2131361981;

  private View view2131361979;

  private View view2131361984;

  private View view2131361987;

  @UiThread
  public StudentDashboardFragment_ViewBinding(final StudentDashboardFragment target, View source) {
    this.target = target;

    View view;
    target.rootLayout = Utils.findRequiredViewAsType(source, R.id.rootLayout, "field 'rootLayout'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.llExam, "method 'subFragments'");
    view2131361988 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.subFragments(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llAttendance, "method 'subFragments'");
    view2131361980 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.subFragments(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llTransport, "method 'subFragments'");
    view2131362007 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.subFragments(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llFine, "method 'subFragments'");
    view2131361991 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.subFragments(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llGrade, "method 'subFragments'");
    view2131361992 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.subFragments(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llTimetable, "method 'subFragments'");
    view2131362006 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.subFragments(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llHallTicket, "method 'subFragments'");
    view2131361993 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.subFragments(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llInout, "method 'subFragments'");
    view2131361994 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.subFragments(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llDiscipline, "method 'subFragments'");
    view2131361985 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.subFragments(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llNotice, "method 'subFragments'");
    view2131361998 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.subFragments(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llRoom, "method 'subFragments'");
    view2131362001 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.subFragments(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llComplaint, "method 'subFragments'");
    view2131361981 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.subFragments(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llAcademics, "method 'subFragments'");
    view2131361979 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.subFragments(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llDetails, "method 'subFragments'");
    view2131361984 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.subFragments(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llDocuments, "method 'subFragments'");
    view2131361987 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.subFragments(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    StudentDashboardFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rootLayout = null;

    view2131361988.setOnClickListener(null);
    view2131361988 = null;
    view2131361980.setOnClickListener(null);
    view2131361980 = null;
    view2131362007.setOnClickListener(null);
    view2131362007 = null;
    view2131361991.setOnClickListener(null);
    view2131361991 = null;
    view2131361992.setOnClickListener(null);
    view2131361992 = null;
    view2131362006.setOnClickListener(null);
    view2131362006 = null;
    view2131361993.setOnClickListener(null);
    view2131361993 = null;
    view2131361994.setOnClickListener(null);
    view2131361994 = null;
    view2131361985.setOnClickListener(null);
    view2131361985 = null;
    view2131361998.setOnClickListener(null);
    view2131361998 = null;
    view2131362001.setOnClickListener(null);
    view2131362001 = null;
    view2131361981.setOnClickListener(null);
    view2131361981 = null;
    view2131361979.setOnClickListener(null);
    view2131361979 = null;
    view2131361984.setOnClickListener(null);
    view2131361984 = null;
    view2131361987.setOnClickListener(null);
    view2131361987 = null;
  }
}
