// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.timetable;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StudentTimeTableFragment_ViewBinding implements Unbinder {
  private StudentTimeTableFragment target;

  @UiThread
  public StudentTimeTableFragment_ViewBinding(StudentTimeTableFragment target, View source) {
    this.target = target;

    target.timeTable = Utils.findRequiredViewAsType(source, R.id.lvExp, "field 'timeTable'", ExpandableListView.class);
    target.txtError = Utils.findRequiredViewAsType(source, R.id.txtError, "field 'txtError'", TextView.class);
    target.mRootLayout = Utils.findRequiredViewAsType(source, R.id.root_layout, "field 'mRootLayout'", ConstraintLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    StudentTimeTableFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.timeTable = null;
    target.txtError = null;
    target.mRootLayout = null;
  }
}
