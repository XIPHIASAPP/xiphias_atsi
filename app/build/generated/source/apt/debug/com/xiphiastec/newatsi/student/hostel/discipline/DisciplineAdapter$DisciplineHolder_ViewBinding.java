// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.hostel.discipline;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DisciplineAdapter$DisciplineHolder_ViewBinding implements Unbinder {
  private DisciplineAdapter.DisciplineHolder target;

  @UiThread
  public DisciplineAdapter$DisciplineHolder_ViewBinding(DisciplineAdapter.DisciplineHolder target,
      View source) {
    this.target = target;

    target.mDiscipline = Utils.findRequiredViewAsType(source, R.id.tv_discipline, "field 'mDiscipline'", TextView.class);
    target.mFine = Utils.findRequiredViewAsType(source, R.id.tv_fine, "field 'mFine'", TextView.class);
    target.mTvDay = Utils.findRequiredViewAsType(source, R.id.tv_day, "field 'mTvDay'", TextView.class);
    target.mTvMon = Utils.findRequiredViewAsType(source, R.id.tv_mon, "field 'mTvMon'", TextView.class);
    target.llDisciplinelayout = Utils.findRequiredViewAsType(source, R.id.llDisciplinelayout, "field 'llDisciplinelayout'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DisciplineAdapter.DisciplineHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mDiscipline = null;
    target.mFine = null;
    target.mTvDay = null;
    target.mTvMon = null;
    target.llDisciplinelayout = null;
  }
}
