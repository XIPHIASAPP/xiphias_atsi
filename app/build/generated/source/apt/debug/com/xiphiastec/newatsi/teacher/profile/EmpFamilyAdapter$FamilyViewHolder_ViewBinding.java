// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.profile;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmpFamilyAdapter$FamilyViewHolder_ViewBinding implements Unbinder {
  private EmpFamilyAdapter.FamilyViewHolder target;

  @UiThread
  public EmpFamilyAdapter$FamilyViewHolder_ViewBinding(EmpFamilyAdapter.FamilyViewHolder target,
      View source) {
    this.target = target;

    target.tvName = Utils.findRequiredViewAsType(source, R.id.tvName, "field 'tvName'", TextView.class);
    target.tvNumber = Utils.findRequiredViewAsType(source, R.id.tvNumber, "field 'tvNumber'", TextView.class);
    target.tvRelation = Utils.findRequiredViewAsType(source, R.id.tvRelation, "field 'tvRelation'", TextView.class);
    target.tvCompany = Utils.findRequiredViewAsType(source, R.id.tvCompany, "field 'tvCompany'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    EmpFamilyAdapter.FamilyViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvName = null;
    target.tvNumber = null;
    target.tvRelation = null;
    target.tvCompany = null;
  }
}
