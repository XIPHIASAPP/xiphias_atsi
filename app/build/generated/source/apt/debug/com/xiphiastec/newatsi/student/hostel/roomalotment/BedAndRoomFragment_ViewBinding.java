// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.hostel.roomalotment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BedAndRoomFragment_ViewBinding implements Unbinder {
  private BedAndRoomFragment target;

  @UiThread
  public BedAndRoomFragment_ViewBinding(BedAndRoomFragment target, View source) {
    this.target = target;

    target.rootLayout = Utils.findRequiredViewAsType(source, R.id.root_layout, "field 'rootLayout'", LinearLayout.class);
    target.cvRoomDetails = Utils.findRequiredViewAsType(source, R.id.cvRoomDetails, "field 'cvRoomDetails'", CardView.class);
    target.txtError = Utils.findRequiredViewAsType(source, R.id.txtError, "field 'txtError'", TextView.class);
    target.mViewList = Utils.listOf(
        Utils.findRequiredViewAsType(source, R.id.name, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.address, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.building_floor, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.room, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.date, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.in_charge_name, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.in_charge_number, "field 'mViewList'", TextView.class));
  }

  @Override
  @CallSuper
  public void unbind() {
    BedAndRoomFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rootLayout = null;
    target.cvRoomDetails = null;
    target.txtError = null;
    target.mViewList = null;
  }
}
