// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.attendance.dailyattendance;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DailyAttendanceFragment_ViewBinding implements Unbinder {
  private DailyAttendanceFragment target;

  private View view2131361951;

  private View view2131361952;

  private View view2131361982;

  private View view2131362004;

  @UiThread
  public DailyAttendanceFragment_ViewBinding(final DailyAttendanceFragment target, View source) {
    this.target = target;

    View view;
    target.searchDate = Utils.findRequiredViewAsType(source, R.id.et_search_date, "field 'searchDate'", AppCompatTextView.class);
    target.dailyRecycler = Utils.findRequiredViewAsType(source, R.id.dailyRecycler, "field 'dailyRecycler'", RecyclerView.class);
    target.subjectRecycler = Utils.findRequiredViewAsType(source, R.id.subjectRecycler, "field 'subjectRecycler'", RecyclerView.class);
    target.txtError = Utils.findRequiredViewAsType(source, R.id.txtError, "field 'txtError'", TextView.class);
    target.mRootLayout = Utils.findRequiredViewAsType(source, R.id.root_layout, "field 'mRootLayout'", LinearLayout.class);
    target.vDaily = Utils.findRequiredView(source, R.id.vDaily, "field 'vDaily'");
    target.vSubject = Utils.findRequiredView(source, R.id.vSubject, "field 'vSubject'");
    target.cvAttendanceDetails = Utils.findRequiredView(source, R.id.cvAttendanceDetails, "field 'cvAttendanceDetails'");
    view = Utils.findRequiredView(source, R.id.ib_pick_date, "method 'onClick'");
    view2131361951 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ib_search_date, "method 'onClick'");
    view2131361952 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llDaily, "method 'viewDailyClicked'");
    view2131361982 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.viewDailyClicked();
      }
    });
    view = Utils.findRequiredView(source, R.id.llSubject, "method 'viewSubjectClicked'");
    view2131362004 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.viewSubjectClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    DailyAttendanceFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.searchDate = null;
    target.dailyRecycler = null;
    target.subjectRecycler = null;
    target.txtError = null;
    target.mRootLayout = null;
    target.vDaily = null;
    target.vSubject = null;
    target.cvAttendanceDetails = null;

    view2131361951.setOnClickListener(null);
    view2131361951 = null;
    view2131361952.setOnClickListener(null);
    view2131361952 = null;
    view2131361982.setOnClickListener(null);
    view2131361982 = null;
    view2131362004.setOnClickListener(null);
    view2131362004 = null;
  }
}
