// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.profile;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmpAssetsFragment_ViewBinding implements Unbinder {
  private EmpAssetsFragment target;

  @UiThread
  public EmpAssetsFragment_ViewBinding(EmpAssetsFragment target, View source) {
    this.target = target;

    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recycler_view, "field 'recyclerView'", EmptyViewRecyclerView.class);
    target.mTvEmpty = Utils.findRequiredViewAsType(source, R.id.tv_empty, "field 'mTvEmpty'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    EmpAssetsFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerView = null;
    target.mTvEmpty = null;
  }
}
