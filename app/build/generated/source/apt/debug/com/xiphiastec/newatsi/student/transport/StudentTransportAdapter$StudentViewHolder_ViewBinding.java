// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.transport;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StudentTransportAdapter$StudentViewHolder_ViewBinding implements Unbinder {
  private StudentTransportAdapter.StudentViewHolder target;

  @UiThread
  public StudentTransportAdapter$StudentViewHolder_ViewBinding(StudentTransportAdapter.StudentViewHolder target,
      View source) {
    this.target = target;

    target.mTvVehicleTypeNumber = Utils.findRequiredViewAsType(source, R.id.tv_vehicle_type_number, "field 'mTvVehicleTypeNumber'", TextView.class);
    target.mTvRouteName = Utils.findRequiredViewAsType(source, R.id.tv_route_name, "field 'mTvRouteName'", TextView.class);
    target.mTvStoppageName = Utils.findRequiredViewAsType(source, R.id.tv_stoppage_name, "field 'mTvStoppageName'", TextView.class);
    target.tv_driver = Utils.findRequiredViewAsType(source, R.id.tv_driver, "field 'tv_driver'", TextView.class);
    target.tv_conductor = Utils.findRequiredViewAsType(source, R.id.tv_conductor, "field 'tv_conductor'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    StudentTransportAdapter.StudentViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTvVehicleTypeNumber = null;
    target.mTvRouteName = null;
    target.mTvStoppageName = null;
    target.tv_driver = null;
    target.tv_conductor = null;
  }
}
