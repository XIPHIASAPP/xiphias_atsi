// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.syllabus;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmpSyllabusAdapter$SyllabusViewHolder_ViewBinding implements Unbinder {
  private EmpSyllabusAdapter.SyllabusViewHolder target;

  @UiThread
  public EmpSyllabusAdapter$SyllabusViewHolder_ViewBinding(EmpSyllabusAdapter.SyllabusViewHolder target,
      View source) {
    this.target = target;

    target.subjectName = Utils.findRequiredViewAsType(source, R.id.subject_name, "field 'subjectName'", TextView.class);
    target.topicName = Utils.findRequiredViewAsType(source, R.id.topic_name, "field 'topicName'", TextView.class);
    target.subjectGroup = Utils.findRequiredViewAsType(source, R.id.subject_group, "field 'subjectGroup'", TextView.class);
    target.date = Utils.findRequiredViewAsType(source, R.id.date, "field 'date'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    EmpSyllabusAdapter.SyllabusViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.subjectName = null;
    target.topicName = null;
    target.subjectGroup = null;
    target.date = null;
  }
}
