// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.disciplinary;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmpCreateDisciplinaryActionFragment_ViewBinding implements Unbinder {
  private EmpCreateDisciplinaryActionFragment target;

  private View view2131361851;

  @UiThread
  public EmpCreateDisciplinaryActionFragment_ViewBinding(final EmpCreateDisciplinaryActionFragment target,
      View source) {
    this.target = target;

    View view;
    target.rootLayout = Utils.findRequiredViewAsType(source, R.id.rootLayout, "field 'rootLayout'", LinearLayout.class);
    target.action = Utils.findRequiredViewAsType(source, R.id.action, "field 'action'", EditText.class);
    target.indiscipline = Utils.findRequiredViewAsType(source, R.id.indiscipline, "field 'indiscipline'", EditText.class);
    target.description = Utils.findRequiredViewAsType(source, R.id.description, "field 'description'", EditText.class);
    target.actions = Utils.findRequiredViewAsType(source, R.id.actions, "field 'actions'", Spinner.class);
    view = Utils.findRequiredView(source, R.id.btnSubmit, "field 'btnSubmit' and method 'onSubmitClick'");
    target.btnSubmit = Utils.castView(view, R.id.btnSubmit, "field 'btnSubmit'", Button.class);
    view2131361851 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onSubmitClick();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    EmpCreateDisciplinaryActionFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rootLayout = null;
    target.action = null;
    target.indiscipline = null;
    target.description = null;
    target.actions = null;
    target.btnSubmit = null;

    view2131361851.setOnClickListener(null);
    view2131361851 = null;
  }
}
