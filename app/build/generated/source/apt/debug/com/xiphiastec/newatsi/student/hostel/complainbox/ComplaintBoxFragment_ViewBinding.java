// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.hostel.complainbox;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ComplaintBoxFragment_ViewBinding implements Unbinder {
  private ComplaintBoxFragment target;

  private View view2131361849;

  private View view2131361983;

  private View view2131362147;

  @UiThread
  public ComplaintBoxFragment_ViewBinding(final ComplaintBoxFragment target, View source) {
    this.target = target;

    View view;
    target.mDate = Utils.findRequiredViewAsType(source, R.id.etCdate, "field 'mDate'", TextView.class);
    target.complaint = Utils.findRequiredViewAsType(source, R.id.etComplaint, "field 'complaint'", EditText.class);
    target.placeOccurrence = Utils.findRequiredViewAsType(source, R.id.etOccurence, "field 'placeOccurrence'", EditText.class);
    target.spinner = Utils.findRequiredViewAsType(source, R.id.spnCategory, "field 'spinner'", Spinner.class);
    target.personName = Utils.findRequiredViewAsType(source, R.id.etPersonName, "field 'personName'", EditText.class);
    target.mRoot = Utils.findRequiredViewAsType(source, R.id.root, "field 'mRoot'", LinearLayout.class);
    target.mImagePreview = Utils.findRequiredViewAsType(source, R.id.ivImage, "field 'mImagePreview'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.btnAttach, "method 'onClick'");
    view2131361849 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llDatePick, "method 'onClick'");
    view2131361983 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.submit, "method 'onClick'");
    view2131362147 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ComplaintBoxFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mDate = null;
    target.complaint = null;
    target.placeOccurrence = null;
    target.spinner = null;
    target.personName = null;
    target.mRoot = null;
    target.mImagePreview = null;

    view2131361849.setOnClickListener(null);
    view2131361849 = null;
    view2131361983.setOnClickListener(null);
    view2131361983 = null;
    view2131362147.setOnClickListener(null);
    view2131362147 = null;
  }
}
