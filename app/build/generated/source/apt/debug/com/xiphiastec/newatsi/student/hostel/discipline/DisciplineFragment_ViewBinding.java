// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.hostel.discipline;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DisciplineFragment_ViewBinding implements Unbinder {
  private DisciplineFragment target;

  @UiThread
  public DisciplineFragment_ViewBinding(DisciplineFragment target, View source) {
    this.target = target;

    target.mRecyclerView = Utils.findRequiredViewAsType(source, R.id.recycler_view, "field 'mRecyclerView'", EmptyViewRecyclerView.class);
    target.mTvEmpty = Utils.findRequiredViewAsType(source, R.id.tv_empty, "field 'mTvEmpty'", TextView.class);
    target.mDetails = Utils.findRequiredView(source, R.id.details, "field 'mDetails'");
  }

  @Override
  @CallSuper
  public void unbind() {
    DisciplineFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mRecyclerView = null;
    target.mTvEmpty = null;
    target.mDetails = null;
  }
}
