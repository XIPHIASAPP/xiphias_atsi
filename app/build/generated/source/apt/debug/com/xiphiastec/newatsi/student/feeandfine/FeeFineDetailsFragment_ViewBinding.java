// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.feeandfine;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FeeFineDetailsFragment_ViewBinding implements Unbinder {
  private FeeFineDetailsFragment target;

  private View view2131362003;

  private View view2131361990;

  private View view2131361991;

  @UiThread
  public FeeFineDetailsFragment_ViewBinding(final FeeFineDetailsFragment target, View source) {
    this.target = target;

    View view;
    target.feeRecyclerView = Utils.findRequiredViewAsType(source, R.id.rvFee, "field 'feeRecyclerView'", RecyclerView.class);
    target.fineRecyclerView = Utils.findRequiredViewAsType(source, R.id.rvFine, "field 'fineRecyclerView'", RecyclerView.class);
    target.vStudent = Utils.findRequiredView(source, R.id.vStudent, "field 'vStudent'");
    target.vFee = Utils.findRequiredView(source, R.id.vfee, "field 'vFee'");
    target.vFine = Utils.findRequiredView(source, R.id.vfine, "field 'vFine'");
    target.name = Utils.findRequiredViewAsType(source, R.id.name, "field 'name'", TextView.class);
    target.cvStudentDetails = Utils.findRequiredViewAsType(source, R.id.cvStudentDetails, "field 'cvStudentDetails'", CardView.class);
    target.cvFeeDetails = Utils.findRequiredViewAsType(source, R.id.cvFeeDetails, "field 'cvFeeDetails'", CardView.class);
    target.cvFineDetails = Utils.findRequiredViewAsType(source, R.id.cvFineDetails, "field 'cvFineDetails'", CardView.class);
    view = Utils.findRequiredView(source, R.id.llStudent, "method 'viewStudentClicked'");
    view2131362003 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.viewStudentClicked();
      }
    });
    view = Utils.findRequiredView(source, R.id.llFee, "method 'viewFeeClicked'");
    view2131361990 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.viewFeeClicked();
      }
    });
    view = Utils.findRequiredView(source, R.id.llFine, "method 'viewFineClicked'");
    view2131361991 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.viewFineClicked();
      }
    });
    target.mViewList = Utils.listOf(
        Utils.findRequiredViewAsType(source, R.id.tvName, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tvRegistrationNumber, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tvTotalPaid, "field 'mViewList'", TextView.class), 
        Utils.findRequiredViewAsType(source, R.id.tvPending, "field 'mViewList'", TextView.class));
  }

  @Override
  @CallSuper
  public void unbind() {
    FeeFineDetailsFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.feeRecyclerView = null;
    target.fineRecyclerView = null;
    target.vStudent = null;
    target.vFee = null;
    target.vFine = null;
    target.name = null;
    target.cvStudentDetails = null;
    target.cvFeeDetails = null;
    target.cvFineDetails = null;
    target.mViewList = null;

    view2131362003.setOnClickListener(null);
    view2131362003 = null;
    view2131361990.setOnClickListener(null);
    view2131361990 = null;
    view2131361991.setOnClickListener(null);
    view2131361991 = null;
  }
}
