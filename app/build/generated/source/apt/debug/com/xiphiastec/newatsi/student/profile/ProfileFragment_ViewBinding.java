// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.profile;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.FrameLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProfileFragment_ViewBinding implements Unbinder {
  private ProfileFragment target;

  private View view2131361799;

  private View view2131362076;

  private View view2131361900;

  @UiThread
  public ProfileFragment_ViewBinding(final ProfileFragment target, View source) {
    this.target = target;

    View view;
    target.mRootLayout = Utils.findRequiredViewAsType(source, R.id.root_layout, "field 'mRootLayout'", FrameLayout.class);
    view = Utils.findRequiredView(source, R.id.academics, "method 'onViewClicked'");
    view2131361799 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.profile_details, "method 'onViewClicked'");
    view2131362076 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.document, "method 'onViewClicked'");
    view2131361900 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ProfileFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mRootLayout = null;

    view2131361799.setOnClickListener(null);
    view2131361799 = null;
    view2131362076.setOnClickListener(null);
    view2131362076 = null;
    view2131361900.setOnClickListener(null);
    view2131361900 = null;
  }
}
