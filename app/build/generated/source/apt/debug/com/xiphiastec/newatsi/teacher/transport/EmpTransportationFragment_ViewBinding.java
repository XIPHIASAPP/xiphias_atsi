// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.teacher.transport;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmpTransportationFragment_ViewBinding implements Unbinder {
  private EmpTransportationFragment target;

  @UiThread
  public EmpTransportationFragment_ViewBinding(EmpTransportationFragment target, View source) {
    this.target = target;

    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recycler_view, "field 'recyclerView'", RecyclerView.class);
    target.txtError = Utils.findRequiredViewAsType(source, R.id.txtError, "field 'txtError'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    EmpTransportationFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerView = null;
    target.txtError = null;
  }
}
