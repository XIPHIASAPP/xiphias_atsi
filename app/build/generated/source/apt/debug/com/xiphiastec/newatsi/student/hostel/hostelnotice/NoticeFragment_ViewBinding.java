// Generated code from Butter Knife. Do not modify!
package com.xiphiastec.newatsi.student.hostel.hostelnotice;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.xiphiastec.newatsi.R;
import com.xiphiastec.newatsi.widget.EmptyViewRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class NoticeFragment_ViewBinding implements Unbinder {
  private NoticeFragment target;

  private View view2131361949;

  private View view2131361952;

  @UiThread
  public NoticeFragment_ViewBinding(final NoticeFragment target, View source) {
    this.target = target;

    View view;
    target.mEtSearchDate = Utils.findRequiredViewAsType(source, R.id.et_search_date, "field 'mEtSearchDate'", AppCompatTextView.class);
    target.mRecyclerView = Utils.findRequiredViewAsType(source, R.id.recycler_view, "field 'mRecyclerView'", EmptyViewRecyclerView.class);
    target.mTvEmpty = Utils.findRequiredViewAsType(source, R.id.tv_empty, "field 'mTvEmpty'", TextView.class);
    target.mDetails = Utils.findRequiredView(source, R.id.details, "field 'mDetails'");
    view = Utils.findRequiredView(source, R.id.ib_date_pick, "method 'onViewClicked'");
    view2131361949 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ib_search_date, "method 'onViewClicked'");
    view2131361952 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    NoticeFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mEtSearchDate = null;
    target.mRecyclerView = null;
    target.mTvEmpty = null;
    target.mDetails = null;

    view2131361949.setOnClickListener(null);
    view2131361949 = null;
    view2131361952.setOnClickListener(null);
    view2131361952 = null;
  }
}
