/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.xiphiastec.newatsi;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.xiphiastec.newatsi";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 3;
  public static final String VERSION_NAME = "2.1";
  // Fields from default config.
  public static final String BASEURL = "https://www.atsi.in/atsiservices/api/";
}
