package com.xiphiastec.domain;

import com.xiphiastec.mvp.model.OneClassModel;
import com.xiphiastec.network.Repository;

import java.util.List;

import rx.Observable;

/**
 * Created by Admin on 03-04-2017.
 */

public class FetchCalendarsUsecase implements Usecase<List<OneClassModel>> {
    private Repository mRepository;

    public FetchCalendarsUsecase(Repository repository) {
        mRepository = repository;
    }

    @Override
    public Observable<List<OneClassModel>> execute() {
        return mRepository.getOneClassModel().map(new ResponseMappingFunc<List<OneClassModel>>());
    }
}
