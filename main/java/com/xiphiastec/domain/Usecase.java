package com.xiphiastec.domain;

import rx.Observable;

/**
 * Created by Admin on 03-04-2017.
 */
public interface Usecase<T> {
    Observable<T> execute();
}
