package com.xiphiastec.domain;

import com.xiphiastec.mvp.model.ResponseWrapper;

import rx.functions.Func1;

/**
 * Created by Admin on 03-04-2017.
 */

public class ResponseMappingFunc<R> implements Func1<ResponseWrapper<R>, R> {
    @Override
    public R call(ResponseWrapper<R> rResponseWrapper) {
        if (rResponseWrapper == null) {
            return null;
        }
        return rResponseWrapper.body;
    }
}
