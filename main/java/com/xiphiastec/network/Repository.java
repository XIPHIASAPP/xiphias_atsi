package com.xiphiastec.network;

import com.xiphiastec.mvp.model.OneClassModel;
import com.xiphiastec.mvp.model.ResponseWrapper;

import java.util.List;

import rx.Observable;

/**
 * Created by Admin on 03-04-2017.
 */

public interface Repository {
    Observable<ResponseWrapper<List<OneClassModel>>> getOneClassModel();
}
