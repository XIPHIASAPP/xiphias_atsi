package com.xiphiastec.mvp.view;

/**
 * Created by Admin on 08-04-2017.
 */

public interface BaseView<T> extends View{
    void onLoading();

    void onError(String error);

    void onResponse(T response);

    void onComplete();
}
