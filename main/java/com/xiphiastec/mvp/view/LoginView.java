package com.xiphiastec.mvp.view;

import com.xiphiastec.mvp.model.login.LoginResponse;

/**
 * Created by Admin on 08-04-2017.
 */

public interface LoginView extends BaseView<LoginResponse> {

    void onEmailError();

    void onPasswordError();
}
