package com.xiphiastec.mvp.view;

import com.xiphiastec.mvp.model.OneClassModel;

import java.util.List;

/**
 * Created by Admin on 01-04-2017.
 */

public interface OneClassView extends View {
    void showLoading();

    void showError();

    void showResponse(List<OneClassModel> list);

    void complete();
}
