package com.xiphiastec.mvp.presenter;

import com.xiphiastec.domain.FetchCalendarsUsecase;
import com.xiphiastec.mvp.view.OneClassView;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Admin on 01-04-2017.
 */

public class OneClassPresenter implements Presenter<OneClassView> {
    private OneClassView mOneClassView;
    private CompositeSubscription mCompositeSubscription;
    private FetchCalendarsUsecase mFetchCalendarsUsecase;

    public OneClassPresenter(FetchCalendarsUsecase fetchCalendarsUsecase) {
        mFetchCalendarsUsecase = fetchCalendarsUsecase;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription != null && !mCompositeSubscription.isUnsubscribed()) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription.clear();
        }
    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(OneClassView view) {
        this.mOneClassView = view;
        getOneClassModel();
    }

    private void getOneClassModel() {
        mOneClassView.showLoading();
        mCompositeSubscription.add(
                mFetchCalendarsUsecase.execute()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnTerminate(() -> mOneClassView.complete())
                        .onErrorReturn(throwable -> {
                            throwable.printStackTrace();
                            mOneClassView.showError();
                            return null;
                        })
                        .subscribe(list -> mOneClassView.showResponse(list))
        );
    }
}
