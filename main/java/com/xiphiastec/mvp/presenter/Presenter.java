package com.xiphiastec.mvp.presenter;

import com.xiphiastec.mvp.view.View;

/**
 * Created by Admin on 01-04-2017.
 */

public interface Presenter<T extends View> {
    void onCreate();

    void onStart();

    void onStop();

    void onPause();

    void attachView(T view);

}
